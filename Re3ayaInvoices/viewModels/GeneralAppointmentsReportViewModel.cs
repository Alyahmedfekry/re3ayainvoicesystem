﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class GeneralAppointmentsReportViewModel
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }
        public int AppointmentsCount { get; set; }
        public decimal? AppointmentsTotal { get; set; }
        public int CanceledAppointmentsCount { get; set; }
        public int ConfirmedAppointmentsCount { get; set; }
        public int UnconfirmedAppointmentsCount { get; set; }
        public int InvoiceCount { get; set; }
        public decimal? InvoiceTotal { get; set; }
        public int PaidInvoiceCount { get; set; }
        public decimal? PaidInvoiceTotal { get; set; }
        public int UnpaidInvoicesCount { get; set; }
        public decimal? UnpaidInvoicesTotal { get; set; }
        public int CanceledInvoiceCount { get; set; }
        public decimal? CanceledInvoiceTotal { get; set; }
        public decimal? CanceledAppointmentsTotal { get; set; }
        public decimal? ConfirmedAppointmentsTotal { get; set; }
        public decimal? UnconfirmedAppointmentsTotal { get; set; }
    }
}