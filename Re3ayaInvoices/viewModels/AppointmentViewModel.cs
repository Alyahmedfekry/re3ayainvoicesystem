﻿using System;

namespace Re3ayaInvoices.viewModels
{
    public class AppointmentViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public DateTime? Date { get; set; }
        public string PatientName { get; set; }
        public string PatientPhone { get; set; }
        public string Status { get; set; }
        public decimal? TotalAmount { get; set; }
        public bool? Is_Paid { get; set; }



    }
}
