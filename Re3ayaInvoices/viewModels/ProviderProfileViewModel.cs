﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class ProviderProfileViewModel
    {
        public string Code { get; set; }
        public string ArabicName { get; set; }
        public string EnglishName { get; set; }
        public string Email { get; set; }
        public string MasterPhone { get; set; }
        public string SecondPhone { get; set; }
        public string ThirdPhone { get; set; }
    }
}