﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class AppointmentStatisticsViewModel
    {
        public AppointmentStatisticsViewModel()
        {
            AppointmentMedicalProviders = new List<AppointmentMedicalProvider>();
        }
        public DateTime? CreatedDate { get; set; }
        public string CityName { get; set; }
        public string RegionName { get; set; }
        public int? CityId { get; set; }
        public int? RegionId { get; set; }
        public List<AppointmentMedicalProvider> AppointmentMedicalProviders { get; set; }
    }
    public class InvoiceStatisticsViewModel
    {
        public InvoiceStatisticsViewModel()
        {
            InvoiceMedicalProviders = new List<InvoiceMedicalProvider>();
            AllInvoiceMedicalProviders = new List<AllInvoiceMedicalProvider>();
            summarizeInvoiceProviders = new List<SummarizeInvoiceProvider>();
        }
        public DateTime? InvoiceDate { get; set; }
        public string CityName { get; set; }
        public string RegionName { get; set; }
        public int? CityId { get; set; }
        public int? RegionId { get; set; }
        public List<InvoiceMedicalProvider> InvoiceMedicalProviders { get; set; }
        public List<AllInvoiceMedicalProvider> AllInvoiceMedicalProviders { get; set; }
        public List<SummarizeInvoiceProvider> summarizeInvoiceProviders { get; set; }
        public List<InvoiceMedicalProvider> invoiceMedicalProviders { get; set; }
    }
    public class AllInvoiceMedicalProvider
    {
        public DateTime? InvoiceDate { get; set; }
        public int? InvoiceType { get; set; }
        public string InvoiceTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? SubscriptionTotalAmount { get; set; }
        public decimal? AppointmentTotalAmount { get; set; }

        public int SubscriptionInvoicesCount { get; set; }
        public decimal? SubscriptionInvoicesTotal { get; set; }
        public int SubscriptionPaidInvoicesCount { get; set; }
        public int SubscriptionUnpaidInvoicesCount { get; set; }
        public int SubscriptionCancelInvoicesCount { get; set; }
        public decimal? SubscriptionPaidInvoicesTotal { get; set; }
        public decimal? SubscriptionUnpaidInvoicesTotal { get; set; }
        public decimal? SubscriptionCancelInvoicesTotal { get; set; }


        public int AppointmentInvoicesCount { get; set; }
        public decimal? AppointmentInvoicesTotal { get; set; }
        public int AppointmentPaidInvoicesCount { get; set; }
        public int AppointmentUnpaidInvoicesCount { get; set; }
        public int AppointmentCancelInvoicesCount { get; set; }
        public decimal? AppointmentPaidInvoicesTotal { get; set; }
        public decimal? AppointmentUnpaidInvoicesTotal { get; set; }
        public decimal? AppointmentCancelInvoicesTotal { get; set; }


        public string CityName { get; set; }
        public int? CityId { get; set; }
        public string RegionName { get; set; }
        public int? RegionId { get; set; }
        public string ProviderType { get; set; }
        public int ProviderTypeId { get; set; }
        public int? Status { get; set; }

    }
    public class InvoiceMedicalProvider
    {
        public DateTime? InvoiceDate { get; set; }
        public int? InvoiceType { get; set; }
        public string InvoiceTypeName { get; set; }
        public decimal? TotalAmount { get; set; }
        public string CityName { get; set; }
        public int? CityId { get; set; }
        public string RegionName { get; set; }
        public int? RegionId { get; set; }
        public string ProviderType { get; set; }
        public int ProviderTypeId { get; set; }
        public int? Status { get; set; }
        public int InvoicesCount { get; set; }
        public decimal? InvoicesTotal { get; set; }
        public int PaidInvoicesCount { get; set; }
        public int UnpaidInvoicesCount { get; set; }
        public int CancelInvoicesCount { get; set; }
        public decimal? PaidInvoicesTotal { get; set; }
        public decimal? UnpaidInvoicesTotal { get; set; }
        public decimal? CancelInvoicesTotal { get; set; }
        public decimal? AdministrativeFees { get; set; }
        public bool HasAdministrative { get; set; }
        public decimal? SubscriptionFees { get; set; }
    }
    public class SummarizeInvoiceProvider
    {
        public int ProviderTypeId { get; set; }
        public int? InvoiceType { get; set; }
        public int NumberOfPaidSubscription { get; set; }
        public decimal? SubscriptionPaidTotal { get; set; }
        public int NumberOfUnpaidSubscription { get; set; }
        public decimal? UnpaidSubscriptionTotal { get; set; }
        public int NumberOfCancelSubscription { get; set; }
        public decimal? CancelSubscriptionTotal { get; set; }

        public int NumberOfPaidReservation { get; set; }
        public decimal? ReservationPaidTotal { get; set; }
        public int NumberOfUnpaidReservation { get; set; }
        public decimal? UnpaidReservationTotal { get; set; }
        public int NumberOfCancelReservation { get; set; }
        public decimal? CancelReservationTotal { get; set; }

        public int NumberOfPaidAdministrative { get; set; }
        public decimal? AdministrativePaidTotal { get; set; }
        public int NumberOfUnpaidAdministrative { get; set; }
        public decimal? UnpaidAdministrativeTotal { get; set; }
        public int NumberOfCancelAdministrative { get; set; }
        public decimal? CancelAdministrativeTotal { get; set; }
    }
}