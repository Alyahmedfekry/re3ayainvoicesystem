﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceDetailsViewModel
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
        public decimal? Debit { get; set; }

        [Compare("Debit")]
        public decimal? Credit { get; set; }
    }
}