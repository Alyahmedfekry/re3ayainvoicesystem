﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceFollowupViewModel
    {
        public int Id { get; set; }
        public int? InvoiceId { get; set; }
        public int? UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        [Required(ErrorMessage ="Required!")]
        public string Comment { get; set; }
        public string Note { get; set; }
    }
}