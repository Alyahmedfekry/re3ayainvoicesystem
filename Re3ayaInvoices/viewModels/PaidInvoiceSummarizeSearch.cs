﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class PaidInvoiceSummarizeSearch
    {
        public PaidInvoiceSummarizeSearch()
        {
            AgentIds = new List<int>();
        }
        public List<int> AgentIds { get; set; }
        public bool? AllAgents { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public int ReportType { get; set; }
        public bool? Unpaid { get; set; }
    }
}