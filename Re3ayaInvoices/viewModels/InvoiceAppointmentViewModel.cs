﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceAppointmentViewModel
    {
        public int Id { get; set; }
        public DateTime? VisitDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public string Code { get; set; }
        public int? Status { get; set; }
        public string StatusName { get; set; }
        public string PatientName { get; set; }
        public string PatientPhone { get; set; }
        public decimal? Re3ayaFees { get; set; }
        public decimal? AppointmentFees { get; set; }

    }
}