﻿using System;

namespace Re3ayaInvoices.viewModels
{
    public class SummarizeAchievementSearch
    {
        public DateTime? From { get; set; }
        public DateTime? ToDate { get; set; }
        public int? ProviderType { get; set; }
        public int? InvoiceType { get; set; }
        public int? AcheviementType { get; set; }
        public int? SerialNumber { get; set; }

    }
}
