﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class SystemMenuViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Required",AllowEmptyStrings =false)]
        public string NameEn { get; set; }
        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        public string NameAr { get; set; }
        public int? ActionId { get; set; }
        public int? ParentId { get; set; }
        [Required(ErrorMessage = "Required")]
        public int? Rank { get; set; }
    }
}