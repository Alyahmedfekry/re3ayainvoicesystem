﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceSummarizationSearch
    {
        public int? InvoiceType { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}