﻿using System;

namespace Re3ayaInvoices.viewModels
{
    public class AccounttingLogVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string InvoiceType { get; set; }
        public string Message { get; set; }
        public DateTime? EventDate { get; set; }
        public string Code { get; set; }
    }
    public class AccountLogSearchVM
    {
        public string Code { get; set; }
        public int? InvoiceType { get; set; }
        public int? LogType { get; set; }
        //public int? DocId { get; set; }
        //public int? EntityId { get; set; }
    }
}
