﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class ResetInvoiceData
    {
        public int? AppointmentId { get; set; }
        public int? ProviderType { get; set; }
    }
}