﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Helpers;
namespace Re3ayaInvoices.viewModels
{
    public class SystemRoleViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        [RegularExpression(Regex.ArabicLetterPattern, ErrorMessage = "arabic letter only")]
        public string ArabicName { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        [RegularExpression(Regex.EnglishLetterPattern, ErrorMessage = "english letter only")]
        public string EnglishName { get; set; }

        [Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        public string RoleDesc { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsMain { get; set; }
        //[Required(ErrorMessage = "Required", AllowEmptyStrings = false)]
        public string DashboardUrl { get; set; }
        public List<int?> ActionsId { get; set; }
        public List<int?> MenusId { get; set; }
    }
}