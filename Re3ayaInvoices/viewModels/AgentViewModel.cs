﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Helpers;
namespace Re3ayaInvoices.viewModels
{
    public class AgentViewModel
    {
        public int Account_Agent_ID { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        [Display(Name = "Agent Name")]
        public string Account_Agent_Name { get; set; }
        [Required(ErrorMessage = "Mobile Number is Required")]
        [RegularExpression(Regex.MobilePattern_EG, ErrorMessage = " invalid mobile number!")]
        [Display(Name = "Mobile Number")]
        public string Account_Agent_Phone { get; set; }
        public bool? IsActive { get; set; }
        [Display(Name = "Region(s)")]
        public List<int> RegionIds { get; set; }
        public List<int> CityIds { get; set; }
        public bool AllRegions { get; set; }
    }
}