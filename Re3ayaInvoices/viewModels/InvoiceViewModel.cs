﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceViewModel
    {
        public InvoiceViewModel()
        {
            invoiceDetails = new List<InvoiceDetailsViewModel>();
        }
        public int InvoiceId { get; set; }
        public int? Invoice_Number { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalPaid { get; set; }
        public DateTime? InvoiceDate { get; set; }

        [Required(ErrorMessage ="Required!")]
        public int? AcheivementType { get; set; }
        public string AcheivementTypeName { get; set; }

        [Required(ErrorMessage = "Required!")]
        [DataType(DataType.DateTime)]
        public DateTime? AcheivementDateTime { get; set; }
        public string ReceiptNumber { get; set; }
        public string ReceiptChar { get; set; }
        public int? Type { get; set; }
        public string TypeName { get; set; }
        public DateTime? InvoiceCreationDatetime { get; set; }
        public string AgentName { get; set; }
        public int? SerialNumber { get; set; }
        public bool? IsPaid { get; set; }
        public bool? IsCancel { get; set; }
        public bool? IsDoctor { get; set; }
        public bool? Is_Deduction { get; set; }
        public bool? ProviderActiveStatus { get; set; }
        public string providerName { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderPhone { get; set; }
        public string ProviderAddress { get; set; }
        public int? ProviderType { get; set; }
        public string ProviderTypeName { get; set; }
        public int? CityId { get; set; }
        public int? RegionId { get; set; }
        public string RegionName { get; set; }
        public string CityName { get; set; }
        public bool? IsPaidBefore { get; set; }
        public List<InvoiceDetailsViewModel> invoiceDetails { get; set; }
    }
}
