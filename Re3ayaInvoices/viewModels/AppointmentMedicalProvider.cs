﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class AppointmentMedicalProvider
    {
        public DateTime? CreatedDate { get; set; }
        public string CityName { get; set; }
        public int? CityId { get; set; }
        public string RegionName { get; set; }
        public int? RegionId { get; set; }
        public string ProviderType { get; set; }
        public int ProviderTypeId { get; set; }
        public int? Status { get; set; }
        public decimal? Re3ayaFees { get; set; }

        public int AppointmentsCount { get; set; }
        public decimal? AppointmentsTotal { get; set; }
        public int ConfirmedAppointmentsCount { get; set; }
        public int UnconfirmedAppointmenstCount { get; set; }
        public int CancelAppointmentsCount { get; set; }
        public decimal? ConfirmedAppointmentsTotal { get; set; }
        public decimal? UnconfirmedAppointmentsTotal { get; set; }
        public decimal? CancelAppointmentsTotal { get; set; }
    }
}