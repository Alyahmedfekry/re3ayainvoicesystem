﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class AppointmentReportSearch
    {
        public AppointmentReportSearch()
        {
            RegionIds = new List<int>();
            CityIds = new List<int>();
        }
        public List<int> CityIds { get; set; }
        public List<int> RegionIds { get; set; }
        public bool? AllRegions { get; set; }
        public bool? AllCity { get; set; }
        public int? AgentId { get; set; }
        public int ReportType { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}