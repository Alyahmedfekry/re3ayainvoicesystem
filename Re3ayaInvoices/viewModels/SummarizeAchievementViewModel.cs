﻿using System;

namespace Re3ayaInvoices.viewModels
{
    public class SummarizeAchievementViewModel
    {
        public int? InvoiceNumber { get; set; }
        public int? InvoiceId { get; set; }
        public string ReceiptNumber { get; set; }
        public DateTime? AcheivementDateTime { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProviderPhone { get; set; }
        public string ProviderRegion { get; set; }
        public string AcheivementTypeName { get; set; }
        public decimal? AcheivementTotal { get; set; }
        public string InvoiceType { get; set; }
        public string AgentName { get; set; }
        public string InvcoiceChar { get; set; }
        public int? SerialNumber { get; set; }
    }
}
