﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class SystemUserViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "required", AllowEmptyStrings = false)]
        public string FullName { get; set; }
        [Required(ErrorMessage = "required", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email!!")]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public bool? IsActive { get; set; }
        [Required(ErrorMessage = "required")]
        public List<int?> Roles { get; set; }
        public List<string> RolesName { get; set; }
    }
}