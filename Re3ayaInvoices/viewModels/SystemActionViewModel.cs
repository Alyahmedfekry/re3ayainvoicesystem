﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class SystemActionViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Required")]
        public string ActionName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string ActionLink { get; set; }
        public string ControllerName { get; set; }
        [Required(ErrorMessage = "Required")]
        public int? ControllerId { get; set; }
    }
}