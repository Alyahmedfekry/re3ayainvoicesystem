﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class PaidInvoiceSummarizeViewModel
    {
        public int Day { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public decimal? SubscriptionTotal { get; set; }
        public decimal? OnlineSubscriptionTotal { get; set; }
        public decimal? AdmistrativeTotal { get; set; }
        public decimal? ReservationTotal { get; set; }
        public decimal? HomevisitTotal { get; set; }
        public int SubscriptionCount { get; set; }
        public int OnlineSubscriptionCount { get; set; }
        public int AdmistrativeCount { get; set; }
        public int ReservationCount { get; set; }
        public int HomevisitCount { get; set; }
        public decimal? PaidTotal { get; set; }
        public string AgentName { get; set; }
        public int? AgentId { get; set; }
    }
}