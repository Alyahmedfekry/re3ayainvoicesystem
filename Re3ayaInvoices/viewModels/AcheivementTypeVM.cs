﻿using System.ComponentModel.DataAnnotations;

namespace Re3ayaInvoices.viewModels
{
    public class AcheivementTypeVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name En is Required")]
        [Display(Name = "Name En")]
        public string NameEn { get; set; }

        [Required(ErrorMessage = "Name AR is Required")]
        [Display(Name = "Name AR")]
        public string Name_Ar { get; set; }
    }
}
