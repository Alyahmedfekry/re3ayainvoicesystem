﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class SystemControllerViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string VisualName { get; set; }
    }
}