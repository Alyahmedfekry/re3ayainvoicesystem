﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class PrintViewModel
    {
        public PrintViewModel()
        {
            Region = new List<int>();
        }
        [Required(AllowEmptyStrings =false,ErrorMessage ="Please Choose Date")]
        public DateTime InvoiceDate { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Choose Medical Provider")]

        public int MedicalProvider { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Choose Invoice Type")]

        public int InvoiceType { get; set; }
        public  int? AgentId { get; set; }
        public List<int> Region { get; set; }
        public int? Governorate { get; set; }


    }
}