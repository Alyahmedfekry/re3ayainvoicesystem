﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceLogViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public int InvoiceNumber { get; set; }
        public string ActionName { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}