﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class ResetInvoiceDTO
    {
        public DateTime? CreationDate { get; set; }
        public int? ProviderId { get; set; }
        public int? ProviderType { get; set; }
        public int? InvoiceId { get; set; }
        public int? AppointmentId { get; set; }
    }
}