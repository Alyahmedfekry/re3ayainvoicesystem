﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class DoctorHasAppointmentViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string ProviderType { get; set; }
        public string Status { get; set; }
        public string Speciality { get; set; }
        public string MainAddress { get; set; }
        public string RegionName { get; set; }
        public string CityName { get; set; }
        public int? NumberOfAppointmnts { get; set; }
    }
}