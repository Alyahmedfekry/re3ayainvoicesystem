﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceStatisticViewModel
    {
        public DateTime? invoice_date { get; set; }
        public int invoice_count { get; set; }
        public string governrate_name { get; set; }
        public string region_name { get; set; }

        public decimal? subscription_total;

        public int subscription_total_count;
        public decimal? subscription_paid { get; set; }
        public int subscription_paid_count { get; set; }
        public decimal? subscription_net { get; set; }
        public int subscription_net_count { get; set; }
        public decimal? adminstrative_total { get; set; }
        public int adminstrative_total_count { get; set; }
        public decimal? adminstrative_paid { get; set; }
        public int adminstrative_paid_count { get; set; }
        public decimal? adminstrative_net { get; set; }
        public int adminstrative_net_count { get; set; }
        public decimal? confirmedappointment_total { get; set; }
        public int confirmedappointment_total_count { get; set; }
        public decimal? confirmedappointment_paid { get; set; }
        public int confirmedappointment_paid_count { get; set; }
        public decimal? confirmedappointment_net { get; set; }
        public int confirmedappointment_net_count { get; set; }
        public decimal? unconfirmedappointment_total { get; set; }
        public int unconfirmedappointment_total_count { get; set; }
        public decimal? unconfirmedappointment_paid { get; set; }
        public int unconfirmedappointment_paid_count { get; set; }
        public decimal? unconfirmedappointment_net { get; set; }
        public int unconfirmedappointment_net_count { get; set; }
        public decimal? video_total { get; set; }
        public int video_total_count { get; set; }
        public decimal? video_paid { get; set; }
        public int video_paid_count { get; set; }
        public decimal? video_net { get; set; }
        public int video_net_count { get; set; }
        public decimal? chat_total { get; set; }
        public int chat_total_count { get; set; }
        public decimal? chat_paid { get; set; }
        public int chat_paid_count { get; set; }
        public decimal? chat_net { get; set; }
        public int chat_net_count { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string speciality { get; set; }
    }
}