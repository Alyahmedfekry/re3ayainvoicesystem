﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Helpers;
namespace Re3ayaInvoices.viewModels
{
    public class UserMenuViewModel
    {
        public int Id { get; set; }
        public string MenuItemName { get; set; }
        public int? ParentId { get; set; }
        public bool IsAction { get; set; }
        public string ActionLink { get; set; }
        public int? Rank { get; set; }
        public bool Checked { get; set; }
        public List<UserMenuViewModel> Childs { get; set; }
        public string CreateUserMenu(UserMenuViewModel usermenu)
        {
            string tree = "";
            foreach (var child in usermenu.Childs)
            {
                string actionlink = Common.Ase_DecryptString(child.ActionLink);
                string menu_itemname = Common.Ase_DecryptString(child.MenuItemName);
                if (child.IsAction)
                {
                    tree += "<li class='menu-item' aria-haspopup='true'> " +
                        "<a href=" + actionlink + " class='menu-link'> " +
                        "<i class='menu-bullet menu-bullet-dot'>" +
                        "<span></span>" +
                        "</i>" +
                        " <span style='menu-text'>" + menu_itemname + "</span>" +
                        " </a> " +
                        "</li>";
                }
                else
                {
                    tree += "<li class='menu-item menu-item-submenu' aria-haspopup='true' data-menu-toggle='hover'>" +
                        "<a href='javascript:;' class='menu-link menu-toggle'>" +
                        "<i class='menu-bullet menu-bullet-line'>" +
                        "<span></span>" +
                        "</i>" +
                        "<span class='menu-text'>" + menu_itemname + "</span>" +
                        "<i class='menu-arrow'></i>" +
                        "</a>" +
                        "<div class='menu-submenu'>" +
                        " <i class='menu-arrow'></i>" +
                        "<ul class='menu-subnav'>" +
                        "" + CreateUserMenu(child) + "</ul>" +
                        "</div>"+
                        "</li>";
                }
            }

            return tree;
        }
        public bool HasAccessTo(string action)
        {
            var result = false;

            return result;
        }
    }
}