﻿using System;
using System.Collections.Generic;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceSearch
    {
        public InvoiceSearch()
        {
            CityIds = new List<int>();
            RegionIds = new List<int>();
            AgentIds = new List<int>();
        }
        public int? InvoiceNumber { get; set; }
        public int? type { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public int? Status { get; set; }
        public List<int> AgentIds { get; set; }
        public List<int> CityIds { get; set; }
        public List<int> RegionIds { get; set; }
        public int? ClosedStatus { get; set; }
        public int? reporttype { get; set; }
        public bool? isPeryear { get; set; }
        public int? ProviderType { get; set; }
        public string ProviderName { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderPhone { get; set; }
        public int? ProviderHas { get; set; }
        public int? MathOperatorId { get; set; }
        public decimal? InvoiceTotal { get; set; }
    }
}
