﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class PaymentReportSearch
    {
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public int? ProviderType { get; set; }
        public int? InvoiceType { get; set; }
    }
}