﻿using System;
using System.Collections.Generic;

namespace Re3ayaInvoices.viewModels
{
    public class InvoiceSummaryViewModel
    {
        public InvoiceSummaryViewModel()
        {
            InvoiceSummaryMedicalProvider = new List<InvoiceSummaryMedicalProvider>();
        }
        public DateTime InvoiceDate { get; set; }
        public List<InvoiceSummaryMedicalProvider> InvoiceSummaryMedicalProvider { get; set; }
        public int NumberOfInvoices { get; set; }
        public int NumberOfClosedInvoices { get; set; }
        public decimal? TotalInvoiceAmount { get; set; }
        public decimal? TotalPaidInvoiceAmount { get; set; }
    }
    public class InvoiceSummaryMedicalProvider
    {
        public string ProviderTypeName { get; set; }
        public int ProviderType { get; set; }
        public int NumberOfInvoices { get; set; }
    }
    public class InvoiceSummaryEntityViewModel
    {
        public DateTime? InvoiceDate { get; set; }
        public int NumberOfInvoices { get; set; }
        public int NumberOfClosedInvoices { get; set; }
        public string EntityName { get; set; }
        public byte? type { get; set; }
        public decimal? TotalInvoiceAmount { get; set; }
        public decimal? TotalPaidInvoiceAmount { get; set; }

    }
}
