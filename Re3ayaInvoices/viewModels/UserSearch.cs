﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.viewModels
{
    public class UserSearch
    {
        public string Name { get; set; }
        public bool? ActiveStatus { get; set; }
        public int? RoleId { get; set; }
    }
}