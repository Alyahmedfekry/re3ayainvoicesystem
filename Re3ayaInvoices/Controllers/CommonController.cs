﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.viewModels;
using Re3ayaInvoices.Services;
namespace Re3ayaInvoices.Controllers
{
    public class CommonController : Controller
    {
        private readonly CommonService commonService;
        public CommonController()
        {
            commonService = new CommonService();
        }
        [HttpGet]
        public ActionResult GetRegionList(int cityid)
        {
            var result = commonService.RegionMultiList(cityid);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetMultiRegionList(List<int> cityid)
        {
            var result = commonService.RegionMultiList(cityid);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetActivatedProviderIds(int providertype, string providercode,int?cityid)
        {
            var result =commonService.GetActivatedProviderIds(providertype, providercode,cityid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult sendwhatsappmessage(SummarizeAchievementSearch search)
        {
            ViewBag.search = search;
            TempData["search"] = search;
          
            var list = commonService.summarizeachievementsForWhatsApp(search);
            foreach (var item in list)
            {
                commonService.SendWhatsAppMessage(item.InvoiceId);
            }
            return RedirectToAction("SummarizeAchievement","Report");

        }
    }
}