﻿using Newtonsoft.Json;
using PagedList;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.Helpers;

namespace Re3ayaInvoices.Controllers
{
    public class AccountController : Controller
    {
        private readonly Re3aya247Entities context;
        private readonly SystemService systemService;
        private readonly CommonService commonService;
        private readonly LogService logService;
        //get
        public AccountController()
        {
            context = new Re3aya247Entities();
            systemService = new SystemService();
            commonService = new CommonService();
            logService = new LogService();
        }
        [HttpGet]
        [AllowAnonymous]
        [OverrideActionFilters]
        public ActionResult Login(string ReturnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                return LogOut();
            }
            ViewBag.ReturnUrl = ReturnUrl;

            return View();
        }
        [AllowAnonymous]
        [OverrideActionFilters]
        public ActionResult LogOut()
        {
            HttpCookie cookie = new HttpCookie("ReInvCookie", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);

            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel loginvm, string ReturnUrl = "")
        {
            if (ModelState.IsValid)
            {
                var user = systemService.Login(loginvm.Email, loginvm.Password);
                if (user != null)
                {
                    CustomSerializeModel userModel = new CustomSerializeModel
                    {
                        UserId = user.Id,
                        Name = Common.Ase_DecryptString(user.FullName),
                    };
                    string userData = JsonConvert.SerializeObject(userModel);
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Egypt Standard Time");
                    DateTime tstTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, tst);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                        (
                        1, userModel.Name, tstTime, tstTime.AddMinutes(15), false, userData
                        );
                    string ticket = FormsAuthentication.Encrypt(authTicket);
                    string cookiename = "ReInvCookie";
                    HttpCookie cookie = new HttpCookie(cookiename, ticket);
                    FormsAuthentication.SetAuthCookie(cookiename, false);
                    Response.Cookies.Add(cookie);

                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        string mainpage = Common.Ase_DecryptString(user.UserRoles.Where(r => r.SystemRole.IsMain == true).FirstOrDefault().SystemRole.RoleLandingPage);
                        var split = mainpage.Split('/');
                        return RedirectToAction(split[2], split[1]);
                    }
                }
            }
            ModelState.AddModelError("", "Something Wrong : Username or Password invalid ^_^ ");
            return View(loginvm);
        }

        [HttpGet]
        public ActionResult UserLog(InvoiceLogSearch search, int? page)
        {
            var loglist = logService.invoiceLogs(search, page);
            ViewBag.userid = commonService.UsersList(search.UserId);
            ViewBag.search = search;
            ViewBag.actionid = commonService.Action_TypeList(search.ActionId);

            return View(loglist);
        }
    }
}