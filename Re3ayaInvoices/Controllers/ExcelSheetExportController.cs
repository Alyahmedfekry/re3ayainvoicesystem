﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.viewModels;
using System.IO;
using Re3ayaInvoices.Helpers;
using System.Threading;

namespace Re3ayaInvoices.Controllers
{
    public class ExcelSheetExportController : Controller
    {
        private readonly ExcelSheetExportService excelSheetExportService;
        public ExcelSheetExportController()
        {
            excelSheetExportService = new ExcelSheetExportService();
        }
        public ActionResult PaymentReport(PaymentReportSearch search)
        {
            var cultureInfo = new System.Globalization.CultureInfo("ar-EG");
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            if (search.From.HasValue)
            {
                search.From = new DateTime(search.From.Value.Year, search.From.Value.Month, 1).Date;
                if (!search.To.HasValue)
                {
                    search.To = search.From;
                }
                else
                {
                    search.To = new DateTime(search.To.Value.Year, search.To.Value.Month, 1).Date;
                }
            }
            string sheetname = "-تقرير مدفوعات";
            switch (search.InvoiceType)
            {
                case (int)Common.InvoiceType.SubscriptionInvoice:
                    sheetname += "إشتراكات";
                    break;
                case (int)Common.InvoiceType.AppointmentInvoice:
                    sheetname += "حجوزات";
                    break;
                case (int)Common.InvoiceType.OnlineSubscriptionInvoice:
                    sheetname += "إشتراكات أونلاين";
                    break;
                case (int)Common.InvoiceType.HomeVisitInvoice:
                    sheetname += "زيارة منزلية";
                    break;
            }
            switch (search.ProviderType)
            {
                case (int)Common.MedicalProviderType.doctor:
                    sheetname += "أطباء ";
                    break;
                case (int)Common.MedicalProviderType.hospital:
                    sheetname += "مستشفيات ";
                    break;
                case (int)Common.MedicalProviderType.lab:
                    sheetname += "معامل ";
                    break;
                case (int)Common.MedicalProviderType.intencivecare:
                    sheetname += "رعاية مركزة ";
                    break;
                case (int)Common.MedicalProviderType.medicalcenter:
                    sheetname += "مراكز طبية ";
                    break;
                case (int)Common.MedicalProviderType.radiologycenter:
                    sheetname += "مراكز أشعة  ";
                    break;
                case (int)Common.MedicalProviderType.pharmacy:
                    sheetname += "صيدليات ";
                    break;
                case (int)Common.MedicalProviderType.bloodbank:
                    sheetname += "بنك الدم ";
                    break;
                case (int)Common.MedicalProviderType.newborncare:
                    sheetname += "حضانات ";
                    break;
            }
            var sheet = excelSheetExportService.PaymentReportSheet(search, sheetname);

            sheetname += (search.From.HasValue ? "الفترة بين " + search.From.Value.ToString("MMMM yyyy") +
                "-" + search.To.Value.ToString("MMMM yyyy") : "");

            return File(sheet, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "" + sheetname + ".xlsx");
        }
        public ActionResult InvoiceStatistics(DateTime FromDate, DateTime ToDate, int providertype, int sheettype)
        {
            string sheetpath = "";
            var stream = new MemoryStream();
            string sheetname = "";
            switch (sheettype)
            {
                case 0:
                    sheetpath = providertype == (int)Common.MedicalProviderType.doctor ?
                        Server.MapPath("~/ExcelSheets/Accounting.xlsx") : Server.MapPath("~/ExcelSheets/EntityAccounting.xlsx");
                    stream = excelSheetExportService.InvoiceStatisticsTotalSheet(FromDate, ToDate, providertype, sheetpath);
                    sheetname = "إجمالى_" + FromDate.ToShortDateString() + "_" + ToDate.ToShortDateString() + "";
                    break;
                case 1:
                    sheetpath = providertype == (int)Common.MedicalProviderType.doctor ?
                        Server.MapPath("~/ExcelSheets/Accounting_Governrate.xlsx") : Server.MapPath("~/ExcelSheets/EntityAccounting_Governrate.xlsx");
                    stream = excelSheetExportService.InvoiceStatisticsGovrenrateSheet(FromDate, ToDate, providertype, sheetpath);
                    sheetname = "محافظات_" + FromDate.ToShortDateString() + "_" + ToDate.ToShortDateString() + "";
                    break;
                case 2:
                    sheetpath = providertype == (int)Common.MedicalProviderType.doctor ?
                        Server.MapPath("~/ExcelSheets/Accounting_Region.xlsx") : Server.MapPath("~/ExcelSheets/EntityAccounting_Region.xlsx");
                    stream = excelSheetExportService.InvoiceStatisticsRegionSheet(FromDate, ToDate, providertype, sheetpath);
                    sheetname = "مناطق" + FromDate.ToShortDateString() + "_" + ToDate.ToShortDateString() + "";
                    break;
            }

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "" + sheetname + ".xlsx");
        }
        public ActionResult InvoicesSheet(InvoiceSearch search)
        {
            var stream = new MemoryStream();
            string sheetname = "قائمة الفواتير";
            if (search.From.HasValue)
            {
                search.From = new DateTime(search.From.Value.Year, search.From.Value.Month, 1).Date;
                if (!search.To.HasValue)
                {
                    search.To = search.From;
                }
                search.To = new DateTime(search.To.Value.Year, search.To.Value.Month, 1).Date;
            }
            stream = excelSheetExportService.InvoicesSheet(search, sheetname);

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "" + sheetname + ".xlsx");
        }
        public ActionResult PaidInvoicesSummarizeReport(PaidInvoiceSummarizeSearch search)
        {
            if (search.AllAgents.HasValue)
            {
                search.AgentIds = new List<int>();
            }
            string sheetpath = Server.MapPath("~/ExcelSheets/PaidInvoiceSummarizeSheet.xlsx");
            string sheetname =search.Unpaid.HasValue?"تقرير غير المحصل": "تقرير التحصيلات";
            sheetname +=search.From.HasValue ? " الفترة من "+ search.From.Value.ToShortDateString()+" إلى "
                 + search.To.Value.ToShortDateString():"";
            var result = excelSheetExportService.SummarizePaidInvoices(search,sheetpath);

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "" + sheetname + ".xlsx");
        }
        [AllowAnonymous]
        [OverrideActionFilters]
        public ActionResult ProviderStatement()
        {
            string sheetpath = Server.MapPath("~/ExcelSheets/Providerstatement.xlsx");
            var result = excelSheetExportService.ProviderStatement(sheetpath);

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ProviderStatement.xlsx");
        }
        public ActionResult UserLogSheet(InvoiceLogSearch search)
        {
            var result = excelSheetExportService.InvoiceLogSheet(search);

            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "userlog.xlsx");
        }
    }
}