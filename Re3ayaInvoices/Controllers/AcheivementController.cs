﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
namespace Re3ayaInvoices.Controllers
{
    public class AcheivementController : Controller
    {
        private readonly Re3aya247Entities context;
        public AcheivementController()
        {
            context = new Re3aya247Entities();
        }
        // GET: Acheivement
        public ActionResult Index()
        {
            var acheivementtype = context.AcheivementTypes.ToList();

            return View(acheivementtype);
        }
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AcheivementTypeVM acheivementType)
        {
            if (ModelState.IsValid)
            {
                AcheivementType ach = new AcheivementType();
                ach.Name_Ar = acheivementType.Name_Ar;
                ach.Name_En = acheivementType.NameEn;
                context.AcheivementTypes.Add(ach);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(acheivementType);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var acheivementtype = context.AcheivementTypes.Find(id);
            var vm = new AcheivementTypeVM
            {
                Id = acheivementtype.Id,
                NameEn = acheivementtype.Name_En,
                Name_Ar = acheivementtype.Name_Ar
            };
            return View(vm);
        }

        [HttpPost]
        public ActionResult Edit(AcheivementTypeVM acheivementType)
        {
            if (ModelState.IsValid)
            {
                var ach = context.AcheivementTypes.Where(m => m.Id == acheivementType.Id).FirstOrDefault();

                ach.Name_Ar = acheivementType.Name_Ar;
                ach.Name_En = acheivementType.NameEn;

                context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(acheivementType);
        }
    }
}