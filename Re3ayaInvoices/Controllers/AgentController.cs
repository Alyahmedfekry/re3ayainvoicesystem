﻿
using Re3ayaInvoices.CustomAuthentication;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.Services;
namespace Re3ayaInvoices.Controllers
{
    //get
    public class AgentController : Controller
    {
        Re3aya247Entities db = new Re3aya247Entities();
        private readonly BasicConfigService basicConfigService;
        private readonly CommonService commonService;
        public AgentController()
        {
            basicConfigService = new BasicConfigService();
            commonService = new CommonService();
        }
        public ActionResult Index(int? page,string name)
        {
            var result = basicConfigService.GetAgents(page,name);
            ViewBag.startindex = ((result.PageSize * result.PageNumber) - result.PageSize) + 1;
            ViewBag.name = name;

            return View(result);
        }
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.citylist = commonService.CityMultilist();

            return View();
        }
        [HttpPost]
        public ActionResult Create(AgentViewModel agentvm)
        {
            if (ModelState.IsValid)
            {
                basicConfigService.CreateAgent(agentvm);
              
                return RedirectToAction("Index");
            }
            ViewBag.citylist = commonService.CityMultilist();

            return View(agentvm);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var vm = basicConfigService.GetAgentById(id);
            ViewBag.citylist = commonService.CityMultilist();
          
            return View(vm);
        }
        [HttpPost]
        public ActionResult Edit(AgentViewModel agentvm)
        {
            if (ModelState.IsValid)
            {
                var agent = db.Account_Agent.Find(agentvm.Account_Agent_ID);
                if (agent == null)
                {
                    return HttpNotFound();
                }
                if (agent.AgentRegions != null)
                {
                    db.AgentRegions.RemoveRange(agent.AgentRegions);
                    db.SaveChanges();
                }
                if (agentvm.AllRegions)
                {
                    if (agentvm.CityIds != null)
                    {
                        foreach (var city in agentvm.CityIds)
                        {
                            var regions = db.Regions.Where(r => r.CityID == city);
                            if (regions != null)
                            {
                                foreach (var region in regions)
                                {
                                    var agentregion = new AgentRegion
                                    {
                                        RegionId = region.RegionID
                                    };
                                    agent.AgentRegions.Add(agentregion);
                                }
                            }
                        }
                    }
                }
                else if (agentvm.RegionIds != null)
                {
                    foreach (var region in agentvm.RegionIds)
                    {
                        var agentregion = new AgentRegion
                        {
                            RegionId = region
                        };
                        agent.AgentRegions.Add(agentregion);
                    }
                }
                agent.Account_Agent_Name = agentvm.Account_Agent_Name;
                agent.Account_Agent_Phone = agentvm.Account_Agent_Phone;
                db.SaveChanges();
               
                return RedirectToAction("Index");
            }
            var citylist = db.Governrates.ToList().Select(g => new SelectListItem
            {
                Text = g.Gov_Name_AR,
                Value = g.GovID.ToString(),
                Selected = agentvm.CityIds.Contains(g.GovID)
            });

            ViewBag.citylist = citylist;
            return View(agentvm);
        }
        [HttpPost]
        public ActionResult GetRegions(List<int> cityid)
        {
            if (cityid != null)
            {
                var regions = db.Regions.Where(r => cityid.Contains(r.CityID))
                    .Select(r => new
                    {
                        id = r.RegionID,
                        text = r.Region_Name_AR + " - " + r.Governrate.Gov_Name_AR,

                    }).ToList();

                return Json(regions);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        [OverrideActionFilters]
        public ActionResult ChangeActiveStatus(int id, bool isactive)
        {
            basicConfigService.UpdateAgentStatus(isactive, id);
            return Json("Status Updated Successfully>", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetRegionsAgent(int agent_id)
        {
            var agent = db.Account_Agent.Find(agent_id);
            var Regions = new List<int>();
            if (agent != null)
            {
                List<int> city = new List<int>();

                if (agent.AgentRegions != null)
                {
                    Regions = new List<int>();
                    for (var i = 0; i < agent.AgentRegions.Count(); i++)
                    {
                        Regions.Add(Convert.ToInt32(agent.AgentRegions.ElementAt(i).RegionId));
                        var cityid = db.Regions.Find(Regions[i]).CityID;
                        city.Add(cityid);
                    }
                }
                var regions = db.Regions.Where(m => Regions.Contains(m.RegionID))
                    .Select(r => new
                    {
                        id = r.RegionID,
                        text = r.Region_Name_AR + " - " + r.Governrate.Gov_Name_AR,

                    }).ToList();

                return Json(regions);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}
