﻿using Newtonsoft.Json;
using OfficeOpenXml;
using Re3ayaInvoices.CustomAuthentication;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.viewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Telerik.Reporting;
using Re3ayaInvoices.Filters;
namespace Re3ayaInvoices.Controllers
{
    public class HomeController : Controller
    {
        private readonly CommonService commonService;
        private readonly InvoiceService invoiceService;
        public HomeController()
        {
            commonService = new CommonService();
            invoiceService = new InvoiceService();
        }
        [HttpGet]
        //get
        public ActionResult Index(int? pagesize, int? page, InvoiceSummarizationSearch search)
        {
            if (search.From.HasValue)
            {
                search.From = new DateTime(search.From.Value.Year, search.From.Value.Month, 1).Date;
                if (!search.To.HasValue)
                {
                    search.To = search.From;
                }
                search.To = new DateTime(search.To.Value.Year, search.To.Value.Month, 1).Date;
            }
            var result = invoiceService.MonthlyInvoicesSummarization(pagesize, page, search);
            ViewBag.search = search;
            ViewBag.startindex = ((result.PageSize * result.PageNumber) - result.PageSize) + 1;
            ViewBag.InvoiceType = commonService.InvoiceTypeSelectList(search.InvoiceType);

            return View(result);
        }
        [HttpGet]
        public ActionResult Invoices(int? page, InvoiceSearch search)
        {
            if (search.From.HasValue)
            {
                search.From = new DateTime(search.From.Value.Year, search.From.Value.Month, 1).Date;
                if (!search.To.HasValue)
                {
                    search.To = search.From;
                }
            }
            var invoices = invoiceService.Invoices(page, search);
            ViewBag.CityIds = commonService.CityMultilist();
            ViewBag.AgentIds = commonService.AgentMultilist();
            ViewBag.search = search;
            ViewBag.startindex = ((invoices.PageSize * invoices.PageNumber) - invoices.PageSize) + 1;

            return View(invoices);
        }
        [HttpGet]
        public ActionResult InvoiceDetails(int id)
        {
            var invoice = invoiceService.InvoiceDetails(id);
            ViewBag.AcheivementType = commonService.AcheivementTypes();

            return View(invoice);
        }
        [HttpGet]
        [OverrideActionFilters]
        [AllowAnonymous]
        public ActionResult InvoiceDetailsPartial(int id)
        {
            var details = invoiceService.InvoiceDetails(id);

            return PartialView(details);
        }
        public ActionResult InvoiceAppointments(int id)
        {
            var appointments = invoiceService.GetInvoiceAppointments(id);
            ViewBag.invoiceid = id;

            return PartialView(appointments);
        }
        [HttpPost]
        public ActionResult UpdateInvoice(InvoiceViewModel vm)
        {
            invoiceService.ValidateUpdateInvoice(vm, this.ModelState);
            if (ModelState.IsValid)
            {
                var user = User as CustomPrincipal;
                invoiceService.UpdateInvoice(vm,user.UserId);

                return RedirectToAction("Invoices", new { ProviderType = vm.ProviderType, ProviderCode = vm.ProviderCode });
            }
            ViewBag.AcheivementType = commonService.AcheivementTypes();

            return View("InvoiceDetails", vm);
        }
        [HttpPost]
        [OverrideActionFilters]
        public ActionResult ResetInvoice(int id, int[] confirmedapps, int[] canceledapps)
        {
            Dictionary<int, bool> invoiceappointmentsreset = new Dictionary<int, bool>();
            if (confirmedapps != null)
            {
                foreach (var confirmedapp in confirmedapps)
                {
                    invoiceappointmentsreset.Add(confirmedapp, true);
                }
            }
            if (canceledapps != null)
            {
                foreach (var canceledapp in canceledapps)
                {
                    invoiceappointmentsreset.Add(canceledapp, false);
                }
            }
            if (invoiceappointmentsreset.Count() > 0)
            {
                invoiceService.UpdateInvoiceAppointmentsStatus(id, invoiceappointmentsreset);
            }
            var user = User as CustomPrincipal;
            invoiceService.ResetInvoice(id, null, null,user.UserId);

            return Json("Invoice Reset Successfully", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult CreateInvoice()
        {
            ViewBag.cityid = commonService.CityList();
            ViewBag.invoicetypeid = commonService.InvoiceTypes();

            return View();
        }
        [HttpGet]
        public ActionResult CreateAppointmentInvoice()
        {
            ViewBag.cityid = commonService.CityList();
           
            return View();
        }
        [HttpPost]
        [OverrideActionFilters]
        public ActionResult CreateAppointmentInvoice(DateTime fromdate, DateTime todate, int providertype, int providerid)
        {
            invoiceService.CreateAppointmentInvoice(providertype, providerid, fromdate, todate);

            return Json("");
        }
        [HttpPost]
        [OverrideActionFilters]
        public ActionResult CreateInvoice(DateTime invoicedate, int providertype, int providerid, int invoicetype)
        {
            invoiceService.CreateInvoice(providertype, providerid, invoicetype, invoicedate);

            return Json("");
        }
        [HttpGet]
        public ActionResult CreateOnlineInvoice()
        {
            ViewBag.cityid = commonService.CityList();

            return View();
        }
        [HttpPost]
        [OverrideActionFilters]
        public ActionResult CreateOnlineInvoice(DateTime invoicedate, int providerid,int? subscriptiontype)
        {
            invoiceService.CreateOnlineInvoice(providerid, invoicedate, subscriptiontype);

            return Json("");
        }
        [HttpGet]
        [OverrideActionFilters]
        public ActionResult CancelInvoice(string password, int invoiceid)
        {
            string hashpassword = Helpers.Crypto.Hash(password);
            var isvalid = true; /*context.AccountingLog_Type.Any(t => t.Id == 5 && t.TypeName == hashpassword);*/
            if (isvalid)
            {
                var user = User as CustomPrincipal;
                invoiceService.UpdateInvoiceStatus(invoiceid, (int)Common.InvoiceStatus.cancel,user.UserId);

                return Json(new { status = 1, msg = "Success" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, msg = "Incorrect Password!!" }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [OverrideActionFilters]
        public ActionResult CancelPaid(string password, int invoiceid)
        {
            string hashpassword = Helpers.Crypto.Hash(password);
            var isvalid = true; /*context.AccountingLog_Type.Any(t => t.Id == 5 && t.TypeName == hashpassword);*/
            if (isvalid)
            {
                var user = User as CustomPrincipal;
                invoiceService.UpdateInvoiceStatus(invoiceid, (int)Common.InvoiceStatus.unpaid,user.UserId);

                return Json(new { status = 1, msg = "Success" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0, msg = "Incorrect Password!!" }, JsonRequestBehavior.AllowGet);
        }
        [OverrideActionFilters]
        public JsonResult UpdateDeductionStatus(int invoice_id, bool status)
        {
            try
            {
                invoiceService.UpdateDeductionStatus(invoice_id, status);

                return Json(new { success = true, message = "status changed successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "something went wrong" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
