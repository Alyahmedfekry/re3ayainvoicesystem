﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.viewModels;
using Re3ayaInvoices.Helpers;
namespace Re3ayaInvoices.Controllers
{
    public class CommunicationController : Controller
    {
        private readonly CommunicationService communicationService;
        private readonly InvoiceService invoiceService;
        public CommunicationController()
        {
            communicationService = new CommunicationService();
            invoiceService = new InvoiceService();
        }
        public ActionResult SendWhatsapp_AchievementInvoices(InvoiceSearch search)
        {
            search.Status = (int)Common.InvoiceStatus.paid;
            if (search.From.HasValue)
            {
                search.From = new DateTime(search.From.Value.Year, search.From.Value.Month, 1).Date;
                if (!search.To.HasValue)
                {
                    search.To = search.From;
                }
                search.To = new DateTime(search.To.Value.Year, search.To.Value.Month, 1).Date;
            }
            var invoices = invoiceService.AchievementInvoices(search).ToList();
            foreach(var invoice in invoices)
            {
                communicationService.SendWhatsapp(invoice.InvoiceId);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public JsonResult SendWhatsapp(int invoice_id)
        {
            communicationService.SendWhatsapp(invoice_id);

            return Json(new { success = true, message = "Message sent successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
}