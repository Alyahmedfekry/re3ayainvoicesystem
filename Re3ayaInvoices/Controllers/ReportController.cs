﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.viewModels;
namespace Re3ayaInvoices.Controllers
{
    public class ReportController : Controller
    {
        private readonly ReportService reportService;
        private readonly ExcelSheetExportService excelSheetExportService;
        private readonly CommonService commonService;
        public ReportController()
        {
            reportService = new ReportService();
            commonService = new CommonService();
            excelSheetExportService = new ExcelSheetExportService();
        }
        [HttpGet]
        public ActionResult DownloadInvoice(int id, int providertype)
        {
            string reportname;
            var stream = reportService.GenerateInvoiceFile(id, providertype, out reportname);

            return File(stream, "application/pdf", "" + reportname + ".pdf");
        }
        [HttpGet]
        public ActionResult DownloadInvoices(InvoiceSearch search)
        {
            string reportname;
            var stream = reportService.GenerateInvoiceFile(search, out reportname);

            return File(stream, "application/pdf", "" + reportname + ".pdf");
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult DownloadUnpaidSubscriptionInvoices(InvoiceSearch search)
        {
            string reportname;
            var stream = reportService.GenerateUnpaidSubscriptionInvoices(search, out reportname);

            return File(stream, "application/pdf", "" + reportname + ".pdf");
        }
        [HttpGet]
        public ActionResult PaymentReport(PaymentReportSearch search)
        {
            ViewBag.InvoiceType = commonService.InvoiceTypes();
            var result = new List<InvoiceSummaryViewModel>();
            if (search.ProviderType.HasValue)
            {
                if (search.From.HasValue)
                {
                    search.From = new DateTime(search.From.Value.Year, search.From.Value.Month, 1).Date;
                    if (!search.To.HasValue)
                    {
                        search.To = search.From;
                    }
                    else
                    {
                        search.To = new DateTime(search.To.Value.Year, search.To.Value.Month, 1).Date;
                    }
                }
                result = reportService.CreatePaymentReport(search);
            }

            ViewBag.search = search;

            return View(result);
        }
        [HttpGet]
        public ActionResult InvoiceStatistics()
        {
            return View();
        }
        [HttpGet]
        public ActionResult AppointmentsReports()
        {
            ViewBag.CityId = commonService.CityMultilist();
            ViewBag.AgentId = commonService.AgentList();

            return View();
        }
        [HttpGet]
        public ActionResult CreateAppointmentsReports(AppointmentReportSearch search)
        {
            var cultureInfo = new System.Globalization.CultureInfo("ar-EG");
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            var stream = new MemoryStream();
            string sheetname = "";
            string sheetpath = "";
            if (search.ReportType == 3 || search.ReportType == 2)
            {
                if (search.From.HasValue && !search.To.HasValue)
                {
                    search.To = search.From;
                }
            }
            else
            {
                int lastday = 0;
                if (search.From.HasValue)
                {
                    search.From = new DateTime(search.From.Value.Year, search.From.Value.Month, 1).Date;
                    if (!search.To.HasValue)
                    {
                        lastday = DateTime.DaysInMonth(search.From.Value.Year, search.From.Value.Month);
                        search.To = new DateTime(search.From.Value.Year, search.From.Value.Month, lastday).Date;
                    }
                    else
                    {
                        lastday = DateTime.DaysInMonth(search.To.Value.Year, search.To.Value.Month);
                        search.To = new DateTime(search.To.Value.Year, search.To.Value.Month, lastday).Date;
                    }
                }
            }
            if (search.AllRegions.HasValue)
            {
                search.RegionIds = new List<int>();
            }
            if (search.AllCity.HasValue)
            {
                search.CityIds = new List<int>();
            }
            switch (search.ReportType)
            {
                case 1:
                    sheetname = "تقرير عام بالحجوزات ";
                    stream = excelSheetExportService.GeneralAppointmentsSheet(search.From, search.To, sheetname);
                    break;
                case 2:
                    sheetname = "أطباء لديهم حجوزات  ";
                    stream = excelSheetExportService.DoctorsHasAppointmentsSheet(search, sheetname);
                    break;
                case 3:
                    sheetname = "توزيع الحجوزات على الجهات الطبية ";
                    sheetpath = Server.MapPath("~/ExcelSheets/MedicalProviderAppointments_Governrate.xlsx");
                    stream = excelSheetExportService.AppointmentMedicalProviderSheet(search, sheetpath);
                    break;
                case 4:
                    sheetname = "حساب الأطباء للمناطق ";
                    sheetpath = Server.MapPath("~/ExcelSheets/DoctorInvoicesStatistic.xlsx");
                    stream = excelSheetExportService.DoctorInvoicesStatisticSheet(search, sheetpath);
                    break;
                case 5:
                    sheetname = "توزيع الفوانير على الجهات الطبية ";
                    sheetpath = Server.MapPath("~/ExcelSheets/MedicalProviderInvoices_Governrate.xlsx");
                    stream = excelSheetExportService.InvoiceMedicalProvidersSheet(search, sheetpath);
                    break;
                case 6:
                    sheetname = "ملخص فواتير الحجوزات والأشتراكات على الجهات الطبية ";
                    sheetpath = Server.MapPath("~/ExcelSheets/SummarizeInvoiceProvider.xlsx");
                    stream = excelSheetExportService.SummarizeInvoiceProvider(search, sheetpath);
                    break;
            }
            if (search.From.HasValue)
            {
                sheetname += "الفترة بين " + search.From.Value.ToString("MMMM yyyy") +
                    "-" + search.To.Value.ToString("MMMM yyyy");
            }

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "" + sheetname + ".xlsx");
        }
        [HttpGet]
        public ActionResult PaidInvoicesSummarizeReport()
        {
            ViewBag.AgentIds = commonService.AgentList();
            return View();
        }
        public ActionResult SummarizeAchievement(SummarizeAchievementSearch search, int page = 1)
        {
            ViewBag.search = search;
            if (TempData["Error"] != null)
            {
                ViewBag.Error = TempData["Error"];
                ViewBag.search = TempData["search"];
            }
            var summarizeachievements = reportService.summarizeachievements(page, search);

            return View(summarizeachievements);
        }
    }
}