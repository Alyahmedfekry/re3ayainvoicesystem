﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.viewModels;
namespace Re3ayaInvoices.Controllers
{
    public class SystemController : Controller
    {
        private readonly SystemService systemService;
        private readonly CommonService commonService;
        public SystemController()
        {
            systemService = new SystemService();
            commonService = new CommonService();
        }
        [HttpGet]
        [OverrideActionFilters]
        [AllowAnonymous]
        public ActionResult Roles(int? page, string name)
        {
            var result = systemService.GetRoles(page, name);
            ViewBag.startindex = ((result.PageSize * result.PageNumber) - result.PageSize) + 1;
            ViewBag.name = name;

            return View(result);
        }
        [HttpGet]
        [OverrideActionFilters]
        [AllowAnonymous]

        public ActionResult Users(int? page, UserSearch search)
        {
            var result = systemService.GetUsers(page, search);
            ViewBag.search = search;
            ViewBag.startindex = ((result.PageSize * result.PageNumber) - result.PageSize) + 1;

            return View(result);
        }
        [HttpGet]
        public ActionResult UserDetails(int id)
        {
            var vm = systemService.GetUserDetails(id);

            return PartialView(vm);
        }
        [HttpGet]
        public ActionResult CreateRole()
        {
            ViewBag.actions = commonService.SystemActionList();
            ViewBag.systemmenus = commonService.SystemMenuList();
            return View();
        }
        [HttpPost]
        public ActionResult CreateRole(SystemRoleViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.CreateRole(vm);

                return RedirectToAction("Roles");
            }
            ViewBag.actions = commonService.SystemActionList();
            ViewBag.systemmenus = commonService.SystemMenuList();

            return View(vm);
        }
        [HttpGet]
        public ActionResult CreateUser()
        {
            ViewBag.roles = commonService.RoleMultiList();
            return View();
        }
        [HttpPost]
        public ActionResult CreateUser(SystemUserViewModel vm)
        {
            var h = Common.Hash(vm.Password);
            if (ModelState.IsValid)
            {
                
                systemService.CreateUser(vm);

                return RedirectToAction("Users");
            }
            ViewBag.roles = commonService.RoleMultiList();

            return View(vm);
        }
        [HttpGet]
        public ActionResult EditRole(int id)
        {
            var role = systemService.GetRole(id);
            ViewBag.actions = commonService.SystemActionList();
            ViewBag.systemmenus = commonService.SystemMenuList();

            return View(role);
        }
        [HttpPost]
        public ActionResult EditRole(SystemRoleViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.EditRole(vm);

                return RedirectToAction("Roles");
            }
            ViewBag.actions = commonService.SystemActionList();
            ViewBag.systemmenus = commonService.SystemMenuList();

            return View(vm);
        }
        [HttpGet]
        public ActionResult EditUser(int id)
        {
            ViewBag.roles = commonService.RoleMultiList();
            var vm = systemService.GetUser(id);

            return View(vm);
        }
        [HttpGet]
        public ActionResult UpdateUserStatus(int id, bool isactive)
        {
            systemService.UpdateUserStatus(id, isactive);

            return Json("Active Status Has Been Changed Successfully!", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditUser(SystemUserViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.EditUser(vm);

                return RedirectToAction("Users");
            }
            ViewBag.roles = commonService.RoleMultiList();

            return View(vm);
        }
        [HttpGet]
        public ActionResult SystemCtrls(int?page)
        {
            var result = systemService.GetSystemCtrls(page);
            return View(result);
        }
        [HttpGet]
        public ActionResult SystemActions(int?page,int controllerid)
        {
            var result = systemService.GetSystemActions(page, controllerid);
            return View(result);
        }
        [HttpGet]
        public ActionResult CreateSystemCtrl()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateSystemCtrl(SystemControllerViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.CreateSystemController(vm);
                return RedirectToAction("SystemCtrls");
            }
            return View(vm);
        }
        [HttpGet]
        public ActionResult EditSystemCtrl(int id)
        {
            var vm = systemService.GetSystemControllerById(id);

            return View(vm);
        }
        [HttpPost]
        public ActionResult EditSystemCtrl(SystemControllerViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.EditSystemController(vm);

                return RedirectToAction("SystemCtrls");
            }

            return View(vm);
        }
        [HttpGet]
        public ActionResult CreateSystemAction()
        {
            ViewBag.ControllerId = commonService.ControllerList();

            return View();
        }
        [HttpPost]
        public ActionResult CreateSystemAction(SystemActionViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.CreateSystemAction(vm);

                return RedirectToAction("SystemActions");
            }

            ViewBag.ControllerId = commonService.ControllerList();

            return View(vm);
        }
        [HttpGet]
        public ActionResult EditSystemAction(int id)
        {
            var vm = systemService.GetSystemActionById(id);
            ViewBag.ControllerId = commonService.ControllerList();

            return View(vm);
        }
        [HttpPost]
        public ActionResult EditSystemAction(SystemActionViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.EditSystemAction(vm);

                return RedirectToAction("SystemActions");
            }

            ViewBag.ControllerId = commonService.ControllerList();

            return View(vm);
        }
        [HttpGet]
        public ActionResult SystemMenu(int? page)
        {
            var result = systemService.GetSystemMenus(page);

            return View(result);
        }
        [HttpGet]
        public ActionResult CreateSystemMenu()
        {
            ViewBag.ParentId = commonService.ParentSystemMenus();
            ViewBag.ActionId = commonService.SystemActionList();

            return View(); 
        }
        [HttpPost]
        public ActionResult CreateSystemMenu(SystemMenuViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.CreateSystemMenu(vm);
                return RedirectToAction("SystemMenu");
            }
            ViewBag.ParentId = commonService.ParentSystemMenus();
            ViewBag.ActionId = commonService.SystemActionList();

            return View(vm);
        }
        [HttpGet]
        public ActionResult EditSystemMenu(int id)
        {
            var vm = systemService.GetSystemMenu(id);
            ViewBag.ActionId = commonService.SystemActionList();
            ViewBag.ParentId = commonService.ParentSystemMenus();
            return View(vm);
        }
        [HttpPost]
        public ActionResult EditSystemMenu(SystemMenuViewModel vm)
        {
            if (ModelState.IsValid)
            {
                systemService.EditSystemMenu(vm);

                return RedirectToAction("SystemMenu");
            }
            ViewBag.ParentId = commonService.ParentSystemMenus();
            ViewBag.ActionId = commonService.SystemActionList();

            return View(vm);
        }
    }
}