﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Re3ayaInvoices.Helpers
{
    public class Regex
    {
        public const string MobilePattern_EG = "^(01)[0125][0-9]{8}$";
        public const string NationalIdPattern_EG = "^[0-9]{14}$";
        public const string ArabicLetterPattern = "^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF\\s]*$";
        public const string EnglishLetterPattern = "^[a-zA-Z\\s]*$";
        public const string ArabicEnglishLetterPattern = "^[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FFa-zA-Z\\s]*$";
        public const string EnglishLetter_NumberPattern = "^[a-zA-Z0-9]*$";
        public const string NumberOnlyPattern = "^[0-9]*$";
        public const string NotEnglishLetterPattern = "^[^a-zA-Z]*$";
        public const string NotArabicLetterPattern = "^[^\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]*$";
        public const string PasswordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]){6,20}$";
    }
}