﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace Re3ayaInvoices.Helpers
{
    public class Common
    {
        public const int AppId = 1;
        public enum CompanyId
        {
            Reaya=1,
            Wize=2
        }
        public enum MedicalProviderType
        {
            doctor = 0,
            hospital = 1,
            lab = 2,
            intencivecare = 3,
            medicalcenter = 4,
            radiologycenter = 5,
            pharmacy = 6,
            bloodbank = 7,
            newborncare = 8,
            homevisit = 9,
            offer=10
        }
        public enum ReportType
        {
            daily=1,
            monthly=2,
            yearly=3
        }
        public enum InvoiceItem
        {
            SubscriptionFees = 1,
            AdminstrativeExpenses = 2,
            VideoSubscriptionFees = 3,
            ChatSubscriptionFees = 4,
            ConfirmedAppointments = 5,
            Nonconfirmedappointments = 6,
            ServicesAds = 7
        }
        public enum InvoiceType
        {
            SubscriptionInvoice = 1,
            AppointmentInvoice = 2,
            OnlineSubscriptionInvoice = 3,
            HomeVisitInvoice = 4
        }
        public enum InvoiceStatus
        {
            paid=1,
            unpaid=2,
            cancel=3,
        }
        public enum LogType
        {
            No_achievement = 1,
            No_mainaddress = 2,
            No_Address = 4,
            updatestatus = 6,
            reset = 7,
            create = 8
        }
        public enum ActionType
        {
            Acheivement=1,
            CancelAcheivement=2,
            Cancel=3,
            Reset=4
        }
        public static Dictionary<int, string> MedicalProviders()
        {
            var medicalproviders = new Dictionary<int, string>()
        {
            {(int)Common.MedicalProviderType.doctor,"دكتور"},
            {(int)Common.MedicalProviderType.hospital,"مستشفى"},
            {(int)Common.MedicalProviderType.lab,"معمل"},
            {(int)Common.MedicalProviderType.intencivecare,"رعاية مركزة"},
            {(int)Common.MedicalProviderType.medicalcenter,"مركز طبى"},
            {(int)Common.MedicalProviderType.radiologycenter,"مركز أشعة"},
            {(int)Common.MedicalProviderType.pharmacy,"صيدلية"},
            {(int)Common.MedicalProviderType.bloodbank,"بنك الدم"},
            {(int)Common.MedicalProviderType.newborncare,"حضانة"},
            {(int)Common.MedicalProviderType.homevisit,"زيارات منزلية"},
            {(int)Common.MedicalProviderType.offer,"عروض"},
        };
            return medicalproviders;
        }
       
        private const string AseKey = "b14ca5898a4e4133bbce2ea2315a1916";
        public static string Ase_EncryptString(string plaintext)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(AseKey);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plaintext);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }
        public static string Ase_DecryptString(string ciphertext)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(ciphertext);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(AseKey);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
        public static String WhatsappSenderNotification(string Phone, MemoryStream file)
        {
            Phone = "2" + Phone;
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://re3.io/Home/WhatsappSender?key=MNO745$%_TUV__HHD&Phonenumber=" + Phone + "&Messagebody=" + file);
            myRequest.Method = "GET";
            WebResponse myResponse = myRequest.GetResponse();
            StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
            string result = sr.ReadToEnd();
            sr.Close();
            myResponse.Close();

            return result.Replace('"', ' ');
        }
        public static string Hash(string value)
        {
            return Convert.ToBase64String(
                System.Security.Cryptography.SHA256.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(value))
                );
        }
    }
}