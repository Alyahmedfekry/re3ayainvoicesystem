﻿using System;
using System.Net.Http;
using System.Web.Http;

namespace Re3ayaInvoices.Helpers
{
    public static class SMSMisr
    {
        const string SenderName = "re3aya247";
        const string Username = "QiTUNWsi";
        const string password = "I1OOFsgGAw";



        [NonAction]
        public static string SendMessage(string PhoneNumer, string Text)
        {
            try
            {
                if (!PhoneNumer.StartsWith("2") || !PhoneNumer.StartsWith("+2"))
                {
                    PhoneNumer = "2" + PhoneNumer;
                }
                HttpClient client = new HttpClient();
                Uri baseAddress = new Uri("https://smsmisr.com/");
                client.BaseAddress = baseAddress;
                var sendtime = DateTime.Now.ToString("yyyy-MM-dd-HH-mm");
                var response = client.PostAsJsonAsync("api/webapi/?" +
                  "username=" + Username +
                  "&password=" + password +
                  "&language=2" +
                  "&sender=" + SenderName +
                  "&mobile=" + PhoneNumer +
                  "&message=" + Text +
                  "&DelayUntil=" + sendtime, ""
                  ).Result.Content.ReadAsStringAsync();


                return "";

            }
            catch (Exception)
            {

                return "";
            }
        }
    }

    public class SMSMisrResponse
    {
        //{ "code": "1901", "SMSID":7511, "vodafone": 0, "etisalat": 1, "orange": 0, "we": 0, "Language": "English", "Vodafone_cost": 0, "Etisalat_cost": 1, "orange_cost": 0, "we_cost": 0, }
        public string code { get; set; }
        public string SMSID { get; set; }
        public string vodafone { get; set; }
        public string etisalat { get; set; }
        public string orange { get; set; }
        public string we { get; set; }
        public string Language { get; set; }
        public string Vodafone_cost { get; set; }
        public string Etisalat_cost { get; set; }
        public string orange_cost { get; set; }
        public string we_cost { get; set; }
    }
}
