﻿using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Filters;
using Re3ayaInvoices.CustomAuthentication;
namespace Re3ayaInvoices
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionFilter());
            filters.Add(new SystemAuthorize());
            filters.Add(new UserActionFilter());
        }
    }
}
