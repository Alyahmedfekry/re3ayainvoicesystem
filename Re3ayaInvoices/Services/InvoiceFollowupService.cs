﻿using PagedList;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Re3ayaInvoices.Services
{
    public class InvoiceFollowupService
    {
        private readonly Re3aya247Entities context;
        public InvoiceFollowupService()
        {
            context = new Re3aya247Entities();
        }
        public IPagedList<InvoiceFollowupViewModel> GetFollowups(int?page,int id)
        {
            var result = context.InvoiceFollowups.Where(f =>
            (f.InvoiceId == id)).Select(f => new InvoiceFollowupViewModel
            {
                Id = f.Id,
                CreatedDate = f.CreatedDate
            }).OrderByDescending(f => f.CreatedDate).ToPagedList(page ?? 1, 10);

            return result;
        }
        public void Create(InvoiceFollowupViewModel vm)
        {
            var followup = new InvoiceFollowup
            {
                CreatedDate = vm.CreatedDate,
                Comment = vm.Comment,
                InvoiceId = vm.InvoiceId,
                Note = vm.Note,
                UserId = vm.UserId
            };
            context.InvoiceFollowups.Add(followup);

            context.SaveChanges();
        }
        public InvoiceFollowupViewModel GetById(int id)
        {
            var followup = context.InvoiceFollowups.Find(id);
            var vm = new InvoiceFollowupViewModel
            {
                Id = followup.Id,
                Comment = followup.Comment,
                Note = followup.Note,
                InvoiceId = followup.InvoiceId
            };
            return vm;
        }
        public void Edit(InvoiceFollowupViewModel vm)
        {
            var followup = context.InvoiceFollowups.Find(vm.Id);
            followup.Comment = vm.Comment;
            followup.Note = vm.Note;

            context.SaveChanges();
        }
    }
}