﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.viewModels;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.Models;
using System.Data.Entity;

namespace Re3ayaInvoices.Services
{
    public class ExcelSheetExportService
    {
        private readonly ReportService reportService;
        private readonly InvoiceService invoiceService;
        private readonly LogService logService;
        public ExcelSheetExportService()
        {
            reportService = new ReportService();
            invoiceService = new InvoiceService();
            logService = new LogService();
        }
        public MemoryStream PaymentReportSheet(PaymentReportSearch search, string sheetname)
        {
            var result = reportService.CreatePaymentReport(search);
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add(sheetname);
            workSheet.Cells[1, 1].Value = "تاريخ الفاتورة";
            workSheet.Cells[1, 2].Value = "عدد الفواتير";
            workSheet.Cells[1, 3].Value = "الإجمالى";
            workSheet.Cells[1, 4].Value = "عدد الفواتير المحصلة";
            workSheet.Cells[1, 5].Value = "الإجمالى";
            workSheet.Cells[1, 6].Value = "عدد الفواتير المستحقة";
            workSheet.Cells[1, 7].Value = "الإجمالى";
            workSheet.Row(1).Height = 20;
            int columnindex = 1;
            int rowindex = 2;
            foreach (var appointemnt in result)
            {
                workSheet.Row(rowindex).Height = 20;

                workSheet.Cells[rowindex, columnindex].Value = appointemnt.InvoiceDate;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.NumberOfInvoices;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = Math.Round(appointemnt.TotalInvoiceAmount.Value, 2);
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.NumberOfClosedInvoices;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = Math.Round(appointemnt.TotalPaidInvoiceAmount.Value, 2);
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = (appointemnt.NumberOfInvoices - appointemnt.NumberOfClosedInvoices);
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = Math.Round((appointemnt.TotalInvoiceAmount.Value - appointemnt.TotalPaidInvoiceAmount.Value), 2);

                columnindex = 1;
                rowindex++;
            }
            workSheet.Cells.AutoFitColumns();
            workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            workSheet.Column(1).Style.Numberformat.Format = "dd-mm-yyyy";
            workSheet.View.FreezePanes(1, 1);
            var memoryStream = new MemoryStream();
            excel.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
        public MemoryStream InvoicesSheet(InvoiceSearch search, string sheetname)
        {
            var invoices = search.ProviderType == (int)Common.MedicalProviderType.doctor ?
             invoiceService.DoctorInvocies(search).ToList().OrderBy(i => i.RegionId)
                          .ThenBy(i => Convert.ToInt32(i.ProviderCode))
                    : invoiceService.Invoices(search).ToList().OrderBy(i => i.RegionId)
                          .ThenBy(i => Convert.ToInt64(i.ProviderCode));

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add(sheetname);
            workSheet.Cells[1, 1].Value = "رقم الفاتورة";
            workSheet.Cells[1, 2].Value = "تاريخ الفاتورة";
            workSheet.Cells[1, 3].Value = "الإجمالى";
            workSheet.Cells[1, 4].Value = "كود الدكتور";
            workSheet.Cells[1, 5].Value = "أسم الدكتور";
            workSheet.Cells[1, 6].Value = "الحالة";
            workSheet.Cells[1, 7].Value = " تليفون";
            workSheet.Cells[1, 8].Value = "المحافظة";
            workSheet.Cells[1, 9].Value = "المنطقة";
            workSheet.Cells[1, 10].Value = "العنوان";
            workSheet.Cells[1, 11].Value = "نوع الفاتورة";

            workSheet.Row(1).Height = 20;
            int columnindex = 1;
            int rowindex = 2;
            foreach (var invoice in invoices)
            {
                workSheet.Row(rowindex).Height = 20;

                workSheet.Cells[rowindex, columnindex].Value = invoice.Invoice_Number;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.InvoiceDate;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.TotalAmount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ProviderCode;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.providerName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ProviderActiveStatus.GetValueOrDefault() ? "مفعل" : "غير مفعل";
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ProviderPhone;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.CityName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.RegionName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ProviderAddress;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.TypeName;
                columnindex = 1;
                rowindex++;
            }
            workSheet.Cells.AutoFitColumns();
            workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            workSheet.Column(2).Style.Numberformat.Format = "dd-mm-yyyy";
            workSheet.View.FreezePanes(1, 1);
            var memoryStream = new MemoryStream();
            excel.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
        public MemoryStream AchievementInvoicesSheet(InvoiceSearch search, string sheetname)
        {
            var result = invoiceService.AchievementInvoices(search).OrderByDescending(i => i.SerialNumber).ToList();
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add(sheetname);
            workSheet.Cells[1, 1].Value = "كود البيان";
            workSheet.Cells[1, 2].Value = "رقم الفاتورة";
            workSheet.Cells[1, 3].Value = "نوع الفاتورة";
            workSheet.Cells[1, 4].Value = "رمز الفاتورة";
            workSheet.Cells[1, 5].Value = "رقم الإيصال";
            workSheet.Cells[1, 6].Value = "تاريخ التحصيل";
            workSheet.Cells[1, 7].Value = "نوع التحصيل";
            workSheet.Cells[1, 8].Value = "قيمة التحصيل";
            workSheet.Cells[1, 9].Value = "كود الجهة الطبية";
            workSheet.Cells[1, 10].Value = " أسم الجهة الطبية";
            workSheet.Cells[1, 11].Value = "تليفون الجهة الطبية";
            workSheet.Cells[1, 12].Value = "البيان";
            workSheet.Cells[1, 13].Value = "أسم المندوب";
            workSheet.Cells[1, 14].Value = "المنطقة";

            workSheet.Row(1).Height = 20;
            int columnindex = 1;
            int rowindex = 2;
            foreach (var invoice in result)
            {
                workSheet.Row(rowindex).Height = 20;

                workSheet.Cells[rowindex, columnindex].Value = invoice.Invoice_Number;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.SerialNumber;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.TypeName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ReceiptChar;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ReceiptNumber;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.AcheivementDateTime;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.AcheivementTypeName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.TotalPaid;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ProviderCode;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.providerName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.ProviderPhone;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = "";
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.AgentName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = invoice.RegionName;

                columnindex = 1;
                rowindex++;
            }
            workSheet.Cells.AutoFitColumns();
            workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            workSheet.Column(6).Style.Numberformat.Format = "dd-mm-yyyy";
            workSheet.View.FreezePanes(1, 1);
            var memoryStream = new MemoryStream();
            excel.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;

        }
        public MemoryStream InvoiceStatisticsTotalSheet(DateTime FromDate, DateTime ToDate, int providertype, string sheetpath)
        {
            int rowindex = 4;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetpath));
            var workbook = package.Workbook;
            var invoices = new List<InvoiceStatisticViewModel>();
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var mainheader = worksheet.Cells["1:1"].ToList();
                    var subheader = worksheet.Cells["2:2"].ToList();
                    mainheader.RemoveAll(c => c.Value == null);
                    subheader.RemoveAll(c => c.Value == null);

                    var result = reportService.InvoiceStatisticsTotal(FromDate, ToDate, providertype, sheetpath);
                    if (providertype == 0)
                    {
                        foreach (var invoice in invoices)
                        {
                            int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                            worksheet.Cells[rowindex, column_name_month].Value = invoice.invoice_date;

                            int column_name_invoice_count = mainheader.First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            worksheet.Cells[rowindex, column_name_invoice_count].Value = invoice.invoice_count;

                            int column_name_subscription = mainheader.First(c => c.Value.ToString() == "الأشتراكات السنوية").Start.Column;
                            int column_subscription_total = worksheet.Cells[3, column_name_subscription].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_subscription_total_count = worksheet.Cells[3, column_name_subscription + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_paid = worksheet.Cells[3, column_name_subscription + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_subscription_paid_count = worksheet.Cells[3, column_name_subscription + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_net = worksheet.Cells[3, column_name_subscription + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_subscription_net_count = worksheet.Cells[3, column_name_subscription + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_subscription_total].Value = invoice.subscription_total ?? 0;
                            worksheet.Cells[rowindex, column_subscription_paid].Value = invoice.subscription_paid ?? 0;
                            worksheet.Cells[rowindex, column_subscription_net].Value = invoice.subscription_net ?? 0;

                            worksheet.Cells[rowindex, column_subscription_total_count].Value = invoice.subscription_total_count;
                            worksheet.Cells[rowindex, column_subscription_paid_count].Value = invoice.subscription_paid_count;
                            worksheet.Cells[rowindex, column_subscription_net_count].Value = invoice.subscription_net_count;

                            int column_name_adminstrative = mainheader.First(c => c.Value.ToString() == "مصاريف إدارية").Start.Column;
                            int column_adminstrative_total = worksheet.Cells[3, column_name_adminstrative].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_adminstrative_total_count = worksheet.Cells[3, column_name_adminstrative + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_paid = worksheet.Cells[3, column_name_adminstrative + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_adminstrative_paid_count = worksheet.Cells[3, column_name_adminstrative + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_net = worksheet.Cells[3, column_name_adminstrative + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_adminstrative_net_count = worksheet.Cells[3, column_name_adminstrative + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_adminstrative_total].Value = invoice.adminstrative_total ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_paid].Value = invoice.adminstrative_paid ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_net].Value = invoice.adminstrative_net ?? 0;

                            worksheet.Cells[rowindex, column_adminstrative_total_count].Value = invoice.adminstrative_total_count;
                            worksheet.Cells[rowindex, column_adminstrative_paid_count].Value = invoice.adminstrative_paid_count;
                            worksheet.Cells[rowindex, column_adminstrative_net_count].Value = invoice.adminstrative_net_count;

                            int column_name_video = subheader.First(c => c.Value.ToString() == "فيديو").Start.Column;
                            int column_video_total = worksheet.Cells[3, column_name_video].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_video_total_count = worksheet.Cells[3, column_name_video + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_video_paid = worksheet.Cells[3, column_name_video + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_video_paid_count = worksheet.Cells[3, column_name_video + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_video_net = worksheet.Cells[3, column_name_video + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_video_net_count = worksheet.Cells[3, column_name_video + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_video_total].Value = invoice.video_total ?? 0;
                            worksheet.Cells[rowindex, column_video_paid].Value = invoice.video_paid ?? 0;
                            worksheet.Cells[rowindex, column_video_net].Value = invoice.video_net ?? 0;

                            worksheet.Cells[rowindex, column_video_total_count].Value = invoice.video_total_count;
                            worksheet.Cells[rowindex, column_video_paid_count].Value = invoice.video_paid_count;
                            worksheet.Cells[rowindex, column_video_net_count].Value = invoice.video_net_count;

                            int column_name_chat = subheader.First(c => c.Value.ToString() == "شات").Start.Column;
                            int column_chat_total = worksheet.Cells[3, column_name_chat].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_chat_total_count = worksheet.Cells[3, column_name_chat + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_chat_paid = worksheet.Cells[3, column_name_chat + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_chat_paid_count = worksheet.Cells[3, column_name_chat + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_chat_net = worksheet.Cells[3, column_name_chat + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_chat_net_count = worksheet.Cells[3, column_name_chat + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_chat_total].Value = invoice.chat_total ?? 0;
                            worksheet.Cells[rowindex, column_chat_paid].Value = invoice.chat_paid ?? 0;
                            worksheet.Cells[rowindex, column_chat_net].Value = invoice.chat_net ?? 0;

                            worksheet.Cells[rowindex, column_chat_total_count].Value = invoice.chat_total_count;
                            worksheet.Cells[rowindex, column_chat_paid_count].Value = invoice.chat_paid_count;
                            worksheet.Cells[rowindex, column_chat_net_count].Value = invoice.chat_net_count;

                            int column_name_confirmed_appointment = subheader.First(c => c.Value.ToString() == "مؤكدة").Start.Column;
                            int column_confirmed_appointment_total = worksheet.Cells[3, column_name_confirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_confirmed_appointment_total_count = worksheet.Cells[3, column_name_confirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_paid = worksheet.Cells[3, column_name_confirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_confirmed_appointment_paid_count = worksheet.Cells[3, column_name_confirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_net = worksheet.Cells[3, column_name_confirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_confirmed_appointment_net_count = worksheet.Cells[3, column_name_confirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_confirmed_appointment_total].Value = invoice.confirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid].Value = invoice.confirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net].Value = invoice.confirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_confirmed_appointment_total_count].Value = invoice.confirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid_count].Value = invoice.confirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net_count].Value = invoice.confirmedappointment_net_count;

                            int column_name_unconfirmed_appointment = subheader.First(c => c.Value.ToString() == "غير مؤكدة").Start.Column;
                            int column_unconfirmed_appointment_total = worksheet.Cells[3, column_name_unconfirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_unconfirmed_appointment_total_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_paid = worksheet.Cells[3, column_name_unconfirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_unconfirmed_appointment_paid_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_net = worksheet.Cells[3, column_name_unconfirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_unconfirmed_appointment_net_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total].Value = invoice.unconfirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid].Value = invoice.unconfirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net].Value = invoice.unconfirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total_count].Value = invoice.unconfirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid_count].Value = invoice.unconfirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net_count].Value = invoice.unconfirmedappointment_net_count;

                            int column_name_total = subheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_total = worksheet.Cells[3, column_name_total].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_paid = worksheet.Cells[3, column_name_total + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_invoice_net = worksheet.Cells[3, column_name_total + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;

                            worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($C$3:$AL$3=\"الإجمالى\")*C" + rowindex + ":AL" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_paid].Formula = "=SUMPRODUCT(($C$3:$AL$3=\"المدفوع\")*C" + rowindex + ":AL" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_net].Formula = "=SUMPRODUCT(($C$3:$AL$3=\"المتبقى\")*C" + rowindex + ":AL" + rowindex + ")";

                            rowindex++;
                        }
                    }
                    else
                    {
                        foreach (var invoice in invoices)
                        {
                            int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                            worksheet.Cells[rowindex, column_name_month].Value = invoice.invoice_date;

                            int column_name_invoice_count = mainheader.First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            worksheet.Cells[rowindex, column_name_invoice_count].Value = invoice.invoice_count;

                            int column_name_subscription = mainheader.First(c => c.Value.ToString() == "الأشتراكات السنوية").Start.Column;
                            int column_subscription_total = worksheet.Cells[3, column_name_subscription].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_subscription_total_count = worksheet.Cells[3, column_name_subscription + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_paid = worksheet.Cells[3, column_name_subscription + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_subscription_paid_count = worksheet.Cells[3, column_name_subscription + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_net = worksheet.Cells[3, column_name_subscription + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_subscription_net_count = worksheet.Cells[3, column_name_subscription + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_subscription_total].Value = invoice.subscription_total ?? 0;
                            worksheet.Cells[rowindex, column_subscription_paid].Value = invoice.subscription_paid ?? 0;
                            worksheet.Cells[rowindex, column_subscription_net].Value = invoice.subscription_net ?? 0;

                            worksheet.Cells[rowindex, column_subscription_total_count].Value = invoice.subscription_total_count;
                            worksheet.Cells[rowindex, column_subscription_paid_count].Value = invoice.subscription_paid_count;
                            worksheet.Cells[rowindex, column_subscription_net_count].Value = invoice.subscription_net_count;

                            int column_name_adminstrative = mainheader.First(c => c.Value.ToString() == "مصاريف إدارية").Start.Column;
                            int column_adminstrative_total = worksheet.Cells[3, column_name_adminstrative].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_adminstrative_total_count = worksheet.Cells[3, column_name_adminstrative + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_paid = worksheet.Cells[3, column_name_adminstrative + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_adminstrative_paid_count = worksheet.Cells[3, column_name_adminstrative + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_net = worksheet.Cells[3, column_name_adminstrative + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_adminstrative_net_count = worksheet.Cells[3, column_name_adminstrative + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_adminstrative_total].Value = invoice.adminstrative_total ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_paid].Value = invoice.adminstrative_paid ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_net].Value = invoice.adminstrative_net ?? 0;

                            worksheet.Cells[rowindex, column_adminstrative_total_count].Value = invoice.adminstrative_total_count;
                            worksheet.Cells[rowindex, column_adminstrative_paid_count].Value = invoice.adminstrative_paid_count;
                            worksheet.Cells[rowindex, column_adminstrative_net_count].Value = invoice.adminstrative_net_count;

                            int column_name_confirmed_appointment = subheader.First(c => c.Value.ToString() == "مؤكدة").Start.Column;
                            int column_confirmed_appointment_total = worksheet.Cells[3, column_name_confirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_confirmed_appointment_total_count = worksheet.Cells[3, column_name_confirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_paid = worksheet.Cells[3, column_name_confirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_confirmed_appointment_paid_count = worksheet.Cells[3, column_name_confirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_net = worksheet.Cells[3, column_name_confirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_confirmed_appointment_net_count = worksheet.Cells[3, column_name_confirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_confirmed_appointment_total].Value = invoice.confirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid].Value = invoice.confirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net].Value = invoice.confirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_confirmed_appointment_total_count].Value = invoice.confirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid_count].Value = invoice.confirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net_count].Value = invoice.confirmedappointment_net_count;

                            int column_name_unconfirmed_appointment = subheader.First(c => c.Value.ToString() == "غير مؤكدة").Start.Column;
                            int column_unconfirmed_appointment_total = worksheet.Cells[3, column_name_unconfirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_unconfirmed_appointment_total_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_paid = worksheet.Cells[3, column_name_unconfirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_unconfirmed_appointment_paid_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_net = worksheet.Cells[3, column_name_unconfirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_unconfirmed_appointment_net_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total].Value = invoice.unconfirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid].Value = invoice.unconfirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net].Value = invoice.unconfirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total_count].Value = invoice.unconfirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid_count].Value = invoice.unconfirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net_count].Value = invoice.unconfirmedappointment_net_count;

                            int column_name_total = subheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_total = worksheet.Cells[3, column_name_total].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_paid = worksheet.Cells[3, column_name_total + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_invoice_net = worksheet.Cells[3, column_name_total + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;

                            worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($C$3:$Z$3=\"الإجمالى\")*C" + rowindex + ":Z" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_paid].Formula = "=SUMPRODUCT(($C$3:$Z$3=\"المدفوع\")*C" + rowindex + ":Z" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_net].Formula = "=SUMPRODUCT(($C$3:$Z$3=\"المتبقى\")*C" + rowindex + ":Z" + rowindex + ")";

                            rowindex++;
                        }
                    }

                    worksheet.Cells.AutoFitColumns();
                }
            }
            var memoryStream = new MemoryStream();
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
        public MemoryStream InvoiceStatisticsGovrenrateSheet(DateTime FromDate, DateTime ToDate, int providertype, string sheetpath)
        {
            int rowindex = 4;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetpath));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var mainheader = worksheet.Cells["1:1"].ToList();
                    var subheader = worksheet.Cells["2:2"].ToList();
                    mainheader.RemoveAll(c => c.Value == null);
                    subheader.RemoveAll(c => c.Value == null);

                    var result = reportService.InvoiceStatisticsGovrenrate(FromDate, ToDate, providertype, sheetpath);
                    if (providertype == 0)
                    {
                        foreach (var invoice in result)
                        {
                            int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                            worksheet.Cells[rowindex, column_name_month].Value = invoice.invoice_date;

                            int column_name_governrate = mainheader.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                            worksheet.Cells[rowindex, column_name_governrate].Value = invoice.governrate_name;
                            int column_name_invoice_count = mainheader.First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            worksheet.Cells[rowindex, column_name_invoice_count].Value = invoice.invoice_count;

                            int column_name_subscription = mainheader.First(c => c.Value.ToString() == "الأشتراكات السنوية").Start.Column;
                            int column_subscription_total = worksheet.Cells[3, column_name_subscription].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_subscription_total_count = worksheet.Cells[3, column_name_subscription + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_paid = worksheet.Cells[3, column_name_subscription + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_subscription_paid_count = worksheet.Cells[3, column_name_subscription + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_net = worksheet.Cells[3, column_name_subscription + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_subscription_net_count = worksheet.Cells[3, column_name_subscription + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_subscription_total].Value = invoice.subscription_total ?? 0;
                            worksheet.Cells[rowindex, column_subscription_paid].Value = invoice.subscription_paid ?? 0;
                            worksheet.Cells[rowindex, column_subscription_net].Value = invoice.subscription_net ?? 0;

                            worksheet.Cells[rowindex, column_subscription_total_count].Value = invoice.subscription_total_count;
                            worksheet.Cells[rowindex, column_subscription_paid_count].Value = invoice.subscription_paid_count;
                            worksheet.Cells[rowindex, column_subscription_net_count].Value = invoice.subscription_net_count;

                            int column_name_adminstrative = mainheader.First(c => c.Value.ToString() == "مصاريف إدارية").Start.Column;
                            int column_adminstrative_total = worksheet.Cells[3, column_name_adminstrative].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_adminstrative_total_count = worksheet.Cells[3, column_name_adminstrative + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_paid = worksheet.Cells[3, column_name_adminstrative + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_adminstrative_paid_count = worksheet.Cells[3, column_name_adminstrative + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_net = worksheet.Cells[3, column_name_adminstrative + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_adminstrative_net_count = worksheet.Cells[3, column_name_adminstrative + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_adminstrative_total].Value = invoice.adminstrative_total ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_paid].Value = invoice.adminstrative_paid ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_net].Value = invoice.adminstrative_net ?? 0;

                            worksheet.Cells[rowindex, column_adminstrative_total_count].Value = invoice.adminstrative_total_count;
                            worksheet.Cells[rowindex, column_adminstrative_paid_count].Value = invoice.adminstrative_paid_count;
                            worksheet.Cells[rowindex, column_adminstrative_net_count].Value = invoice.adminstrative_net_count;

                            int column_name_video = subheader.First(c => c.Value.ToString() == "فيديو").Start.Column;
                            int column_video_total = worksheet.Cells[3, column_name_video].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_video_total_count = worksheet.Cells[3, column_name_video + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_video_paid = worksheet.Cells[3, column_name_video + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_video_paid_count = worksheet.Cells[3, column_name_video + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_video_net = worksheet.Cells[3, column_name_video + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_video_net_count = worksheet.Cells[3, column_name_video + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_video_total].Value = invoice.video_total ?? 0;
                            worksheet.Cells[rowindex, column_video_paid].Value = invoice.video_paid ?? 0;
                            worksheet.Cells[rowindex, column_video_net].Value = invoice.video_net ?? 0;

                            worksheet.Cells[rowindex, column_video_total_count].Value = invoice.video_total_count;
                            worksheet.Cells[rowindex, column_video_paid_count].Value = invoice.video_paid_count;
                            worksheet.Cells[rowindex, column_video_net_count].Value = invoice.video_net_count;

                            int column_name_chat = subheader.First(c => c.Value.ToString() == "شات").Start.Column;
                            int column_chat_total = worksheet.Cells[3, column_name_chat].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_chat_total_count = worksheet.Cells[3, column_name_chat + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_chat_paid = worksheet.Cells[3, column_name_chat + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_chat_paid_count = worksheet.Cells[3, column_name_chat + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_chat_net = worksheet.Cells[3, column_name_chat + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_chat_net_count = worksheet.Cells[3, column_name_chat + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_chat_total].Value = invoice.chat_total ?? 0;
                            worksheet.Cells[rowindex, column_chat_paid].Value = invoice.chat_paid ?? 0;
                            worksheet.Cells[rowindex, column_chat_net].Value = invoice.chat_net ?? 0;

                            worksheet.Cells[rowindex, column_chat_total_count].Value = invoice.chat_total_count;
                            worksheet.Cells[rowindex, column_chat_paid_count].Value = invoice.chat_paid_count;
                            worksheet.Cells[rowindex, column_chat_net_count].Value = invoice.chat_net_count;

                            int column_name_confirmed_appointment = subheader.First(c => c.Value.ToString() == "مؤكدة").Start.Column;
                            int column_confirmed_appointment_total = worksheet.Cells[3, column_name_confirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_confirmed_appointment_total_count = worksheet.Cells[3, column_name_confirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_paid = worksheet.Cells[3, column_name_confirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_confirmed_appointment_paid_count = worksheet.Cells[3, column_name_confirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_net = worksheet.Cells[3, column_name_confirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_confirmed_appointment_net_count = worksheet.Cells[3, column_name_confirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_confirmed_appointment_total].Value = invoice.confirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid].Value = invoice.confirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net].Value = invoice.confirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_confirmed_appointment_total_count].Value = invoice.confirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid_count].Value = invoice.confirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net_count].Value = invoice.confirmedappointment_net_count;

                            int column_name_unconfirmed_appointment = subheader.First(c => c.Value.ToString() == "غير مؤكدة").Start.Column;
                            int column_unconfirmed_appointment_total = worksheet.Cells[3, column_name_unconfirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_unconfirmed_appointment_total_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_paid = worksheet.Cells[3, column_name_unconfirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_unconfirmed_appointment_paid_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_net = worksheet.Cells[3, column_name_unconfirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_unconfirmed_appointment_net_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total].Value = invoice.unconfirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid].Value = invoice.unconfirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net].Value = invoice.unconfirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total_count].Value = invoice.unconfirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid_count].Value = invoice.unconfirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net_count].Value = invoice.unconfirmedappointment_net_count;

                            int column_name_total = subheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_total = worksheet.Cells[3, column_name_total].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_paid = worksheet.Cells[3, column_name_total + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_invoice_net = worksheet.Cells[3, column_name_total + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;


                            worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($D$3:$AM$3=\"الإجمالى\")*D" + rowindex + ":AM" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_paid].Formula = "=SUMPRODUCT(($D$3:$AM$3=\"المدفوع\")*D" + rowindex + ":AM" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_net].Formula = "=SUMPRODUCT(($D$3:$AM$3=\"المتبقى\")*D" + rowindex + ":AM" + rowindex + ")";
                            rowindex++;
                        }
                    }
                    else
                    {
                        foreach (var invoice in result)
                        {
                            int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                            worksheet.Cells[rowindex, column_name_month].Value = invoice.invoice_date;

                            int column_name_governrate = mainheader.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                            worksheet.Cells[rowindex, column_name_governrate].Value = invoice.governrate_name;
                            int column_name_invoice_count = mainheader.First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            worksheet.Cells[rowindex, column_name_invoice_count].Value = invoice.invoice_count;

                            int column_name_subscription = mainheader.First(c => c.Value.ToString() == "الأشتراكات السنوية").Start.Column;
                            int column_subscription_total = worksheet.Cells[3, column_name_subscription].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_subscription_total_count = worksheet.Cells[3, column_name_subscription + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_paid = worksheet.Cells[3, column_name_subscription + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_subscription_paid_count = worksheet.Cells[3, column_name_subscription + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_net = worksheet.Cells[3, column_name_subscription + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_subscription_net_count = worksheet.Cells[3, column_name_subscription + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_subscription_total].Value = invoice.subscription_total ?? 0;
                            worksheet.Cells[rowindex, column_subscription_paid].Value = invoice.subscription_paid ?? 0;
                            worksheet.Cells[rowindex, column_subscription_net].Value = invoice.subscription_net ?? 0;

                            worksheet.Cells[rowindex, column_subscription_total_count].Value = invoice.subscription_total_count;
                            worksheet.Cells[rowindex, column_subscription_paid_count].Value = invoice.subscription_paid_count;
                            worksheet.Cells[rowindex, column_subscription_net_count].Value = invoice.subscription_net_count;

                            int column_name_adminstrative = mainheader.First(c => c.Value.ToString() == "مصاريف إدارية").Start.Column;
                            int column_adminstrative_total = worksheet.Cells[3, column_name_adminstrative].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_adminstrative_total_count = worksheet.Cells[3, column_name_adminstrative + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_paid = worksheet.Cells[3, column_name_adminstrative + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_adminstrative_paid_count = worksheet.Cells[3, column_name_adminstrative + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_net = worksheet.Cells[3, column_name_adminstrative + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_adminstrative_net_count = worksheet.Cells[3, column_name_adminstrative + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_adminstrative_total].Value = invoice.adminstrative_total ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_paid].Value = invoice.adminstrative_paid ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_net].Value = invoice.adminstrative_net ?? 0;

                            worksheet.Cells[rowindex, column_adminstrative_total_count].Value = invoice.adminstrative_total_count;
                            worksheet.Cells[rowindex, column_adminstrative_paid_count].Value = invoice.adminstrative_paid_count;
                            worksheet.Cells[rowindex, column_adminstrative_net_count].Value = invoice.adminstrative_net_count;


                            int column_name_confirmed_appointment = subheader.First(c => c.Value.ToString() == "مؤكدة").Start.Column;
                            int column_confirmed_appointment_total = worksheet.Cells[3, column_name_confirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_confirmed_appointment_total_count = worksheet.Cells[3, column_name_confirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_paid = worksheet.Cells[3, column_name_confirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_confirmed_appointment_paid_count = worksheet.Cells[3, column_name_confirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_net = worksheet.Cells[3, column_name_confirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_confirmed_appointment_net_count = worksheet.Cells[3, column_name_confirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_confirmed_appointment_total].Value = invoice.confirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid].Value = invoice.confirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net].Value = invoice.confirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_confirmed_appointment_total_count].Value = invoice.confirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid_count].Value = invoice.confirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net_count].Value = invoice.confirmedappointment_net_count;

                            int column_name_unconfirmed_appointment = subheader.First(c => c.Value.ToString() == "غير مؤكدة").Start.Column;
                            int column_unconfirmed_appointment_total = worksheet.Cells[3, column_name_unconfirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_unconfirmed_appointment_total_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_paid = worksheet.Cells[3, column_name_unconfirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_unconfirmed_appointment_paid_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_net = worksheet.Cells[3, column_name_unconfirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_unconfirmed_appointment_net_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total].Value = invoice.unconfirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid].Value = invoice.unconfirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net].Value = invoice.unconfirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total_count].Value = invoice.unconfirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid_count].Value = invoice.unconfirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net_count].Value = invoice.unconfirmedappointment_net_count;

                            int column_name_total = subheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_total = worksheet.Cells[3, column_name_total].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_paid = worksheet.Cells[3, column_name_total + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_invoice_net = worksheet.Cells[3, column_name_total + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;


                            worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($D$3:$AA$3=\"الإجمالى\")*D" + rowindex + ":AA" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_paid].Formula = "=SUMPRODUCT(($D$3:$AA$3=\"المدفوع\")*D" + rowindex + ":AA" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_net].Formula = "=SUMPRODUCT(($D$3:$AA$3=\"المتبقى\")*D" + rowindex + ":AA" + rowindex + ")";
                            rowindex++;
                        }
                    }
                    worksheet.Cells.AutoFitColumns();
                }
            }
            var memoryStream = new MemoryStream();
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
        public MemoryStream InvoiceStatisticsRegionSheet(DateTime FromDate, DateTime ToDate, int providertype, string sheetpath)
        {
            int rowindex = 4;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetpath));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var mainheader = worksheet.Cells["1:1"].ToList();
                    var subheader = worksheet.Cells["2:2"].ToList();
                    mainheader.RemoveAll(c => c.Value == null);
                    subheader.RemoveAll(c => c.Value == null);

                    var result = reportService.InvoiceStatisticsRegion(FromDate, ToDate, providertype, sheetpath);
                    if (providertype == 0)
                    {
                        foreach (var invoice in result)
                        {
                            int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                            worksheet.Cells[rowindex, column_name_month].Value = invoice.invoice_date;

                            int column_name_governrate = mainheader.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                            worksheet.Cells[rowindex, column_name_governrate].Value = invoice.governrate_name;

                            int column_name_region = mainheader.First(c => c.Value.ToString() == "المنطقة").Start.Column;
                            worksheet.Cells[rowindex, column_name_region].Value = invoice.region_name;

                            int column_name_invoice_count = mainheader.First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            worksheet.Cells[rowindex, column_name_invoice_count].Value = invoice.invoice_count;

                            int column_name_subscription = mainheader.First(c => c.Value.ToString() == "الأشتراكات السنوية").Start.Column;
                            int column_subscription_total = worksheet.Cells[3, column_name_subscription].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_subscription_total_count = worksheet.Cells[3, column_name_subscription + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_paid = worksheet.Cells[3, column_name_subscription + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_subscription_paid_count = worksheet.Cells[3, column_name_subscription + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_net = worksheet.Cells[3, column_name_subscription + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_subscription_net_count = worksheet.Cells[3, column_name_subscription + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_subscription_total].Value = invoice.subscription_total ?? 0;
                            worksheet.Cells[rowindex, column_subscription_paid].Value = invoice.subscription_paid ?? 0;
                            worksheet.Cells[rowindex, column_subscription_net].Value = invoice.subscription_net ?? 0;

                            worksheet.Cells[rowindex, column_subscription_total_count].Value = invoice.subscription_total_count;
                            worksheet.Cells[rowindex, column_subscription_paid_count].Value = invoice.subscription_paid_count;
                            worksheet.Cells[rowindex, column_subscription_net_count].Value = invoice.subscription_net_count;

                            int column_name_adminstrative = mainheader.First(c => c.Value.ToString() == "مصاريف إدارية").Start.Column;
                            int column_adminstrative_total = worksheet.Cells[3, column_name_adminstrative].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_adminstrative_total_count = worksheet.Cells[3, column_name_adminstrative + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_paid = worksheet.Cells[3, column_name_adminstrative + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_adminstrative_paid_count = worksheet.Cells[3, column_name_adminstrative + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_net = worksheet.Cells[3, column_name_adminstrative + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_adminstrative_net_count = worksheet.Cells[3, column_name_adminstrative + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_adminstrative_total].Value = invoice.adminstrative_total ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_paid].Value = invoice.adminstrative_paid ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_net].Value = invoice.adminstrative_net ?? 0;

                            worksheet.Cells[rowindex, column_adminstrative_total_count].Value = invoice.adminstrative_total_count;
                            worksheet.Cells[rowindex, column_adminstrative_paid_count].Value = invoice.adminstrative_paid_count;
                            worksheet.Cells[rowindex, column_adminstrative_net_count].Value = invoice.adminstrative_net_count;

                            int column_name_video = subheader.First(c => c.Value.ToString() == "فيديو").Start.Column;
                            int column_video_total = worksheet.Cells[3, column_name_video].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_video_total_count = worksheet.Cells[3, column_name_video + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_video_paid = worksheet.Cells[3, column_name_video + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_video_paid_count = worksheet.Cells[3, column_name_video + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_video_net = worksheet.Cells[3, column_name_video + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_video_net_count = worksheet.Cells[3, column_name_video + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_video_total].Value = invoice.video_total ?? 0;
                            worksheet.Cells[rowindex, column_video_paid].Value = invoice.video_paid ?? 0;
                            worksheet.Cells[rowindex, column_video_net].Value = invoice.video_net ?? 0;

                            worksheet.Cells[rowindex, column_video_total_count].Value = invoice.video_total_count;
                            worksheet.Cells[rowindex, column_video_paid_count].Value = invoice.video_paid_count;
                            worksheet.Cells[rowindex, column_video_net_count].Value = invoice.video_net_count;

                            int column_name_chat = subheader.First(c => c.Value.ToString() == "شات").Start.Column;
                            int column_chat_total = worksheet.Cells[3, column_name_chat].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_chat_total_count = worksheet.Cells[3, column_name_chat + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_chat_paid = worksheet.Cells[3, column_name_chat + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_chat_paid_count = worksheet.Cells[3, column_name_chat + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_chat_net = worksheet.Cells[3, column_name_chat + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_chat_net_count = worksheet.Cells[3, column_name_chat + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_chat_total].Value = invoice.chat_total ?? 0;
                            worksheet.Cells[rowindex, column_chat_paid].Value = invoice.chat_paid ?? 0;
                            worksheet.Cells[rowindex, column_chat_net].Value = invoice.chat_net ?? 0;

                            worksheet.Cells[rowindex, column_chat_total_count].Value = invoice.chat_total_count;
                            worksheet.Cells[rowindex, column_chat_paid_count].Value = invoice.chat_paid_count;
                            worksheet.Cells[rowindex, column_chat_net_count].Value = invoice.chat_net_count;

                            int column_name_confirmed_appointment = subheader.First(c => c.Value.ToString() == "مؤكدة").Start.Column;
                            int column_confirmed_appointment_total = worksheet.Cells[3, column_name_confirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_confirmed_appointment_total_count = worksheet.Cells[3, column_name_confirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_paid = worksheet.Cells[3, column_name_confirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_confirmed_appointment_paid_count = worksheet.Cells[3, column_name_confirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_net = worksheet.Cells[3, column_name_confirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_confirmed_appointment_net_count = worksheet.Cells[3, column_name_confirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_confirmed_appointment_total].Value = invoice.confirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid].Value = invoice.confirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net].Value = invoice.confirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_confirmed_appointment_total_count].Value = invoice.confirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid_count].Value = invoice.confirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net_count].Value = invoice.confirmedappointment_net_count;

                            int column_name_unconfirmed_appointment = subheader.First(c => c.Value.ToString() == "غير مؤكدة").Start.Column;
                            int column_unconfirmed_appointment_total = worksheet.Cells[3, column_name_unconfirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_unconfirmed_appointment_total_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_paid = worksheet.Cells[3, column_name_unconfirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_unconfirmed_appointment_paid_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_net = worksheet.Cells[3, column_name_unconfirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_unconfirmed_appointment_net_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total].Value = invoice.unconfirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid].Value = invoice.unconfirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net].Value = invoice.unconfirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total_count].Value = invoice.unconfirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid_count].Value = invoice.unconfirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net_count].Value = invoice.unconfirmedappointment_net_count;

                            int column_name_total = subheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_total = worksheet.Cells[3, column_name_total].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_paid = worksheet.Cells[3, column_name_total + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_invoice_net = worksheet.Cells[3, column_name_total + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;

                            worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($D$3:$AN$3=\"الإجمالى\")*D" + rowindex + ":AN" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_paid].Formula = "=SUMPRODUCT(($D$3:$AN$3=\"المدفوع\")*D" + rowindex + ":AN" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_net].Formula = "=SUMPRODUCT(($D$3:$AN$3=\"المتبقى\")*D" + rowindex + ":AN" + rowindex + ")";

                            rowindex++;
                        }
                    }
                    else
                    {
                        foreach (var invoice in result)
                        {
                            int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                            worksheet.Cells[rowindex, column_name_month].Value = invoice.invoice_date;

                            int column_name_governrate = mainheader.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                            worksheet.Cells[rowindex, column_name_governrate].Value = invoice.governrate_name;

                            int column_name_region = mainheader.First(c => c.Value.ToString() == "المنطقة").Start.Column;
                            worksheet.Cells[rowindex, column_name_region].Value = invoice.region_name;

                            int column_name_invoice_count = mainheader.First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            worksheet.Cells[rowindex, column_name_invoice_count].Value = invoice.invoice_count;

                            int column_name_subscription = mainheader.First(c => c.Value.ToString() == "الأشتراكات السنوية").Start.Column;
                            int column_subscription_total = worksheet.Cells[3, column_name_subscription].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_subscription_total_count = worksheet.Cells[3, column_name_subscription + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_paid = worksheet.Cells[3, column_name_subscription + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_subscription_paid_count = worksheet.Cells[3, column_name_subscription + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_subscription_net = worksheet.Cells[3, column_name_subscription + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_subscription_net_count = worksheet.Cells[3, column_name_subscription + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_subscription_total].Value = invoice.subscription_total ?? 0;
                            worksheet.Cells[rowindex, column_subscription_paid].Value = invoice.subscription_paid ?? 0;
                            worksheet.Cells[rowindex, column_subscription_net].Value = invoice.subscription_net ?? 0;

                            worksheet.Cells[rowindex, column_subscription_total_count].Value = invoice.subscription_total_count;
                            worksheet.Cells[rowindex, column_subscription_paid_count].Value = invoice.subscription_paid_count;
                            worksheet.Cells[rowindex, column_subscription_net_count].Value = invoice.subscription_net_count;

                            int column_name_adminstrative = mainheader.First(c => c.Value.ToString() == "مصاريف إدارية").Start.Column;
                            int column_adminstrative_total = worksheet.Cells[3, column_name_adminstrative].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_adminstrative_total_count = worksheet.Cells[3, column_name_adminstrative + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_paid = worksheet.Cells[3, column_name_adminstrative + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_adminstrative_paid_count = worksheet.Cells[3, column_name_adminstrative + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;
                            int column_adminstrative_net = worksheet.Cells[3, column_name_adminstrative + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_adminstrative_net_count = worksheet.Cells[3, column_name_adminstrative + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_adminstrative_total].Value = invoice.adminstrative_total ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_paid].Value = invoice.adminstrative_paid ?? 0;
                            worksheet.Cells[rowindex, column_adminstrative_net].Value = invoice.adminstrative_net ?? 0;

                            worksheet.Cells[rowindex, column_adminstrative_total_count].Value = invoice.adminstrative_total_count;
                            worksheet.Cells[rowindex, column_adminstrative_paid_count].Value = invoice.adminstrative_paid_count;
                            worksheet.Cells[rowindex, column_adminstrative_net_count].Value = invoice.adminstrative_net_count;

                            int column_name_confirmed_appointment = subheader.First(c => c.Value.ToString() == "مؤكدة").Start.Column;
                            int column_confirmed_appointment_total = worksheet.Cells[3, column_name_confirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_confirmed_appointment_total_count = worksheet.Cells[3, column_name_confirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_paid = worksheet.Cells[3, column_name_confirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_confirmed_appointment_paid_count = worksheet.Cells[3, column_name_confirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_confirmed_appointment_net = worksheet.Cells[3, column_name_confirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_confirmed_appointment_net_count = worksheet.Cells[3, column_name_confirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;


                            worksheet.Cells[rowindex, column_confirmed_appointment_total].Value = invoice.confirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid].Value = invoice.confirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net].Value = invoice.confirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_confirmed_appointment_total_count].Value = invoice.confirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_paid_count].Value = invoice.confirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_confirmed_appointment_net_count].Value = invoice.confirmedappointment_net_count;

                            int column_name_unconfirmed_appointment = subheader.First(c => c.Value.ToString() == "غير مؤكدة").Start.Column;
                            int column_unconfirmed_appointment_total = worksheet.Cells[3, column_name_unconfirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_unconfirmed_appointment_total_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 1].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_paid = worksheet.Cells[3, column_name_unconfirmed_appointment + 2].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_unconfirmed_appointment_paid_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 3].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            int column_unconfirmed_appointment_net = worksheet.Cells[3, column_name_unconfirmed_appointment + 4].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                            int column_unconfirmed_appointment_net_count = worksheet.Cells[3, column_name_unconfirmed_appointment + 5].First(c => c.Value.ToString() == "فاتورة").Start.Column;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total].Value = invoice.unconfirmedappointment_total ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid].Value = invoice.unconfirmedappointment_paid ?? 0;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net].Value = invoice.unconfirmedappointment_net ?? 0;

                            worksheet.Cells[rowindex, column_unconfirmed_appointment_total_count].Value = invoice.unconfirmedappointment_total_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_paid_count].Value = invoice.unconfirmedappointment_paid_count;
                            worksheet.Cells[rowindex, column_unconfirmed_appointment_net_count].Value = invoice.unconfirmedappointment_net_count;

                            int column_name_total = subheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_total = worksheet.Cells[3, column_name_total].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            int column_invoice_paid = worksheet.Cells[3, column_name_total + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                            int column_invoice_net = worksheet.Cells[3, column_name_total + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;

                            worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($D$3:$AB$3=\"الإجمالى\")*D" + rowindex + ":AB" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_paid].Formula = "=SUMPRODUCT(($D$3:$AB$3=\"المدفوع\")*D" + rowindex + ":AB" + rowindex + ")";
                            worksheet.Cells[rowindex, column_invoice_net].Formula = "=SUMPRODUCT(($D$3:$AB$3=\"المتبقى\")*D" + rowindex + ":AB" + rowindex + ")";

                            rowindex++;
                        }
                    }

                    worksheet.Cells.AutoFitColumns();
                }
            }
            var memoryStream = new MemoryStream();
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
        public MemoryStream DoctorInvoicesStatisticSheet(AppointmentReportSearch search, string sheetpath)
        {
            int rowindex = 4;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetpath));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var invoices = reportService.DoctorInvoicesStatistic(search);
                    var mainheader = worksheet.Cells["1:1"].ToList();
                    var subheader = worksheet.Cells["2:2"].ToList();
                    mainheader.RemoveAll(c => c.Value == null);
                    subheader.RemoveAll(c => c.Value == null);
                    foreach (var invoice in invoices)
                    {
                        int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                        worksheet.Cells[rowindex, column_name_month].Value = invoice.InvoiceDate;
                        int column_name_governrate = mainheader.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                        worksheet.Cells[rowindex, column_name_governrate].Value = invoice.City;
                        int column_name_region = mainheader.First(c => c.Value.ToString() == "المنطقة").Start.Column;
                        worksheet.Cells[rowindex, column_name_region].Value = invoice.Region;

                        //int column_name_invoice_count = mainheader.First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                        //worksheet.Cells[rowindex, column_name_invoice_count].Value = invoice.;
                        int column_name_doctor_code = mainheader.First(c => c.Value.ToString() == "كود الدكتور").Start.Column;
                        worksheet.Cells[rowindex, column_name_doctor_code].Value = invoice.Doctor_CODE;
                        int column_name_doctor_name = mainheader.First(c => c.Value.ToString() == "أسم الدكتور").Start.Column;
                        worksheet.Cells[rowindex, column_name_doctor_name].Value = invoice.Doctor_Name_AR;
                        int column_name_doctor_speciality = mainheader.First(c => c.Value.ToString() == "التخصص").Start.Column;
                        worksheet.Cells[rowindex, column_name_doctor_speciality].Value = invoice.SpecialityName_AR;
                        int column_name_subscription = mainheader.First(c => c.Value.ToString() == "الأشتراكات السنوية").Start.Column;
                        int column_subscription_total = worksheet.Cells[3, column_name_subscription].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_subscription_paid = worksheet.Cells[3, column_name_subscription + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                        int column_subscription_net = worksheet.Cells[3, column_name_subscription + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                        worksheet.Cells[rowindex, column_subscription_total].Value = invoice.SubscriptionTotal ?? 0;
                        worksheet.Cells[rowindex, column_subscription_paid].Value = invoice.SubscriptionPaid ?? 0;
                        worksheet.Cells[rowindex, column_subscription_net].Value = invoice.NetSubscription ?? 0;

                        int column_name_adminstrative = mainheader.First(c => c.Value.ToString() == "مصاريف إدارية").Start.Column;
                        int column_adminstrative_total = worksheet.Cells[3, column_name_adminstrative].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_adminstrative_paid = worksheet.Cells[3, column_name_adminstrative + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                        int column_adminstrative_net = worksheet.Cells[3, column_name_adminstrative + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                        worksheet.Cells[rowindex, column_adminstrative_total].Value = invoice.AdminstrativeTotal ?? 0;
                        worksheet.Cells[rowindex, column_adminstrative_paid].Value = invoice.AdminstrativePaid ?? 0;
                        worksheet.Cells[rowindex, column_adminstrative_net].Value = invoice.NetAdminstrative ?? 0;

                        int column_name_video = subheader.First(c => c.Value.ToString() == "فيديو").Start.Column;
                        int column_video_total = worksheet.Cells[3, column_name_video].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_video_paid = worksheet.Cells[3, column_name_video + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                        int column_video_net = worksheet.Cells[3, column_name_video + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                        worksheet.Cells[rowindex, column_video_total].Value = invoice.SubscriptionVideoTotal ?? 0;
                        worksheet.Cells[rowindex, column_video_paid].Value = invoice.SubscriptionVideoPaid ?? 0;
                        worksheet.Cells[rowindex, column_video_net].Value = invoice.NetSubscriptionVideo ?? 0;

                        int column_name_chat = subheader.First(c => c.Value.ToString() == "شات").Start.Column;
                        int column_chat_total = worksheet.Cells[3, column_name_chat].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_chat_paid = worksheet.Cells[3, column_name_chat + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                        int column_chat_net = worksheet.Cells[3, column_name_chat + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                        worksheet.Cells[rowindex, column_chat_total].Value = invoice.SubscriptionChatTotal ?? 0;
                        worksheet.Cells[rowindex, column_chat_paid].Value = invoice.SubscriptionChatPaid ?? 0;
                        worksheet.Cells[rowindex, column_chat_net].Value = invoice.NetSubscriptionChat ?? 0;

                        int column_name_confirmed_appointment = subheader.First(c => c.Value.ToString() == "مؤكدة").Start.Column;
                        int column_confirmed_appointment_total = worksheet.Cells[3, column_name_confirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_confirmed_appointment_paid = worksheet.Cells[3, column_name_confirmed_appointment + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                        int column_confirmed_appointment_net = worksheet.Cells[3, column_name_confirmed_appointment + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                        worksheet.Cells[rowindex, column_confirmed_appointment_total].Value = invoice.ConfirmedAppointmentTotal ?? 0;
                        worksheet.Cells[rowindex, column_confirmed_appointment_paid].Value = invoice.ConfirmedAppointmentPaid ?? 0;
                        worksheet.Cells[rowindex, column_confirmed_appointment_net].Value = invoice.NetConfirmedAppointment ?? 0;

                        int column_name_unconfirmed_appointment = subheader.First(c => c.Value.ToString() == "غير مؤكدة").Start.Column;
                        int column_unconfirmed_appointment_total = worksheet.Cells[3, column_name_unconfirmed_appointment].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_unconfirmed_appointment_paid = worksheet.Cells[3, column_name_unconfirmed_appointment + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                        int column_unconfirmed_appointment_net = worksheet.Cells[3, column_name_unconfirmed_appointment + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;
                        worksheet.Cells[rowindex, column_unconfirmed_appointment_total].Value = invoice.UnconfirmedAppointmentTotal ?? 0;
                        worksheet.Cells[rowindex, column_unconfirmed_appointment_paid].Value = invoice.UnconfirmedAppointmentPaid ?? 0;
                        worksheet.Cells[rowindex, column_unconfirmed_appointment_net].Value = invoice.NetUnconfirmedAppointment ?? 0;

                        int column_name_total = subheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_invoice_total = worksheet.Cells[3, column_name_total].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_invoice_paid = worksheet.Cells[3, column_name_total + 1].First(c => c.Value.ToString() == "المدفوع").Start.Column;
                        int column_invoice_net = worksheet.Cells[3, column_name_total + 2].First(c => c.Value.ToString() == "المتبقى").Start.Column;

                        worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($H$3:$Y$3=\"الإجمالى\")*H" + rowindex + ":Y" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_paid].Formula = "=SUMPRODUCT(($H$3:$Y$3=\"المدفوع\")*H" + rowindex + ":Y" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_net].Formula = "=SUMPRODUCT(($H$3:$Y$3=\"المتبقى\")*H" + rowindex + ":Y" + rowindex + ")";

                        rowindex++;
                    }

                    worksheet.Cells.AutoFitColumns();
                }
            }
            var memoryStream = new MemoryStream();
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
        public MemoryStream GeneralAppointmentsSheet(DateTime? from, DateTime? to, string sheetname)
        {
            var result = reportService.GeneralAppointments(from, to);

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add(sheetname);
            workSheet.Cells[1, 1].Value = "الشهر";
            workSheet.Cells[1, 2].Value = "عدد الحجوزات";
            workSheet.Cells[1, 3].Value = "القيمة";
            workSheet.Cells[1, 4].Value = "عدد الفواتير";
            workSheet.Cells[1, 5].Value = "إجمالى الفواتير";
            workSheet.Cells[1, 6].Value = "عدد الحجوزات المؤكدة";
            workSheet.Cells[1, 7].Value = "إجمالى الحجوزات المؤكدة";
            workSheet.Cells[1, 8].Value = "عدد الحجوزات غير المؤكدة";
            workSheet.Cells[1, 9].Value = "إجمالى الحجوزات غير المؤكدة";
            workSheet.Cells[1, 10].Value = "عدد الحجوزات الملغاة";
            workSheet.Cells[1, 11].Value = "إجمالى الحجوزات الملغاة";
            workSheet.Cells[1, 12].Value = "عدد الفواتير المدفوعة";
            workSheet.Cells[1, 13].Value = "إجمالى الفواتير المدفوعة";
            workSheet.Cells[1, 14].Value = "عدد الفواتير الملغاة";
            workSheet.Cells[1, 15].Value = "إجمالى الفواتير الملغاة";
            workSheet.Cells[1, 16].Value = "عدد الفواتير المتبقية";
            workSheet.Cells[1, 17].Value = "إجمالى الفواتير المتبقية";

            workSheet.Row(1).Height = 20;
            int columnindex = 1;
            int rowindex = 2;
            foreach (var appointemnt in result)
            {
                workSheet.Row(rowindex).Height = 20;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.Month + "-" + appointemnt.Year;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.AppointmentsCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.AppointmentsTotal;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.InvoiceCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.InvoiceTotal;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.ConfirmedAppointmentsCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.ConfirmedAppointmentsTotal;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.UnconfirmedAppointmentsCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.UnconfirmedAppointmentsTotal;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.CanceledAppointmentsCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.CanceledAppointmentsTotal;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.PaidInvoiceCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.PaidInvoiceTotal;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.CanceledInvoiceCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.CanceledInvoiceTotal;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.UnpaidInvoicesCount;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.UnpaidInvoicesTotal;
                columnindex = 1;

                rowindex++;
            }
            workSheet.Cells.AutoFitColumns();
            workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            workSheet.View.FreezePanes(1, 1);
            var memoryStream = new MemoryStream();
            excel.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0;

            return memoryStream;
        }
        public MemoryStream DoctorsHasAppointmentsSheet(AppointmentReportSearch search, string sheetname)
        {
            var result = reportService.DoctorHasAppointments(search);
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add(sheetname);
            workSheet.Cells[1, 1].Value = "كود";
            workSheet.Cells[1, 2].Value = "الحالة";
            workSheet.Cells[1, 3].Value = "الأسم";
            workSheet.Cells[1, 4].Value = "التخصص";
            workSheet.Cells[1, 5].Value = "المحافظة";
            workSheet.Cells[1, 6].Value = "المنطقة";
            workSheet.Cells[1, 7].Value = "العنوان";
            workSheet.Cells[1, 8].Value = "الإجمالى";
            workSheet.Row(1).Height = 20;
            int columnindex = 1;
            int rowindex = 2;
            foreach (var appointemnt in result)
            {
                workSheet.Row(rowindex).Height = 20;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.Code;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.Status;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.Name;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.Speciality;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.CityName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.RegionName;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.MainAddress;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = appointemnt.NumberOfAppointmnts;
                columnindex = 1;
                rowindex++;
            }
            workSheet.Cells.AutoFitColumns();
            workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            workSheet.View.FreezePanes(1, 1);
            var memoryStream = new MemoryStream();
            excel.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0;

            return memoryStream;
        }
        public MemoryStream AppointmentMedicalProviderSheet(AppointmentReportSearch search, string sheetname)
        {
            var medicalproviders = new Dictionary<int, string>()
        {
            {(int)Common.MedicalProviderType.doctor,"دكتور"},
            {(int)Common.MedicalProviderType.hospital,"مستشفى"},
            {(int)Common.MedicalProviderType.lab,"معمل"},
            {(int)Common.MedicalProviderType.intencivecare,"رعاية مركزة"},
            {(int)Common.MedicalProviderType.medicalcenter,"مركز طبى"},
            {(int)Common.MedicalProviderType.radiologycenter,"مركز أشعة"},
            {(int)Common.MedicalProviderType.pharmacy,"صيدلية"},
            //{(int)Common.MedicalProviderType.bloodbank,"بنك الدم"},
            {(int)Common.MedicalProviderType.newborncare,"حضانة"},
            {(int)Common.MedicalProviderType.homevisit,"زيارات منزلية"},
            {(int)Common.MedicalProviderType.offer,"عروض"},
        };

            int rowindex = 3;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetname));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var mainheader = worksheet.Cells["1:1"].ToList();
                    mainheader.RemoveAll(c => c.Value == null);
                    var result = reportService.AppointmentsMedicalProvider(search);
                    foreach (var app in result)
                    {
                        int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                        int column_name_city = mainheader.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                        int column_name_region = mainheader.First(c => c.Value.ToString() == "المنطقة").Start.Column;
                        worksheet.Cells[rowindex, column_name_month].Value = app.CreatedDate;
                        worksheet.Cells[rowindex, column_name_city].Value = app.CityName;
                        worksheet.Cells[rowindex, column_name_region].Value = app.RegionName;
                        foreach (var provider in medicalproviders)
                        {
                            var providerstatistic = app.AppointmentMedicalProviders.Where(p =>
                                                    p.ProviderTypeId == provider.Key).SingleOrDefault();
                            if (providerstatistic == null)
                            {
                                continue;
                            }
                            int column_name = mainheader.First(c => c.Value.ToString() == provider.Value).Start.Column;
                            int column_confirm_total_count = worksheet.Cells[2, column_name].First(c => c.Value.ToString() == "مؤكد").Start.Column;
                            int column_confirm_total = worksheet.Cells[2, column_name + 1].First(c => c.Value.ToString() == "القيمة").Start.Column;
                            int column_unconfirm_total_count = worksheet.Cells[2, column_name + 2].First(c => c.Value.ToString() == "غير مؤكد").Start.Column;
                            int column_unconfirm_total = worksheet.Cells[2, column_name + 3].First(c => c.Value.ToString() == "القيمة").Start.Column;
                            int column_cancel_total_count = worksheet.Cells[2, column_name + 4].First(c => c.Value.ToString() == "ملغى").Start.Column;
                            int column_cancel_total = worksheet.Cells[2, column_name + 5].First(c => c.Value.ToString() == "القيمة").Start.Column;
                            int appointment_total_count = worksheet.Cells[2, column_name + 6].First(c => c.Value.ToString() == "عدد الحجوزات").Start.Column;
                            int appointment_total = worksheet.Cells[2, column_name + 7].First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                            worksheet.Cells[rowindex, column_confirm_total_count].Value = providerstatistic.ConfirmedAppointmentsCount;
                            worksheet.Cells[rowindex, column_confirm_total].Value = providerstatistic.ConfirmedAppointmentsTotal;
                            worksheet.Cells[rowindex, column_unconfirm_total_count].Value = providerstatistic.UnconfirmedAppointmenstCount;
                            worksheet.Cells[rowindex, column_unconfirm_total].Value = providerstatistic.UnconfirmedAppointmentsTotal;
                            worksheet.Cells[rowindex, column_cancel_total_count].Value = providerstatistic.CancelAppointmentsCount;
                            worksheet.Cells[rowindex, column_cancel_total].Value = providerstatistic.CancelAppointmentsTotal;
                            worksheet.Cells[rowindex, appointment_total_count].Value = providerstatistic.AppointmentsCount;
                            worksheet.Cells[rowindex, appointment_total].Value = providerstatistic.AppointmentsTotal;
                        }
                        int column_name_total = mainheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_invoice_confirm_total_count = worksheet.Cells[2, column_name_total].First(c => c.Value.ToString() == "مؤكد").Start.Column;
                        int column_invoice_confirm_total = worksheet.Cells[2, column_name_total + 1].First(c => c.Value.ToString() == "القيمة").Start.Column;
                        int column_invoice_unconfirm_total_count = worksheet.Cells[2, column_name_total + 2].First(c => c.Value.ToString() == "غير مؤكد").Start.Column;
                        int column_invoice_unconfirm_total = worksheet.Cells[2, column_name_total + 3].First(c => c.Value.ToString() == "القيمة").Start.Column;
                        int column_invoice_cancel_total_count = worksheet.Cells[2, column_name_total + 4].First(c => c.Value.ToString() == "ملغى").Start.Column;
                        int column_invoice_cancel_total = worksheet.Cells[2, column_name_total + 5].First(c => c.Value.ToString() == "القيمة").Start.Column;
                        int column_invoice__total_count = worksheet.Cells[2, column_name_total + 6].First(c => c.Value.ToString() == "عدد الحجوزات").Start.Column;
                        int column_invoice_total = worksheet.Cells[2, column_name_total + 7].First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                        worksheet.Cells[rowindex, column_invoice_confirm_total_count].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"مؤكد\")*D" + rowindex + ":CM" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_confirm_total].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"القيمة\")*D" + rowindex + ":CM" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_unconfirm_total_count].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"غير مؤكد\")*D" + rowindex + ":CM" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_unconfirm_total].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"القيمة\")*D" + rowindex + ":CM" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_cancel_total_count].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"ملغى\")*D" + rowindex + ":CM" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_cancel_total].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"القيمة\")*D" + rowindex + ":CM" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice__total_count].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"عدد الحجوزات\")*D" + rowindex + ":CM" + rowindex + ")";
                        worksheet.Cells[rowindex, column_invoice_total].Formula = "=SUMPRODUCT(($D$2:$CM$2=\"الإجمالى\")*D" + rowindex + ":CM" + rowindex + ")";

                        rowindex++;
                    }

                    worksheet.Cells.AutoFitColumns();
                }
            }
            var memoryStream = new MemoryStream();
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0;

            return memoryStream;

        }
        public MemoryStream InvoiceMedicalProvidersSheet(AppointmentReportSearch search, string sheetname)
        {
            var medicalproviders = new Dictionary<int, string>()
        {
            {(int)Common.MedicalProviderType.doctor,"دكتور"},
            {(int)Common.MedicalProviderType.hospital,"مستشفى"},
            {(int)Common.MedicalProviderType.lab,"معمل"},
            {(int)Common.MedicalProviderType.intencivecare,"رعاية مركزة"},
            {(int)Common.MedicalProviderType.medicalcenter,"مركز طبى"},
            {(int)Common.MedicalProviderType.radiologycenter,"مركز أشعة"},
            {(int)Common.MedicalProviderType.pharmacy,"صيدلية"},
            //{(int)Common.MedicalProviderType.bloodbank,"بنك الدم"},
            {(int)Common.MedicalProviderType.newborncare,"حضانة"},
            {(int)Common.MedicalProviderType.homevisit,"زيارات منزلية"},
            {(int)Common.MedicalProviderType.offer,"عروض"},
        };
            int row_subheader1_index = 2;
            int row_subheader2_index = 3;
            int rowvalue_index = 4;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetname));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var mainheader = worksheet.Cells["1:1"].ToList();
                    mainheader.RemoveAll(c => c.Value == null);
                    var result = reportService.InvoicesMedicalProvider(search);
                    foreach (var app in result)
                    {
                        int column_name_month = mainheader.First(c => c.Value.ToString() == "الشهر").Start.Column;
                        int column_name_city = mainheader.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                        int column_name_region = mainheader.First(c => c.Value.ToString() == "المنطقة").Start.Column;

                        worksheet.Cells[rowvalue_index, column_name_month].Value = app.InvoiceDate;
                        worksheet.Cells[rowvalue_index, column_name_city].Value = app.CityName;
                        worksheet.Cells[rowvalue_index, column_name_region].Value = app.RegionName;
                        foreach (var provider in medicalproviders)
                        {
                            var providerstatistic = app.InvoiceMedicalProviders.Where(p =>
                                                    p.ProviderTypeId == provider.Key).SingleOrDefault();
                            if (providerstatistic == null)
                            {
                                continue;
                            }
                            int column_providername = mainheader.First(c => c.Value.ToString() == provider.Value).Start.Column;
                            int column_confirm = worksheet.Cells[row_subheader1_index, column_providername]
                                .First(c => c.Value.ToString() == "محصل").Start.Column;
                            int column_unconfirm = worksheet.Cells[row_subheader1_index, column_providername + 2]
                                .First(c => c.Value.ToString() == "غير محصل").Start.Column;
                            int column_cancel = worksheet.Cells[row_subheader1_index, column_providername + 4]
                                .First(c => c.Value.ToString() == "ملغى").Start.Column;
                            int column_total = worksheet.Cells[row_subheader1_index, column_providername + 6]
                                .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                            int column_confirm_count = worksheet.Cells[row_subheader2_index, column_confirm]
                                .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            int column_confirm_total = worksheet.Cells[row_subheader2_index, column_confirm + 1]
                                .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                            int column_unconfirm_count = worksheet.Cells[row_subheader2_index, column_unconfirm]
                                .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            int column_unconfirm_total = worksheet.Cells[row_subheader2_index, column_unconfirm + 1]
                                .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                            int column_cancel_count = worksheet.Cells[row_subheader2_index, column_cancel]
                                .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            int column_cancel_total = worksheet.Cells[row_subheader2_index, column_cancel + 1]
                                .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                            int column_total_count = worksheet.Cells[row_subheader2_index, column_total]
                                .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                            //int column_total_total_total = worksheet.Cells[row_subheader2_index, column_total + 1]
                            //    .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                            worksheet.Cells[rowvalue_index, column_confirm_count].Value
                                = providerstatistic.PaidInvoicesCount;
                            worksheet.Cells[rowvalue_index, column_confirm_total].Value
                                = providerstatistic.PaidInvoicesTotal;
                            worksheet.Cells[rowvalue_index, column_unconfirm_count].Value
                                = providerstatistic.UnpaidInvoicesCount;
                            worksheet.Cells[rowvalue_index, column_unconfirm_total].Value
                                = providerstatistic.UnpaidInvoicesTotal;
                            worksheet.Cells[rowvalue_index, column_cancel_count].Value
                                = providerstatistic.CancelInvoicesCount;
                            worksheet.Cells[rowvalue_index, column_cancel_total].Value
                                = providerstatistic.CancelInvoicesTotal;
                            worksheet.Cells[rowvalue_index, column_total_count].Value
                                = providerstatistic.InvoicesCount;
                            //worksheet.Cells[rowvalue_index, column_total_total].Value
                            //    = providerstatistic.InvoicesTotal;

                        }
                        int column_name_total = mainheader.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        int column_total_confirm = worksheet.Cells[row_subheader1_index, column_name_total]
                            .First(c => c.Value.ToString() == "محصل").Start.Column;
                        int column_total_unconfirm = worksheet.Cells[row_subheader1_index, column_name_total + 2]
                            .First(c => c.Value.ToString() == "غير محصل").Start.Column;
                        int column_total_cancel = worksheet.Cells[row_subheader1_index, column_name_total + 4]
                            .First(c => c.Value.ToString() == "ملغى").Start.Column;
                        int column_total_total = worksheet.Cells[row_subheader1_index, column_name_total + 6]
                            .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                        int column_total_confirm_count = worksheet.Cells[row_subheader2_index, column_total_confirm]
                               .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                        int column_total_confirm_total = worksheet.Cells[row_subheader2_index, column_total_confirm + 1]
                            .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                        int column_total_unconfirm_count = worksheet.Cells[row_subheader2_index, column_total_unconfirm]
                            .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                        int column_total_unconfirm_total = worksheet.Cells[row_subheader2_index, column_total_unconfirm + 1]
                            .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                        int column_total_cancel_count = worksheet.Cells[row_subheader2_index, column_total_cancel]
                            .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                        int column_total_cancel_total = worksheet.Cells[row_subheader2_index, column_total_cancel + 1]
                            .First(c => c.Value.ToString() == "الإجمالى").Start.Column;

                        int column_total_total_count = worksheet.Cells[row_subheader2_index, column_total_total]
                            .First(c => c.Value.ToString() == "عدد الفواتير").Start.Column;
                        int column_total_total_total = worksheet.Cells[row_subheader2_index, column_total_total + 1]
                            .First(c => c.Value.ToString() == "الإجمالى").Start.Column;



                        worksheet.Cells[rowvalue_index, column_total_confirm_count].Formula
                            = "=SUMPRODUCT(($D$2:$CM$2=\"محصل\")*($D$3:$CM$3=\"عدد الفواتير\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";
                        worksheet.Cells[rowvalue_index, column_total_confirm_count].Formula
                            = "=SUMPRODUCT(($D$2:$CM$2=\"محصل\")*($D$3:$CM$3=\"الإجمالى\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";


                        worksheet.Cells[rowvalue_index, column_total_unconfirm_count].Formula
                            = "=SUMPRODUCT(($D$2:$CM$2=\"غير محصل\")*($D$3:$CM$3=\"عدد الفواتير\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";
                        worksheet.Cells[rowvalue_index, column_total_unconfirm_count].Formula
                            = "=SUMPRODUCT(($D$2:$CM$2=\"غير محصل\")*($D$3:$CM$3=\"الإجمالى\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";


                        worksheet.Cells[rowvalue_index, column_total_cancel_count].Formula
                            = "=SUMPRODUCT(($D$2:$CM$2=\"ملغى\")*($D$3:$CM$3=\"عدد الفواتير\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";
                        worksheet.Cells[rowvalue_index, column_total_cancel_count].Formula
                           = "=SUMPRODUCT(($D$2:$CM$2=\"ملغى\")*($D$3:$CM$3=\"الإجمالى\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";


                        worksheet.Cells[rowvalue_index, column_total_total_total].Formula
                            = "=SUMPRODUCT(($D$2:$CM$2=\"الإجمالى\")*($D$3:$CM$3=\"عدد الفواتير\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";
                        worksheet.Cells[rowvalue_index, column_total_total_total].Formula
                           = "=SUMPRODUCT(($D$2:$CM$2=\"الإجمالى\")*($D$3:$CM$3=\"الإجمالى\")*D" + rowvalue_index + ":CM" + rowvalue_index + ")";

                        rowvalue_index++;
                    }

                    worksheet.Cells.AutoFitColumns();
                }
            }
            var memoryStream = new MemoryStream();
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;

        }
        public MemoryStream SummarizeInvoiceProvider(AppointmentReportSearch search, string sheetname)
        {
            var memoryStream = new MemoryStream();
            int rowindex_value = 5;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetname));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var header1 = worksheet.Cells["1:1"].ToList();
                    header1.RemoveAll(c => c.Value == null);
                    var invoices = reportService.InvoiceProviderSummarize(search);
                    foreach (var invoice in invoices)
                    {
                        int column_name_month = header1.First(c => c.Value.ToString() == "الشهر").Start.Column;
                        int column_name_city = header1.First(c => c.Value.ToString() == "المحافظة").Start.Column;
                        int column_name_region = header1.First(c => c.Value.ToString() == "المنطقة").Start.Column;
                        worksheet.Cells[rowindex_value, column_name_month].Value = invoice.InvoiceDate;
                        worksheet.Cells[rowindex_value, column_name_city].Value = invoice.CityName;
                        worksheet.Cells[rowindex_value, column_name_region].Value = invoice.RegionName;
                        foreach (var provider in Common.MedicalProviders())
                        {
                            var providerstatistic = invoice.summarizeInvoiceProviders.Where(p =>
                                                   provider.Key == p.ProviderTypeId).SingleOrDefault();
                            if (providerstatistic == null)
                            {
                                continue;
                            }
                            //main header
                            int column_name = header1.First(c => c.Value.ToString() == provider.Value).Start.Column;
                            //subheader1
                            int column_name_subscription = worksheet.Cells[2, column_name]
                                .Where(c => c.Value.ToString() == "إشتراك").FirstOrDefault().Start.Column;
                            int column_name_administrative = worksheet.Cells[2, column_name + 6]
                               .Where(c => c.Value.ToString() == "م.إدارية").FirstOrDefault().Start.Column;
                            int column_name_reservation = worksheet.Cells[2, column_name + 12]
                               .Where(c => c.Value.ToString() == "حجوزات").FirstOrDefault().Start.Column;
                            int column_name_subscription_paid = worksheet.Cells[3, column_name_subscription]
                               .Where(c => c.Value.ToString() == "محصل").FirstOrDefault().Start.Column;
                            int column_name_subscription_unpaid = worksheet.Cells[3, column_name_subscription + 2]
                              .Where(c => c.Value.ToString() == "غير محصل").FirstOrDefault().Start.Column;
                            int column_name_subscription_cancel = worksheet.Cells[3, column_name_subscription + 4]
                              .Where(c => c.Value.ToString() == "ملغى").FirstOrDefault().Start.Column;

                            int column_name_subscription_paid_count = worksheet.Cells[4, column_name_subscription_paid]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_subscription_paid_total = worksheet.Cells[4, column_name_subscription_paid + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;
                            int column_name_subscription_unpaid_count = worksheet.Cells[4, column_name_subscription_unpaid]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_subscription_unpaid_total = worksheet.Cells[4, column_name_subscription_unpaid + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;
                            int column_name_subscription_cancel_count = worksheet.Cells[4, column_name_subscription_cancel]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_subscription_cancel_total = worksheet.Cells[4, column_name_subscription_cancel + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;

                            worksheet.Cells[rowindex_value, column_name_subscription_paid_count].Value = providerstatistic.NumberOfPaidSubscription;
                            worksheet.Cells[rowindex_value, column_name_subscription_paid_total].Value = providerstatistic.SubscriptionPaidTotal;
                            worksheet.Cells[rowindex_value, column_name_subscription_unpaid_count].Value = providerstatistic.NumberOfUnpaidSubscription;
                            worksheet.Cells[rowindex_value, column_name_subscription_unpaid_total].Value = providerstatistic.UnpaidSubscriptionTotal;
                            worksheet.Cells[rowindex_value, column_name_subscription_cancel_count].Value = providerstatistic.NumberOfCancelSubscription;
                            worksheet.Cells[rowindex_value, column_name_subscription_cancel_total].Value = providerstatistic.CancelSubscriptionTotal;

                            int column_name_administrative_paid = worksheet.Cells[3, column_name_administrative]
                            .Where(c => c.Value.ToString() == "محصل").FirstOrDefault().Start.Column;
                            int column_name_administrative_unpaid = worksheet.Cells[3, column_name_administrative + 2]
                              .Where(c => c.Value.ToString() == "غير محصل").FirstOrDefault().Start.Column;
                            int column_name_administrative_cancel = worksheet.Cells[3, column_name_administrative + 4]
                              .Where(c => c.Value.ToString() == "ملغى").FirstOrDefault().Start.Column;

                            int column_name_administrative_paid_count = worksheet.Cells[4, column_name_administrative_paid]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_administrative_paid_total = worksheet.Cells[4, column_name_administrative_paid + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;
                            int column_name_administrative_unpaid_count = worksheet.Cells[4, column_name_administrative_unpaid]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_administrative_unpaid_total = worksheet.Cells[4, column_name_administrative_unpaid + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;
                            int column_name_administrative_cancel_count = worksheet.Cells[4, column_name_administrative_cancel]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_administrative_cancel_total = worksheet.Cells[4, column_name_administrative_cancel + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;

                            worksheet.Cells[rowindex_value, column_name_administrative_paid_count].Value = providerstatistic.NumberOfPaidAdministrative;
                            worksheet.Cells[rowindex_value, column_name_administrative_paid_total].Value = providerstatistic.AdministrativePaidTotal;
                            worksheet.Cells[rowindex_value, column_name_administrative_unpaid_count].Value = providerstatistic.NumberOfUnpaidAdministrative;
                            worksheet.Cells[rowindex_value, column_name_administrative_unpaid_total].Value = providerstatistic.UnpaidAdministrativeTotal;
                            worksheet.Cells[rowindex_value, column_name_administrative_cancel_count].Value = providerstatistic.NumberOfCancelAdministrative;
                            worksheet.Cells[rowindex_value, column_name_administrative_cancel_total].Value = providerstatistic.CancelAdministrativeTotal;

                            int column_name_reservation_paid = worksheet.Cells[3, column_name_reservation]
                            .Where(c => c.Value.ToString() == "محصل").FirstOrDefault().Start.Column;
                            int column_name_reservation_unpaid = worksheet.Cells[3, column_name_reservation + 2]
                              .Where(c => c.Value.ToString() == "غير محصل").FirstOrDefault().Start.Column;
                            int column_name_reservation_cancel = worksheet.Cells[3, column_name_reservation + 4]
                              .Where(c => c.Value.ToString() == "ملغى").FirstOrDefault().Start.Column;

                            int column_name_reservation_paid_count = worksheet.Cells[4, column_name_reservation_paid]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_reservation_paid_total = worksheet.Cells[4, column_name_reservation_paid + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;
                            int column_name_reservation_unpaid_count = worksheet.Cells[4, column_name_reservation_unpaid]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_reservation_unpaid_total = worksheet.Cells[4, column_name_reservation_unpaid + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;
                            int column_name_reservation_cancel_count = worksheet.Cells[4, column_name_reservation_cancel]
                              .Where(c => c.Value.ToString() == "عدد").FirstOrDefault().Start.Column;
                            int column_name_reservation_cancel_total = worksheet.Cells[4, column_name_reservation_cancel + 1]
                              .Where(c => c.Value.ToString() == "قيمة").FirstOrDefault().Start.Column;

                            worksheet.Cells[rowindex_value, column_name_reservation_paid_count].Value = providerstatistic.NumberOfPaidReservation;
                            worksheet.Cells[rowindex_value, column_name_reservation_paid_total].Value = providerstatistic.ReservationPaidTotal;
                            worksheet.Cells[rowindex_value, column_name_reservation_unpaid_count].Value = providerstatistic.NumberOfUnpaidReservation;
                            worksheet.Cells[rowindex_value, column_name_reservation_unpaid_total].Value = providerstatistic.UnpaidReservationTotal;
                            worksheet.Cells[rowindex_value, column_name_reservation_cancel_count].Value = providerstatistic.NumberOfCancelReservation;
                            worksheet.Cells[rowindex_value, column_name_reservation_cancel_total].Value = providerstatistic.CancelReservationTotal;


                        }

                        rowindex_value++;
                    }
                    worksheet.Cells.AutoFitColumns();
                }
            }

            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
        public MemoryStream SummarizePaidInvoices(PaidInvoiceSummarizeSearch search, string sheetpath)
        {
            var memoryStream = new MemoryStream();
            int rowindex = 3;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetpath));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var header1 = worksheet.Cells["1:1"].ToList();
                    header1.RemoveAll(c => c.Value == null);
                    var result = search.Unpaid.HasValue
                        ? reportService.CreateUnPaidInvoicesSummarizeReport(search)
                        : reportService.CreatePaidInvoicesSummarizeReport(search);
                    foreach (var invoice in result)
                    {
                        int column_name_date = header1.First(c => c.Value.ToString() == "التاريخ").Start.Column;
                        int column_name_agent = header1.First(c => c.Value.ToString() == "المندوب").Start.Column;
                        int column_name_subscription = header1.First(c => c.Value.ToString() == "الأشتراكات").Start.Column;
                        int column_name_admistrative = header1.First(c => c.Value.ToString() == "م.إدارية").Start.Column;
                        int column_name_reservation = header1.First(c => c.Value.ToString() == "حجوزات").Start.Column;
                        int column_name_onlinesubscription = header1.First(c => c.Value.ToString() == "زيارات منزلية")
                            .Start.Column;
                        int column_name_homevisit = header1.First(c => c.Value.ToString() == "أونلاين").Start.Column;
                        int column_name_total = header1.First(c => c.Value.ToString() == "الإجمالى").Start.Column;
                        worksheet.Cells[rowindex, column_name_date].Value = new DateTime(invoice.Year, invoice.Month, invoice.Day).Date;
                        worksheet.Cells[rowindex, column_name_agent].Value = invoice.AgentName;
                        worksheet.Cells[rowindex, column_name_subscription].Value = invoice.SubscriptionTotal;
                        worksheet.Cells[rowindex, column_name_subscription + 1].Value = invoice.SubscriptionCount;
                        worksheet.Cells[rowindex, column_name_admistrative].Value = invoice.AdmistrativeTotal;
                        worksheet.Cells[rowindex, column_name_admistrative + 1].Value = invoice.AdmistrativeCount;
                        worksheet.Cells[rowindex, column_name_reservation].Value = invoice.ReservationTotal;
                        worksheet.Cells[rowindex, column_name_reservation + 1].Value = invoice.ReservationCount;
                        worksheet.Cells[rowindex, column_name_onlinesubscription].Value = invoice.OnlineSubscriptionTotal;
                        worksheet.Cells[rowindex, column_name_onlinesubscription + 1].Value = invoice.OnlineSubscriptionCount;
                        worksheet.Cells[rowindex, column_name_homevisit].Value = invoice.HomevisitTotal;
                        worksheet.Cells[rowindex, column_name_homevisit + 1].Value = invoice.HomevisitCount;
                        worksheet.Cells[rowindex, column_name_total].Value = invoice.PaidTotal;
                        rowindex++;
                    }
                    worksheet.Cells.AutoFitColumns();
                }
            }
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0;

            return memoryStream;
        }
        public MemoryStream ProviderStatement(string sheetpath)
        {
            var memoryStream = new MemoryStream();
            int rowindex = 3;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage(new FileInfo(sheetpath));
            var workbook = package.Workbook;
            if (workbook != null)
            {
                var worksheet = workbook.Worksheets["Sheet1"];
                if (worksheet != null)
                {
                    var header1 = worksheet.Cells["1:1"].ToList();
                    var subheader = worksheet.Cells["2:2"].ToList();
                    header1.RemoveAll(c => c.Value == null);
                    subheader.RemoveAll(c => c.Value == null);

                    var context = new Re3aya247Entities();
                    context.Database.CommandTimeout = 8000;
                    var result = context.Invoices.Where(i =>
                         (i.DoctorId.HasValue)
                      && (!i.IsPaid.HasValue || !i.IsPaid.Value)
                      && (!i.IsCanceled.HasValue || !i.IsCanceled.Value)
                    ).GroupBy(i => new
                    {
                        name = i.Doctor.Doctor_Name_AR
                    ,
                        code = i.Doctor.ContractID
                    ,
                        phone1 = i.Doctor.PhoneNumber1_Master
                    ,
                        phone2 = i.Doctor.PhoneNumber2
                    ,
                        phone3 = i.Doctor.PhoneNumber3
                    ,
                        status = i.Doctor.Is_Active
                    ,
                        speciality = i.Doctor.Speciality.SpecialityName_AR
                    ,
                        region = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR
                    ,
                        city = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Governrate.Gov_Name_AR

                    ,
                        id = i.Doctor.DoctorID
                    }).Select(d => new
                    {
                        name = d.Key.name,
                        code = d.Key.code,
                        phone1 = d.Key.phone1,
                        phone2 = d.Key.phone2,
                        phone3 = d.Key.phone3,
                        speciality = d.Key.speciality,
                        region = d.Key.region,
                        city=d.Key.city,
                        id=d.Key.id,
                        statusname = d.Key.status.Value ? "Active" : "Inactive",
                        invoicelist = d.GroupBy(i => i.InvoiceStartDate.Value.Year).Select(x => new
                        {
                            year = x.Key,
                            subscriptioninvoice = x.Where(n => n.TypeId == (int)Common.InvoiceType.SubscriptionInvoice)
                              .Select(s => s.TotalAmount).Sum(),
                            reservationinvoice = x.Where(n => n.TypeId == (int)Common.InvoiceType.AppointmentInvoice)
                              .Select(s => s.TotalAmount).Sum(),
                        }).ToList()
                    }).OrderBy(d=>d.code).ToList();
                    foreach (var doctor in result)
                    {
                        int column_name_name = header1.First(c => c.Value.ToString() == "Name").Start.Column;
                        int column_name_code = header1.First(c => c.Value.ToString() == "Code").Start.Column;
                        int column_name_phone1 = header1.First(c => c.Value.ToString() == "Phone1").Start.Column;
                        int column_name_phone2 = header1.First(c => c.Value.ToString() == "Phone2").Start.Column;
                        int column_name_phone3 = header1.First(c => c.Value.ToString() == "Phone3").Start.Column;
                        int column_name_status = header1.First(c => c.Value.ToString() == "Status").Start.Column;
                        int column_name_speciality = header1.First(c => c.Value.ToString() == "Speciality").Start.Column;
                        int column_name_region = header1.First(c => c.Value.ToString() == "Region").Start.Column;
                        int column_name_city = header1.First(c => c.Value.ToString() == "City").Start.Column;

                        int column_name_2020 = header1.First(c => c.Value.ToString() == "2020").Start.Column;
                        int column_name_2021 = header1.First(c => c.Value.ToString() == "2021").Start.Column;
                        int column_2020_Subscription = worksheet.Cells[2, column_name_2020]
                            .First(c => c.Value.ToString() == "Subscription").Start.Column;
                        int column_2020_Reservation = worksheet.Cells[2, column_name_2020 + 1]
                            .First(c => c.Value.ToString() == "Reservation").Start.Column;
                        int column_2020_appscount = worksheet.Cells[2, column_name_2020 + 2]
                            .First(c => c.Value.ToString() == "Number Of Appointments").Start.Column;
                        int column_2021_Subscription = worksheet.Cells[2, column_name_2021]
                            .First(c => c.Value.ToString() == "Subscription").Start.Column;
                        int column_2021_Reservation = worksheet.Cells[2, column_name_2021 + 1]
                            .First(c => c.Value.ToString() == "Reservation").Start.Column;
                        int column_2021_appscount = worksheet.Cells[2, column_name_2021 + 2]
                            .First(c => c.Value.ToString() == "Number Of Appointments").Start.Column;
                        worksheet.Cells[rowindex, column_name_name].Value = doctor.name;
                        worksheet.Cells[rowindex, column_name_code].Value = doctor.code;
                        worksheet.Cells[rowindex, column_name_phone1].Value = doctor.phone1;
                        worksheet.Cells[rowindex, column_name_phone2].Value = doctor.phone2;
                        worksheet.Cells[rowindex, column_name_phone3].Value = doctor.phone3;
                        worksheet.Cells[rowindex, column_name_status].Value = doctor.statusname;
                        worksheet.Cells[rowindex, column_name_speciality].Value = doctor.speciality;
                        worksheet.Cells[rowindex, column_name_region].Value = doctor.region;
                        worksheet.Cells[rowindex, column_name_city].Value = doctor.city;

                        foreach (var invoice in doctor.invoicelist)
                        {
                            if (invoice.year == 2020)
                            {
                                worksheet.Cells[rowindex, column_2020_Subscription].Value = invoice.subscriptioninvoice;
                                worksheet.Cells[rowindex, column_2020_Reservation].Value = invoice.reservationinvoice;
                                if(invoice.reservationinvoice.HasValue && invoice.reservationinvoice.Value != 0)
                                {
                                    var appscount = context.Appointments.Where(ap => (ap.Address_Book.Doctor_ID == doctor.id)
                                 && (ap.BookingStatus != 5)
                                 && (ap.Appointment_Date.Value.Year == 2020)).Count();
                                    worksheet.Cells[rowindex, column_2020_appscount].Value = appscount;
                                }
                            }
                            else if (invoice.year == 2021)
                            {
                                worksheet.Cells[rowindex, column_2021_Subscription].Value = invoice.subscriptioninvoice;
                                worksheet.Cells[rowindex, column_2021_Reservation].Value = invoice.reservationinvoice;
                                if (invoice.reservationinvoice.HasValue && invoice.reservationinvoice.Value != 0)
                                {
                                    DateTime comparedate = new DateTime(2021, 8, 31).Date;
                                    var appscount = context.Appointments.Where(ap => (ap.Address_Book.Doctor_ID == doctor.id)
                                && (ap.BookingStatus != 5)
                                && (DbFunctions.TruncateTime(ap.Appointment_Date)<= comparedate)).Count();
                                    worksheet.Cells[rowindex, column_2021_appscount].Value = appscount;
                                }
                            }
                        }
                        rowindex++;
                    }
                }
            }
            package.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0;

            return memoryStream;
        }
        public MemoryStream InvoiceLogSheet(InvoiceLogSearch search)
        {
            var result = logService.InvoiceLogQuery(search);
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("userlog");
            workSheet.Cells[1, 1].Value = "التاريخ";
            workSheet.Cells[1, 2].Value = "الوقت";
            workSheet.Cells[1, 3].Value = "المستخدم";
            workSheet.Cells[1, 4].Value = "Action";
            workSheet.Row(1).Height = 20;
            int columnindex = 1;
            int rowindex = 2;
            foreach (var log in result)
            {
                workSheet.Row(rowindex).Height = 20;
                workSheet.Cells[rowindex, columnindex].Value = log.CreatedDate.Value;
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = DateTime.Today.Add(log.CreatedDate.Value.TimeOfDay).AddHours(2).ToString("hh:mm tt");
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = log.UserName==""?"Customer Support": Common.Ase_DecryptString( log.UserName);
                columnindex++;
                workSheet.Cells[rowindex, columnindex].Value = log.ActionName;
                columnindex = 1;
                rowindex++;
            }
            workSheet.Cells.AutoFitColumns();
            workSheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            workSheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            workSheet.Column(1).Style.Numberformat.Format = "dd-mm-yyyy";
            workSheet.View.FreezePanes(2, 3);
            var memoryStream = new MemoryStream();
            excel.SaveAs(memoryStream);
            memoryStream.Flush();
            memoryStream.Position = 0; //Not sure if this is required

            return memoryStream;
        }
    }
}