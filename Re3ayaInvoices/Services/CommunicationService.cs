﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
using Re3ayaInvoices.Helpers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using System.Net.Mail;

namespace Re3ayaInvoices.Services
{
    public class CommunicationService
    {
        const string new_server_key = "AAAA8oeDEBM:APA91bFc_YjSPkCQ52T8TO7SSFiPgqOoeyGe037xZiXV03YkLNB1n6FtsUgvfRzSVXe4Q-ao4BzPeDCOVOsdrDXSkeJkhqmztUR8StkS_DWHM2vrYUjLZMTpt4kFQddCBDo-q4CPQlUi";
        const long new_sender_id = 1041655599123;
        private readonly Re3aya247Entities context;
        public CommunicationService()
        {
            context = new Re3aya247Entities();
        }
        public void SendWhatsapp(int? invoice_id)
        {
            var invoice = context.Invoices.Find(invoice_id);
            var message = "";
            switch (invoice.TypeId)
            {
                case (int)Common.InvoiceType.SubscriptionInvoice:
                    message = "عميلنا العزيز رعاية ٢٤٧ تبلغكم بانة تم سداد فاتورة الاشتراك السنوى لسنة " + invoice.InvoiceStartDate.Value.Year + " بقيمة " + invoice.TotalAmount.Value.ToString("0.00") + " للتفاصيل اضغط الرابط" + Environment.NewLine + "http://re3.io/BXIQ17";
                    break;
                case (int)Common.InvoiceType.AppointmentInvoice:
                    message = "عميلنا العزيز رعاية ٢٤٧ تبلغكم بانة تم سداد فاتورة الحجوزات لشهر " + invoice.InvoiceStartDate.Value.ToString("MMMM yyyy") + " بقيمة " + invoice.TotalAmount.Value.ToString("0.00") + " للتفاصيل اضغط الرابط" + Environment.NewLine + "http://re3.io/BXIQ17";
                    break;
                case (int)Common.InvoiceType.OnlineSubscriptionInvoice:
                    message = "عميلنا العزيز رعاية ٢٤٧ تبلغكم بانة تم سداد فاتورة الاشتراك الاونلاين لسنة " + invoice.InvoiceStartDate.Value.Year + " بقيمة " + invoice.TotalAmount.Value.ToString("0.00") + " للتفاصيل اضغط الرابط" + Environment.NewLine + "http://re3.io/BXIQ17";
                    break;
            }
            var phoneNumber = invoice.DoctorId.HasValue ? invoice.Doctor.PhoneNumber1_Master : invoice.Health_Entitiy.Health_Entitiy_MasterPhone;

            foreach (var item in context.DoctorDevices.Where(m => m.DoctorID == invoice.DoctorId).ToList())
            {
                //CommonServices.SendNotification(item.DeviceToken, "رعاية 247", message);
            }
            WhatsappSenderForInvoices("2" + phoneNumber, message);
        }
        public static String WhatsappSenderForInvoices(string Phone, string Body)
        {
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://re3.io/Home/WhatsappSender?key=MNO745$%_TUV__HHD&Phonenumber=" + Phone + "&Messagebody=" + Body + "&Isinvoice=true");
            myRequest.Method = "GET";
            WebResponse myResponse = myRequest.GetResponse();
            StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
            string result = sr.ReadToEnd();
            sr.Close();
            myResponse.Close();
            return result.Replace('"', ' ');
        }
        public static void SendNotification(string Devicetoken, string title, string body)
        {
            try
            {
                //Firebase 
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                //serverKey - Key from Firebase cloud messaging server  
                tRequest.Headers.Add(string.Format("Authorization: key={0}", new_server_key));
                //Sender Id - From firebase project setting  
                tRequest.Headers.Add(string.Format("Sender: id={0}", new_sender_id));
                tRequest.ContentType = "application/json";
                var payload = new
                {
                    to = Devicetoken,
                    priority = "high",
                    content_available = true,
                    notification = new
                    {
                        body = body,
                        title = title,
                        badge = 1

                    },
                };
                string postbody = JsonConvert.SerializeObject(payload).ToString();
                Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    //result.Response = sResponseFromServer;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

        }
        public void SendEmail(string msg, string subject, string senderemail, string senderpassword, string recipientemail)
        {
            try
            {
                var fromEmail = new MailAddress(senderemail, "Re3aya247");
                var fromEmailPassword = senderpassword;
                var toEmail = new MailAddress(recipientemail);
                string body = msg;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                };
                using (var message = new MailMessage(fromEmail, toEmail)
                {
                    Subject = subject,
                    Body = body,
                })
                    smtp.Send(message);
            }
            catch (Exception)
            {
            }
        }
    }
}