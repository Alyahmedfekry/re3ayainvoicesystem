﻿using System;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.CustomAuthentication;
using Re3ayaInvoices.Helpers;
namespace Re3ayaInvoices.Services
{
    public class AuthorizeService
    {
        private readonly Re3aya247Entities context;
        public AuthorizeService()
        {
            context = new Re3aya247Entities();
        }
        public bool HasAccessTo(string controllername, string actionname)
        {
            string ctrl = Common.Ase_EncryptString(controllername);
            string action= Common.Ase_EncryptString(actionname);
            var user = HttpContext.Current.User as CustomPrincipal;
            var hasaccess = context.UserActions.Any(u =>
               (u.isAllowed == true)
            && ((u.SystemController.ControllerName ==ctrl) || u.SystemAction.ActionName == action
                 && u.SystemAction.SystemController.ControllerName ==ctrl)
            && (user.Rolesid.Contains(u.RoleId)));

            return hasaccess;
        }
    }
}