﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.viewModels;
using PagedList;
namespace Re3ayaInvoices.Services
{
    public class SystemService : IDisposable
    {
        private readonly Re3aya247Entities context;
        public SystemService()
        {
            context = new Re3aya247Entities();
        }
        public IPagedList<SystemRoleViewModel> GetRoles(int? page, string name)
        {
            var result = context.SystemRoles.Where(r=>r.AppId==(int)Common.AppId).ToList()
                .Select(r => new SystemRoleViewModel
                {
                    Id = r.RoleID,
                    ArabicName = Common.Ase_DecryptString(r.RoleTitleArabic),
                    EnglishName = Common.Ase_DecryptString(r.RoleTitleEnglish),
                    RoleDesc = Common.Ase_DecryptString(r.RoleDesc),
                    IsActive = r.IsActive
                }).Where(r =>
            (string.IsNullOrEmpty(name) || r.ArabicName.Trim().Contains(name.Trim())))
            .OrderBy(r => r.Id).ToPagedList(page ?? 1, 20);

            return result;
        }
        public IPagedList<SystemUserViewModel> GetUsers(int? page, UserSearch search)
        {
            var result = context.Users.Where(u =>
                (u.AppId==(int)Common.AppId)
              &&(!search.RoleId.HasValue || u.UserRoles.Select(a => a.RoleId).Contains(search.RoleId))).ToList()
            .Select(u => new SystemUserViewModel
            {
                Id = u.Id,
                FullName = Common.Ase_DecryptString(u.FullName),
                RolesName = u.UserRoles.Select(user => user.SystemRole.RoleTitleArabic).ToList(),
                IsActive = u.IsActive,
            }).OrderBy(u => u.Id).ToPagedList(page ?? 1, 10);

            return result;
        }
        public SystemUserViewModel GetUserDetails(int id)
        {
            var user = context.Users.Find(id);
            var vm = new SystemUserViewModel
            {
                Email = Common.Ase_DecryptString(user.Email),
                FullName = Common.Ase_DecryptString(user.FullName),
                RolesName = user.UserRoles.Select(u => u.SystemRole.RoleTitleArabic).ToList()
            };
            return vm;
        }
        public SystemRoleViewModel GetRole(int id)
        {
            var role = context.SystemRoles.Find(id);
            var vm = new SystemRoleViewModel
            {
                ArabicName = Common.Ase_DecryptString(role.RoleTitleArabic),
                EnglishName = Common.Ase_DecryptString(role.RoleTitleEnglish),
                DashboardUrl = Common.Ase_DecryptString(role.RoleLandingPage),
                RoleDesc = Common.Ase_DecryptString(role.RoleDesc),
                Id = role.RoleID,
                IsMain = role.IsMain ?? false,
                ActionsId = role.UserActions.Where(u =>
                  (!u.ControllerId.HasValue)).Select(u => u.ActionId).ToList(),
                MenusId=role.UserMenus.Select(u=>u.MenuId).ToList()
            };

            return vm;
        }
        public SystemUserViewModel GetUser(int id)
        {
            var user = context.Users.Find(id);
            var vm = new SystemUserViewModel
            {
                Id = user.Id,
                Email = Common.Ase_DecryptString(user.Email),
                FullName = Common.Ase_DecryptString(user.FullName),
                Roles = user.UserRoles.Select(r => r.RoleId).ToList(),
                IsActive = user.IsActive
            };

            return vm;
        }
        public void CreateRole(SystemRoleViewModel vm)
        {
            var role = new SystemRole
            {
                RoleTitleArabic = Common.Ase_EncryptString(vm.ArabicName.Trim()),
                RoleTitleEnglish = Common.Ase_EncryptString(vm.EnglishName.Trim()),
                RoleDesc = Common.Ase_EncryptString(vm.RoleDesc.Trim()),
                RoleLandingPage = Common.Ase_EncryptString(vm.DashboardUrl.Trim()),
                IsMain = vm.IsMain,
                IsActive = false,
                AppId=Common.AppId
            };
            if (vm.ActionsId != null)
            {
                foreach (var id in vm.ActionsId)
                {
                    var useraction = new UserAction
                    {
                        ActionId = id,
                        isAllowed = true,
                    };
                    role.UserActions.Add(useraction);
                }
            }
            if (vm.MenusId != null)
            {
                foreach (var id in vm.MenusId)
                {
                    var usermenu = new UserMenu
                    {
                        MenuId = id,
                        IsAllow = true,
                    };
                    role.UserMenus.Add(usermenu);
                }
            }
            context.SystemRoles.Add(role);
            context.SaveChanges();
        }
        public void EditRole(SystemRoleViewModel vm)
        {
            var role = context.SystemRoles.Find(vm.Id);
            role.RoleTitleArabic = Common.Ase_EncryptString(vm.ArabicName.Trim());
            role.RoleTitleEnglish = Common.Ase_EncryptString(vm.EnglishName.Trim());
            role.RoleDesc = Common.Ase_EncryptString(vm.RoleDesc.Trim());
            //role.RoleLandingPage = Common.Ase_EncryptString(vm.DashboardUrl.Trim());
            role.IsMain = vm.IsMain ?? false;
            context.UserActions.RemoveRange(role.UserActions);
            context.UserMenus.RemoveRange(role.UserMenus);
            if (vm.ActionsId != null)
            {
                foreach (var id in vm.ActionsId)
                {
                    var useraction = new UserAction
                    {
                        ActionId = id,
                        isAllowed = true,
                    };
                    role.UserActions.Add(useraction);
                }
            }
            if (vm.MenusId != null)
            {
                foreach (var id in vm.MenusId)
                {
                    var usermenu = new UserMenu
                    {
                        MenuId = id,
                        IsAllow = true,
                    };
                    role.UserMenus.Add(usermenu);
                }
            }
            context.SaveChanges();
        }
        public void UpdateRoleStatus(int id, bool isactive)
        {
            var role = context.SystemRoles.Find(id);
            role.IsActive = isactive;

            context.SaveChanges();
        }
        public void CreateUser(SystemUserViewModel vm)
        {
            var user = new User
            {
                FullName = Common.Ase_EncryptString(vm.FullName.Trim()),
                Email = Common.Ase_EncryptString(vm.Email.Trim()),
                Password = Common.Ase_EncryptString(vm.Password),
                IsActive = false,
                AppId=(int)Common.AppId
            };
            if (vm.Roles != null)
            {
                foreach (var role in vm.Roles)
                {
                    var userrole = new UserRole
                    {
                        RoleId = role
                    };
                    user.UserRoles.Add(userrole);
                }
            }
            context.Users.Add(user);

            context.SaveChanges();
        }
        public void EditUser(SystemUserViewModel vm)
        {
            var user = context.Users.Find(vm.Id);
            user.FullName = Common.Ase_EncryptString(vm.FullName.Trim());
            user.Email = Common.Ase_EncryptString(vm.Email.Trim());
            context.UserRoles.RemoveRange(user.UserRoles);
            if (vm.Roles != null)
            {
                foreach (var role in vm.Roles)
                {
                    var userrole = new UserRole
                    {
                        RoleId = role
                    };
                    user.UserRoles.Add(userrole);
                }
            }
            context.SaveChanges();
        }
        public void UpdateUserStatus(int id, bool isactive)
        {
            var user = context.Users.Find(id);
            user.IsActive = isactive;

            context.SaveChanges();
        }
        public IPagedList<SystemControllerViewModel> GetSystemCtrls(int? page)
        {
            var result = context.SystemControllers.ToList()
          .Select(u => new SystemControllerViewModel
          {
              Id = u.ControllerID,
              VisualName = Common.Ase_DecryptString(u.VisualName),
          }).OrderBy(u => u.Id).ToPagedList(page ?? 1, 10);

            return result;
        }
        public IPagedList<SystemActionViewModel> GetSystemActions(int? page, int ctrlid)
        {
            var result = context.SystemActions.Where(s=>s.ControllerID==ctrlid).ToList()
          .Select(u => new SystemActionViewModel
          {
              Id = u.ActionID,
              ActionName = Common.Ase_DecryptString(u.ActionName),
          }).OrderBy(u => u.Id).ToPagedList(page ?? 1, 10);

            return result;
        }
        public SystemActionViewModel GetSystemActionById(int id)
        {
            var action = context.SystemActions.Find(id);
            var vm = new SystemActionViewModel
            {
                Id = action.ActionID,
                ActionName = Common.Ase_DecryptString(action.ActionName),
                ActionLink = Common.Ase_DecryptString(action.ActionLink),
                ControllerId = action.ControllerID
            };

            return vm;
        }
        public SystemControllerViewModel GetSystemControllerById(int id)
        {
            var ctrl = context.SystemControllers.Find(id);
            var vm = new SystemControllerViewModel
            {
                Id = ctrl.ControllerID,
                Name = Common.Ase_DecryptString(ctrl.ControllerName),
                VisualName = Common.Ase_DecryptString(ctrl.VisualName),
            };

            return vm;
        }
        public void CreateSystemController(SystemControllerViewModel vm)
        {
            var ctrl = new SystemController
            {
                ControllerName = Common.Ase_EncryptString(vm.Name.Trim()),
                VisualName = Common.Ase_EncryptString(vm.VisualName.Trim()),
                AppId = Common.AppId
            };
            context.SystemControllers.Add(ctrl);
            context.SaveChanges();
        }
        public void EditSystemController(SystemControllerViewModel vm)
        {
            var ctrl = context.SystemControllers.Find(vm.Id);
            ctrl.ControllerName = Common.Ase_EncryptString(vm.Name.Trim());
            ctrl.VisualName = Common.Ase_EncryptString(vm.VisualName.Trim());
            context.SaveChanges();
        }
        public void CreateSystemAction(SystemActionViewModel vm)
        {
            var action = new SystemAction
            {
                ActionName = Common.Ase_EncryptString(vm.ActionName.Trim()),
                ActionLink = Common.Ase_EncryptString(vm.ActionLink.Trim()),
                ControllerID = vm.ControllerId.Value
            };
            context.SystemActions.Add(action);
            context.SaveChanges();
        }
        public void EditSystemAction(SystemActionViewModel vm)
        {
            var action = context.SystemActions.Find(vm.Id);
            action.ActionName = Common.Ase_EncryptString(vm.ActionName.Trim());
            action.ActionLink = Common.Ase_EncryptString(vm.ActionLink.Trim());
            action.ControllerID = vm.ControllerId.Value;

            context.SaveChanges();
        }
        public IPagedList<SystemMenuViewModel> GetSystemMenus(int? page)
        {

            var result = context.SystemMenus.ToList()
          .Select(u => new SystemMenuViewModel
          {
              Id = u.Id,
              NameAr = Common.Ase_DecryptString(u.NameAr),
          }).OrderBy(u => u.Id).ToPagedList(page ?? 1, 10);

            return result;
        }
        public void CreateSystemMenu(SystemMenuViewModel vm)
        {
            var menu = new SystemMenu
            {
                NameAr = Common.Ase_EncryptString(vm.NameAr.Trim()),
                NameEn = Common.Ase_EncryptString(vm.NameEn.Trim()),
                ParentId = vm.ParentId,
                ActionId = vm.ActionId,
                Rank = vm.Rank
            };
            context.SystemMenus.Add(menu);
            context.SaveChanges();
        }
        public SystemMenuViewModel GetSystemMenu(int id)
        {
            var menu = context.SystemMenus.Find(id);
            var result = new SystemMenuViewModel
            {
                NameAr = Common.Ase_DecryptString(menu.NameAr),
                NameEn = Common.Ase_DecryptString(menu.NameEn),
                ParentId = menu.ParentId,
                ActionId = menu.ActionId,
                Rank = menu.Rank
            };
            return result;
        }
        public void EditSystemMenu(SystemMenuViewModel vm)
        {
            var menu = context.SystemMenus.Find(vm.Id);
            menu.NameAr = Common.Ase_EncryptString(vm.NameAr.Trim());
            menu.NameEn = Common.Ase_EncryptString(vm.NameEn.Trim());
            menu.ParentId = vm.ParentId;
            menu.ActionId = vm.ActionId;
            menu.Rank = vm.Rank;
            context.SaveChanges();
        }
        public User Login(string email, string password)
        {
            string username = Common.Ase_EncryptString(email);
            string userpassword = Common.Ase_EncryptString(password);
            var user = context.Users.Where(u =>
              (u.IsActive == true)
            && (u.Email == username)
            && (u.Password == userpassword)).Single();

            return user;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}