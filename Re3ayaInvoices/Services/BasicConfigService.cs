﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Re3ayaInvoices.viewModels;
using Re3ayaInvoices.Models;
using PagedList;
namespace Re3ayaInvoices.Services
{
    public class BasicConfigService:IDisposable
    {
        private readonly Re3aya247Entities context;
        public BasicConfigService()
        {
            context = new Re3aya247Entities();
        }
        public IPagedList<AgentViewModel> GetAgents(int?page,string name)
        {
            int pagesize = 10;
            var agents =context.Account_Agent.Where(a=>
            string.IsNullOrEmpty(name.Trim())||a.Account_Agent_Name.ToLower().Trim().Contains(name.ToLower().Trim())
            ).Select(a=>new AgentViewModel
            {
                 Account_Agent_ID=a.Account_Agent_ID,
                 Account_Agent_Name=a.Account_Agent_Name,
                 Account_Agent_Phone=a.Account_Agent_Phone,
                 IsActive=a.IsActive??false
                 
            }).OrderByDescending(i => i.Account_Agent_ID).ToPagedList(page ?? 1, pagesize);

            return agents;
        }

        public AgentViewModel GetAgentById(int id)
        {
            var agent = context.Account_Agent.Find(id);
            var vm = new AgentViewModel
            {
                 Account_Agent_ID=agent.Account_Agent_ID,
                 Account_Agent_Name=agent.Account_Agent_Name,
                 Account_Agent_Phone=agent.Account_Agent_Phone,
                 RegionIds=agent.AgentRegions.Select(a=>a.RegionId??0).ToList(),
                 CityIds= agent.AgentRegions.Select(a=>a.Region.CityID).Distinct().ToList()
            };
            return vm;
        }
        public void UpdateAgentStatus(bool isactive,int id)
        {
           var agent= context.Account_Agent.Find(id);
            agent.IsActive = isactive;
            context.SaveChanges();
        }
        public void CreateAgent(AgentViewModel vm)
        {
            var agent = new Account_Agent
            {
                Account_Agent_Name = vm.Account_Agent_Name.Trim(),
                Account_Agent_Phone = vm.Account_Agent_Phone
            };
            if (vm.AllRegions)
            {
                var regions = context.Regions.Where(r => vm.CityIds.Contains(r.CityID))
                    .Select(r => new AgentRegion
                    {
                        RegionId=r.RegionID
                    });
                foreach(var region in regions)
                {
                    agent.AgentRegions.Add(region);
                }
            }
            else
            {
                foreach (var region in vm.RegionIds)
                {
                    var agentregion = new AgentRegion
                    {
                        RegionId = region
                    };
                    agent.AgentRegions.Add(agentregion);
                }
            }
            context.Account_Agent.Add(agent);
            context.SaveChanges();
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}