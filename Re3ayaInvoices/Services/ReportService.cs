﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.Services;
using Telerik.Reporting;
using Re3ayaInvoices.Helpers;
using OfficeOpenXml;
using System.Data.Entity;
using Re3ayaInvoices.viewModels;
using OfficeOpenXml.Style;
using PagedList;

namespace Re3ayaInvoices.Services
{
    public class ReportService : IDisposable
    {
        private readonly Re3aya247Entities context;
        private readonly InvoiceService invoiceService;
        public ReportService()
        {
            context = new Re3aya247Entities();
            invoiceService = new InvoiceService();
        }
        public MemoryStream GenerateInvoiceFile(int id, int providertype, out string filename)
        {
            var stream = new MemoryStream();
            var reportBook = new ReportBook();
            var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            var reportSource = new Telerik.Reporting.UriReportSource();
            string reportpath = "";
            filename = "";
            bool ispaid = false;
            var invoice = context.Invoices.Find(id);
            if (invoice != null)
            {
                ispaid = invoice.IsPaid.GetValueOrDefault();
                switch (invoice.TypeId)
                {
                    case (int)Common.InvoiceType.SubscriptionInvoice:
                        filename = "فاتورة إشتراك " + invoice.InvoiceStartDate.Value.ToString("MMMM,yyyy") + " رقم" + invoice.Invoice_Number;
                        reportpath = GetSubscriptionInvoiceReportName(providertype, ispaid);
                        break;
                    case (int)Common.InvoiceType.AppointmentInvoice:
                        filename = "فاتورة حجوزات " + invoice.InvoiceStartDate.Value.ToString("MMMM,yyyy") + " رقم" + invoice.Invoice_Number;
                        reportpath = GetReservationInvoiceReportName(providertype, ispaid);
                        reportSource.Parameters.Add("ProviderType", providertype);
                        break;
                    case (int)Common.InvoiceType.OnlineSubscriptionInvoice:
                        filename = "فاتورة إشتراك أونلاين " + invoice.InvoiceStartDate.Value.ToString("MMMM,yyyy") + " رقم" + invoice.Invoice_Number;
                        reportpath = ispaid ? "~/ReportsTRDX/RealSingleDoctorOnlineSubscriptionInvoiceReport.trdx"
                            : "~/ReportsTRDX/SingleDoctorOnlineSubscriptionInvoiceReport.trdx";
                        break;
                    case (int)Common.InvoiceType.HomeVisitInvoice:
                        filename = "فاتورة زيارة منزلية " + invoice.InvoiceStartDate.Value.ToString("MMMM,yyyy") + " رقم" + invoice.Invoice_Number;
                        reportpath = ispaid ? "~/RealReportsTRDX/RealSingleNewFinalDoctorInvoiceReportNew.trdx"
                            : "~/ReportsTRDX/SingleNewFinalDoctorInvoiceReportNew.trdx";
                        reportSource.Parameters.Add("ProviderType", providertype);

                        break;
                }
                reportSource.Uri = HttpContext.Current.Server.MapPath(reportpath);
                reportSource.Parameters.Add("InvoiceId", invoice.InvoiceID);
                if (providertype == (int)Common.MedicalProviderType.hospital || providertype == (int)Common.MedicalProviderType.medicalcenter)
                {
                    reportSource.Parameters.Add("HealthEntityType", providertype);
                }
                reportBook.ReportSources.Add(reportSource);
                var IRS = new Telerik.Reporting.InstanceReportSource();
                IRS.ReportDocument = reportBook;
                Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", IRS, null);
                //
                stream = new MemoryStream(result.DocumentBytes);
                stream.Flush(); //Always catches me out
                stream.Position = 0; //Not sure if this is required
            }

            return stream;
        }
        public MemoryStream GenerateInvoiceFile(InvoiceSearch search, out string filename)
        {
            var reportBook = new ReportBook();
            var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            var reportSource = new Telerik.Reporting.UriReportSource();
            filename = "Invoice report";
            if (search.RegionIds.Count == 0)
            {
                search.RegionIds = context.Regions.Select(r => r.RegionID).ToList();
            }
            reportSource.Uri = HttpContext.Current.Server.MapPath("~/ReportsTRDX/NewFinalDoctorInvoiceReportNew.trdx");
            reportSource.Parameters.Add("ProviderType", search.ProviderType);
            reportSource.Parameters.Add("day", search.From.Value.Day);
            reportSource.Parameters.Add("month", search.From.Value.Month);
            reportSource.Parameters.Add("year", search.From.Value.Year);
            reportSource.Parameters.Add("today", search.To.Value.Day);
            reportSource.Parameters.Add("tomonth", search.To.Value.Month);
            reportSource.Parameters.Add("toyear", search.To.Value.Year);
            reportSource.Parameters.Add("RegionId", search.RegionIds);
            reportSource.Parameters.Add("mathoperator", search.MathOperatorId??0);
            reportSource.Parameters.Add("compratorvalue", search.InvoiceTotal??0);
            if (search.AgentIds.Count() != 0)
            {
                reportSource.Parameters.Add("AgentId", search.AgentIds.FirstOrDefault());
            }
            reportBook.ReportSources.Add(reportSource);
            var IRS = new Telerik.Reporting.InstanceReportSource();
            IRS.ReportDocument = reportBook;
            Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", IRS, null);
            MemoryStream stream = new MemoryStream(result.DocumentBytes);
            //PdfWriter pdfWriter = new PdfWriter(stream);
            stream.Flush(); 
            stream.Position = 0;

            return stream;
        }
        public MemoryStream GenerateUnpaidSubscriptionInvoices(InvoiceSearch search, out string filename)
        {
            var reportBook = new ReportBook();
            var reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
            var reportSource = new Telerik.Reporting.UriReportSource();
            filename = "إشتراكات لديهم حجوزات";
            if (search.RegionIds.Count == 0)
            {
                search.RegionIds = context.Regions.Select(r => r.RegionID).ToList();
            }
            reportSource.Uri = HttpContext.Current.Server.MapPath("~/ReportsTRDX/UnpaidSubscription_HasReservation.trdx");
            //reportSource.Parameters.Add("ProviderType", search.ProviderType);
            reportSource.Parameters.Add("FromDate", search.From.Value.ToString("yyyy-MM-dd"));
            reportSource.Parameters.Add("ToDate", search.To.Value.ToString("yyyy-MM-dd"));
            reportSource.Parameters.Add("year", search.From.Value.Year);
            reportSource.Parameters.Add("RegionId", search.RegionIds);

            /*reportSource.Parameters.Add("day", search.From.Value.Day);
            reportSource.Parameters.Add("month", search.From.Value.Month);
            reportSource.Parameters.Add("year", search.From.Value.Year);
            reportSource.Parameters.Add("today", search.To.Value.Day);
            reportSource.Parameters.Add("tomonth", search.To.Value.Month);
            reportSource.Parameters.Add("toyear", search.To.Value.Year);*/
            if (search.AgentIds.Count() != 0)
            {
                reportSource.Parameters.Add("AgentId", search.AgentIds.FirstOrDefault());
            }
            reportBook.ReportSources.Add(reportSource);
            var IRS = new Telerik.Reporting.InstanceReportSource();
            IRS.ReportDocument = reportBook;
            Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", IRS, null);
            MemoryStream stream = new MemoryStream(result.DocumentBytes);
            //PdfWriter pdfWriter = new PdfWriter(stream);
            stream.Flush();
            stream.Position = 0;

            return stream;
        }
        string GetSubscriptionInvoiceReportName(int providertype, bool ispaid)
        {
            string reportpath = "";
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                reportpath = ispaid ? "~/ReportsTRDX/RealSingleDoctorSubscriptionInvoiceReport.trdx"
                            : "~/ReportsTRDX/SingleDoctorSubscriptionInvoiceReport.trdx";
            }
            else
            {
                reportpath = ispaid ? "~/ReportsTRDX/RealSingleEntitySubscriptionInvoiceReport.trdx"
                            : "~/ReportsTRDX/SingleEntitySubscriptionInvoiceReport.trdx";
            }

            return reportpath;
        }
        string GetReservationInvoiceReportName(int providertype, bool ispaid)
        {
            string reportpath = "";
            switch (providertype)
            {
                case (int)Common.MedicalProviderType.doctor:
                    reportpath = ispaid ? "~/ReportsTRDX/RealSingleNewFinalDoctorInvoiceReportNew.trdx"
                            : "~/ReportsTRDX/SingleNewFinalDoctorInvoiceReportNew.trdx";
                    break;
                case (int)Common.MedicalProviderType.hospital:
                    reportpath = ispaid ? "~/ReportsTRDX/RealSingleNewFinalDoctorInvoiceReportNew.trdx"
                           : "~/ReportsTRDX/SingleNewFinalDoctorInvoiceReportNew.trdx";
                    break;
                case (int)Common.MedicalProviderType.lab:
                    reportpath = ispaid ? "~/ReportsTRDX/RealSingleNewFinalDoctorInvoiceReportNew.trdx"
                           : "~/ReportsTRDX/SingleNewFinalDoctorInvoiceReportNew.trdx";
                    break;
                case (int)Common.MedicalProviderType.intencivecare:
                    reportpath = ispaid ? ""
                           : "";
                    break;
                case (int)Common.MedicalProviderType.medicalcenter:
                    reportpath = ispaid ? "~/ReportsTRDX/RealSingleNewFinalDoctorInvoiceReportNew.trdx"
                           : "~/ReportsTRDX/SingleNewFinalDoctorInvoiceReportNew.trdx";
                    break;
                case (int)Common.MedicalProviderType.radiologycenter:
                    reportpath = ispaid ? "~/ReportsTRDX/RealSingleNewFinalDoctorInvoiceReportNew.trdx"
                           : "~/ReportsTRDX/SingleNewFinalDoctorInvoiceReportNew.trdx";
                    break;
                case (int)Common.MedicalProviderType.pharmacy:
                    reportpath = ispaid ? ""
                           : "";
                    break;
                case (int)Common.MedicalProviderType.bloodbank:
                    reportpath = ispaid ? ""
                           : "";
                    break;
                case (int)Common.MedicalProviderType.newborncare:
                    reportpath = ispaid ? ""
                           : "";
                    break;
            }
            return reportpath;
        }
        public List<InvoiceSummaryViewModel> CreatePaymentReport(PaymentReportSearch search)
        {
            var summary = context.Invoices.Where(i =>
            (i.TypeId == search.InvoiceType)
         && (i.IsCanceled != true)
         && (search.ProviderType == (int)Common.MedicalProviderType.doctor ? i.DoctorId.HasValue
            : i.Health_Entitiy.Health_Entitiy_Type == search.ProviderType)
         && (!search.From.HasValue || (i.InvoiceStartDate >= search.From.Value && i.InvoiceStartDate <= search.To.Value)))
             .GroupBy(i => i.InvoiceStartDate)
              .Select(i => new InvoiceSummaryViewModel
              {
                  InvoiceDate = i.Key.Value,
                  NumberOfInvoices = i.Count(),
                  TotalInvoiceAmount = i.Select(a => a.TotalAmount).Sum() ?? 0,
                  NumberOfClosedInvoices = i.Where(a => a.IsPaid == true).Count(),
                  TotalPaidInvoiceAmount = i.Where(a => a.IsPaid == true).Select(a => a.TotalAmount).Sum() ?? 0
              }).OrderBy(i => i.InvoiceDate).ToList();

            return summary;
        }
        public List<InvoiceStatisticViewModel> InvoiceStatisticsTotal(DateTime FromDate, DateTime ToDate, int providertype, string sheetpath)
        {
            var invoices = new List<InvoiceStatisticViewModel>();
            var products = context.Account_Products.Where(p =>
                             (p.InvoiceItem.IsActive.Value)
                          && (p.ProviderType == providertype)).ToList();
            int subscriptionproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.SubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
            int adminstrativeproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)).SingleOrDefault().Account_Porduct_ID;
            int confirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.ConfirmedAppointments)).SingleOrDefault().Account_Porduct_ID;
            int unconfirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.Nonconfirmedappointments)).SingleOrDefault().Account_Porduct_ID;
            int videoproductid = 0, chatproductid = 0;
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                videoproductid = products.Where(p =>
               (p.ItemId == (int)Common.InvoiceItem.VideoSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
                chatproductid = products.Where(p =>
               (p.ItemId == (int)Common.InvoiceItem.ChatSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
            }

            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                invoices = context.Invoices.Include(i => i.Account_Doctor).Where(m =>
                  (m.InvoiceStartDate >= FromDate && m.InvoiceStartDate <= ToDate)
                && (m.DoctorId.HasValue)
                   && (m.IsCanceled != true))
              .GroupBy(i => i.InvoiceStartDate).OrderBy(i => i.Key).Select(i => new InvoiceStatisticViewModel
              {
                  invoice_date = i.Key,
                  invoice_count = i.Count(),

                  subscription_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Debit),
                  subscription_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  subscription_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Credit),
                  subscription_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  subscription_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                  subscription_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                  adminstrative_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Debit),
                  adminstrative_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  adminstrative_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Credit),
                  adminstrative_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  adminstrative_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                  adminstrative_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                  confirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Debit),
                  confirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  confirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Credit),
                  confirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  confirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                  confirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                  unconfirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Debit),
                  unconfirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  unconfirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Credit),
                  unconfirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  unconfirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                  unconfirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                  video_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Debit),
                  video_total_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  video_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Credit),
                  video_paid_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  video_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                  video_net_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                  chat_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Debit),
                  chat_total_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  chat_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Credit),
                  chat_paid_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                  chat_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                  chat_net_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

              }).ToList();
            }
            else
            {
                invoices = context.Invoices.Include(i => i.Account_Doctor).Where(m =>
                   (m.InvoiceStartDate >= FromDate && m.InvoiceStartDate <= ToDate)
                 && (!m.DoctorId.HasValue)
                 && (m.IsCanceled != true)
                 && (m.Health_Entitiy.Health_Entitiy_Type == providertype))
               .GroupBy(i => i.InvoiceStartDate).OrderBy(i => i.Key).Select(i => new InvoiceStatisticViewModel
               {
                   invoice_date = i.Key,
                   invoice_count = i.Count(),

                   subscription_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Debit),
                   subscription_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   subscription_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Credit),
                   subscription_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   subscription_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                   subscription_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                   adminstrative_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Debit),
                   adminstrative_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   adminstrative_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Credit),
                   adminstrative_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   adminstrative_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                   adminstrative_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                   confirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Debit),
                   confirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   confirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Credit),
                   confirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   confirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                   confirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                   unconfirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Debit),
                   unconfirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   unconfirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Credit),
                   unconfirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                   unconfirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                   unconfirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                   .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),
               }).ToList();
            }

            return invoices;
        }
        public List<InvoiceStatisticViewModel> InvoiceStatisticsGovrenrate(DateTime FromDate, DateTime ToDate, int providertype, string sheetpath)
        {
            context.Database.CommandTimeout = 180;
            var invoices = new List<InvoiceStatisticViewModel>();
            var products = context.Account_Products.Where(p =>
                             (p.InvoiceItem.IsActive.Value)
                          && (p.ProviderType == providertype)).ToList();
            int subscriptionproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.SubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
            int adminstrativeproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)).SingleOrDefault().Account_Porduct_ID;
            int confirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.ConfirmedAppointments)).SingleOrDefault().Account_Porduct_ID;
            int unconfirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.Nonconfirmedappointments)).SingleOrDefault().Account_Porduct_ID;
            int videoproductid = 0, chatproductid = 0;
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                videoproductid = products.Where(p =>
               (p.ItemId == (int)Common.InvoiceItem.VideoSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
                chatproductid = products.Where(p =>
               (p.ItemId == (int)Common.InvoiceItem.ChatSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
            }
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                invoices = context.Invoices.Include(i => i.Account_Doctor).Where(m =>
                  (m.DoctorId.HasValue)
                     && (m.IsCanceled != true)
                && (m.InvoiceStartDate >= FromDate && m.InvoiceStartDate <= ToDate))
           .GroupBy(i => new
           {
               gov = i.Doctor.Address_Book.Where(d => d.IsMain == true).FirstOrDefault().Region.Governrate.Gov_Name_AR
           ,
               govid = i.Doctor.Address_Book.Where(d => d.IsMain == true).FirstOrDefault().Region.Governrate.GovID
           ,
               invoice_date = i.InvoiceStartDate
           })
            .OrderBy(i => i.Key.govid).Select(i => new InvoiceStatisticViewModel
            {
                governrate_name = i.Key.gov,
                invoice_date = i.Key.invoice_date,
                invoice_count = i.Count(),

                subscription_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Debit),
                subscription_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                subscription_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Credit),
                subscription_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                subscription_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                subscription_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                adminstrative_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Debit),
                adminstrative_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                adminstrative_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Credit),
                adminstrative_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                adminstrative_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                adminstrative_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                confirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Debit),
                confirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                confirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Credit),
                confirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                confirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                confirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                unconfirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Debit),
                unconfirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                unconfirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Credit),
                unconfirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                unconfirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                unconfirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                video_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Debit),
                video_total_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                video_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Credit),
                video_paid_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                video_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                video_net_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                chat_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Debit),
                chat_total_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                chat_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Credit),
                chat_paid_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                chat_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                chat_net_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),
            }).ToList();

            }
            else
            {
                invoices = context.Invoices.Include(i => i.Account_Doctor).Where(m =>
   (!m.DoctorId.HasValue)
&& (m.IsCanceled != true)
&& (m.Health_Entitiy.Health_Entitiy_Type == providertype)
&& (m.InvoiceStartDate >= FromDate && m.InvoiceStartDate <= ToDate))
.GroupBy(i => new
{
    gov = i.Health_Entitiy.Region.Governrate.Gov_Name_AR
,
    govid = i.Health_Entitiy.Region.Governrate.GovID
,
    invoice_date = i.InvoiceStartDate
})
.OrderBy(i => i.Key.govid).Select(i => new InvoiceStatisticViewModel
{
    governrate_name = i.Key.gov,
    invoice_date = i.Key.invoice_date,
    invoice_count = i.Count(),
    subscription_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Debit),
    subscription_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    subscription_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Credit),
    subscription_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    subscription_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    subscription_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

    adminstrative_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Debit),
    adminstrative_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    adminstrative_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Credit),
    adminstrative_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    adminstrative_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    adminstrative_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

    confirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Debit),
    confirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    confirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Credit),
    confirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    confirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    confirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

    unconfirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Debit),
    unconfirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    unconfirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Credit),
    unconfirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    unconfirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    unconfirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),
}).ToList();

            }
            return invoices;
        }
        public List<InvoiceStatisticViewModel> InvoiceStatisticsRegion(DateTime FromDate, DateTime ToDate, int providertype, string sheetpath)
        {
            context.Database.CommandTimeout = 180;
            var invoices = new List<InvoiceStatisticViewModel>();
            var products = context.Account_Products.Where(p =>
                             (p.InvoiceItem.IsActive.Value)
                          && (p.ProviderType == providertype)).ToList();
            int subscriptionproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.SubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
            int adminstrativeproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)).SingleOrDefault().Account_Porduct_ID;
            int confirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.ConfirmedAppointments)).SingleOrDefault().Account_Porduct_ID;
            int unconfirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.Nonconfirmedappointments)).SingleOrDefault().Account_Porduct_ID;
            int videoproductid = 0, chatproductid = 0;
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                videoproductid = products.Where(p =>
               (p.ItemId == (int)Common.InvoiceItem.VideoSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
                chatproductid = products.Where(p =>
               (p.ItemId == (int)Common.InvoiceItem.ChatSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;
            }
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                invoices = context.Invoices.Include(i => i.Account_Doctor)
                    .Where(m =>
                       (m.InvoiceStartDate >= FromDate && m.InvoiceStartDate <= ToDate)
                          && (m.IsCanceled != true)
                     && (m.DoctorId.HasValue))
            .GroupBy(i => new
            {
                region = i.Doctor.Address_Book.Where(d => d.IsMain == true).FirstOrDefault().Region.Region_Name_AR
                ,
                invoice_date = i.InvoiceStartDate
                ,
                gov = i.Doctor.Address_Book.Where(d => d.IsMain == true).FirstOrDefault().Region.Governrate.Gov_Name_AR
            })
            .OrderBy(i => i.Key.gov).Select(i => new InvoiceStatisticViewModel
            {
                governrate_name = i.Key.gov,
                region_name = i.Key.region,
                invoice_date = i.Key.invoice_date,
                invoice_count = i.Count(),

                subscription_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Debit),
                subscription_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                subscription_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Credit),
                subscription_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                subscription_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                subscription_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                adminstrative_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Debit),
                adminstrative_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                adminstrative_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Credit),
                adminstrative_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                adminstrative_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                adminstrative_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                confirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Debit),
                confirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                confirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Credit),
                confirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                confirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                confirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                unconfirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Debit),
                unconfirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                unconfirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Credit),
                unconfirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                unconfirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                unconfirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                video_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Debit),
                video_total_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                video_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Credit),
                video_paid_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                video_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                video_net_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                chat_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Debit),
                chat_total_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
                chat_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Credit),
                chat_paid_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
                chat_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                chat_net_count = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                  .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

            }).ToList();
            }
            else
            {
                invoices = context.Invoices.Include(i => i.Account_Doctor)
    .Where(m =>
   (!m.DoctorId.HasValue)
      && (m.IsCanceled != true)
&& (m.Health_Entitiy.Health_Entitiy_Type == providertype)
&& (m.InvoiceStartDate >= FromDate && m.InvoiceStartDate <= ToDate))
.GroupBy(i => new
{
    region = i.Health_Entitiy.Region.Region_Name_AR
,
    invoice_date = i.InvoiceStartDate
,
    gov = i.Health_Entitiy.Region.Governrate.Gov_Name_AR
})
.OrderBy(i => i.Key.gov).Select(i => new InvoiceStatisticViewModel
{
    governrate_name = i.Key.gov,
    region_name = i.Key.region,
    invoice_date = i.Key.invoice_date,
    invoice_count = i.Count(),
    subscription_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Debit),
    subscription_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    subscription_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Credit),
    subscription_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    subscription_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    subscription_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

    adminstrative_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Debit),
    adminstrative_total_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    adminstrative_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Credit),
    adminstrative_paid_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    adminstrative_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    adminstrative_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

    confirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Debit),
    confirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    confirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Credit),
    confirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    confirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    confirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

    unconfirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Debit),
    unconfirmedappointment_total_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Select(ac => ac.InvoiceID).Distinct().Count(),
    unconfirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Credit),
    unconfirmedappointment_paid_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && c.Credit.HasValue ? c.Credit > 0 : false)).Select(ac => ac.InvoiceID).Distinct().Count(),
    unconfirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
    unconfirmedappointment_net_count = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
  .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),
}).ToList();

            }

            return invoices;
        }
        public List<DoctorInvoicesStatistic_Result> DoctorInvoicesStatistic(AppointmentReportSearch search)
        {
            string regionids =string.Join(",",search.RegionIds);
            string cityids = string.Join(",", search.CityIds);
            var result = context.DoctorInvoicesStatistic(search.From,search.To, cityids,search.AllCity
                ,regionids,search.AllRegions).ToList();

            return result;

            /*var products = context.Account_Products.Where(p =>
                     (p.InvoiceItem.IsActive.Value)
                  && (p.ProviderType == (int)Common.MedicalProviderType.doctor)).ToList();
            int subscriptionproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.SubscriptionFees)).SingleOrDefault().Account_Porduct_ID;

            int adminstrativeproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)).SingleOrDefault().Account_Porduct_ID;

            int confirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.ConfirmedAppointments)).SingleOrDefault().Account_Porduct_ID;

            int unconfirmproductid = products.Where(p =>
            (p.ItemId == (int)Common.InvoiceItem.Nonconfirmedappointments)).SingleOrDefault().Account_Porduct_ID;

            int videoproductid = products.Where(p =>
           (p.ItemId == (int)Common.InvoiceItem.VideoSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;

            int chatproductid = products.Where(p =>
           (p.ItemId == (int)Common.InvoiceItem.ChatSubscriptionFees)).SingleOrDefault().Account_Porduct_ID;

            var invoices = context.Invoices.Include(i => i.Account_Doctor)
                .Where(m =>
                    (!search.From.HasValue || (m.InvoiceStartDate >=search.From && m.InvoiceStartDate <= search.To))
                 && (m.IsCanceled != true)
                 && (m.DoctorId.HasValue)
                 && ((search.CityIds.Count()>0&&search.RegionIds.Count()==0)
                     ?search.CityIds.Contains(m.Doctor.Address_Book.Where(ad => ad.IsMain == true)
                       .FirstOrDefault().Region.CityID):true)
                 && (search.RegionIds.Count() == 0 || 
                    search.RegionIds.Contains(m.Doctor.Address_Book.Where(ad => ad.IsMain == true)
                     .FirstOrDefault().RegionID.Value))
               ).GroupBy(i => new
               {
                   invoice_date = i.InvoiceStartDate,
                   cityid=i.Doctor.Address_Book.Where(d => d.IsMain == true)
                      .FirstOrDefault().Region.Governrate.Gov_Name_AR,
                   doctor = i.Doctor
               }).Select(i => new InvoiceStatisticViewModel
               {
                   governrate_name = i.Key.doctor.Address_Book.Where(d => d.IsMain == true)
                      .FirstOrDefault().Region.Governrate.Gov_Name_AR,
                   region_name = i.Key.doctor.Address_Book.Where(d => d.IsMain == true)
                      .FirstOrDefault().Region.Region_Name_AR,
                   invoice_date = i.Key.invoice_date,
                   code = i.Key.doctor.Doctor_CODE,
                   speciality = i.Key.doctor.Speciality.SpecialityName_AR,
                   name = i.Key.doctor.Doctor_Name_AR,
                   invoice_count = i.Count(),
                   subscription_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Debit),
                   subscription_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid)).Sum(acc => acc.Credit),
                   subscription_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),
                   subscription_net_count = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == subscriptionproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Select(ac => ac.InvoiceID).Distinct().Count(),

                   adminstrative_total = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Debit),
                   adminstrative_paid = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid)).Sum(acc => acc.Credit),
                   adminstrative_net = i.Where(a => a.TypeId == 1).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == adminstrativeproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),

                   confirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Debit),
                   confirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid)).Sum(acc => acc.Credit),
                   confirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == confirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),

                   unconfirmedappointment_total = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Debit),
                   unconfirmedappointment_paid = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid)).Sum(acc => acc.Credit),
                   unconfirmedappointment_net = i.Where(a => a.TypeId == 2).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == unconfirmproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),

                   video_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Debit),
                   video_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid)).Sum(acc => acc.Credit),
                   video_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == videoproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),

                   chat_total = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Debit),
                   chat_paid = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid)).Sum(acc => acc.Credit),
                   chat_net = i.Where(a => a.TypeId == 3 && a.DoctorId.HasValue).Select(a => a.Account_Doctor)
                     .SelectMany(ac => ac.Where(c => c.ProductID == chatproductid && (c.Credit.HasValue ? c.Credit.Value == 0 : true))).Sum(acc => acc.Debit),

               }).ToList();
               
            return invoices;*/

        }
        public List<GeneralAppointmentsReportViewModel> GeneralAppointments(DateTime? from, DateTime? to)
        {
            var invoices = context.Invoices.Include(acc => acc.Account_Doctor)
                .Include(ap => ap.Invoice_Appointment).Where(acc =>
                       (acc.InvoiceStartDate >= from && acc.InvoiceStartDate <= to)
                     && (acc.TypeId == (int)Common.InvoiceType.AppointmentInvoice))
                   .Select(i => new
                   {
                       Date = i.InvoiceStartDate,
                       ProviderType = i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                         : i.Health_Entitiy.Health_Entitiy_Type,
                       AppointmentCount = i.Invoice_Appointment.Count(),
                       ConfirmAppointmentCount = context.Re3Appointment
                       .Where(ap => i.Invoice_Appointment.Select(v => v.Appointment_Id).Contains(ap.AppointmentId)
                          && (ap.ProviderType == (i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                             : i.Health_Entitiy.Health_Entitiy_Type))
                          && (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6 || ap.BookingStatus == 7)).Count(),
                       UnconfirmAppointmentCount = context.Re3Appointment
                       .Where(ap => i.Invoice_Appointment.Select(v => v.Appointment_Id).Contains(ap.AppointmentId)
                          && (ap.ProviderType == (i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                             : i.Health_Entitiy.Health_Entitiy_Type))
                          && (ap.BookingStatus == 1 || ap.BookingStatus == 2)).Count(),
                       CancelAppointmentCount = context.Re3Appointment
                       .Where(ap => i.Invoice_Appointment.Select(v => v.Appointment_Id).Contains(ap.AppointmentId)
                          && (ap.ProviderType == (i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                             : i.Health_Entitiy.Health_Entitiy_Type))
                          && (ap.BookingStatus == 5)).Count(),
                       AppointmentTotal = (i.DoctorId.HasValue
                              || i.Health_Entitiy.Health_Entitiy_Type == (int)Common.MedicalProviderType.hospital
                              || i.Health_Entitiy.Health_Entitiy_Type == (int)Common.MedicalProviderType.medicalcenter)
                                ? context.Appointments.Where(ap => i.Invoice_Appointment.Select(v => v.Appointment_Id)
                                  .Contains(ap.Appointment_ID)).Select(ap => ap.Fees_For_Re3aya).Sum()
                          : i.Health_Entitiy.Health_Entitiy_Type == (int)Common.MedicalProviderType.lab ?
                           context.LabsAppointments.Where(ap => i.Invoice_Appointment.Select(v => v.Appointment_Id)
                           .Contains(ap.ID)).Select(ap => ap.Fees).Sum() : 0,
                       TotalAmount = i.TotalAmount,
                       Status = (!i.IsCanceled.HasValue || !i.IsCanceled.Value) ? (int)Common.InvoiceStatus.cancel
                         : (i.IsPaid.Value && (!i.IsCanceled.HasValue || !i.IsCanceled.Value)) ? (int)Common.InvoiceStatus.paid
                         : ((!i.IsPaid.HasValue || !i.IsPaid.Value) && (!i.IsCanceled.HasValue || !i.IsCanceled.Value))
                         ? (int)Common.InvoiceStatus.paid : 0,
                   }).GroupBy(ap => ap.Date).Select(ap => new GeneralAppointmentsReportViewModel
                   {
                       Date = ap.Key.Value,
                       AppointmentsCount = ap.Select(p => p.AppointmentCount).Count(),
                       //AppointmentsTotal = ap,
                       //CanceledAppointmentsCount = ap.Select(a=>a. ap.Where(a => a.Status == 5).Count(),
                       //CanceledAppointmentsTotal = ap.Where(a => a.Status == 5).Select(a => a.Re3ayaFees).Sum(),
                       //ConfirmedAppointmentsCount = ap.Where(a =>
                       //   a.Status == 3 || a.Status == 4 || a.Status == 6 || a.Status == 7).Count(),
                       //ConfirmedAppointmentsTotal = ap.Where(a =>
                       //     a.Status == 3 || a.Status == 4 || a.Status == 6 || a.Status == 7).Select(a => a.Re3ayaFees).Sum(),
                       //UnconfirmedAppointmentsCount = ap.Where(a =>
                       //   a.Status == 1 || a.Status == 2).Count(),
                       //UnconfirmedAppointmentsTotal = ap.Where(a =>
                       //   a.Status == 1 || a.Status == 2).Select(a => a.Re3ayaFees).Sum(),
                       InvoiceCount = ap.Count(),
                       PaidInvoiceCount = ap.Where(i => i.Status == (int)Common.InvoiceStatus.paid).Count(),
                       PaidInvoiceTotal = ap.Where(i => i.Status == (int)Common.InvoiceStatus.paid)
                         .Select(i => i.TotalAmount).Sum(),
                       UnpaidInvoicesCount = ap.Where(i => i.Status == (int)Common.InvoiceStatus.unpaid).Count(),
                       UnpaidInvoicesTotal = ap.Where(i => i.Status == (int)Common.InvoiceStatus.unpaid)
                         .Select(i => i.TotalAmount).Sum(),
                       CanceledInvoiceCount = ap.Where(i => i.Status == (int)Common.InvoiceStatus.cancel).Count(),
                       CanceledInvoiceTotal = ap.Where(i => i.Status == (int)Common.InvoiceStatus.cancel)
                         .Select(i => i.TotalAmount).Sum()
                   }).OrderBy(ap => ap.Month).ThenBy(ap => ap.Year).ToList();

            return invoices;

        }
        public List<DoctorHasAppointmentViewModel> DoctorHasAppointments(AppointmentReportSearch search)
        {
            //
            var result = context.Doctors
                         .Where(d => 
                          (d.Doctor_CODE != "0")
                         && ((search.RegionIds.Count() == 0 && search.CityIds.Count() > 0)
                              ? search.CityIds.Contains(d.Address_Book.Where(a=>a.IsMain==true).FirstOrDefault().Region.Governrate.GovID)
                              : (search.RegionIds.Count() > 0)
                              ? search.RegionIds.Contains(d.Address_Book.Where(a => a.IsMain == true).FirstOrDefault().RegionID ?? 0) : true))
                         .Select(d => new DoctorHasAppointmentViewModel
                         {
                             Code = d.Doctor_CODE,
                             ProviderType =  "دكتور",
                             Name = d.Doctor_Name_AR,
                             Speciality = d.Speciality.SpecialityName_AR,
                             Status = d.Is_Active == true ? "مفعل" : "غير مفعل",
                             MainAddress = d.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Address_Name_AR,
                             CityName = d.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region
                                .Governrate.Gov_Name_AR,
                             RegionName = d.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR,
                             NumberOfAppointmnts = d.Address_Book.SelectMany(ad => ad.Appointments).Where(ap =>
                               (!search.From.HasValue || (DbFunctions.TruncateTime(ap.Appointment_Creation_Datetime) >= search.From
                                  && DbFunctions.TruncateTime(ap.Appointment_Creation_Datetime) <= search.To))).Count(),
                         }).OrderByDescending(d => d.NumberOfAppointmnts).ToList();
            /*
            var result = context.Appointments.Where(ap =>
                             (!search.From.HasValue || (DbFunctions.TruncateTime(ap.Appointment_Creation_Datetime) >= search.From
                                 && DbFunctions.TruncateTime(ap.Appointment_Creation_Datetime) <= search.To))
                           && ((search.RegionIds.Count() == 0 && search.CityIds.Count()>0)
                              ? search.CityIds.Contains(ap.Address_Book.Region.CityID)
                              : (search.RegionIds.Count() > 0)
                               ? search.RegionIds.Contains(ap.Address_Book.RegionID ?? 0) : true)
                              ).Select(a=>new
                              {
                                  AppointemntId=a.Appointment_ID,
                                  DoctorCode=a.Address_Book.Doctor.Doctor_CODE,
                                  DoctorName=a.Address_Book.Doctor.Doctor_Name_AR,
                                  SpecialityName=a.Address_Book.Doctor.Speciality.SpecialityName_AR,
                                  ActiveStatus=a.Address_Book.Doctor.Is_Active,
                                  AddressName=a.Address_Book.IsMain==true?a.Address_Book.Address_Name_AR:"",
                                  CityName=a.Address_Book.IsMain==true?a.Address_Book.Region.Governrate.Gov_Name_AR:"",
                                  RegionName=a.Address_Book.IsMain==true?a.Address_Book.Region.Region_Name_AR:"",
                              }).GroupBy(d =>new
                              {
                                  code =d.DoctorCode,
                                  name =d.DoctorName,
                                  speciality =d.SpecialityName,
                                  status=d.ActiveStatus,
                              })
                              .Select(d => new DoctorHasAppointmentViewModel
                              {
                                  Code = d.Key.code,
                                  ProviderType = null,
                                  Name = d.Key.name,
                                  Speciality = d.Key.speciality,
                                  Status = d.Key.status == true ? "مفعل" : "غير مفعل",
                                  MainAddress = d.Where(ad=>ad.AddressName!="").FirstOrDefault().AddressName,
                                  CityName = d.Where(ad => ad.CityName != "").FirstOrDefault().CityName,
                                  RegionName = d.Where(ad => ad.RegionName != "").FirstOrDefault().RegionName,
                                  NumberOfAppointmnts =d.Select(ad=>ad.AppointemntId).Count()
                              }).ToList();*/
            return result;
        }
        public List<AppointmentStatisticsViewModel> AppointmentsMedicalProvider(AppointmentReportSearch search)
        {
            var appointmentquery = context.Appointments.Where(ap => !ap.Address_Book.EntityId.HasValue)
              .Select(a => new AppointmentMedicalProvider
              {
                  CreatedDate = a.Appointment_Creation_Datetime,
                  Status = a.BookingStatus,
                  ProviderType = "doctor",
                  ProviderTypeId = (int)Common.MedicalProviderType.doctor,
                  Re3ayaFees = a.Fees_For_Re3aya,
                  CityId = a.Address_Book.Region.CityID,
                  CityName = a.Address_Book.Region.Governrate.Gov_Name_AR,
                  RegionId = a.Address_Book.RegionID,
                  RegionName = a.Address_Book.Region.Region_Name_AR
              }).Union(context.LabsAppointments.Select(a => new AppointmentMedicalProvider
              {
                  CreatedDate = a.AppointmentCreationDatetime,
                  Status = a.BookingStatus,
                  ProviderType = "lab",
                  ProviderTypeId = (int)Common.MedicalProviderType.lab,
                  Re3ayaFees = a.Fees,
                  CityId = a.LabsBranch.Health_Entitiy.Region.CityID,
                  CityName = a.LabsBranch.Health_Entitiy.Region.Governrate.Gov_Name_AR,
                  RegionId = a.LabsBranch.Health_Entitiy.Health_Entitiy_RegionID,
                  RegionName = a.LabsBranch.Health_Entitiy.Region.Region_Name_AR
              })).Union(context.RadiologyCenterAppointments.Select(a => new AppointmentMedicalProvider
              {
                  CreatedDate = a.AppointmentCreationDatetime,
                  Status = a.BookingStatus,
                  ProviderType = "radiology",
                  ProviderTypeId = (int)Common.MedicalProviderType.radiologycenter,
                  Re3ayaFees = a.Fees,
                  CityId = a.RadiologyCenterBranch.Health_Entitiy.Region.CityID,
                  CityName = a.RadiologyCenterBranch.Health_Entitiy.Region.Governrate.Gov_Name_AR,
                  RegionId = a.RadiologyCenterBranch.Health_Entitiy.Health_Entitiy_RegionID,
                  RegionName = a.RadiologyCenterBranch.Health_Entitiy.Region.Region_Name_AR
              })).Union(context.PharmacyOrders.Select(a => new AppointmentMedicalProvider
              {
                  CreatedDate = a.Date,
                  Status = a.OrderStatus,
                  ProviderType = "pharmacy",
                  ProviderTypeId = (int)Common.MedicalProviderType.pharmacy,
                  Re3ayaFees = 0,
                  CityId = a.Health_Entitiy.Region.CityID,
                  CityName = a.Health_Entitiy.Region.Governrate.Gov_Name_AR,
                  RegionId = a.Health_Entitiy.Health_Entitiy_RegionID,
                  RegionName = a.Health_Entitiy.Region.Region_Name_AR
              })).Union(context.Appointments.Where(ap => ap.Address_Book.Doctor.ProviderType == 1)
                    .Select(a => new AppointmentMedicalProvider
                    {
                        CreatedDate = a.Appointment_Creation_Datetime,
                        Status = a.BookingStatus,
                        ProviderType = "hosptial",
                        ProviderTypeId = (int)Common.MedicalProviderType.hospital,
                        Re3ayaFees = a.Fees_For_Re3aya,
                        CityId = a.Address_Book.Region.CityID,
                        CityName = a.Address_Book.Region.Governrate.Gov_Name_AR,
                        RegionId = a.Address_Book.RegionID,
                        RegionName = a.Address_Book.Region.Region_Name_AR
                    })).Union(context.IntensiveCare_Appointment.Select(a => new AppointmentMedicalProvider
                    {
                        CreatedDate = a.CreationDate,
                        Status = a.StatusId,
                        ProviderType = "intensivecare",
                        ProviderTypeId = (int)Common.MedicalProviderType.intencivecare,
                        Re3ayaFees = 300,
                        CityId = a.Health_Entitiy.Region.CityID,
                        CityName = a.Health_Entitiy.Region.Governrate.Gov_Name_AR,
                        RegionId = a.Health_Entitiy.Health_Entitiy_RegionID,
                        RegionName = a.Health_Entitiy.Region.Region_Name_AR
                    })).Union(context.Newborn_Appointment.Select(a => new AppointmentMedicalProvider
                    {
                        CreatedDate = a.CreationDate,
                        Status = a.StatusId,
                        ProviderType = "newborn",
                        ProviderTypeId = (int)Common.MedicalProviderType.newborncare,
                        Re3ayaFees = 300,
                        CityId = a.Health_Entitiy.Region.CityID,
                        CityName = a.Health_Entitiy.Region.Governrate.Gov_Name_AR,
                        RegionId = a.Health_Entitiy.Health_Entitiy_RegionID,
                        RegionName = a.Health_Entitiy.Region.Region_Name_AR
                    })).Union(context.Appointments.Where(a => a.Address_Book.Doctor.ProviderType == 4)
                   .Select(a => new AppointmentMedicalProvider
                   {
                       CreatedDate = a.Appointment_Creation_Datetime,
                       Status = a.BookingStatus,
                       ProviderType = "medicalcenter",
                       ProviderTypeId = (int)Common.MedicalProviderType.medicalcenter,
                       Re3ayaFees = a.Fees_For_Re3aya,
                       CityId = a.Address_Book.Region.CityID,
                       CityName = a.Address_Book.Region.Governrate.Gov_Name_AR,
                       RegionId = a.Address_Book.RegionID,
                       RegionName = a.Address_Book.Region.Region_Name_AR
                   })).Union(context.HomeVisitAppointments.Select(a => new AppointmentMedicalProvider
                   {
                       CreatedDate = a.AppointmentCreationDate,
                       Status = a.AppointmentStatus_Id,
                       ProviderType = "homevisit",
                       ProviderTypeId = (int)Common.MedicalProviderType.homevisit,
                       Re3ayaFees = a.Fees,
                       CityId = a.HomeVisitRegion.Region.CityID,
                       CityName = a.HomeVisitRegion.Region.Governrate.Gov_Name_AR,
                       RegionId = a.Home_Visit_Region_Id,
                       RegionName = a.HomeVisitRegion.Region.Region_Name_AR
                   })).Union(context.OfferBookings.Select(a => new AppointmentMedicalProvider
                   {
                       CreatedDate = a.Creation_Date,
                       Status = a.Booking_Status,
                       ProviderType = "Offers",
                       ProviderTypeId = (int)Common.MedicalProviderType.offer,
                       Re3ayaFees = 0,
                       CityId = 0,
                       CityName = "",
                       RegionId = 0,
                       RegionName = ""
                   }));

            var result = appointmentquery.Where(ap =>
                ((!search.From.HasValue) || (DbFunctions.TruncateTime(ap.CreatedDate) >= search.From
                     && DbFunctions.TruncateTime(ap.CreatedDate) <= search.To))
              && (search.CityIds.Count()>0 && search.RegionIds.Count() == 0 ? search.CityIds.Contains(ap.CityId??0) : true)
              && (search.AllRegions.HasValue || search.RegionIds.Count() == 0 || search.RegionIds.Contains(ap.RegionId ?? 0))
             ).ToList().GroupBy(ap =>
              new
              {
                  CreatedDate = ap.CreatedDate.HasValue
                  ? new DateTime(ap.CreatedDate.Value.Year, ap.CreatedDate.Value.Month, 1) : ap.CreatedDate,
                  CityId = ap.CityId,
                  CityName = ap.CityName,
                  RegionId = (search.RegionIds.Count() > 0 || search.AllRegions.HasValue) ? ap.RegionId : null,
                  RegionName = (search.RegionIds.Count() > 0 || search.AllRegions.HasValue) ? ap.RegionName : null,
              }).Select(ap => new AppointmentStatisticsViewModel
              {
                  CreatedDate = ap.Key.CreatedDate,
                  CityId = ap.Key.CityId,
                  RegionId = ap.Key.RegionId,
                  CityName = ap.Key.CityName,
                  RegionName = ap.Key.RegionName,
                  AppointmentMedicalProviders = ap.GroupBy(p => p.ProviderTypeId)
                    .Select(p => new AppointmentMedicalProvider
                    {
                        ProviderTypeId = p.Key,
                        AppointmentsCount = p.Count(),
                        AppointmentsTotal = p.Select(f => f.Re3ayaFees).Sum(),
                        ConfirmedAppointmentsCount = p.Where(s => s.Status == 3 || s.Status == 4 || s.Status == 6 || s.Status == 7).Count(),
                        ConfirmedAppointmentsTotal = p.Where(s => s.Status == 3 || s.Status == 4 || s.Status == 6 || s.Status == 7)
                          .Select(f => f.Re3ayaFees).Sum(),
                        UnconfirmedAppointmenstCount = p.Where(s => s.Status == 1 || s.Status == 2).Count(),
                        UnconfirmedAppointmentsTotal = p.Where(s => s.Status == 1 || s.Status == 2).Select(f => f.Re3ayaFees).Sum(),
                        CancelAppointmentsCount = p.Where(s => s.Status == 5).Count(),
                        CancelAppointmentsTotal = p.Where(s => s.Status == 5).Select(f => f.Re3ayaFees).Sum()
                    }).ToList()
              }).OrderBy(p => p.CityId).ThenBy(p => p.CreatedDate).ThenBy(p => p.RegionId).ToList();

            return result;
        }
        public List<InvoiceStatisticsViewModel> InvoicesMedicalProvider(AppointmentReportSearch search)
        {
            var invoices = context.Invoices.Include(acc => acc.Account_Doctor).Where(acc =>
                     (!search.From.HasValue || (acc.InvoiceStartDate >= search.From && acc.InvoiceStartDate <= search.To))
                  && (acc.TypeId == (int)Common.InvoiceType.AppointmentInvoice)
                  && (!search.AgentId.HasValue || acc.Account_Agent.Account_Agent_ID == search.AgentId))
                  .Select(i => new InvoiceMedicalProvider
                  {
                      ProviderTypeId = i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                       : (int)i.Health_Entitiy.Health_Entitiy_Type,
                      InvoiceDate = i.InvoiceStartDate,
                      Status = (i.IsPaid.Value && (!i.IsCanceled.HasValue || !i.IsCanceled.Value)) 
                      ? (int)Common.InvoiceStatus.paid
                      : (!i.IsPaid.Value && (!i.IsCanceled.HasValue || !i.IsCanceled.Value))
                      ? (int)Common.InvoiceStatus.unpaid
                      : i.IsCanceled.Value ? (int?)Common.InvoiceStatus.cancel
                      : null,
                      TotalAmount = i.TotalAmount ?? 0,
                      CityId = i.DoctorId.HasValue 
                      ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.CityID
                      : i.Health_Entitiy.Region.CityID,
                      CityName = i.DoctorId.HasValue 
                      ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Governrate.Gov_Name_AR
                      : i.Health_Entitiy.Region.Governrate.Gov_Name_AR,
                      RegionId = i.DoctorId.HasValue
                      ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().RegionID
                      : i.Health_Entitiy.Health_Entitiy_RegionID,
                      RegionName = i.DoctorId.HasValue
                      ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR
                      : i.Health_Entitiy.Region.Region_Name_AR,
                  }).Where(i =>
                      ((search.CityIds.Count()>0 && search.RegionIds.Count() == 0) ?search.CityIds.Contains(i.CityId??0) : true)
                   && (search.RegionIds.Count()>0?search.RegionIds.Contains(i.RegionId ?? 0):true)
                        ).ToList()
                   .GroupBy(i => new
                   {
                       InvoiceDate = i.InvoiceDate,
                       CityId =(search.CityIds.Count>0||search.AllCity.HasValue)?i.CityId:null,
                       CityName = (search.CityIds.Count > 0 || search.AllCity.HasValue)?i.CityName:null,
                       RegionId = (search.RegionIds.Count()>0 ||search.AllRegions.HasValue||search.AgentId.HasValue) 
                       ? i.RegionId : null,
                       RegionName = (search.RegionIds.Count()>0||search.AllRegions.HasValue||search.AgentId.HasValue)
                       ? i.RegionName : null,
                   }).Select(i => new InvoiceStatisticsViewModel
                   {
                       InvoiceDate = i.Key.InvoiceDate,
                       CityId = i.Key.CityId,
                       RegionId = i.Key.RegionId,
                       CityName = i.Key.CityName,
                       RegionName = i.Key.RegionName,
                       InvoiceMedicalProviders = i.GroupBy(p => p.ProviderTypeId)
                      .Select(p => new InvoiceMedicalProvider
                      {
                          ProviderTypeId = p.Key,
                          InvoicesCount = p.Count(),
                          InvoicesTotal = p.Select(f => f.TotalAmount).Sum(),
                          PaidInvoicesCount = p.Where(s => s.Status == (int)Common.InvoiceStatus.paid).Count(),
                          PaidInvoicesTotal = p.Where(s => s.Status == (int)Common.InvoiceStatus.paid)
                          .Select(s => s.TotalAmount).Sum(),
                          UnpaidInvoicesCount = p.Where(s => s.Status == (int)Common.InvoiceStatus.unpaid).Count(),
                          UnpaidInvoicesTotal = p.Where(s => s.Status == (int)Common.InvoiceStatus.unpaid)
                          .Select(s => s.TotalAmount).Sum(),
                          CancelInvoicesCount = p.Where(s => s.Status == (int)Common.InvoiceStatus.cancel).Count(),
                          CancelInvoicesTotal = p.Where(s => s.Status == (int)Common.InvoiceStatus.cancel)
                          .Select(s => s.TotalAmount).Sum(),
                      }).ToList()
                   }).OrderBy(p => p.CityId).ThenBy(p => p.RegionId).ThenBy(p => p.InvoiceDate).ToList();

            return invoices;
        }
        public List<InvoiceStatisticsViewModel> InvoiceProviderSummarize(AppointmentReportSearch search)
        {
            var invoices = context.Invoices.Where(acc =>
                    (!search.From.HasValue || acc.InvoiceStartDate >= search.From && acc.InvoiceStartDate <= search.To)
                  && (acc.TypeId == (int)Common.InvoiceType.SubscriptionInvoice
                    || acc.TypeId == (int)Common.InvoiceType.AppointmentInvoice)
                  && (!search.AgentId.HasValue || acc.AgentID == search.AgentId))
                  .Select(i => new InvoiceMedicalProvider
                  {
                      ProviderTypeId = i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                       : (int)i.Health_Entitiy.Health_Entitiy_Type,
                      InvoiceDate = i.InvoiceStartDate,
                      InvoiceType = i.TypeId,
                      Status = (i.IsPaid.Value && (!i.IsCanceled.HasValue || !i.IsCanceled.Value))
                      ? (int)Common.InvoiceStatus.paid
                      : ((!i.IsPaid.HasValue || !i.IsPaid.Value) && (!i.IsCanceled.HasValue || !i.IsCanceled.Value))
                      ? (int)Common.InvoiceStatus.unpaid
                      : i.IsCanceled.Value ? (int?)Common.InvoiceStatus.cancel : null,
                      HasAdministrative = i.TypeId == (int)Common.InvoiceType.SubscriptionInvoice ?
                          i.Account_Doctor.Any(acc =>
                            (acc.Account_Products.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)) : false,
                      AdministrativeFees = i.TypeId == (int)Common.InvoiceType.SubscriptionInvoice ?
                          i.Account_Doctor.Where(acc =>
                            (acc.Account_Products.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)
                          && (acc.Account_Products.ProviderType == (i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                       : (int)i.Health_Entitiy.Health_Entitiy_Type)))
                          .FirstOrDefault().Debit : null,
                      SubscriptionFees = i.TypeId == (int)Common.InvoiceType.SubscriptionInvoice ?
                          i.Account_Doctor.Where(acc =>
                            (acc.Account_Products.ItemId == (int)Common.InvoiceItem.SubscriptionFees)
                          && (acc.Account_Products.ProviderType == (i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                       : (int)i.Health_Entitiy.Health_Entitiy_Type)))
                          .FirstOrDefault().Debit : null,
                      TotalAmount = i.TotalAmount ?? 0,
                      CityId = i.DoctorId.HasValue ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value)
                      .FirstOrDefault().Region.CityID : i.Health_Entitiy.Region.CityID,
                      CityName = i.DoctorId.HasValue ? (i.Doctor.Address_Book.Where(ad => ad.IsMain.Value)
                      .FirstOrDefault().Region.Governrate.Gov_Name_AR) ?? "لايوجد عنوان مسجل"
                      : i.Health_Entitiy.Region.Governrate.Gov_Name_AR ?? "لايوجد عنوان مسجل",
                      RegionId = i.DoctorId.HasValue ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().RegionID
                      : i.Health_Entitiy.Health_Entitiy_RegionID,
                      RegionName = i.DoctorId.HasValue ? (i.Doctor.Address_Book.Where(ad => ad.IsMain.Value)
                      .FirstOrDefault().Region.Region_Name_AR) ?? "لايوجد عنوان مسجل"
                      : (i.Health_Entitiy.Region.Region_Name_AR) ?? "لايوجد عنوان مسجل",
                  }).Where(i =>
                      (search.CityIds.Count()>0 && search.RegionIds.Count() == 0 ?search.CityIds.Contains(i.CityId??0) : true)
                   && ((search.AllRegions.HasValue || search.RegionIds.Count() == 0) ||search.RegionIds.Contains(i.RegionId ?? 0))).ToList()
                   .GroupBy(i => new
                   {
                       InvoiceDate = i.InvoiceDate,
                       CityId =(search.CityIds.Count() > 0||search.AllCity.HasValue)?i.CityId:null,
                       CityName = (search.CityIds.Count() > 0 || search.AllCity.HasValue)?i.CityName:null,
                       RegionId = (search.RegionIds.Count()> 0 ||search.AllRegions.HasValue ||search.AgentId.HasValue)
                       ? i.RegionId : null,
                       RegionName = (search.RegionIds.Count() > 0 || search.AllRegions.HasValue || search.AgentId.HasValue)
                       ? i.RegionName : null,
                   }).Select(i => new InvoiceStatisticsViewModel
                   {
                       InvoiceDate = i.Key.InvoiceDate,
                       CityId = (search.CityIds.Count() > 0 || search.AllCity.HasValue)?i.Key.CityId:null,
                       RegionId = (search.RegionIds.Count() > 0 || search.AllRegions.HasValue || search.AgentId.HasValue)
                       ? i.Key.RegionId:null,
                       CityName = (search.CityIds.Count() > 0 || search.AllCity.HasValue)?i.Key.CityName:null,
                       RegionName = (search.RegionIds.Count() > 0 || search.AllRegions.HasValue || search.AgentId.HasValue)
                       ? i.Key.RegionName:null,
                       summarizeInvoiceProviders = i.GroupBy(p => new
                       {
                           ProviderTypeId = p.ProviderTypeId,
                       }).Select(p => new SummarizeInvoiceProvider
                       {
                           ProviderTypeId = p.Key.ProviderTypeId,
                           NumberOfPaidSubscription = p.Where(x =>
                               (x.InvoiceType == (int)Common.InvoiceType.SubscriptionInvoice)
                             && (x.Status == (int)Common.InvoiceStatus.paid)).Count(),
                           SubscriptionPaidTotal = p.Where(x =>
                               (x.InvoiceType == (int)Common.InvoiceType.SubscriptionInvoice)
                             && (x.Status == (int)Common.InvoiceStatus.paid)).Select(x => x.SubscriptionFees).Sum() ?? 0,
                           NumberOfUnpaidSubscription = p.Where(x =>
                             (x.InvoiceType == (int)Common.InvoiceType.SubscriptionInvoice)
                           && (x.Status == (int)Common.InvoiceStatus.unpaid)).Count(),
                           UnpaidSubscriptionTotal = p.Where(x =>
                                (x.InvoiceType == (int)Common.InvoiceType.SubscriptionInvoice)
                              && (x.Status == (int)Common.InvoiceStatus.unpaid)).Select(x => x.SubscriptionFees).Sum() ?? 0,
                           NumberOfCancelSubscription = p.Where(x =>
                             (x.InvoiceType == (int)Common.InvoiceType.SubscriptionInvoice)
                           && (x.Status == (int)Common.InvoiceStatus.cancel)).Count(),
                           CancelSubscriptionTotal = p.Where(x =>
                                (x.InvoiceType == (int)Common.InvoiceType.SubscriptionInvoice)
                              && (x.Status == (int)Common.InvoiceStatus.cancel)).Select(x => x.SubscriptionFees).Sum() ?? 0,

                           NumberOfPaidReservation = p.Where(x =>
                             (x.InvoiceType == (int)Common.InvoiceType.AppointmentInvoice)
                           && (x.Status == (int)Common.InvoiceStatus.paid)).Count(),
                           ReservationPaidTotal = p.Where(x =>
                               (x.InvoiceType == (int)Common.InvoiceType.AppointmentInvoice)
                             && (x.Status == (int)Common.InvoiceStatus.paid)).Select(x => x.TotalAmount).Sum() ?? 0,
                           NumberOfUnpaidReservation = p.Where(x =>
                             (x.InvoiceType == (int)Common.InvoiceType.AppointmentInvoice)
                           && (x.Status == (int)Common.InvoiceStatus.unpaid)).Count(),
                           UnpaidReservationTotal = p.Where(x =>
                               (x.InvoiceType == (int)Common.InvoiceType.AppointmentInvoice)
                             && (x.Status == (int)Common.InvoiceStatus.unpaid)).Select(x => x.TotalAmount).Sum() ?? 0,
                           NumberOfCancelReservation = p.Where(x =>
                             (x.InvoiceType == (int)Common.InvoiceType.AppointmentInvoice)
                           && (x.Status == (int)Common.InvoiceStatus.cancel)).Count(),
                           CancelReservationTotal = p.Where(x =>
                               (x.InvoiceType == (int)Common.InvoiceType.AppointmentInvoice)
                             && (x.Status == (int)Common.InvoiceStatus.cancel)).Select(x => x.TotalAmount).Sum() ?? 0,

                           NumberOfPaidAdministrative = p.Where(x =>
                            (x.HasAdministrative)
                          && (x.Status == (int)Common.InvoiceStatus.paid)).Count(),
                           AdministrativePaidTotal = p.Where(x =>
                               (x.HasAdministrative)
                             && (x.Status == (int)Common.InvoiceStatus.paid)).Select(x => x.AdministrativeFees).Sum() ?? 0,
                           NumberOfUnpaidAdministrative = p.Where(x =>
                             (x.HasAdministrative)
                           && (x.Status == (int)Common.InvoiceStatus.unpaid)).Count(),
                           UnpaidAdministrativeTotal = p.Where(x =>
                               (x.HasAdministrative)
                             && (x.Status == (int)Common.InvoiceStatus.unpaid)).Select(x => x.AdministrativeFees).Sum() ?? 0,
                           NumberOfCancelAdministrative = p.Where(x =>
                             (x.HasAdministrative)
                           && (x.Status == (int)Common.InvoiceStatus.cancel)).Count(),
                           CancelAdministrativeTotal = p.Where(x =>
                               (x.HasAdministrative)
                             && (x.Status == (int)Common.InvoiceStatus.cancel)).Select(x => x.AdministrativeFees).Sum() ?? 0,
                       }).ToList()
                   }).OrderBy(p => p.CityId).ThenBy(p => p.RegionId).ThenBy(p => p.InvoiceDate).ToList();

            return invoices;
        }
        public List<PaidInvoiceSummarizeViewModel> CreateUnPaidInvoicesSummarizeReport(PaidInvoiceSummarizeSearch search)
        {
            var invoices = context.Invoices.Where(acc =>
                          (!search.From.HasValue || DbFunctions.TruncateTime(acc.InvoiceStartDate) >= search.From
                            && DbFunctions.TruncateTime(acc.InvoiceStartDate) <= search.To)
                        &&(!acc.IsPaid.Value && (!acc.IsCanceled.HasValue || !acc.IsCanceled.Value))
                        &&(search.AgentIds.Count()>0?search.AgentIds.Contains(acc.AgentID??0):true)
                        ).GroupBy(a =>new
                        {
                            day= search.ReportType == (int)Common.ReportType.daily ? a.InvoiceStartDate.Value.Day:1,
                            month= search.ReportType == (int)Common.ReportType.monthly?a.InvoiceStartDate.Value.Month:1,
                            year=a.InvoiceStartDate.Value.Year,
                            agentid = (search.AgentIds.Count() > 0 || search.AllAgents.HasValue) ? a.AgentID : null,
                            agentname= (search.AgentIds.Count() > 0 || search.AllAgents.HasValue) 
                             ? a.Account_Agent.Account_Agent_Name : null,
                        }
                        ).Select(i => new PaidInvoiceSummarizeViewModel
                        {
                            Day=i.Key.day,
                            Month=i.Key.month,
                            Year=i.Key.year,
                            AgentName=i.Key.agentname,
                            AgentId=i.Key.agentid,
                            AdmistrativeTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice)
                            .SelectMany(a => a.Account_Doctor).Where(acc =>
                                    (acc.Account_Products.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)
                                  && (acc.Account_Products.ProviderType == (acc.Invoice.DoctorId != null ? (int)Common.MedicalProviderType.doctor
                               : (int)acc.Invoice.Health_Entitiy.Health_Entitiy_Type))).Select(a => a.Debit).Sum(),
                            SubscriptionTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice)
                            .SelectMany(a => a.Account_Doctor).Where(acc =>
                                    (acc.Account_Products.ItemId == (int)Common.InvoiceItem.SubscriptionFees)
                                  && (acc.Account_Products.ProviderType == (acc.Invoice.DoctorId != null ? (int)Common.MedicalProviderType.doctor
                               : (int)acc.Invoice.Health_Entitiy.Health_Entitiy_Type))).Select(a => a.Debit).Sum(),
                            ReservationTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.AppointmentInvoice)
                              .Select(a => a.TotalAmount).Sum(),
                            OnlineSubscriptionTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.OnlineSubscriptionInvoice)
                              .Select(a => a.TotalAmount).Sum(),
                            HomevisitTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.HomeVisitInvoice)
                              .Select(a => a.TotalAmount).Sum(),
                            PaidTotal = i.Select(a => a.TotalAmount).Sum(),
                            SubscriptionCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice).Count(),
                            ReservationCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.AppointmentInvoice).Count(),
                            AdmistrativeCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice)
                            .SelectMany(a => a.Account_Doctor).Where(acc =>
                                    (acc.Account_Products.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses))
                                    .Select(a => a.InvoiceID).Count(),
                            OnlineSubscriptionCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.OnlineSubscriptionInvoice).Count(),
                            HomevisitCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.HomeVisitInvoice).Count()
                        }).ToList().OrderBy(p => p.AgentId).ThenBy(a=>new DateTime(a.Year,a.Month,a.Day).Date).ToList();
            return invoices;
        }
        public List<PaidInvoiceSummarizeViewModel> CreatePaidInvoicesSummarizeReport(PaidInvoiceSummarizeSearch search)
        {
            var invoices = context.Invoices.Where(acc =>
                          (!search.From.HasValue || DbFunctions.TruncateTime(acc.AcheivementDateTime) >= search.From
                            && DbFunctions.TruncateTime(acc.AcheivementDateTime) <= search.To)
                        && (acc.IsPaid.Value && (!acc.IsCanceled.HasValue || !acc.IsCanceled.Value))
                        && (search.AgentIds.Count() > 0 ? search.AgentIds.Contains(acc.AgentID ?? 0) : true)
                        ).GroupBy(a => new
                        {
                            day = search.ReportType == (int)Common.ReportType.daily ? a.AcheivementDateTime.Value.Day : 1,
                            month = search.ReportType == (int)Common.ReportType.monthly ? a.AcheivementDateTime.Value.Month : 1,
                            year = a.AcheivementDateTime.Value.Year,
                            agentid = (search.AgentIds.Count() > 0 || search.AllAgents.HasValue) ? a.AgentID : null,
                            agentname = (search.AgentIds.Count() > 0 || search.AllAgents.HasValue)
                             ? a.Account_Agent.Account_Agent_Name : null,
                        }
                        ).Select(i => new PaidInvoiceSummarizeViewModel
                        {
                            Day = i.Key.day,
                            Month = i.Key.month,
                            Year = i.Key.year,
                            AgentName = i.Key.agentname,
                            AgentId = i.Key.agentid,
                            AdmistrativeTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice)
                            .SelectMany(a => a.Account_Doctor).Where(acc =>
                                    (acc.Account_Products.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses)
                                  && (acc.Account_Products.ProviderType == (acc.Invoice.DoctorId != null ? (int)Common.MedicalProviderType.doctor
                               : (int)acc.Invoice.Health_Entitiy.Health_Entitiy_Type))).Select(a => a.Credit).Sum(),
                            SubscriptionTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice)
                            .SelectMany(a => a.Account_Doctor).Where(acc =>
                                    (acc.Account_Products.ItemId == (int)Common.InvoiceItem.SubscriptionFees)
                                  && (acc.Account_Products.ProviderType == (acc.Invoice.DoctorId != null ? (int)Common.MedicalProviderType.doctor
                               : (int)acc.Invoice.Health_Entitiy.Health_Entitiy_Type))).Select(a => a.Credit).Sum(),
                            ReservationTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.AppointmentInvoice)
                              .Select(a => a.TotalPaid).Sum(),
                            OnlineSubscriptionTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.OnlineSubscriptionInvoice)
                              .Select(a => a.TotalPaid).Sum(),
                            HomevisitTotal = i.Where(a => a.TypeId == (int)Common.InvoiceType.HomeVisitInvoice)
                              .Select(a => a.TotalPaid).Sum(),
                            PaidTotal = i.Select(a => a.TotalPaid).Sum(),
                            SubscriptionCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice).Count(),
                            ReservationCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.AppointmentInvoice).Count(),
                            AdmistrativeCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.SubscriptionInvoice)
                            .SelectMany(a => a.Account_Doctor).Where(acc =>
                                    (acc.Account_Products.ItemId == (int)Common.InvoiceItem.AdminstrativeExpenses))
                                    .Select(a => a.InvoiceID).Count(),
                            OnlineSubscriptionCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.OnlineSubscriptionInvoice).Count(),
                            HomevisitCount = i.Where(a => a.TypeId == (int)Common.InvoiceType.HomeVisitInvoice).Count()
                        }).ToList().OrderBy(p => p.AgentId).ThenBy(a => new DateTime(a.Year, a.Month, a.Day).Date).ToList();
            return invoices;
        }
        public IPagedList<SummarizeAchievementViewModel> summarizeachievements(int page, SummarizeAchievementSearch search)
        {
            int pagesize = 24;
            int pageindex = page;
            PagedList.IPagedList<SummarizeAchievementViewModel> invoices;
            invoices = context.Invoices.Where(i =>
            (search.SerialNumber != null ? i.SerialNumber == search.SerialNumber : true) &&
               (i.IsCanceled != true)
            && (i.IsPaid == true)
            && (i.AcheivementDateTime.HasValue)
            && (!search.InvoiceType.HasValue || i.TypeId == search.InvoiceType)
             && (!search.AcheviementType.HasValue || i.AcheivementType_Id == search.AcheviementType)
            && (!search.ProviderType.HasValue || (search.ProviderType == 0 ? i.DoctorId.HasValue
                  : i.Health_Entitiy.Health_Entitiy_Type == search.ProviderType))
            && (!search.From.HasValue || (DbFunctions.TruncateTime(i.AcheivementDateTime) >= search.From
                  && DbFunctions.TruncateTime(i.AcheivementDateTime) <= search.ToDate)))
         .Select(i => new SummarizeAchievementViewModel
         {
             InvoiceNumber = i.Invoice_Number,
             ReceiptNumber = i.ReceiptNumber,
             AcheivementDateTime = i.AcheivementDateTime,
             ProviderCode = i.DoctorId.HasValue ? i.Doctor.Doctor_CODE : i.Health_Entitiy.Health_Entitiy_ContractID,
             ProviderName = i.DoctorId.HasValue ? i.Doctor.Doctor_Name_EN : i.Health_Entitiy.Health_Entitiy_NameEN,
             ProviderPhone = i.DoctorId.HasValue ? i.Doctor.PhoneNumber1_Master : i.Health_Entitiy.Health_Entitiy_MasterPhone,
             AcheivementTypeName = i.AcheivementType == null ? "" : i.AcheivementType.Name_Ar,
             AcheivementTotal = i.TotalPaid ?? 0,
             AgentName = i.Account_Agent.Account_Agent_Name,
             InvoiceType = i.InvoiceType.TypeName,
             ProviderRegion = i.DoctorId.HasValue ? i.Doctor.Address_Book.Where(ad => ad.IsMain == true).FirstOrDefault().Region.Region_Name_AR
                                 : i.Health_Entitiy.Region.Region_Name_AR,
             InvcoiceChar = i.ReceiptChar == null ? "" : i.ReceiptChar,
             SerialNumber = i.SerialNumber == null ? 0 : i.SerialNumber
         })
         .OrderByDescending(i => i.SerialNumber).ToPagedList(pageindex, pagesize);

            return invoices;
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}