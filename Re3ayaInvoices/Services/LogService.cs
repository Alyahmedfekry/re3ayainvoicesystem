﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using PagedList;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
namespace Re3ayaInvoices.Services
{
    public class LogService : IDisposable
    {
        private readonly Re3aya247Entities context;
        public LogService()
        {
            context = new Re3aya247Entities();
        }
        public IPagedList<InvoiceLogViewModel>invoiceLogs(InvoiceLogSearch search,int?page)
        {
            var loglist = InvoiceLogQuery(search).OrderByDescending(u => u.CreatedDate).ToPagedList(page ?? 1, 10);

            return loglist;
        }
        public IQueryable<InvoiceLogViewModel> InvoiceLogQuery(InvoiceLogSearch search)
        {
            var loglist = context.InvoiceLogs.Where(l =>
            (!search.UserId.HasValue || l.UserId == search.UserId)
         && (!search.ActionId.HasValue || l.Action_TypeId == search.ActionId)
         && (!search.From.HasValue || (DbFunctions.TruncateTime(l.CreatedDate) >= search.From
                     && DbFunctions.TruncateTime(l.CreatedDate) <= search.To))
                 ).Select(l => new InvoiceLogViewModel
                 {
                     Id = l.Id,
                     ActionName = l.Invoice_ActionType.NameAr,
                     CreatedDate = l.CreatedDate,
                     InvoiceNumber = l.Invoice.Invoice_Number.Value,
                     UserName = l.UserId.HasValue ? l.User.FullName : ""
                 });

            return loglist;
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}