﻿using PagedList;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Re3ayaInvoices.Services
{
    public class CommonService : IDisposable
    {
        const string new_server_key = "AAAA8oeDEBM:APA91bFc_YjSPkCQ52T8TO7SSFiPgqOoeyGe037xZiXV03YkLNB1n6FtsUgvfRzSVXe4Q-ao4BzPeDCOVOsdrDXSkeJkhqmztUR8StkS_DWHM2vrYUjLZMTpt4kFQddCBDo-q4CPQlUi";
        const long new_sender_id = 1041655599123;
        private readonly Re3aya247Entities context;
        public CommonService()
        {
            context = new Re3aya247Entities();
        }
        public List<int> GetActivatedProviderIds(int providertype, string providercode, int? cityid)
        {
            List<string> providercodes = new List<string>();
            if (!string.IsNullOrWhiteSpace(providercode))
            {
                providercodes = providercode.Trim().Split(',').ToList();
            }
            var ids = new List<int>();
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                ids = context.Doctors.Where(d =>
               (d.Is_Active == true)
            && (!d.CardId.HasValue)
            && (string.IsNullOrEmpty(providercode.Trim()) || providercodes.Contains(d.Doctor_CODE))
            && (!cityid.HasValue || d.Address_Book.Any(m => !m.EntityId.HasValue && m.Region.CityID == cityid))
            ).Select(d => d.DoctorID)
            .ToList();
            }
            else
            {
                ids = context.Health_Entitiy.Where(h =>
                   (!h.IsActive.HasValue || h.IsActive.Value)
                && (h.Health_Entitiy_Type == providertype)
                && (!h.Health_Entity_ParentID.HasValue)
                && (string.IsNullOrEmpty(providercode.Trim()) || providercodes.Contains(h.Health_Entitiy_ContractID))
                && (!cityid.HasValue || h.Region.CityID == cityid)
           ).Select(d => d.Health_Entitiy_ID).ToList();
            }

            return ids;
        }
        public IEnumerable<SelectListItem> AcheivementTypes()
        {
            return new SelectList(context.AcheivementTypes.Select(r => new
            {
                Text = r.Name_Ar,
                Value = r.Id
            }).ToList(), "Value", "Text");

        }
        public IEnumerable<SelectListItem> AgentList(int? selected = null)
        {
            var agents = context.Account_Agent.ToList().Select(a => new SelectListItem
            {
                Text = a.Account_Agent_Name,
                Value = a.Account_Agent_ID.ToString(),
                Selected = a.Account_Agent_ID == selected
            });

            return agents;
        }
        public MultiSelectList AgentMultilist()
        {
            var agents = new MultiSelectList(context.Account_Agent.ToList()
            .Select(g => new
            {
                Text = g.Account_Agent_Name,
                Value = g.Account_Agent_ID.ToString(),
            }), "Value", "Text");

            return agents;
        }
        public IEnumerable<SelectListItem> CityList()
        {
            var cities = context.Governrates.ToList().Select(g => new SelectListItem
            {
                Text = g.Gov_Name_AR,
                Value = g.GovID.ToString()
            });

            return cities;
        }
        public MultiSelectList CityMultilist()
        {
            var cities = new MultiSelectList(context.Governrates.ToList()
             .Select(g => new
             {
                 Text = g.Gov_Name_AR,
                 Value = g.GovID.ToString()
             }), "Value", "Text");

            return cities;
        }
        public SelectList InvoiceTypeSelectList(int? selected = null)
        {
            return new SelectList(context.InvoiceTypes.Select(r => new
            {
                Text = r.TypeName,
                Value = r.Id
            }), "Value", "Text", selected);
        }
        public MultiSelectList RegionMultiList(int? cityid = null)
        {
            var regions = new MultiSelectList(context.Regions.Where(r => !cityid.HasValue || r.CityID == cityid)
              .Select(g => new
              {
                  Text = g.Region_Name_AR,
                  Value = g.RegionID
              }), "Value", "Text");

            return regions;
        }
        public MultiSelectList RegionMultiList(List<int> cityid)
        {
            if (cityid == null)
            {
                cityid = new List<int>();
            }
            var regions = new MultiSelectList(context.Regions.Where(r => cityid.Count() == 0 || cityid.Contains(r.CityID))
              .Select(g => new
              {
                  Text = g.Region_Name_AR,
                  Value = g.RegionID
              }), "Value", "Text");

            return regions;
        }
        public List<SummarizeAchievementViewModel> summarizeachievementsForWhatsApp(SummarizeAchievementSearch search)
        {

            List<SummarizeAchievementViewModel> invoices;

            invoices = context.Invoices.Where(i =>
            (search.SerialNumber != null ? i.SerialNumber == search.SerialNumber : true) &&
               (i.IsCanceled != true)
            && (i.IsPaid == true)
            && (i.AcheivementDateTime.HasValue)
            && (!search.InvoiceType.HasValue || i.TypeId == search.InvoiceType)
             && (!search.AcheviementType.HasValue || i.AcheivementType_Id == search.AcheviementType)
            && (!search.ProviderType.HasValue || (search.ProviderType == 0 ? i.DoctorId.HasValue
                  : i.Health_Entitiy.Health_Entitiy_Type == search.ProviderType))
            && (!search.From.HasValue || (DbFunctions.TruncateTime(i.AcheivementDateTime) >= search.From
                  && DbFunctions.TruncateTime(i.AcheivementDateTime) <= search.ToDate)))
         .Select(i => new SummarizeAchievementViewModel
         {
             InvoiceNumber = i.Invoice_Number,
             ProviderPhone = i.DoctorId.HasValue ? i.Doctor.PhoneNumber1_Master : i.Health_Entitiy.Health_Entitiy_MasterPhone,
             InvoiceId = i.InvoiceID,
             SerialNumber = i.SerialNumber == null ? 0 : i.SerialNumber
         })
         .OrderByDescending(i => i.SerialNumber).ToList();

            return invoices;
        }
        public void SendWhatsAppMessage(int? invoice_id)
        {
            #region SMS

            var InvoiceInfo = context.Invoices.Where(m => m.InvoiceID == invoice_id).FirstOrDefault();
            var message = "";
            if (InvoiceInfo.TypeId == 1)
            {
                message = "عميلنا العزيز رعاية ٢٤٧ تبلغكم بانة تم سداد فاتورة الاشتراك السنوى لسنة " + InvoiceInfo.InvoiceStartDate.Value.Year + " بقيمة " + InvoiceInfo.TotalAmount.Value.ToString("0.00") + " للتفاصيل اضغط الرابط" + Environment.NewLine + "http://re3.io/BXIQ17";
            }
            else if (InvoiceInfo.TypeId == 2)
            {
                message = "عميلنا العزيز رعاية ٢٤٧ تبلغكم بانة تم سداد فاتورة الحجوزات لشهر " + InvoiceInfo.InvoiceStartDate.Value.ToString("MMMM yyyy") + " بقيمة " + InvoiceInfo.TotalAmount.Value.ToString("0.00") + " للتفاصيل اضغط الرابط" + Environment.NewLine + "http://re3.io/BXIQ17";
            }
            else
            {
                message = "عميلنا العزيز رعاية ٢٤٧ تبلغكم بانة تم سداد فاتورة الاشتراك الاونلاين لسنة " + InvoiceInfo.InvoiceStartDate.Value.Year + " بقيمة " + InvoiceInfo.TotalAmount.Value.ToString("0.00") + " للتفاصيل اضغط الرابط" + Environment.NewLine + "http://re3.io/BXIQ17";

            }
            ////http://re3.io/BXIQ17 ==>https://re3aya247.com/ar/Doctorslogin?returnurl=/arDoctorP/DoctorInvoices
            var phoneNumber = InvoiceInfo.DoctorId.HasValue ? InvoiceInfo.Doctor.PhoneNumber1_Master : InvoiceInfo.Health_Entitiy.Health_Entitiy_MasterPhone;
            //SMSMisr.SendMessage(phoneNumber, message);
            //CommonServices.WhatsappSender("2" + phoneNumber, message);
            /*foreach (var item in context.DoctorDevices.Where(m => m.DoctorID == InvoiceInfo.DoctorId).ToList())
            {
                CommonServices.SendNotification(item.DeviceToken, "رعاية 247", message);
            }*/
            //var text = "https://web.whatsapp.com/send?phone=" + "+2" + phoneNumber + "&text=" + message;
            WhatsappSenderForInvoices("2" + phoneNumber, message);
            #endregion
        }
        public static String WhatsappSenderForInvoices(string Phone, string Body)
        {
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("http://re3.io/Home/WhatsappSender?key=MNO745$%_TUV__HHD&Phonenumber=" + Phone + "&Messagebody=" + Body + "&Isinvoice=true");
            myRequest.Method = "GET";
            WebResponse myResponse = myRequest.GetResponse();
            StreamReader sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
            string result = sr.ReadToEnd();
            sr.Close();
            myResponse.Close();
            return result.Replace('"', ' ');
        }
        public SelectList InvoiceTypes()
        {
            return new SelectList(context.InvoiceTypes.Select(r => new
            {
                Text = r.TypeName,
                Value = r.Id
            }), "Value", "Text");
        }
        public SelectList ControllerList()
        {
            return new SelectList(context.SystemControllers.ToList().Select(r => new
            {
                Text = Common.Ase_DecryptString(r.ControllerName),
                Value = r.ControllerID
            }), "Value", "Text");
        }
        public SelectList ParentSystemMenus()
        {
            return new SelectList(context.SystemMenus.Where(s => !s.ActionId.HasValue).ToList().Select(r => new
            {
                Text = Common.Ase_DecryptString(r.NameAr),
                Value = r.Id
            }), "Value", "Text");
        }
        public SelectList SystemActionList()
        {
            return new SelectList(context.SystemActions.Where(s => s.SystemController.AppId == 1).ToList().Select(r => new
            {
                Text = Common.Ase_DecryptString(r.SystemController.VisualName) + "-"
                   + Common.Ase_DecryptString(r.ActionName),
                Value = r.ActionID
            }), "Value", "Text");
        }
        public SelectList SystemMenuList()
        {
            return new SelectList(context.SystemMenus.Where(s => s.AppId == Common.AppId).ToList().Select(r => new
            {
                Text = Common.Ase_DecryptString(r.NameAr),
                Value = r.Id
            }), "Value", "Text");
        }
        public MultiSelectList RoleMultiList()
        {
            var roles = new MultiSelectList(context.SystemRoles.Where(r => r.AppId == (int)Common.AppId)
              .ToList().Select(g => new
              {
                  Text = Common.Ase_DecryptString(g.RoleTitleArabic),
                  Value = g.RoleID.ToString()
              }), "Value", "Text");

            return roles;
        }
        public SelectList UsersList(int? selected = null)
        {
            return new SelectList(context.Users.Where(s => s.AppId == Common.AppId).ToList()
                .Select(r => new
                {
                    Text = Common.Ase_DecryptString(r.FullName),
                    Value = r.Id,
                    selected = selected.HasValue ? selected == r.Id : false
                }), "Value", "Text");
        }
        public SelectList Action_TypeList(int? selected = null)
        {
            return new SelectList(context.Invoice_ActionType
                .Select(r => new
                {
                    Text =r.NameAr,
                    Value = r.Id,
                    selected = selected.HasValue ? selected == r.Id : false
                }), "Value", "Text");
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}
