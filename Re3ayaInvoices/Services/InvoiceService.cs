﻿using PagedList;
using Re3ayaInvoices.Helpers;
using Re3ayaInvoices.Models;
using Re3ayaInvoices.viewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
namespace Re3ayaInvoices.Services
{
    public class InvoiceService : IDisposable
    {
        private readonly Re3aya247Entities context;
        private readonly CommonService commonServices;
        public InvoiceService()
        {
            context = new Re3aya247Entities();
            commonServices = new CommonService();
        }
        public IPagedList<InvoiceSummaryViewModel> MonthlyInvoicesSummarization(int? pagesize, int? page, InvoiceSummarizationSearch search)
        {
            var summary = context.Invoices.Where(i =>
               (!search.InvoiceType.HasValue || i.TypeId == search.InvoiceType)
             && (!search.From.HasValue || (DbFunctions.TruncateTime(i.InvoiceStartDate) >= search.From
                    && DbFunctions.TruncateTime(i.InvoiceStartDate) <= search.To)))
             .GroupBy(i => i.InvoiceStartDate).Select(i => new InvoiceSummaryViewModel
             {
                 InvoiceDate = i.Key.Value,
                 NumberOfInvoices = i.Count(),
                 InvoiceSummaryMedicalProvider = i.GroupBy(p =>
                      p.DoctorId.HasValue ? 0 : p.Health_Entitiy.Health_Entitiy_Type)
                    .Select(p => new InvoiceSummaryMedicalProvider
                    {
                        ProviderType = p.Key.Value,
                        NumberOfInvoices = p.Count()
                    }).ToList()
             }).OrderByDescending(i => i.InvoiceDate).ToPagedList(page ?? 1, pagesize ?? 20);

            return summary;
        }
        public IPagedList<InvoiceViewModel> Invoices(int? page, InvoiceSearch search)
        {
            int pagesize = 10;
            PagedList.IPagedList<InvoiceViewModel> invoices;
            if (search.ProviderType == (int)Common.MedicalProviderType.doctor)
            {
                if (search.MathOperatorId.HasValue)
                {
                invoices = SumDoctorInvocies(search).OrderByDescending(i => i.TotalAmount).ToPagedList(page ?? 1, pagesize);
                }
                else
                {
                    invoices = DoctorInvocies(search).OrderByDescending(i => i.InvoiceDate).ToPagedList(page ?? 1, pagesize);
                }
            }
            else
            {
                invoices = Invoices(search).OrderByDescending(i => i.InvoiceDate).ToPagedList(page ?? 1, pagesize);
            }

            return invoices;
        }
        public IQueryable<InvoiceViewModel> Invoices(InvoiceSearch search)
        {
            var invoices = context.Invoices.Where(m =>
                   (!m.DoctorId.HasValue)
                && (!search.type.HasValue || m.TypeId == search.type)
                && (!search.From.HasValue || (DbFunctions.TruncateTime(m.InvoiceStartDate) >= search.From
                     && DbFunctions.TruncateTime(m.InvoiceStartDate) <= search.To))
                && (!search.Status.HasValue ||
                       (search.Status.Value == (int)Common.InvoiceStatus.paid
                         ? (m.IsPaid.Value && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                     : search.Status.Value == (int)Common.InvoiceStatus.unpaid
                         ? ((!m.IsPaid.HasValue || !m.IsPaid.Value) && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                     : search.Status.Value == (int)Common.InvoiceStatus.cancel
                         ? m.IsCanceled.Value : true))
                && (!search.InvoiceNumber.HasValue || m.Invoice_Number == search.InvoiceNumber)
                && (search.AgentIds.Count() == 0 || search.AgentIds.Contains(m.AgentID ?? 0))
                && (!search.ProviderType.HasValue || m.Health_Entitiy.Health_Entitiy_Type == search.ProviderType)
                && (!search.MathOperatorId.HasValue || (search.MathOperatorId == 1 ? m.TotalAmount == search.InvoiceTotal
                  :search.MathOperatorId == 2 ? m.TotalAmount != search.InvoiceTotal : search.MathOperatorId == 3
                  ?search.InvoiceTotal > m.TotalAmount : search.MathOperatorId == 4 ? search.InvoiceTotal >= m.TotalAmount
                  :search.MathOperatorId == 5 ? search.InvoiceTotal < m.TotalAmount
                  :search.MathOperatorId == 6 ? search.InvoiceTotal <= m.TotalAmount : true)))
                .Select(i => new InvoiceViewModel
                {
                    InvoiceId = i.InvoiceID,
                    InvoiceDate = i.InvoiceStartDate,
                    Invoice_Number = i.Invoice_Number,
                    TotalAmount = i.TotalAmount,
                    SerialNumber = i.SerialNumber ?? 0,
                    IsPaid = i.IsPaid ?? false,
                    IsPaidBefore = i.IsPaidBefore ?? false,
                    IsCancel = i.IsCanceled ?? false,
                    TypeName = i.InvoiceType.TypeName,
                    AgentName = i.AgentID.HasValue ? i.Account_Agent.Account_Agent_Name : "غير مسجل",
                    ProviderCode = i.Health_Entitiy.Health_Entitiy_ContractID,
                    providerName = i.Health_Entitiy.Health_Entitiy_NameAR,
                    ProviderAddress = i.Health_Entitiy.Health_Entitiy_AddressAR,
                    ProviderActiveStatus = i.Health_Entitiy.IsActive ?? true,
                    ProviderType = i.Health_Entitiy.Health_Entitiy_Type,
                    RegionId = i.Health_Entitiy.Health_Entitiy_RegionID,
                    RegionName = i.Health_Entitiy.Region.Region_Name_AR,
                    CityId = i.Health_Entitiy.Region.CityID,
                    CityName = i.Health_Entitiy.Region.Governrate.Gov_Name_AR
                }).Where(a =>
                 (search.CityIds.Count() > 0 && search.RegionIds.Count() == 0 ? search.CityIds.Contains(a.CityId ?? 0) : true)
              && (search.RegionIds.Count() == 0 || search.RegionIds.Contains(a.RegionId ?? 0))
              && (string.IsNullOrEmpty(search.ProviderCode) || a.ProviderCode == search.ProviderCode.Trim())
              && (string.IsNullOrEmpty(search.ProviderName)
                    || a.providerName.Trim().ToLower().Contains(search.ProviderName.Trim().ToLower())));

            return invoices;
        }
        public IQueryable<InvoiceViewModel> DoctorInvocies(InvoiceSearch search)
        {
            if (search.ProviderHas.HasValue)
            {
                return DoctorHas(search);
            }
            if (search.MathOperatorId.HasValue)
            {
                return SumDoctorInvocies(search);
            }
            var invoices = context.Invoices.Where(m =>
              (!search.type.HasValue || m.TypeId == search.type)
           && (m.DoctorId.HasValue)
           && (!search.From.HasValue || (DbFunctions.TruncateTime(m.InvoiceStartDate) >= search.From
               && DbFunctions.TruncateTime(m.InvoiceStartDate) <= search.To))
           && (!search.Status.HasValue || (search.Status.Value == (int)Common.InvoiceStatus.paid
                  ? (m.IsPaid.Value && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                  : search.Status.Value == (int)Common.InvoiceStatus.unpaid
                  ? ((!m.IsPaid.HasValue || !m.IsPaid.Value) && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                  : search.Status.Value == (int)Common.InvoiceStatus.cancel
                  ? m.IsCanceled.Value : true))
           && (!search.InvoiceNumber.HasValue || m.Invoice_Number == search.InvoiceNumber)
           && (search.AgentIds.Count() == 0 || search.AgentIds.Contains(m.AgentID ?? 0))
           &&(!search.MathOperatorId.HasValue||(search.MathOperatorId==1?m.TotalAmount==search.InvoiceTotal
                 :search.MathOperatorId==2?m.TotalAmount!=search.InvoiceTotal:search.MathOperatorId==3
                 ?search.InvoiceTotal>m.TotalAmount:search.MathOperatorId==4?search.InvoiceTotal>=m.TotalAmount
                 :search.MathOperatorId==5?search.InvoiceTotal<m.TotalAmount
                 :search.MathOperatorId==6?search.InvoiceTotal<=m.TotalAmount:true)))
            .Select(i => new InvoiceViewModel
            {
                InvoiceId = i.InvoiceID,
                InvoiceDate = i.InvoiceStartDate,
                Invoice_Number = i.Invoice_Number,
                TotalAmount = i.TotalAmount,
                SerialNumber = i.SerialNumber ?? 0,
                IsPaid = i.IsPaid ?? false,
                IsPaidBefore=i.IsPaidBefore??false,
                IsCancel = i.IsCanceled ?? false,
                TypeName = i.InvoiceType.TypeName,
                AgentName = i.AgentID.HasValue ? i.Account_Agent.Account_Agent_Name : "غير مسجل",
                ProviderCode = i.Doctor.Doctor_CODE,
                providerName = i.Doctor.Doctor_Name_AR,
                ProviderAddress = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Address_Name_AR,
                ProviderActiveStatus = i.Doctor.Is_Active,
                ProviderType = 0,
                RegionId = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().RegionID,
                RegionName = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR,
                CityId = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.CityID,
                CityName = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Governrate.Gov_Name_AR,
            }).Where(a =>
                 (search.CityIds.Count() > 0 && search.RegionIds.Count() == 0 ? search.CityIds.Contains(a.CityId ?? 0) : true)
              && (search.RegionIds.Count() == 0 || search.RegionIds.Contains(a.RegionId ?? 0))
              && (string.IsNullOrEmpty(search.ProviderCode) || a.ProviderCode == search.ProviderCode.Trim())
              && (string.IsNullOrEmpty(search.ProviderName)
                    || a.providerName.Trim().ToLower().Contains(search.ProviderName.Trim().ToLower())));

            return invoices;
        }
        public IQueryable<InvoiceViewModel> SumDoctorInvocies(InvoiceSearch search)
        {
            var invoices = context.Invoices.Where(m =>
              (!search.type.HasValue || m.TypeId == search.type)
           && (m.DoctorId.HasValue)
           && (!search.From.HasValue || (DbFunctions.TruncateTime(m.InvoiceStartDate) >= search.From
               && DbFunctions.TruncateTime(m.InvoiceStartDate) <= search.To))
           && (!search.Status.HasValue || (search.Status.Value == (int)Common.InvoiceStatus.paid
                  ? (m.IsPaid.Value && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                  : search.Status.Value == (int)Common.InvoiceStatus.unpaid
                  ? ((!m.IsPaid.HasValue || !m.IsPaid.Value) && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                  : search.Status.Value == (int)Common.InvoiceStatus.cancel
                  ? m.IsCanceled.Value : true))
           //&& (search.AgentIds.Count() == 0 || search.AgentIds.Contains(m.AgentID ?? 0))
           && (!search.MathOperatorId.HasValue || (search.MathOperatorId == 1 ? m.TotalAmount == search.InvoiceTotal
                 : search.MathOperatorId == 2 ? m.TotalAmount != search.InvoiceTotal : search.MathOperatorId == 3
                 ? search.InvoiceTotal > m.TotalAmount : search.MathOperatorId == 4 ? search.InvoiceTotal >= m.TotalAmount
                 : search.MathOperatorId == 5 ? search.InvoiceTotal < m.TotalAmount
                 : search.MathOperatorId == 6 ? search.InvoiceTotal <= m.TotalAmount : true)))
            .GroupBy(d=>d.DoctorId).Select(i => new InvoiceViewModel
            {
                TotalAmount = i.Sum(d=>d.TotalAmount),
                ProviderCode = i.Select(d=>d.Doctor.Doctor_CODE).FirstOrDefault(),
                providerName = i.Select(d=>d.Doctor.Doctor_Name_AR).FirstOrDefault(),
                ProviderAddress = i.Select(d=>d.Doctor).FirstOrDefault().Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Address_Name_AR,
                ProviderActiveStatus = i.Select(d=>d.Doctor.Is_Active).FirstOrDefault(),
                ProviderType = 0,
                RegionId = i.Select(d=>d.Doctor).FirstOrDefault().Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().RegionID,
                RegionName = i.Select(d=>d.Doctor).FirstOrDefault().Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR,
                CityId = i.Select(d=>d.Doctor).FirstOrDefault().Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.CityID,
                CityName = i.Select(d=>d.Doctor).FirstOrDefault().Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Governrate.Gov_Name_AR,
            }).Where(a =>
                 (search.CityIds.Count() > 0 && search.RegionIds.Count() == 0 ? search.CityIds.Contains(a.CityId ?? 0) : true)
              && (search.RegionIds.Count() == 0 || search.RegionIds.Contains(a.RegionId ?? 0))
              && (string.IsNullOrEmpty(search.ProviderCode) || a.ProviderCode == search.ProviderCode.Trim())
              && (string.IsNullOrEmpty(search.ProviderName)
                    || a.providerName.Trim().ToLower().Contains(search.ProviderName.Trim().ToLower())));

            return invoices;
        }
        IQueryable<InvoiceViewModel> DoctorHas(InvoiceSearch search)
        {
            if (search.ProviderHas.Value == 1)
            {
                var invoices = context.Invoices.Where(m =>
        (m.DoctorId.HasValue)
     && (DbFunctions.TruncateTime(m.InvoiceStartDate).Value.Year == DateTime.Now.Year)
     && ((!m.IsCanceled.HasValue || !m.IsCanceled.Value))
     && (!search.InvoiceNumber.HasValue || m.Invoice_Number == search.InvoiceNumber)
     && (search.AgentIds.Count() == 0 || search.AgentIds.Contains(m.AgentID ?? 0)))
     .GroupBy(i => i.DoctorId).Where(g => g.Count() == 1
               && !g.Select(a => a.TypeId).Contains((int)Common.InvoiceType.AppointmentInvoice))
     .SelectMany(i => i).Select(i => new InvoiceViewModel
     {
         InvoiceId = i.InvoiceID,
         InvoiceDate = i.InvoiceStartDate,
         Invoice_Number = i.Invoice_Number,
         TotalAmount = i.TotalAmount,
         SerialNumber = i.SerialNumber ?? 0,
         IsPaid = i.IsPaid ?? false,
         IsCancel = i.IsCanceled ?? false,
         TypeName = i.InvoiceType.TypeName,
         AgentName = i.AgentID.HasValue ? i.Account_Agent.Account_Agent_Name : "غير مسجل",
         ProviderCode = i.Doctor.Doctor_CODE,
         providerName = i.Doctor.Doctor_Name_AR,
         ProviderAddress = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Address_Name_AR,
         ProviderActiveStatus = i.Doctor.Is_Active,
         ProviderType = 0,
         RegionId = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().RegionID,
         RegionName = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR,
         CityId = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.CityID,
         CityName = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Governrate.Gov_Name_AR,
     }).Where(a =>
           (search.CityIds.Count() > 0 && search.RegionIds.Count() == 0 ? search.CityIds.Contains(a.CityId ?? 0) : true)
        && (search.RegionIds.Count() == 0 || search.RegionIds.Contains(a.RegionId ?? 0))
        && (string.IsNullOrEmpty(search.ProviderCode) || a.ProviderCode == search.ProviderCode.Trim())
        && (string.IsNullOrEmpty(search.ProviderName)
              || a.providerName.Trim().ToLower().Contains(search.ProviderName.Trim().ToLower())));

                return invoices;
            }
            else
            {
                var invoices1 = context.Invoices.Where(m =>
          (m.DoctorId.HasValue)
       && (DbFunctions.TruncateTime(m.InvoiceStartDate).Value.Year == DateTime.Now.Year)
       && (!search.InvoiceNumber.HasValue || m.Invoice_Number == search.InvoiceNumber)
       && (search.AgentIds.Count() == 0 || search.AgentIds.Contains(m.AgentID ?? 0)))
     .GroupBy(i => i.DoctorId)
     .Where(g =>
           (g.Any(d=>d.TypeId==(int)Common.InvoiceType.AppointmentInvoice
                  &&(!d.IsCanceled.HasValue || !d.IsCanceled.Value)))
         &&(g.Any(d => !search.From.HasValue || (d.TypeId == (int)Common.InvoiceType.AppointmentInvoice 
             && (DbFunctions.TruncateTime(d.InvoiceStartDate) >= search.From
                       && DbFunctions.TruncateTime(d.InvoiceStartDate) <= search.To))))
         &&(g.Any(d => d.TypeId == (int)Common.InvoiceType.SubscriptionInvoice
             &&(!d.IsPaid.HasValue || !d.IsPaid.Value))))
     .SelectMany(i => i).Where(i=>i.TypeId==(int)Common.InvoiceType.SubscriptionInvoice)
     .Select(i => new InvoiceViewModel
     {
         InvoiceId = i.InvoiceID,
         InvoiceDate = i.InvoiceStartDate,
         Invoice_Number = i.Invoice_Number,
         TotalAmount = i.TotalAmount,
         SerialNumber = i.SerialNumber ?? 0,
         IsPaid = i.IsPaid ?? false,
         IsCancel = i.IsCanceled ?? false,
         Type = i.TypeId,
         TypeName = i.InvoiceType.TypeName,
         AgentName = i.AgentID.HasValue ? i.Account_Agent.Account_Agent_Name : "غير مسجل",
         ProviderCode = i.Doctor.Doctor_CODE,
         providerName = i.Doctor.Doctor_Name_AR,
         ProviderAddress = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Address_Name_AR,
         ProviderActiveStatus = i.Doctor.Is_Active,
         ProviderType = 0,
         RegionId = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().RegionID,
         RegionName = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR,
         CityId = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.CityID,
         CityName = i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Governrate.Gov_Name_AR,
     }).Where(a =>
           (search.CityIds.Count() > 0 && search.RegionIds.Count() == 0 ? search.CityIds.Contains(a.CityId ?? 0) : true)
        && (search.RegionIds.Count() == 0 || search.RegionIds.Contains(a.RegionId ?? 0))
        && (string.IsNullOrEmpty(search.ProviderCode) || a.ProviderCode == search.ProviderCode.Trim())
        && (string.IsNullOrEmpty(search.ProviderName)
              || a.providerName.Trim().ToLower().Contains(search.ProviderName.Trim().ToLower())));

                return invoices1;
            }
        }
        public InvoiceViewModel InvoiceDetails(int id)
        {
            var result = context.Invoices.Where(i => i.InvoiceID == id)
              .Select(i => new InvoiceViewModel
              {
                  InvoiceId = i.InvoiceID,
                  Invoice_Number = i.Invoice_Number,
                  InvoiceCreationDatetime = i.InvoiceCreationDatetime,
                  InvoiceDate = i.InvoiceStartDate,
                  AcheivementDateTime = i.AcheivementDateTime,
                  AgentName = i.Account_Agent.Account_Agent_Name,
                  providerName = i.DoctorId.HasValue ? i.Doctor.Doctor_Name_EN : i.Health_Entitiy.Health_Entitiy_NameEN,
                  ProviderType = i.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor : i.Health_Entitiy.Health_Entitiy_Type,
                  RegionName = i.DoctorId.HasValue ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault()
                              .Region.Region_Name_AR : i.Health_Entitiy.Region.Region_Name_AR,
                  TotalAmount = i.TotalAmount ?? 0,
                  TotalPaid = i.TotalPaid ?? 0,
                  ReceiptNumber = i.ReceiptNumber,
                  ReceiptChar = i.ReceiptChar,
                  AcheivementTypeName = i.AcheivementType_Id.HasValue ? i.AcheivementType.Name_Ar : "",
                  invoiceDetails = i.Account_Doctor.Select(acc => new InvoiceDetailsViewModel
                  {
                      Id = acc.Account_Doctor_ID,
                      ItemName = acc.Account_Products.Account_Product_Name_AR,
                      Debit = acc.Debit ?? 0,
                      Credit = acc.Credit ?? 0
                  }).ToList()
              }).SingleOrDefault();

            return result;
        }
        public void CreateInvoice(int providertype, int providerid, int invoicetype, DateTime invoicedate)
        {
            switch (invoicetype)
            {
                case (int)Common.InvoiceType.SubscriptionInvoice:
                    CreateSubscriptionInvoice(providertype, providerid, invoicedate);
                    break;
                case (int)Common.InvoiceType.AppointmentInvoice:
                    CreateAppointmentInvoice(providertype, providerid, invoicedate);
                    break;
                case (int)Common.InvoiceType.OnlineSubscriptionInvoice:
                    CreateOnlineSubscriptionInvoice(providerid, invoicedate);
                    break;
                case (int)Common.InvoiceType.HomeVisitInvoice:
                    CreateHomeVisitInvoiceInvoice(providerid, invoicedate);
                    break;
            }
        }
        public void CreateAppointmentInvoice(int providertype, int providerid, DateTime fromdate, DateTime todate)
        {
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                int invoicetype = (int)Common.InvoiceType.AppointmentInvoice;
                var invoiceexist = context.Invoices.Any(acc =>
                                   (acc.DoctorId == providerid)
                                && (DbFunctions.TruncateTime(acc.InvoiceStartDate) == fromdate
                                     && DbFunctions.TruncateTime(acc.InvoiceEnddate) == todate)
                                && (acc.TypeId == invoicetype)
                                && (!acc.IsCanceled.HasValue || !acc.IsCanceled.Value));
                if (!invoiceexist)
                {
                    List<Invoice_Appointment> appointmentids = new List<Invoice_Appointment>();
                    AgentRegion agentregion = null;
                    int? agentid = 67;
                    decimal? total = 0;
                    var maxnumber =DateTime.Now.Year>=2022?context.Invoices.Where(i=>i.CompanyId==(int)Common.CompanyId.Wize).Max(n=>n.Invoice_Number)??0
                        : context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Reaya).Max(i => i.Invoice_Number) ?? 0;
                    var addresses = context.Address_Book.Where(ad => ad.Doctor_ID == providerid).Select(a =>
                    new
                    {
                        id = a.AddressBook_ID,
                        ismain = a.IsMain ?? false,
                        regionid = a.RegionID
                    }).ToList();
                    var mainaddress = addresses.Where(ad => ad.ismain).FirstOrDefault();
                    if (mainaddress != null)
                    {
                        var regionid = mainaddress.regionid;
                        agentregion = context.AgentRegions.Where(r =>
                       (r.RegionId == regionid)
                       && (r.Account_Agent.IsActive == true)).FirstOrDefault();
                        if (agentregion != null)
                        {
                            agentid = agentregion.AgentId;
                        }
                    }
                    var products = context.Account_Products.Where(p =>
                       (p.InvoiceItem.IsActive.Value)
                    && (p.ProviderType == providertype)
                    && (p.InvoiceTypeId == invoicetype)
                    ).Select(p => new
                    {
                        itemid = p.ItemId,
                        Account_Porduct_ID = p.Account_Porduct_ID,
                        Account_Product_FixedPrice = p.Account_Product_FixedPrice
                    }).ToList();
                    int confirmedappointmentitem = (int)Common.InvoiceItem.ConfirmedAppointments;
                    var confirmedappointmentproduct = products.Where(p => p.itemid == confirmedappointmentitem)
                             .SingleOrDefault();
                    int unconfirmedappointmentitem = (int)Common.InvoiceItem.Nonconfirmedappointments;
                    var unconfirmedappointmentproduct = products.Where(p => p.itemid == unconfirmedappointmentitem)
                        .SingleOrDefault();
                    Invoice invoice = new Invoice
                    {
                        AgentID = agentid,
                        TypeId = invoicetype,
                        DoctorId = providerid,
                        InvoiceCreationDatetime = DateTime.Now,
                        InvoiceStartDate = fromdate,
                        InvoiceEnddate = todate,
                        Invoice_Number = maxnumber + 1,
                        Accesscode = Guid.NewGuid().ToString(),
                        TotalAmount = 0,
                        IsCanceled = false,
                        IsPaid = false,
                        CompanyId=DateTime.Now.Year>=2022?(int)Common.CompanyId.Wize:(int)Common.CompanyId.Reaya
                    };
                    var addressids = addresses.Select(a => a.id);
                    var appointemnts =
                        context.Appointments.Where(ap =>
                         (addressids.Contains(ap.Address_ID ?? 0))
                       && (DbFunctions.TruncateTime(ap.Appointment_Date) >= fromdate && DbFunctions.TruncateTime(ap.Appointment_Date) <= todate)
                       && (ap.BookingStatus != 5)
                       ).Select(a => new
                       {
                           id = a.Appointment_ID,
                           fees = a.Fees_For_Re3aya,
                           status = a.BookingStatus
                       }).ToList();
                    var paidappointments = context.Invoice_Appointment.Where(a =>
                             (a.Invoice.DoctorId == providerid)
                           && (a.Invoice.IsPaid.Value)
                           && (!a.Invoice.IsCanceled.HasValue || !a.Invoice.IsCanceled.Value))
                           .Select(ap => ap.Appointment_Id).ToList();
                    var removeappointments = appointemnts.Where(ap => paidappointments.Contains(ap.id)).ToList();
                    foreach (var removeappointment in removeappointments)
                    {
                        appointemnts.Remove(removeappointment);
                    }
                    var mintotal = appointemnts.Sum(a => a.fees) ?? 0;
                    if (mintotal >= 30)
                    {
                        var confirmedapplist = appointemnts.Where(ap =>
                      (ap.status == 3 || ap.status == 4 || ap.status == 6 || ap.status == 7));
                        if (confirmedapplist.Count() > 0)
                        {
                            foreach (var appointment in confirmedapplist)
                            {
                                appointmentids.Add(new Invoice_Appointment
                                {
                                    Appointment_Id = appointment.id,
                                    ProviderType = providertype
                                });
                            }
                            total = confirmedapplist.Sum(a => a.fees) ?? 0;
                            if (confirmedappointmentproduct != null)
                            {
                                var confirmedappointments = new Account_Doctor
                                {
                                    ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                                    Datetime = DateTime.Now,
                                    Debit = total
                                };
                                invoice.Account_Doctor.Add(confirmedappointments);
                                invoice.TotalAmount += total;
                                total = 0;
                            }
                        }
                        var unconfirmedapplist = appointemnts.Where(ap => (ap.status == 1 || ap.status == 2)).ToList();
                        if (unconfirmedapplist.Count > 0)
                        {
                            foreach (var appointment in unconfirmedapplist)
                            {
                                appointmentids.Add(new Invoice_Appointment
                                {
                                    Appointment_Id = appointment.id,
                                    ProviderType = providertype
                                });
                            }
                            total = unconfirmedapplist.Sum(a => a.fees) ?? 0;
                            if (unconfirmedappointmentproduct != null)
                            {
                                var unconfirmedappointments = new Account_Doctor
                                {
                                    ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                                    Datetime = DateTime.Now,
                                    Debit = total
                                };
                                invoice.Account_Doctor.Add(unconfirmedappointments);
                                invoice.TotalAmount += total;
                                total = 0;
                            }
                        }
                        //previous apps
                        if (mintotal >= 50)
                        {
                            var previousappointments = context.Appointments.Where(ap =>
                               (addressids.Contains(ap.Address_ID ?? 0))
                             && (DbFunctions.TruncateTime(ap.Appointment_Date).Value.Year == 2020)
                             && (ap.BookingStatus != 5)).Select(a => new
                             {
                                 id = a.Appointment_ID,
                                 fees = a.Fees_For_Re3aya,
                                 status = a.BookingStatus
                             }).ToList();
                           var removeprev_appointments = previousappointments.Where(ap => paidappointments.Contains(ap.id)).ToList();
                             foreach(var removeappointment in removeprev_appointments)
                            {
                                previousappointments.Remove(removeappointment);
                            }
                            var previousconfirmedapplist = previousappointments.Where(ap =>
                             (ap.status == 3 || ap.status == 4 || ap.status == 6 || ap.status == 7));
                            if (previousconfirmedapplist.Count() > 0)
                            {
                                foreach (var appointment in previousconfirmedapplist)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.id,
                                        ProviderType = providertype
                                    });
                                }
                                total = previousconfirmedapplist.Sum(a => a.fees) ?? 0;
                                var confirmedproduct = invoice.Account_Doctor
                                      .Where(d => d.ProductID == confirmedappointmentproduct.Account_Porduct_ID).FirstOrDefault();
                                if (confirmedproduct != null)
                                {
                                    confirmedproduct.Debit += total;
                                    invoice.TotalAmount += total;
                                    total = 0;
                                }
                            }
                            var previousunconfirmedapplist = previousappointments.Where(ap => (ap.status == 1 || ap.status == 2)).ToList();
                            if (previousunconfirmedapplist.Count > 0)
                            {
                                foreach (var appointment in previousunconfirmedapplist)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.id,
                                        ProviderType = providertype
                                    });
                                }
                                total = previousunconfirmedapplist.Sum(a => a.fees) ?? 0;
                                var unconfirmedproduct = invoice.Account_Doctor.Where(a => a.ProductID == unconfirmedappointmentproduct.Account_Porduct_ID).FirstOrDefault();
                                if (unconfirmedproduct != null)
                                {
                                    unconfirmedproduct.Debit += total;
                                    invoice.TotalAmount += total;
                                    total = 0;
                                }
                                else
                                {
                                    var unconfirmedappointments = new Account_Doctor
                                    {
                                        ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(unconfirmedappointments);
                                    invoice.TotalAmount += total;
                                    total = 0;
                                }
                            }
                        }
                        if (invoice.Account_Doctor.Count() > 0)
                        {
                            foreach (var appointment in appointmentids)
                            {
                                invoice.Invoice_Appointment.Add(appointment);
                            }
                            context.Invoices.Add(invoice);
                            context.SaveChanges();
                        }
                    }
                }
            }
        }
        void CreateSubscriptionInvoice(int providertype, int providerid, DateTime invoicedate)
        {
            AgentRegion agentregion = null;
            int invoicetype = (int)Common.InvoiceType.SubscriptionInvoice;
            int? agentid = 67;
            var isexist = context.Invoices.Any(acc =>
                                     (providertype == (int)Common.MedicalProviderType.doctor
                                       ? acc.DoctorId == providerid : acc.HealthentityId == providerid)
                                  && (invoicedate.Year == acc.InvoiceStartDate.Value.Year)
                                  && (acc.TypeId == invoicetype)
                                  && (!acc.IsCanceled.HasValue || !acc.IsCanceled.Value));
            if (!isexist)
            {
                if (providertype == (int)Common.MedicalProviderType.doctor)
                {
                    var doctor = context.Doctors.Where(d => d.DoctorID == providerid)
                                                 .Select(d => new
                                                 {
                                                     doctorid = d.DoctorID,
                                                     addresses = d.Address_Book.Where(ad => ad.IsMain == true)
                                                      .Select(ad => new
                                                      {
                                                          addressid = ad.AddressBook_ID,
                                                          regionid = ad.RegionID,
                                                      })
                                                 }).Single();
                    var products = context.Account_Products.Where(p =>
                                   (p.InvoiceItem.IsActive.Value)
                                && (p.ProviderType == providertype)
                                && (p.InvoiceTypeId == invoicetype)).Select(p => new
                                {
                                    itemid = p.ItemId,
                                    Account_Porduct_ID = p.Account_Porduct_ID,
                                    Account_Product_FixedPrice = p.Account_Product_FixedPrice
                                }).ToList();
                    var maxnumber = DateTime.Now.Year >= 2022 ? context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Wize).Max(n => n.Invoice_Number) ?? 0
                        : context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Reaya).Max(i => i.Invoice_Number) ?? 0;
                    var mainaddress = doctor.addresses.FirstOrDefault();
                    if (mainaddress != null)
                    {
                        var regionid = mainaddress.regionid;
                        agentregion = context.AgentRegions.Where(r =>
                          (r.RegionId == regionid)
                       && (r.Account_Agent.IsActive == true)).FirstOrDefault();
                        if (agentregion != null)
                        {
                            agentid = agentregion.AgentId;
                        }
                    }
                    int lastday = DateTime.DaysInMonth(invoicedate.Year, 1);
                    Invoice invoice = new Invoice
                    {
                        AgentID = agentid,
                        TypeId = invoicetype,
                        DoctorId = doctor.doctorid,
                        InvoiceCreationDatetime = DateTime.Now,
                        InvoiceStartDate = new DateTime(invoicedate.Year, 1, 1),
                        InvoiceEnddate = new DateTime(invoicedate.Year, 1, lastday),
                        Invoice_Number = maxnumber + 1,
                        Accesscode = Guid.NewGuid().ToString(),
                        TotalAmount = 0,
                        IsCanceled = false,
                        IsPaid = false,
                        CompanyId = DateTime.Now.Year >= 2022 ? (int)Common.CompanyId.Wize : (int)Common.CompanyId.Reaya
                    };
                    int subscriptionfeesitem = (int)Common.InvoiceItem.SubscriptionFees;
                    var subscriptionfeesproduct = products.Where(p => p.itemid == subscriptionfeesitem)
                             .SingleOrDefault();
                    if (subscriptionfeesproduct != null)
                    {
                        var subscribtionfees = new Account_Doctor
                        {
                            ProductID = subscriptionfeesproduct.Account_Porduct_ID,
                            Datetime = DateTime.Now,
                            Debit = subscriptionfeesproduct.Account_Product_FixedPrice,
                        };
                        invoice.Account_Doctor.Add(subscribtionfees);
                        invoice.TotalAmount += subscriptionfeesproduct.Account_Product_FixedPrice;
                    }
                    int adminstrativefeesitem = (int)Common.InvoiceItem.AdminstrativeExpenses;
                    var adminstrativefeesproduct = products.Where(p => p.itemid == adminstrativefeesitem)
                        .SingleOrDefault();
                    if (adminstrativefeesproduct != null)
                    {
                        //only once when doctor registered
                        var exist = context.Account_Doctor.Any(i =>
                            (i.Invoice.DoctorId == doctor.doctorid)
                          && (i.Invoice.TypeId == invoicetype)
                          && (i.ProductID == adminstrativefeesproduct.Account_Porduct_ID)
                          && (i.Credit.HasValue && i.Credit != 0));
                        if (!exist)
                        {
                            var adminstrativeexpenses = new Account_Doctor
                            {
                                ProductID = 4,
                                Datetime = DateTime.Now,
                                Debit = adminstrativefeesproduct.Account_Product_FixedPrice
                            };
                            invoice.Account_Doctor.Add(adminstrativeexpenses);
                            invoice.TotalAmount += adminstrativefeesproduct.Account_Product_FixedPrice;
                        }
                    }
                    if (invoice.Account_Doctor.Count() > 0)
                    {
                        context.Invoices.Add(invoice);
                        context.SaveChanges();
                    }
                }
                else
                {
                    var mainbranch = context.Health_Entitiy.Where(h => h.Health_Entitiy_ID == providerid)
                         .Select(h => new
                         {
                             contractid = h.Health_Entitiy_ContractID,
                             regionid = h.Health_Entitiy_RegionID ?? 0,
                             type = h.Health_Entitiy_Type
                         }).Single();
                    var products = context.Account_Products.Where(p =>
                  (p.InvoiceItem.IsActive.Value)
               && (p.ProviderType == mainbranch.type)
               && (p.InvoiceTypeId == invoicetype)
               ).Select(p => new
               {
                   itemid = p.ItemId,
                   Account_Porduct_ID = p.Account_Porduct_ID,
                   Account_Product_FixedPrice = p.Account_Product_FixedPrice
               }).ToList();

                    var maxnumber = context.Invoices.Max(i => i.Invoice_Number) ?? 1;
                    var regionid = mainbranch.regionid;
                    agentregion = context.AgentRegions.Where(r => (r.RegionId == regionid)
                         && (r.Account_Agent.IsActive == true)).FirstOrDefault();
                    if (agentregion != null)
                    {
                        agentid = agentregion.AgentId;
                    }
                    var lastday = DateTime.DaysInMonth(invoicedate.Year, 1);
                    Invoice invoice = new Invoice
                    {
                        AgentID = agentid,
                        HealthentityId = providerid,
                        TypeId = invoicetype,
                        InvoiceCreationDatetime = DateTime.Now,
                        InvoiceStartDate = new DateTime(invoicedate.Year, 1, 1),
                        InvoiceEnddate = new DateTime(invoicedate.Year, 1, lastday),
                        Invoice_Number = maxnumber + 1,
                        Accesscode = Guid.NewGuid().ToString(),
                        TotalAmount = 0,
                        IsCanceled = false,
                        IsPaid = false
                    };
                    //Subscription Fees
                    int subscriptionitem = (int)Common.InvoiceItem.SubscriptionFees;
                    var subscriptionproduct = products.Where(p => p.itemid == subscriptionitem).SingleOrDefault();
                    if (subscriptionproduct != null)
                    {
                        var subscriptionproduct_ispaid = context.Account_Doctor.Any(i =>
                         (i.Invoice.HealthentityId == providerid)
                      && (invoicedate.Year == i.Invoice.InvoiceStartDate.Value.Year)
                      && (i.ProductID == subscriptionproduct.Account_Porduct_ID)
                      && (i.Credit.HasValue && i.Credit != 0));
                        if (!subscriptionproduct_ispaid)
                        {
                            var subscribtionfees = new Account_Doctor
                            {
                                ProductID = subscriptionproduct.Account_Porduct_ID,
                                Datetime = DateTime.Now,
                                Debit = subscriptionproduct.Account_Product_FixedPrice
                            };
                            invoice.Account_Doctor.Add(subscribtionfees);
                            invoice.TotalAmount += subscriptionproduct.Account_Product_FixedPrice;
                        }
                    }
                    //Adminstrative Expenses[only once when provider registered]
                    int adminstrativeitem = (int)Common.InvoiceItem.AdminstrativeExpenses;
                    var adminstrativeproduct = products.Where(p => p.itemid == adminstrativeitem)
                        .SingleOrDefault();
                    if (adminstrativeproduct != null)
                    {
                        var adminstrativeproduct_ispaid = context.Account_Doctor.Any(i =>
                             (i.Invoice.HealthentityId == providerid)
                          && (i.ProductID == adminstrativeproduct.Account_Porduct_ID)
                          && (i.Credit.HasValue && i.Credit != 0));
                        if (!adminstrativeproduct_ispaid)
                        {
                            var adminstrativeexpenses = new Account_Doctor
                            {
                                ProductID = adminstrativeproduct.Account_Porduct_ID,
                                Datetime = DateTime.Now,
                                Debit = adminstrativeproduct.Account_Product_FixedPrice
                            };
                            invoice.Account_Doctor.Add(adminstrativeexpenses);
                            invoice.TotalAmount += adminstrativeproduct.Account_Product_FixedPrice;
                        }
                    }
                    if (invoice.Account_Doctor.Count() > 0)
                    {
                        context.Invoices.Add(invoice);
                        context.SaveChanges();
                    }
                }
            }
        }
        void CreateAppointmentInvoice(int providertype, int providerid, DateTime invoicedate)
        {
            AgentRegion agentregion = null;
            int invoicetype = (int)Common.InvoiceType.AppointmentInvoice;
            int? agentid = 67;
            List<Invoice_Appointment> appointmentids = new List<Invoice_Appointment>();
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                var doctor = context.Doctors.Include(d => d.Address_Book)
                       .Where(d => d.DoctorID == providerid)
                       .Select(d => new
                       {
                           doctorid = d.DoctorID,
                           videofees = d.Basic_Video_fees ?? 0,
                           chatfees = d.Basic_Chat_fees ?? 0,
                           addresses = d.Address_Book.Select(ad => new
                           {
                               addressid = ad.AddressBook_ID,
                               ismain = ad.IsMain,
                               regionid = ad.RegionID,
                               visitfees = ad.Visit_Fees ?? 0,
                           })
                       }).Single();
                var invoiceexist = context.Invoices.Any(acc =>
                                      (acc.DoctorId == doctor.doctorid)
                                   && (acc.InvoiceStartDate.Value.Month == invoicedate.Month
                                       && invoicedate.Year == acc.InvoiceStartDate.Value.Year)
                                   && (acc.TypeId == invoicetype)
                                   && (!acc.IsCanceled.HasValue || !acc.IsCanceled.Value));
                if (!invoiceexist)
                {
                    decimal? total = 0;
                    var maxnumber = DateTime.Now.Year >= 2022 ? context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Wize).Max(n => n.Invoice_Number) ?? 0
                        : context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Reaya).Max(i => i.Invoice_Number) ?? 0;
                    var mainaddress = doctor.addresses.Where(ad => ad.ismain == true).FirstOrDefault();
                    if (mainaddress != null)
                    {
                        var regionid = mainaddress.regionid;
                        agentregion = context.AgentRegions.Where(r =>
                       (r.RegionId == regionid)
                       && (r.Account_Agent.IsActive == true)).FirstOrDefault();
                        if (agentregion != null)
                        {
                            agentid = agentregion.AgentId;
                        }
                    }
                    int lastday = DateTime.DaysInMonth(invoicedate.Year, invoicedate.Month);
                    Invoice invoice = new Invoice
                    {
                        AgentID = agentid,
                        TypeId = invoicetype,
                        DoctorId = doctor.doctorid,
                        InvoiceCreationDatetime = DateTime.Now,
                        InvoiceStartDate = new DateTime(invoicedate.Year, invoicedate.Month, 1),
                        InvoiceEnddate = new DateTime(invoicedate.Year, invoicedate.Month, lastday),
                        Invoice_Number = maxnumber + 1,
                        Accesscode = Guid.NewGuid().ToString(),
                        TotalAmount = 0,
                        IsCanceled = false,
                        IsPaid = false,
                        CompanyId = DateTime.Now.Year >= 2022 ? (int)Common.CompanyId.Wize : (int)Common.CompanyId.Reaya
                    };
                    var products = context.Account_Products.Where(p =>
                      (p.InvoiceItem.IsActive.Value)
                   && (p.ProviderType == providertype)
                   && (p.InvoiceTypeId == invoicetype)
                   ).Select(p => new
                   {
                       itemid = p.ItemId,
                       Account_Porduct_ID = p.Account_Porduct_ID,
                       Account_Product_FixedPrice = p.Account_Product_FixedPrice
                   }).ToList();
                    int confirmedappointmentitem = (int)Common.InvoiceItem.ConfirmedAppointments;
                    var confirmedappointmentproduct = products.Where(p => p.itemid == confirmedappointmentitem)
                             .SingleOrDefault();
                    int unconfirmedappointmentitem = (int)Common.InvoiceItem.Nonconfirmedappointments;
                    var unconfirmedappointmentproduct = products.Where(p => p.itemid == unconfirmedappointmentitem)
                        .SingleOrDefault();

                    List<int> addressids = doctor.addresses.Select(d => d.addressid).ToList();
                    var appointments = context.Appointments.Where(ap =>
                             (addressids.Contains(ap.Address_ID ?? 0))
                           && ((ap.Appointment_Date.Value.Month == invoicedate.Month)
                                && (ap.Appointment_Date.Value.Year == invoicedate.Year)))
                        .ToList();
                    //Confirmed Appointments
                    var confirmedapplist = appointments.Where(ap =>
                        (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6 || ap.BookingStatus == 7));
                    if (confirmedapplist.Count() > 0)
                    {
                        foreach (var appointment in confirmedapplist)
                        {
                            appointmentids.Add(new Invoice_Appointment
                            {
                                Appointment_Id = appointment.Appointment_ID,
                                ProviderType = providertype
                            });
                            if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0 && appointment.SessionType == 1)
                            {
                                if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                {
                                    total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                }
                                else
                                {
                                    var selectedaddress = doctor.addresses.Where(d => d.addressid == appointment.Address_ID).Single();
                                    switch (appointment.AppointmentType)
                                    {
                                        //visit
                                        case 1:
                                            total += Convert.ToDecimal((selectedaddress.visitfees * 10) / 100);
                                            break;
                                        //video
                                        case 2:
                                            total += Convert.ToDecimal((doctor.videofees * 10) / 100);
                                            break;
                                        //chat
                                        case 3:
                                            total += Convert.ToDecimal((doctor.chatfees * 10) / 100);
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                total += appointment.Fees_For_Re3aya;
                            }
                        }
                        if (confirmedappointmentproduct != null)
                        {
                            var confirmedappointments = new Account_Doctor
                            {
                                ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(confirmedappointments);
                            invoice.TotalAmount += total;
                            total = 0;
                        }
                    }
                    //Non confirmed appointments
                    var unconfirmedapplist = appointments.Where(ap =>
                       (ap.BookingStatus == 1 || ap.BookingStatus == 2)).ToList();
                    if (unconfirmedapplist.Count > 0)
                    {
                        foreach (var appointment in unconfirmedapplist)
                        {
                            appointmentids.Add(new Invoice_Appointment
                            {
                                Appointment_Id = appointment.Appointment_ID,
                                ProviderType = providertype
                            });
                            if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0 && appointment.SessionType == 1)
                            {
                                if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                {
                                    total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                }
                                else
                                {
                                    var selectedaddress = doctor.addresses.Where(d => d.addressid == appointment.Address_ID).Single();
                                    switch (appointment.AppointmentType)
                                    {
                                        //visit
                                        case 1:
                                            total += Convert.ToDecimal((selectedaddress.visitfees * 10) / 100);
                                            break;
                                        //video
                                        case 2:
                                            total += Convert.ToDecimal((doctor.videofees * 10) / 100);
                                            break;
                                        //chat
                                        case 3:
                                            total += Convert.ToDecimal((doctor.chatfees * 10) / 100);
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                total += appointment.Fees_For_Re3aya;
                            }
                        }
                        if (unconfirmedappointmentproduct != null)
                        {
                            var unconfirmedappointments = new Account_Doctor
                            {
                                ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(unconfirmedappointments);
                            invoice.TotalAmount += total;
                        }
                    }
                    if (invoice.Account_Doctor.Count() > 0)
                    {
                        foreach (var appointment in appointmentids)
                        {
                            invoice.Invoice_Appointment.Add(appointment);
                        }
                        context.Invoices.Add(invoice);
                        context.SaveChanges();
                    }
                }
            }
            else
            {
                var mainbranch = context.Health_Entitiy.Where(h => h.Health_Entitiy_ID == providerid)
                     .Select(h => new
                     {
                         contractid = h.Health_Entitiy_ContractID,
                         entityid = h.Health_Entitiy_ID,
                         type = h.Health_Entitiy_Type,
                         ismain = true,
                         regionid = h.Health_Entitiy_RegionID ?? 0
                     }).Single();

                var entitylist = context.Health_Entitiy.Where(h => h.Health_Entity_ParentID == providerid)
                    .Select(h => new
                    {
                        contractid = h.Health_Entitiy_ContractID,
                        entityid = h.Health_Entitiy_ID,
                        type = h.Health_Entitiy_Type,
                        ismain = false,
                        regionid = 0
                    }).ToList();
                entitylist.Add(mainbranch);
                var invoiceexist = context.Invoices.Any(acc =>
                               (acc.HealthentityId == providerid)
                            && (acc.TypeId == invoicetype)
                            && (acc.InvoiceStartDate.Value.Month == invoicedate.Month
                                && invoicedate.Year == acc.InvoiceStartDate.Value.Year)
                            && (!acc.IsCanceled.HasValue || !acc.IsCanceled.Value));
                var products = context.Account_Products.Where(p =>
                   (p.InvoiceItem.IsActive.Value)
                && (p.ProviderType == mainbranch.type)
                && (p.InvoiceTypeId == invoicetype))
                .Select(p => new
                {
                    itemid = p.ItemId,
                    Account_Porduct_ID = p.Account_Porduct_ID,
                    Is_Fixed = p.Is_Fixed,
                    Account_Product_FixedPrice = p.Account_Product_FixedPrice
                }).ToList();

                int confirmedappointmentitem = (int)Common.InvoiceItem.ConfirmedAppointments;
                int unconfirmedappointmentitem = (int)Common.InvoiceItem.Nonconfirmedappointments;
                if (!invoiceexist)
                {
                    decimal? total = 0;
                    var maxnumber = context.Invoices.Max(i => i.Invoice_Number) ?? 00;
                    var regionid = entitylist.Where(h => h.ismain).Single().regionid;
                    agentregion = context.AgentRegions.Where(r => (r.RegionId == regionid)
                         && (r.Account_Agent.IsActive == true)).FirstOrDefault();
                    if (agentregion != null)
                    {
                        agentid = agentregion.AgentId;
                    }
                    var lastday = DateTime.DaysInMonth(invoicedate.Year, invoicedate.Month);
                    var confirmedappointmentproduct = products.Where(p => p.itemid == confirmedappointmentitem)
                           .SingleOrDefault();
                    var unconfirmedappointmentproduct = products.Where(p => p.itemid == unconfirmedappointmentitem)
                           .SingleOrDefault();

                    Invoice invoice = new Invoice
                    {
                        AgentID = agentid,
                        HealthentityId = providerid,
                        TypeId = invoicetype,
                        InvoiceCreationDatetime = DateTime.Now,
                        InvoiceStartDate = new DateTime(invoicedate.Year, invoicedate.Month, 1),
                        InvoiceEnddate = new DateTime(invoicedate.Year, invoicedate.Month, lastday),
                        Invoice_Number = maxnumber + 1,
                        Accesscode = Guid.NewGuid().ToString(),
                        TotalAmount = 0,
                        IsCanceled = false,
                        IsPaid = false
                    };

                    List<string> contractidlist = entitylist.Select(e => e.contractid).ToList();
                    List<int> branchids = new List<int>();
                    switch (providertype)
                    {
                        case (int)Common.MedicalProviderType.lab:
                            branchids = context.LabsBranches.Where(l => contractidlist.Contains(l.ContractId))
                                        .Select(b => b.Id).ToList();
                            var labappointments = context.LabsAppointments.Where(ap =>
                                  (branchids.Contains(ap.BranchId ?? 0))
                               && ((ap.AppointmentDate.Value.Month == invoicedate.Month)
                                     && (ap.AppointmentDate.Value.Year == invoicedate.Year)))
                             .ToList();
                            var confirmedappointmentlab = labappointments.Where(ap =>
                               (ap.BookingStatus == 3 || ap.BookingStatus == 4
                                 || ap.BookingStatus == 6 || ap.BookingStatus == 7)).ToList();
                            if (confirmedappointmentlab.Count() > 0)
                            {
                                foreach (var appointment in confirmedappointmentlab)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Is_Insurance.GetValueOrDefault())
                                    {
                                        total += 10;
                                    }
                                    else
                                    {
                                        total += ((appointment.AppointmentActualAmount * 10) / 100);
                                    }
                                }
                                if (confirmedappointmentproduct != null)
                                {
                                    var confirmedappointments = new Account_Doctor
                                    {
                                        ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(confirmedappointments);
                                    invoice.TotalAmount += total;
                                    total = 0;
                                }
                            }
                            var nonconfirmedappointmentlab = labappointments.Where(ap =>
                                (ap.BookingStatus == 1 || ap.BookingStatus == 2)).ToList();
                            if (nonconfirmedappointmentlab.Count() > 0)
                            {
                                foreach (var appointment in nonconfirmedappointmentlab)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Is_Insurance.GetValueOrDefault())
                                    {
                                        total += 10;
                                    }
                                    else
                                    {
                                        total += ((appointment.AppointmentActualAmount * 10) / 100);
                                    }
                                }
                                if (unconfirmedappointmentproduct != null)
                                {
                                    var unconfirmedappointments = new Account_Doctor
                                    {
                                        ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(unconfirmedappointments);
                                    invoice.TotalAmount += total;
                                }
                            }
                            break;
                        case (int)Common.MedicalProviderType.radiologycenter:
                            branchids = context.RadiologyCenterBranches.Where(r => contractidlist.Contains(r.ContractId))
                           .Select(r => r.Id).ToList();

                            var confirmedappointmentradiology = context.RadiologyCenterAppointments.Where(ap =>
                              (branchids.Contains(ap.BranchId ?? 0))
                           && (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6 || ap.BookingStatus == 7)
                           && ((ap.AppointmentDate.Value.Month == invoicedate.Month)
                                     && (ap.AppointmentDate.Value.Year == invoicedate.Year)))
                           .ToList();
                            if (confirmedappointmentradiology.Count() > 0)
                            {
                                foreach (var appointment in confirmedappointmentradiology)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Is_Insurance.GetValueOrDefault())
                                    {
                                        total += 10;
                                    }
                                    else
                                    {
                                        total += ((appointment.AppointmentActualAmount * 10) / 100);
                                    }
                                }
                                if (confirmedappointmentproduct != null)
                                {
                                    var confirmedradappointments = new Account_Doctor
                                    {
                                        ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(confirmedradappointments);
                                    invoice.TotalAmount += total;
                                    total = 0;
                                }
                            }

                            var unconfirmedappoinmentradiology = context.RadiologyCenterAppointments.Where(ap =>
                               (branchids.Contains(ap.BranchId ?? 0))
                            && (ap.BookingStatus == 1 || ap.BookingStatus == 2)
                            && ((ap.AppointmentDate.Value.Month == invoicedate.Month)
                                  && (ap.AppointmentDate.Value.Year == invoicedate.Year)))
                               .ToList();
                            if (unconfirmedappoinmentradiology.Count() > 0)
                            {
                                foreach (var appointment in unconfirmedappoinmentradiology)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Is_Insurance.GetValueOrDefault())
                                    {
                                        total += 10;
                                    }
                                    else
                                    {
                                        total += ((appointment.AppointmentActualAmount * 10) / 100);
                                    }
                                }
                                if (unconfirmedappointmentproduct != null)
                                {
                                    var unconfirmeraddappointments = new Account_Doctor
                                    {
                                        ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(unconfirmeraddappointments);
                                    invoice.TotalAmount += total;
                                }
                            }
                            break;
                        case (int)Common.MedicalProviderType.medicalcenter:
                            var entityidlist = entitylist.Select(e => e.entityid).ToList();
                            branchids = context.Address_Book.Where(ad => entityidlist.Contains(ad.EntityId ?? 0))
                                       .Select(b => b.AddressBook_ID).ToList();

                            var confirmedapplist = context.Appointments.Where(ap =>
                                (branchids.Contains(ap.Address_ID ?? 0))
                                && (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6 || ap.BookingStatus == 7)
                                && ((ap.Appointment_Date.Value.Month == invoicedate.Month)
                                     && (ap.Appointment_Date.Value.Year == invoicedate.Year)))
                                .ToList();
                            if (confirmedapplist.Count > 0)
                            {
                                foreach (var appointment in confirmedapplist)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.Appointment_ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                    {
                                        if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                        {
                                            total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                        }
                                        else
                                        {
                                            var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                            total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                        }
                                    }
                                    else
                                    {
                                        total += appointment.Fees_For_Re3aya;
                                    }
                                }
                                if (confirmedappointmentproduct != null)
                                {
                                    var confirmedappointments = new Account_Doctor
                                    {
                                        ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(confirmedappointments);
                                    invoice.TotalAmount += total;
                                    total = 0;
                                }
                            }
                            var unconfirmedapplist = context.Appointments.Where(ap =>
                               (branchids.Contains(ap.Address_ID ?? 0))
                               && (ap.BookingStatus == 1 || ap.BookingStatus == 2)
                               && ((ap.Appointment_Date.Value.Month == invoicedate.Month)
                                     && (ap.Appointment_Date.Value.Year == invoicedate.Year))).ToList();
                            if (unconfirmedapplist.Count > 0)
                            {
                                foreach (var appointment in unconfirmedapplist)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.Appointment_ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                    {
                                        if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                        {
                                            total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                        }
                                        else
                                        {
                                            var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                            total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                        }
                                    }
                                    else
                                    {
                                        total += appointment.Fees_For_Re3aya;
                                    }
                                }
                                if (unconfirmedappointmentproduct != null)
                                {
                                    var unconfirmedappointments = new Account_Doctor
                                    {
                                        ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(unconfirmedappointments);
                                    invoice.TotalAmount += total;
                                }
                            }
                            break;
                        case (int)Common.MedicalProviderType.hospital:
                            var hospitalidlist = entitylist.Select(e => e.entityid).ToList();
                            branchids = context.Address_Book.Where(ad => hospitalidlist.Contains(ad.EntityId ?? 0))
                                       .Select(b => b.AddressBook_ID).ToList();

                            var confirmedhosp_applist = context.Appointments.Where(ap =>
                                (branchids.Contains(ap.Address_ID ?? 0))
                                && (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6 || ap.BookingStatus == 7)
                                && ((ap.Appointment_Date.Value.Month == invoicedate.Month)
                                      && (ap.Appointment_Date.Value.Year == invoicedate.Year)))
                                .ToList();
                            if (confirmedhosp_applist.Count > 0)
                            {
                                foreach (var appointment in confirmedhosp_applist)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.Appointment_ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                    {
                                        if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                        {
                                            total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                        }
                                        else
                                        {
                                            var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                            total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                        }
                                    }
                                    else
                                    {
                                        total += appointment.Fees_For_Re3aya;
                                    }
                                }
                                if (confirmedappointmentproduct != null)
                                {
                                    var confirmedappointments = new Account_Doctor
                                    {
                                        ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(confirmedappointments);
                                    invoice.TotalAmount += total;
                                    total = 0;
                                }
                            }

                            var unconfirmedhosp_applist = context.Appointments.Where(ap =>
                               (branchids.Contains(ap.Address_ID ?? 0))
                               && (ap.BookingStatus == 1 || ap.BookingStatus == 2)
                               && ((ap.Appointment_Date.Value.Month == invoicedate.Month) && (ap.Appointment_Date.Value.Year == invoicedate.Year))).ToList();
                            if (unconfirmedhosp_applist.Count > 0)
                            {
                                foreach (var appointment in unconfirmedhosp_applist)
                                {
                                    appointmentids.Add(new Invoice_Appointment
                                    {
                                        Appointment_Id = appointment.Appointment_ID,
                                        ProviderType = providertype
                                    });
                                    if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                    {
                                        if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                        {
                                            total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                        }
                                        else
                                        {
                                            var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                            total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                        }
                                    }
                                    else
                                    {
                                        total += appointment.Fees_For_Re3aya;
                                    }
                                }
                                if (unconfirmedappointmentproduct != null)
                                {
                                    var unconfirmedappointments = new Account_Doctor
                                    {
                                        ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                                        Datetime = DateTime.Now,
                                        Debit = total
                                    };
                                    invoice.Account_Doctor.Add(unconfirmedappointments);
                                    invoice.TotalAmount += total;
                                }
                            }
                            break;
                    }
                    if (invoice.Account_Doctor.Count() > 0)
                    {
                        foreach (var appointment in appointmentids)
                        {
                            invoice.Invoice_Appointment.Add(appointment);
                        }
                        context.Invoices.Add(invoice);
                        context.SaveChanges();
                    }
                }
            }
        }
        void CreateOnlineSubscriptionInvoice(int providerid, DateTime invoicedate)
        {
            AgentRegion agentregion = null;
            int invoicetype = (int)Common.InvoiceType.OnlineSubscriptionInvoice;
            int providertype = (int)Common.MedicalProviderType.doctor;
            int? agentid = 67;
            var doctor = context.Doctors.Include(d => d.Address_Book)
                                         .Where(d => d.DoctorID == providerid)
                                         .Select(d => new
                                         {
                                             doctorid = d.DoctorID,
                                             addresses = d.Address_Book.Select(ad => new
                                             {
                                                 addressid = ad.AddressBook_ID,
                                                 ismain = ad.IsMain,
                                                 regionid = ad.RegionID
                                             })
                                         }).Single();

            var invoiceexist = context.Invoices.Any(acc =>
                                 (acc.DoctorId == doctor.doctorid)
                              && (invoicedate.Year == acc.InvoiceStartDate.Value.Year)
                              && (acc.TypeId == invoicetype)
                                 && (acc.IsCanceled != true));
            if (!invoiceexist)
            {
                var products = context.Account_Products.Where(p =>
                    (p.InvoiceItem.IsActive.Value)
                 && (p.ProviderType == providertype)
                 && (p.InvoiceTypeId == invoicetype)
                 ).Select(p => new
                 {
                     itemid = p.ItemId,
                     Account_Porduct_ID = p.Account_Porduct_ID,
                     Account_Product_FixedPrice = p.Account_Product_FixedPrice
                 }).ToList();
                var maxnumber = DateTime.Now.Year >= 2022 ? context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Wize).Max(n => n.Invoice_Number) ?? 0
                    : context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Reaya).Max(i => i.Invoice_Number) ?? 0;
                if (doctor.addresses.Count() > 0)
                {
                    var regionid = doctor.addresses.Where(ad => ad.ismain == true).FirstOrDefault().regionid;
                    agentregion = context.AgentRegions.Where(r =>
                         (r.RegionId == regionid) && (r.Account_Agent.IsActive == true)).FirstOrDefault();
                }
                if (agentregion != null)
                {
                    agentid = agentregion.AgentId;
                }

                Invoice invoice = new Invoice
                {
                    AgentID = agentid,
                    TypeId = invoicetype,
                    DoctorId = doctor.doctorid,
                    InvoiceCreationDatetime = DateTime.Now,
                    InvoiceStartDate = new DateTime(invoicedate.Year, invoicedate.Month, 1),
                    InvoiceEnddate = new DateTime(invoicedate.Year, invoicedate.Month, DateTime.DaysInMonth(invoicedate.Year, invoicedate.Month)),
                    Invoice_Number = maxnumber != null ? maxnumber + 1 : 1,
                    Accesscode = Guid.NewGuid().ToString(),
                    TotalAmount = 0,
                    IsCanceled = false,
                    IsPaid = false,
                    CompanyId = DateTime.Now.Year >= 2022 ? (int)Common.CompanyId.Wize : (int)Common.CompanyId.Reaya
                };
                List<int> addressids = doctor.addresses.Select(d => d.addressid).ToList();

                //video Appointments
                var has_video_started_session = context.Appointments.Any(ap =>
                    (addressids.Contains(ap.Address_ID ?? 0))
                    && (ap.AppointmentType == 2)
                    && (ap.Is_Session_Started == true)
                    && ((ap.Appointment_Creation_Datetime.Value.Year == invoicedate.Year)));
                if (has_video_started_session)
                {
                    int videofeesitem = (int)Common.InvoiceItem.VideoSubscriptionFees;
                    var videoproduct = products.Where(p => p.itemid == videofeesitem).SingleOrDefault();
                    var videosubscription = new Account_Doctor
                    {
                        ProductID = videoproduct.Account_Porduct_ID,
                        Datetime = DateTime.Now,
                        Debit = videoproduct.Account_Product_FixedPrice
                    };
                    invoice.Account_Doctor.Add(videosubscription);
                    invoice.TotalAmount += videoproduct.Account_Product_FixedPrice;
                }
                //chat appointments
                var has_chat_started_session = context.Appointments.Any(ap =>
                   (addressids.Contains(ap.Address_ID ?? 0))
                   && (ap.AppointmentType == 3)
                   && (ap.Is_Session_Started == true)
                   && ((ap.Appointment_Creation_Datetime.Value.Year == invoicedate.Year)));
                int chatfeesitem = (int)Common.InvoiceItem.ChatSubscriptionFees;
                var chatproduct = products.Where(p => p.itemid == chatfeesitem).SingleOrDefault();
                if (has_chat_started_session)
                {
                    var chatsubscription = new Account_Doctor
                    {
                        ProductID = chatproduct.Account_Porduct_ID,
                        Datetime = DateTime.Now,
                        Debit = chatproduct.Account_Product_FixedPrice
                    };
                    invoice.Account_Doctor.Add(chatsubscription);
                    invoice.TotalAmount += chatproduct.Account_Product_FixedPrice;
                }
                if (invoice.Account_Doctor.Count() > 0)
                {
                    context.Invoices.Add(invoice);
                    context.SaveChanges();
                }
            }
        }
        void CreateHomeVisitInvoiceInvoice(int providerid, DateTime invoicedate)
        {
            int invoicetype = (int)Common.InvoiceType.HomeVisitInvoice;
            decimal? total = 0;
            int? agentid = 67;

            var doctor = context.Doctors.Where(d => d.DoctorID == providerid)
                                       .Select(d => new
                                       {
                                           doctorid = d.DoctorID,
                                           videofees = d.Basic_Video_fees ?? 0,
                                           chatfees = d.Basic_Chat_fees ?? 0,
                                           addresses = d.HomeVisitRegions.Select(ad => new
                                           {
                                               addressid = ad.HomeVisitRegions_ID,
                                               regionid = ad.Region_ID,
                                               visitfees = ad.VisitPrice ?? 0,
                                           })
                                       }).Single();
            var invoiceexist = context.Invoices.Any(acc =>
                                  (acc.DoctorId == providerid)
                               && (acc.InvoiceStartDate.Value.Month == invoicedate.Month
                                   && invoicedate.Year == acc.InvoiceStartDate.Value.Year)
                               && (acc.TypeId == invoicetype)
                               && (acc.IsCanceled != true));
            if (!invoiceexist)
            {

                var maxnumber = DateTime.Now.Year >= 2022 ? context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Wize).Max(n => n.Invoice_Number) ?? 0
                    : context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Reaya).Max(i => i.Invoice_Number) ?? 0;
                var lastday = DateTime.DaysInMonth(invoicedate.Year, invoicedate.Month);
                Invoice invoice = new Invoice
                {
                    AgentID = agentid,
                    TypeId = invoicetype,
                    DoctorId = doctor.doctorid,
                    InvoiceCreationDatetime = DateTime.Now,
                    InvoiceStartDate = new DateTime(invoicedate.Year, invoicedate.Month, 1),
                    InvoiceEnddate = new DateTime(invoicedate.Year, invoicedate.Month, lastday),
                    Invoice_Number = maxnumber + 1,
                    Accesscode = Guid.NewGuid().ToString(),
                    TotalAmount = 0,
                    IsCanceled = false,
                    IsPaid = false,
                    CompanyId = DateTime.Now.Year >= 2022 ? (int)Common.CompanyId.Wize : (int)Common.CompanyId.Reaya
                };
                int providertype = (int)Common.MedicalProviderType.homevisit;
                var products = context.Account_Products.Where(p =>
                  (p.InvoiceItem.IsActive.Value)
               && (p.ProviderType == providertype)
               && (p.InvoiceTypeId == invoicetype)
               ).Select(p => new
               {
                   itemid = p.ItemId,
                   Account_Porduct_ID = p.Account_Porduct_ID,
                   Account_Product_FixedPrice = p.Account_Product_FixedPrice
               }).ToList();

                int confirmedappointmentitem = (int)Common.InvoiceItem.ConfirmedAppointments;
                var confirmedappointmentproduct = products.Where(p => p.itemid == confirmedappointmentitem)
                         .SingleOrDefault();

                int unconfirmedappointmentitem = (int)Common.InvoiceItem.Nonconfirmedappointments;
                var unconfirmedappointmentproduct = products.Where(p => p.itemid == unconfirmedappointmentitem)
                    .SingleOrDefault();

                List<int> addressids = doctor.addresses.Select(d => d.addressid).ToList();

                var confirmedapplist = context.HomeVisitAppointments.Where(ap =>
                    (addressids.Contains(ap.Home_Visit_Region_Id ?? 0))
                    && (ap.AppointmentStatus_Id == 3 || ap.AppointmentStatus_Id == 4 || ap.AppointmentStatus_Id == 6
                           || ap.AppointmentStatus_Id == 7)
                    && ((ap.AppointmentCreationDate.Value.Month == invoicedate.Month)
                             && (ap.AppointmentCreationDate.Value.Year == invoicedate.Year)))
                    .ToList();
                if (confirmedapplist.Count > 0)
                {
                    foreach (var appointment in confirmedapplist)
                    {
                        if (appointment.Re3ayaFees.GetValueOrDefault() == 0)
                        {
                            if (appointment.Fees.GetValueOrDefault() > 0)
                            {
                                total += Convert.ToDecimal((appointment.Fees.Value * 10) / 100);
                            }
                            else
                            {
                                var selectedaddress = doctor.addresses
                                    .Where(d => d.addressid == appointment.Home_Visit_Region_Id).Single();
                                total += Convert.ToDecimal((selectedaddress.visitfees * 10) / 100);
                            }
                        }
                        else
                        {
                            total += appointment.Re3ayaFees;
                        }
                    }
                    if (confirmedappointmentproduct != null)
                    {
                        var confirmedappointments = new Account_Doctor
                        {
                            ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                            Datetime = DateTime.Now,
                            Debit = total
                        };
                        invoice.Account_Doctor.Add(confirmedappointments);
                        invoice.TotalAmount += total;
                        total = 0;
                    }
                }
                var unconfirmedapplist = context.HomeVisitAppointments.Where(ap =>
                   (addressids.Contains(ap.Home_Visit_Region_Id ?? 0))
                   && (ap.AppointmentStatus_Id == 1 || ap.AppointmentStatus_Id == 2)
                   && ((ap.AppointmentCreationDate.Value.Month == invoicedate.Month)
                        && (ap.AppointmentCreationDate.Value.Year == invoicedate.Year))).ToList();
                if (unconfirmedapplist.Count > 0)
                {
                    foreach (var appointment in unconfirmedapplist)
                    {
                        if (appointment.Re3ayaFees.GetValueOrDefault() == 0)
                        {
                            if (appointment.Fees.GetValueOrDefault() > 0)
                            {
                                total += Convert.ToDecimal((appointment.Fees.Value * 10) / 100);
                            }
                            else
                            {
                                var selectedaddress = doctor.addresses
                                    .Where(d => d.addressid == appointment.Home_Visit_Region_Id).Single();
                                total += Convert.ToDecimal((selectedaddress.visitfees * 10) / 100);
                            }
                        }
                        else
                        {
                            total += appointment.Re3ayaFees;
                        }
                    }
                    if (unconfirmedappointmentproduct != null)
                    {
                        var unconfirmedappointments = new Account_Doctor
                        {
                            ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                            Datetime = DateTime.Now,
                            Debit = total
                        };
                        invoice.Account_Doctor.Add(unconfirmedappointments);
                        invoice.TotalAmount += total;
                    }
                }
                if (invoice.Account_Doctor.Count() > 0)
                {
                    context.Invoices.Add(invoice);
                    context.SaveChanges();
                }
            }
        }
        public void CreateOnlineInvoice(int providerid, DateTime invoicedate, int? subscriptiontype)
        {
            AgentRegion agentregion = null;
            int invoicetype = (int)Common.InvoiceType.OnlineSubscriptionInvoice;
            int providertype = (int)Common.MedicalProviderType.doctor;
            int? agentid = 67;
            var doctor = context.Doctors.Include(d => d.Address_Book)
                                         .Where(d => d.DoctorID == providerid)
                                         .Select(d => new
                                         {
                                             doctorid = d.DoctorID,
                                             videofees=d.Basic_Video_fees,
                                             chatfees=d.Basic_Chat_fees,
                                             addresses = d.Address_Book.Select(ad => new
                                             {
                                                 addressid = ad.AddressBook_ID,
                                                 ismain = ad.IsMain,
                                                 regionid = ad.RegionID
                                             })
                                         }).Single();

            var invoiceexist = context.Invoices.Any(acc =>
                                 (acc.DoctorId == doctor.doctorid)
                              && (invoicedate.Year == acc.InvoiceStartDate.Value.Year)
                              && (acc.TypeId == invoicetype)
                                 && (acc.IsCanceled != true));
            if (!invoiceexist)
            {
                var products = context.Account_Products.Where(p =>
                   (p.InvoiceItem.IsActive.Value)
                && (p.ProviderType == providertype)
                && (p.InvoiceTypeId == invoicetype)
                ).Select(p => new
                {
                    itemid = p.ItemId,
                    Account_Porduct_ID = p.Account_Porduct_ID,
                    Account_Product_FixedPrice = p.Account_Product_FixedPrice
                }).ToList();
                var maxnumber = DateTime.Now.Year >= 2022 ? context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Wize).Max(n => n.Invoice_Number) ?? 0
                    : context.Invoices.Where(i => i.CompanyId == (int)Common.CompanyId.Reaya).Max(i => i.Invoice_Number) ?? 0;
                if (doctor.addresses.Count() > 0)
                {
                    var regionid = doctor.addresses.Where(ad => ad.ismain == true).FirstOrDefault().regionid;
                    agentregion = context.AgentRegions.Where(r =>
                         (r.RegionId == regionid) && (r.Account_Agent.IsActive == true)).FirstOrDefault();
                }
                if (agentregion != null)
                {
                    agentid = agentregion.AgentId;
                }

                Invoice invoice = new Invoice
                {
                    AgentID = agentid,
                    TypeId = invoicetype,
                    DoctorId = doctor.doctorid,
                    InvoiceCreationDatetime = DateTime.Now,
                    InvoiceStartDate = new DateTime(invoicedate.Year, invoicedate.Month, 1),
                    InvoiceEnddate = new DateTime(invoicedate.Year, invoicedate.Month, DateTime.DaysInMonth(invoicedate.Year, invoicedate.Month)),
                    Invoice_Number = maxnumber != null ? maxnumber + 1 : 1,
                    Accesscode = Guid.NewGuid().ToString(),
                    TotalAmount = 0,
                    IsCanceled = false,
                    IsPaid = false,
                    CompanyId = DateTime.Now.Year >= 2022 ? (int)Common.CompanyId.Wize : (int)Common.CompanyId.Reaya
                };
                if (subscriptiontype.HasValue&& subscriptiontype .Value== 1)//chat
                {
                    int chatfeesitem = (int)Common.InvoiceItem.ChatSubscriptionFees;
                    var chatproduct = products.Where(p => p.itemid == chatfeesitem).SingleOrDefault();
                    var chatsubscription = new Account_Doctor
                    {
                        ProductID = chatproduct.Account_Porduct_ID,
                        Datetime = DateTime.Now,
                        Debit = chatproduct.Account_Product_FixedPrice
                    };
                    invoice.Account_Doctor.Add(chatsubscription);
                    invoice.TotalAmount += chatproduct.Account_Product_FixedPrice;
                }
                else if(subscriptiontype.HasValue&&subscriptiontype.Value == 2)//video
                {
                    int videofeesitem = (int)Common.InvoiceItem.VideoSubscriptionFees;
                    var videoproduct = products.Where(p => p.itemid == videofeesitem).SingleOrDefault();
                    var videosubscription = new Account_Doctor
                    {
                        ProductID = videoproduct.Account_Porduct_ID,
                        Datetime = DateTime.Now,
                        Debit = videoproduct.Account_Product_FixedPrice
                    };
                    invoice.Account_Doctor.Add(videosubscription);
                    invoice.TotalAmount += videoproduct.Account_Product_FixedPrice;
                }
                else//all
                {
                    int chatfeesitem = (int)Common.InvoiceItem.ChatSubscriptionFees;
                    var chatproduct = products.Where(p => p.itemid == chatfeesitem).SingleOrDefault();
                    var chatsubscription = new Account_Doctor
                    {
                        ProductID = chatproduct.Account_Porduct_ID,
                        Datetime = DateTime.Now,
                        Debit = chatproduct.Account_Product_FixedPrice
                    };
                    invoice.Account_Doctor.Add(chatsubscription);
                    invoice.TotalAmount += chatproduct.Account_Product_FixedPrice;
                    int videofeesitem = (int)Common.InvoiceItem.VideoSubscriptionFees;
                    var videoproduct = products.Where(p => p.itemid == videofeesitem).SingleOrDefault();
                    var videosubscription = new Account_Doctor
                    {
                        ProductID = videoproduct.Account_Porduct_ID,
                        Datetime = DateTime.Now,
                        Debit = videoproduct.Account_Product_FixedPrice
                    };
                    invoice.Account_Doctor.Add(videosubscription);
                    invoice.TotalAmount += videoproduct.Account_Product_FixedPrice;
                }
                if (invoice.Account_Doctor.Count() > 0)
                {
                    context.Invoices.Add(invoice);
                    context.SaveChanges();
                }
            }
        }
        public void UpdateInvoice(InvoiceViewModel vm,int userid)
        {
            var invoice = context.Invoices.Find(vm.InvoiceId);
            invoice.IsPaid = true;
            invoice.TotalPaid = invoice.TotalAmount;
            invoice.AcheivementDateTime = vm.AcheivementDateTime;
            invoice.ReceiptNumber = vm.ReceiptNumber;
            invoice.ReceiptChar = vm.ReceiptChar;
            invoice.AcheivementType_Id = vm.AcheivementType;
            var SerialNumber = context.Invoices.Max(m => m.SerialNumber) ?? 0;
            invoice.SerialNumber = SerialNumber + 1;
            foreach (var item in invoice.Account_Doctor)
            {
                var account = vm.invoiceDetails.Where(acc => acc.Id == item.Account_Doctor_ID).SingleOrDefault();
                if (account != null)
                {
                    item.Credit = account.Credit;
                }
            }
            var log = new InvoiceLog
            {
                Action_TypeId = (int)Common.ActionType.Acheivement,
                UserId = userid,
                InvoiceId = vm.InvoiceId,
                CreatedDate = DateTime.Now
            };
            context.InvoiceLogs.Add(log);
            context.SaveChanges();
        }
        public void ResetInvoice(int? id, int? appid, int? providertype,int? userid)
        {
            if (id.HasValue)
            {
                var invoice = context.Invoices.Find(id);
                providertype = invoice.DoctorId.HasValue ? 0 : invoice.Health_Entitiy.Health_Entitiy_Type;
            }
            if (!id.HasValue)
            {
                id = context.Invoice_Appointment.Where(m =>
                  (m.Appointment_Id == appid)
                && (providertype == m.ProviderType)).Select(i => i.Invoice.InvoiceID).Single();
            }
            if (providertype == (int)Common.MedicalProviderType.doctor)
            {
                ResetDoctorInvoice(id.Value);
            }
            else
            {
                ResetHealthInvoice(id.Value);
            }
            var log = new InvoiceLog
            {
                Action_TypeId = (int)Common.ActionType.Reset,
                UserId = userid,
                InvoiceId = id,
                CreatedDate = DateTime.Now
            };
            context.InvoiceLogs.Add(log);
            context.SaveChanges();
        }
        void ResetHealthInvoice(int id)
        {
            var invoice = context.Invoices.Find(id);
            var invoicedate = invoice.InvoiceStartDate.Value;
            int invoicetype = (int)Common.InvoiceType.AppointmentInvoice;
            var appointmentids = new List<int>();
            decimal? total = 0;
            try
            {
                int type = Convert.ToInt32(invoice.Health_Entitiy.Health_Entitiy_Type);
                if (invoice != null)
                {
                    appointmentids = invoice.Invoice_Appointment.Select(i => i.Appointment_Id).ToList();
                    if (invoice.Account_Doctor != null)
                    {
                        context.Account_Doctor.RemoveRange(invoice.Account_Doctor);
                        invoice.TotalAmount = 0;
                        context.SaveChanges();
                    }
                }
                int providertype = (int)type;
                var products = context.Account_Products.Where(p =>
                   (p.InvoiceItem.IsActive.Value)
                && (p.ProviderType == providertype)
                && (p.InvoiceTypeId == invoicetype)
                ).Select(p => new
                {
                    itemid = p.ItemId,
                    Account_Porduct_ID = p.Account_Porduct_ID,
                    Account_Product_FixedPrice = p.Account_Product_FixedPrice
                }).ToList();
                switch (type)
                {
                    case 2://lab
                        var appointments = context.LabsAppointments.Where(ap => appointmentids.Contains(ap.ID));
                        //confirmed
                        var confirmedlab = appointments.Where(ap =>
                          (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6))
                         .ToList();
                        if (confirmedlab.Count() > 0)
                        {
                            foreach (var appointment in confirmedlab)
                            {
                                if (appointment.Is_Insurance.GetValueOrDefault())
                                {
                                    total += 10;
                                }
                                else
                                {
                                    total += ((appointment.AppointmentActualAmount * 10) / 100);
                                }
                            }
                            //total = Convert.ToDecimal((confirmedlab.Sum(c => c.AppointmentActualAmount) * 10) / 100);
                            var confirmedappointments = new Account_Doctor
                            {
                                ProductID = 1,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(confirmedappointments);
                            invoice.TotalAmount += total;
                            total = 0;
                        }
                        //non-confirmed
                        var nonconfirmedlab = appointments.Where(ap => (ap.BookingStatus == 1))
                          .ToList();
                        if (nonconfirmedlab.Count() > 0)
                        {
                            foreach (var appointment in nonconfirmedlab)
                            {
                                if (appointment.Is_Insurance.GetValueOrDefault())
                                {
                                    total += 10;
                                }
                                else
                                {
                                    total += ((appointment.AppointmentActualAmount * 10) / 100);
                                }
                            }
                            //total = Convert.ToDecimal((confirmedlab.Sum(c => c.AppointmentActualAmount) * 10) / 100);
                            var unconfirmedappointments = new Account_Doctor
                            {
                                ProductID = 2,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(unconfirmedappointments);
                            invoice.TotalAmount += total;
                        }
                        break;
                    case 5://radiology
                        var radappointments = context.RadiologyCenterAppointments.Where(ap => appointmentids.Contains(ap.ID));
                        //confirmed
                        var confirmedradiology = radappointments.Where(ap =>
                          (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6))
                       .ToList();
                        if (confirmedradiology.Count() > 0)
                        {
                            foreach (var appointment in confirmedradiology)
                            {
                                if (appointment.Is_Insurance.GetValueOrDefault())
                                {
                                    total += 10;
                                }
                                else
                                {
                                    total += ((appointment.AppointmentActualAmount * 10) / 100);
                                }
                            }
                            //total = Convert.ToDecimal((confirmedradiology.Sum(c => c.AppointmentActualAmount) * 10) / 100);
                            var confirmedradappointments = new Account_Doctor
                            {
                                ProductID = 1,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(confirmedradappointments);
                            invoice.TotalAmount += total;
                            total = 0;
                        }
                        //non-confirmed
                        var unconfirmedradiology = radappointments.Where(ap => (ap.BookingStatus == 1))
                        .ToList();
                        if (unconfirmedradiology.Count() > 0)
                        {
                            foreach (var appointment in unconfirmedradiology)
                            {
                                if (appointment.Is_Insurance.GetValueOrDefault())
                                {
                                    total += 10;
                                }
                                else
                                {
                                    total += ((appointment.AppointmentActualAmount * 10) / 100);
                                }
                            }
                            //total = Convert.ToDecimal((unconfirmedradiology.Sum(n => n.AppointmentActualAmount) * 10) / 100);
                            var unconfirmeraddappointments = new Account_Doctor
                            {
                                ProductID = 2,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(unconfirmeraddappointments);
                            invoice.TotalAmount += total;
                        }
                        break;
                    case 4://medical center
                        var medicalcenterappointments = context.Appointments.Where(ap => appointmentids.Contains(ap.Appointment_ID));
                        //Confirmed Appointments
                        var confirmedapplist = medicalcenterappointments.Where(ap =>
                             (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6
                                || ap.BookingStatus == 7))
                            .ToList();
                        if (confirmedapplist.Count > 0)
                        {
                            foreach (var appointment in confirmedapplist)
                            {
                                if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                {
                                    if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                    {
                                        total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                    }
                                    else
                                    {
                                        var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                        total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                    }
                                }
                                else
                                {
                                    total += appointment.Fees_For_Re3aya;
                                }
                            }
                            var confirmedappointments = new Account_Doctor
                            {
                                ProductID = 1,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(confirmedappointments);
                            invoice.TotalAmount += total;
                            total = 0;
                        }
                        //Non confirmed appointments
                        var unconfirmedapplist = medicalcenterappointments.Where(ap => ap.BookingStatus == 1)
                           .ToList();
                        if (unconfirmedapplist.Count > 0)
                        {
                            foreach (var appointment in unconfirmedapplist)
                            {
                                if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                {
                                    if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                    {
                                        total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                    }
                                    else
                                    {
                                        var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                        total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                    }
                                }
                                else
                                {
                                    total += appointment.Fees_For_Re3aya;
                                }
                            }
                            var unconfirmedappointments = new Account_Doctor
                            {
                                ProductID = 2,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(unconfirmedappointments);
                            invoice.TotalAmount += total;
                        }
                        break;
                    case 1://hospital
                        var hospitalappointments = context.Appointments.Where(ap => appointmentids.Contains(ap.Appointment_ID));
                        //Confirmed Appointments
                        var confirmedhosplist = hospitalappointments.Where(ap =>
                             (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6
                                || ap.BookingStatus == 7))
                            .ToList();
                        if (confirmedhosplist.Count > 0)
                        {
                            foreach (var appointment in confirmedhosplist)
                            {
                                if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                {
                                    if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                    {
                                        total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                    }
                                    else
                                    {
                                        var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                        total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                    }
                                }
                                else
                                {
                                    total += appointment.Fees_For_Re3aya;
                                }
                            }
                            var confirmedappointments = new Account_Doctor
                            {
                                ProductID = 1,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(confirmedappointments);
                            invoice.TotalAmount += total;
                            total = 0;
                        }
                        //Non confirmed appointments
                        var unconfirmedhosplist = hospitalappointments.Where(ap => ap.BookingStatus == 1)
                           .ToList();
                        if (unconfirmedhosplist.Count > 0)
                        {
                            foreach (var appointment in unconfirmedhosplist)
                            {
                                if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0)
                                {
                                    if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                                    {
                                        total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                                    }
                                    else
                                    {
                                        var selectedaddress = context.Address_Book.Where(ad => ad.AddressBook_ID == appointment.Address_ID).Single();
                                        total += Convert.ToDecimal((selectedaddress.Visit_Fees * 10) / 100);
                                    }
                                }
                                else
                                {
                                    total += appointment.Fees_For_Re3aya;
                                }
                            }
                            var unconfirmedappointments = new Account_Doctor
                            {
                                ProductID = 2,
                                Datetime = DateTime.Now,
                                Debit = total
                            };
                            invoice.Account_Doctor.Add(unconfirmedappointments);
                            invoice.TotalAmount += total;
                        }
                        break;
                }
                //register invoice in case of entity has appointments
                if (invoice.Account_Doctor.Count() > 0)
                {
                    context.SaveChanges();
                }
                else
                {
                    context.Invoices.Remove(invoice);
                    context.AccountingLogs.Add(new AccountingLog
                    {
                        DocId = invoice.HealthentityId,
                        EventDate = invoice.InvoiceStartDate,
                        EventDesc = "لاتوجد تحصيلات",
                        TypeId = 2
                    });

                    context.SaveChanges();
                }
                context.AccountingLogs.Add(new AccountingLog
                {
                    EntityId = id,
                    EventDate = invoice.InvoiceStartDate.Value,
                    EventDesc = "لاتوجد تحصيلات",
                    InvoiceType = 2
                });

            }
            catch (Exception ex)
            {
                context.AccountingLogs.Add(new AccountingLog
                {
                    EntityId = id,
                    EventDate = invoice.InvoiceStartDate.Value,
                    EventDesc = ex.Message,
                    InvoiceType = 2
                });
            }
        }
        void ResetDoctorInvoice(int id)
        {
            var invoice = context.Invoices.Find(id);
            var appointmentids = new List<int>();
            decimal? total = 0;
            if (invoice != null)
            {
                appointmentids = invoice.Invoice_Appointment.Select(i => i.Appointment_Id).ToList();
                if (invoice.Account_Doctor != null)
                {
                    context.Account_Doctor.RemoveRange(invoice.Account_Doctor);
                    invoice.TotalAmount = 0;
                    context.SaveChanges();
                }
            }
            var appointments = context.Appointments.Where(ap => appointmentids.Contains(ap.Appointment_ID));
            var invoicedate = invoice.InvoiceStartDate.Value;
            int invoicetype = (int)Common.InvoiceType.AppointmentInvoice;
            try
            {
                var doctor = context.Doctors.Include(d => d.Address_Book)
                                      .Where(d => d.DoctorID == invoice.DoctorId)
                                      .Select(d => new
                                      {
                                          videofees = d.Basic_Video_fees ?? 0,
                                          chatfees = d.Basic_Chat_fees ?? 0,
                                          doctorid = d.DoctorID,
                                          addresses = d.Address_Book
                                           .Select(ad => new
                                           {
                                               visitfees = ad.Visit_Fees ?? 0,
                                               re3ayadiscount = ad.Re3aya_Discount ?? 0,
                                               addressid = ad.AddressBook_ID,
                                               ismain = ad.IsMain,
                                               regionid = ad.RegionID
                                           })
                                      }).Single();
                int providertype = (int)Common.MedicalProviderType.doctor;
                var products = context.Account_Products.Where(p =>
                  (p.InvoiceItem.IsActive.Value)
               && (p.ProviderType == providertype)
               && (p.InvoiceTypeId == invoicetype)
               ).Select(p => new
               {
                   itemid = p.ItemId,
                   Account_Porduct_ID = p.Account_Porduct_ID,
                   Account_Product_FixedPrice = p.Account_Product_FixedPrice
               }).ToList();

                int confirmedappointmentitem = (int)Common.InvoiceItem.ConfirmedAppointments;
                var confirmedappointmentproduct = products.Where(p => p.itemid == confirmedappointmentitem)
                         .SingleOrDefault();

                int unconfirmedappointmentitem = (int)Common.InvoiceItem.Nonconfirmedappointments;
                var unconfirmedappointmentproduct = products.Where(p => p.itemid == unconfirmedappointmentitem)
                    .SingleOrDefault();
                //Confirmed Appointments
                var confirmedapplist = appointments.Where(ap =>
                    (ap.BookingStatus == 3 || ap.BookingStatus == 4 || ap.BookingStatus == 6 || ap.BookingStatus == 7))
                    .ToList();
                if (confirmedapplist.Count > 0)
                {
                    foreach (var appointment in confirmedapplist)
                    {
                        if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0&&appointment.SessionType==1)
                        {
                            if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                            {
                                total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                            }
                            else
                            {
                                var selectedaddress = doctor.addresses.Where(d => d.addressid == appointment.Address_ID).Single();
                                switch (appointment.AppointmentType)
                                {
                                    //visit
                                    case 1:
                                        total += Convert.ToDecimal((selectedaddress.visitfees * 10) / 100);
                                        break;
                                    //video
                                    case 2:
                                        total += Convert.ToDecimal((doctor.videofees * 10) / 100);
                                        break;
                                    //chat
                                    case 3:
                                        total += Convert.ToDecimal((doctor.chatfees * 10) / 100);
                                        break;
                                }
                            }
                        }
                        else
                        {
                            total += appointment.Fees_For_Re3aya;
                        }
                    }
                    if (confirmedappointmentproduct != null)
                    {
                        var confirmedappointments = new Account_Doctor
                        {
                            ProductID = confirmedappointmentproduct.Account_Porduct_ID,
                            Datetime = DateTime.Now,
                            Debit = total
                        };
                        invoice.Account_Doctor.Add(confirmedappointments);
                        invoice.TotalAmount += total;
                        total = 0;
                    }
                }
                //Non confirmed appointments
                var unconfirmedapplist = appointments.Where(ap => (ap.BookingStatus == 1)).ToList();
                if (unconfirmedapplist.Count > 0)
                {
                    foreach (var appointment in unconfirmedapplist)
                    {
                        if (appointment.Fees_For_Re3aya.GetValueOrDefault() == 0 && appointment.SessionType == 1)
                        {
                            if (appointment.Fees_For_Doctor.GetValueOrDefault() > 0)
                            {
                                total += Convert.ToDecimal((appointment.Fees_For_Doctor.Value * 10) / 100);
                            }
                            else
                            {
                                var selectedaddress = doctor.addresses.Where(d => d.addressid == appointment.Address_ID).Single();
                                switch (appointment.AppointmentType)
                                {
                                    //visit
                                    case 1:
                                        total += Convert.ToDecimal((selectedaddress.visitfees * 10) / 100);
                                        break;
                                    //video
                                    case 2:
                                        total += Convert.ToDecimal((doctor.videofees * 10) / 100);
                                        break;
                                    //chat
                                    case 3:
                                        total += Convert.ToDecimal((doctor.chatfees * 10) / 100);
                                        break;
                                }
                            }
                        }
                        else
                        {
                            total += appointment.Fees_For_Re3aya;
                        }
                    }
                    if (unconfirmedappointmentproduct != null)
                    {
                        var unconfirmedappointments = new Account_Doctor
                        {
                            ProductID = unconfirmedappointmentproduct.Account_Porduct_ID,
                            Datetime = DateTime.Now,
                            Debit = total
                        };
                        invoice.Account_Doctor.Add(unconfirmedappointments);
                        invoice.TotalAmount += total;
                    }
                }
                if (invoice.Account_Doctor.Count() > 0)
                {

                    context.SaveChanges();
                }
                else
                {
                    context.Invoices.Remove(invoice);
                    context.AccountingLogs.Add(new AccountingLog
                    {
                        DocId = doctor.doctorid,
                        EventDate = invoice.InvoiceStartDate,
                        EventDesc = "لاتوجد تحصيلات",
                        TypeId = 2
                    });

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                context.AccountingLogs.Add(new AccountingLog
                {
                    DocId = id,
                    EventDate = invoice.InvoiceStartDate,
                    EventDesc = ex.Message.ToString(),
                    TypeId = 2
                });

                context.SaveChanges();
            }
        }
        public void UpdateInvoiceStatus(int id, int statusid,int userid)
        {
            var invoice = context.Invoices.Find(id);
            int actiontype = 0;
            switch (statusid)
            {
                case (int)Common.InvoiceStatus.cancel:
                    invoice.IsCanceled = true;
                    actiontype = (int)Common.ActionType.Cancel;
                    break;
                case (int)Common.InvoiceStatus.unpaid:
                    invoice.TotalPaid = null;
                    invoice.IsPaid = null;
                    invoice.IsPaidBefore = true;
                    invoice.SerialNumber = null;
                    foreach (var acc in invoice.Account_Doctor)
                    {
                        acc.Credit = null;
                    }
                    actiontype = (int)Common.ActionType.CancelAcheivement;
                    break;
            }
            var log = new InvoiceLog
            {
                Action_TypeId =actiontype,
                UserId = userid,
                InvoiceId = id,
                CreatedDate = DateTime.Now
            };
            context.InvoiceLogs.Add(log);
            context.SaveChanges();
        }
        public void UpdateDeductionStatus(int id, bool status)
        {
            var invoice = context.Invoices.Find(id);
            invoice.Is_Deduction = status;
            context.SaveChanges();
        }
        public IQueryable<InvoiceViewModel> AchievementInvoices(InvoiceSearch search)
        {
            var query = context.Invoices.Where(m =>
                  (!search.type.HasValue || m.TypeId == search.type)
               && (!search.From.HasValue || (DbFunctions.TruncateTime(m.InvoiceStartDate) >= search.From
                    && DbFunctions.TruncateTime(m.InvoiceStartDate) <= search.To))
               && (!search.Status.HasValue || search.Status.Value == (int)Common.InvoiceStatus.paid ? (m.IsPaid.Value && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                    : search.Status.Value == (int)Common.InvoiceStatus.unpaid ? ((!m.IsPaid.HasValue || !m.IsPaid.Value) && (!m.IsCanceled.HasValue || !m.IsCanceled.Value))
                    : search.Status.Value == (int)Common.InvoiceStatus.cancel ? m.IsCanceled.Value : true)
               && (!search.InvoiceNumber.HasValue || m.Invoice_Number == search.InvoiceNumber)
               && (search.AgentIds.Count() == 0 || search.AgentIds.Contains(m.AgentID ?? 0))
               && (!search.ProviderType.HasValue || (search.ProviderType == (int)Common.MedicalProviderType.doctor ? m.DoctorId.HasValue
                    : m.Health_Entitiy.Health_Entitiy_Type == search.ProviderType)))
             .Select(i => new InvoiceViewModel
             {
                 InvoiceId = i.InvoiceID,
                 AcheivementDateTime = i.AcheivementDateTime,
                 AcheivementTypeName = i.AcheivementType.Name_Ar,
                 Invoice_Number = i.Invoice_Number,
                 TotalPaid = i.TotalPaid,
                 SerialNumber = i.SerialNumber ?? 0,
                 TypeName = i.InvoiceType.TypeName,
                 AgentName = i.AgentID.HasValue ? i.Account_Agent.Account_Agent_Name : "غير مسجل",
                 ProviderCode = i.DoctorId.HasValue ? i.Doctor.Doctor_CODE : i.Health_Entitiy.Health_Entitiy_ContractID,
                 providerName = i.DoctorId.HasValue ? i.Doctor.Doctor_Name_EN : i.Health_Entitiy.Health_Entitiy_NameEN,
                 ProviderType = i.DoctorId.HasValue ? 0 : i.Health_Entitiy.Health_Entitiy_Type,
                 ProviderPhone = i.DoctorId.HasValue ? i.Doctor.PhoneNumber1_Master
                          : i.Health_Entitiy.Health_Entitiy_MasterPhone,
                 RegionId = i.DoctorId.HasValue ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().RegionID
                          : i.Health_Entitiy.Health_Entitiy_RegionID,
                 RegionName = i.DoctorId.HasValue ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault().Region.Region_Name_AR
                          : i.Health_Entitiy.Region.Region_Name_AR,
                 CityId = i.DoctorId.HasValue ? i.Doctor.Address_Book.Where(ad => ad.IsMain.Value).FirstOrDefault()
                        .Region.CityID : i.Health_Entitiy.Region.CityID,
             }).Where(a =>
                 (search.CityIds.Count() > 0 && search.RegionIds.Count() == 0 ? search.CityIds.Contains(a.CityId ?? 0) : true)
              && (search.RegionIds.Count() == 0 || search.RegionIds.Contains(a.RegionId ?? 0))
              && (!search.ProviderType.HasValue || a.ProviderType == search.ProviderType)
              && (string.IsNullOrEmpty(search.ProviderCode) || a.ProviderCode == search.ProviderCode.Trim())
              && (string.IsNullOrEmpty(search.ProviderName)
                    || a.providerName.Trim().ToLower().Contains(search.ProviderName.Trim().ToLower())));

            return query;
        }
        public void ValidateUpdateInvoice(InvoiceViewModel vm, ModelStateDictionary modelstate)
        {
            if ((vm.AcheivementType == 1) && (string.IsNullOrWhiteSpace(vm.ReceiptNumber) || string.IsNullOrWhiteSpace(vm.ReceiptChar)))
            {
                modelstate.AddModelError("ReceiptNumber", "من فضلك أدخل رقم الإيصال");
                modelstate.AddModelError("ReceiptChar", "من فضلك أدخل رمز الإيصال");
            }
        }
        public void UpdateInvoiceAppointmentsStatus(int invoiceid, Dictionary<int, bool> invoiceappointmentsreset)
        {
            var invoice = context.Invoices.Where(i => i.InvoiceID == invoiceid).Single();
            int providertype = invoice.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                : (int)invoice.Health_Entitiy.Health_Entitiy_Type;
            switch (providertype)
            {
                case (int)Common.MedicalProviderType.doctor:
                    foreach (var appointmentid in invoiceappointmentsreset)
                    {
                        var appointment = context.Appointments.Where(ap => ap.Appointment_ID == appointmentid.Key).Single();
                        appointment.BookingStatus = appointmentid.Value ? 4 : 5;
                    }
                    break;
                case (int)Common.MedicalProviderType.hospital:
                    foreach (var appointmentid in invoiceappointmentsreset)
                    {
                        var appointment = context.Appointments.Where(ap => ap.Appointment_ID == appointmentid.Key).Single();
                        appointment.BookingStatus = appointmentid.Value ? 4 : 5;
                    }
                    break;
                case (int)Common.MedicalProviderType.lab:
                    foreach (var appointmentid in invoiceappointmentsreset)
                    {
                        var appointment = context.LabsAppointments.Where(ap => ap.ID == appointmentid.Key).Single();
                        appointment.BookingStatus = appointmentid.Value ? 4 : 5;
                    }
                    break;
                case (int)Common.MedicalProviderType.intencivecare:
                    foreach (var appointmentid in invoiceappointmentsreset)
                    {
                        var appointment = context.IntensiveCare_Appointment.Where(ap => ap.Id == appointmentid.Key).Single();
                        appointment.StatusId = appointmentid.Value ? 4 : 5;
                    }
                    break;
                case (int)Common.MedicalProviderType.medicalcenter:
                    foreach (var appointmentid in invoiceappointmentsreset)
                    {
                        var appointment = context.Appointments.Where(ap => ap.Appointment_ID == appointmentid.Key).Single();
                        appointment.BookingStatus = appointmentid.Value ? 4 : 5;
                    }
                    break;
                case (int)Common.MedicalProviderType.radiologycenter:
                    foreach (var appointmentid in invoiceappointmentsreset)
                    {
                        var appointment = context.RadiologyCenterAppointments.Where(ap => ap.ID == appointmentid.Key).Single();
                        appointment.BookingStatus = appointmentid.Value ? 4 : 5;
                    }
                    break;
            }

            context.SaveChanges();
        }
        public List<InvoiceAppointmentViewModel> GetInvoiceAppointments(int id)
        {
            var invoice = context.Invoices.Include(i => i.Invoice_Appointment).Where(i => i.InvoiceID == id).Single();
            var appointmentids = invoice.Invoice_Appointment.Select(i => i.Appointment_Id);
            int providertype = invoice.DoctorId.HasValue ? (int)Common.MedicalProviderType.doctor
                : (int)invoice.Health_Entitiy.Health_Entitiy_Type;
            var appointments = new List<InvoiceAppointmentViewModel>();
            switch (providertype)
            {
                case (int)Common.MedicalProviderType.doctor:
                    appointments = context.Appointments.Where(ap =>
                             (appointmentids.Contains(ap.Appointment_ID))).Select(ap => new InvoiceAppointmentViewModel
                             {
                                 Id = ap.Appointment_ID,
                                 Code = ap.Appointment_CODE,
                                 CreationDate = ap.Appointment_Creation_Datetime,
                                 VisitDate = ap.Appointment_Date,
                                 PatientName = ap.PatientName ?? ap.Patient.Patient_FullName,
                                 PatientPhone = ap.Patient.Patient_Mobile,
                                 StatusName = ap.AppointmentStatu.AppointmentStatusNameAR,
                                 Status = ap.BookingStatus,
                                 AppointmentFees = ap.Fees_For_Doctor
                             }).ToList();
                    break;
                case (int)Common.MedicalProviderType.hospital:
                    appointments = context.Appointments.Where(ap =>
                             (appointmentids.Contains(ap.Appointment_ID))).Select(ap => new InvoiceAppointmentViewModel
                             {
                                 Id = ap.Appointment_ID,
                                 Code = ap.Appointment_CODE,
                                 CreationDate = ap.Appointment_Creation_Datetime,
                                 VisitDate = ap.Appointment_Date,
                                 PatientName = ap.PatientName ?? ap.Patient.Patient_FullName,
                                 PatientPhone = ap.Patient.Patient_Mobile,
                                 StatusName = ap.AppointmentStatu.AppointmentStatusNameAR,
                                 Status = ap.BookingStatus,
                                 AppointmentFees = ap.Fees_For_Doctor
                             }).ToList();
                    break;
                case (int)Common.MedicalProviderType.lab:
                    appointments = context.LabsAppointments.Where(ap =>
                            (appointmentids.Contains(ap.ID))).Select(ap => new InvoiceAppointmentViewModel
                            {
                                Id = ap.ID,
                                Code = ap.AppointmentCODE,
                                CreationDate = ap.AppointmentCreationDatetime,
                                VisitDate = ap.AppointmentDate,
                                PatientName = ap.PatientName ?? ap.Patient.Patient_FullName,
                                PatientPhone = ap.Patient.Patient_Mobile,
                                StatusName = ap.AppointmentStatu.AppointmentStatusNameAR,
                                Status = ap.BookingStatus,
                                AppointmentFees = ap.AppointmentActualAmount
                            }).ToList();
                    break;
                case (int)Common.MedicalProviderType.radiologycenter:
                    appointments = context.RadiologyCenterAppointments.Where(ap =>
                            (appointmentids.Contains(ap.ID))).Select(ap => new InvoiceAppointmentViewModel
                            {
                                Id = ap.ID,
                                Code = ap.AppointmentCODE,
                                CreationDate = ap.AppointmentCreationDatetime,
                                VisitDate = ap.AppointmentDate,
                                PatientName = ap.PatientName ?? ap.Patient.Patient_FullName,
                                PatientPhone = ap.Patient.Patient_Mobile,
                                StatusName = ap.AppointmentStatu.AppointmentStatusNameAR,
                                Status = ap.BookingStatus,
                                AppointmentFees = ap.AppointmentActualAmount
                            }).ToList();

                    break;
                case (int)Common.MedicalProviderType.medicalcenter:
                    appointments = context.Appointments.Where(ap =>
                               (appointmentids.Contains(ap.Appointment_ID))).Select(ap => new InvoiceAppointmentViewModel
                               {
                                   Id = ap.Appointment_ID,
                                   Code = ap.Appointment_CODE,
                                   CreationDate = ap.Appointment_Creation_Datetime,
                                   VisitDate = ap.Appointment_Date,
                                   PatientName = ap.PatientName ?? ap.Patient.Patient_FullName,
                                   PatientPhone = ap.Patient.Patient_Mobile,
                                   StatusName = ap.AppointmentStatu.AppointmentStatusNameAR,
                                   Status = ap.BookingStatus,
                                   AppointmentFees = ap.Fees_For_Doctor
                               }).ToList();
                    break;
            }

            return appointments;
        }
        public void Dispose()
        {
            context.Dispose();
        }
    }
}