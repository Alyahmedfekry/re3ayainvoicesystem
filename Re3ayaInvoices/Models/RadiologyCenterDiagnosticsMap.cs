//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Re3ayaInvoices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RadiologyCenterDiagnosticsMap
    {
        public int Id { get; set; }
        public Nullable<int> DiagnosticCategoryId { get; set; }
        public Nullable<int> DiagnosticId { get; set; }
        public int RadiologyCenterId { get; set; }
        public Nullable<int> BranchId { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    
        public virtual Health_Entitiy Health_Entitiy { get; set; }
        public virtual RadiologyCenterBranch RadiologyCenterBranch { get; set; }
    }
}
