//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Re3ayaInvoices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Work_Plan
    {
        public int Doctor_Work_Plan_ID { get; set; }
        public Nullable<int> DayOfWeek_ID { get; set; }
        public Nullable<bool> Is_Working_Day { get; set; }
        public Nullable<System.TimeSpan> Start_Working_Hour { get; set; }
        public Nullable<System.TimeSpan> End_Working_Hour { get; set; }
        public Nullable<int> Address_ID { get; set; }
        public Nullable<int> Hospital_Doc_CardID { get; set; }
        public Nullable<int> Vid_Chat_DID { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<int> Health_Entitiy_ID { get; set; }
    
        public virtual Address_Book Address_Book { get; set; }
        public virtual Health_Entitiy Health_Entitiy { get; set; }
        public virtual HospitalDoc_Card HospitalDoc_Card { get; set; }
    }
}
