//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Re3ayaInvoices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Test
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Test()
        {
            this.LabsAppointmentDetails = new HashSet<LabsAppointmentDetail>();
        }
    
        public int Id { get; set; }
        public string Name_AR { get; set; }
        public string Name_EN { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<int> Test_Group_ID { get; set; }
        public Nullable<int> TestCategoryId { get; set; }
        public string Code { get; set; }
    
        public virtual TestCategory TestCategory { get; set; }
        public virtual TestGroup TestGroup { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LabsAppointmentDetail> LabsAppointmentDetails { get; set; }
    }
}
