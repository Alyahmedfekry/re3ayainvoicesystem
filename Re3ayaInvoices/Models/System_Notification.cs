//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Re3ayaInvoices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class System_Notification
    {
        public int System_NotificationID { get; set; }
        public string Notification_Text { get; set; }
        public string Notification_Title { get; set; }
        public Nullable<System.DateTime> Notification_GenDate { get; set; }
        public Nullable<bool> Notification_IsSeen { get; set; }
        public Nullable<int> Notification_ByUser { get; set; }
    }
}
