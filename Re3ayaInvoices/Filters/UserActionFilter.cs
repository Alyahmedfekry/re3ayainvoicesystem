﻿using Re3ayaInvoices.CustomAuthentication;
using Re3ayaInvoices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.viewModels;
using System.Data.Entity;
namespace Re3ayaInvoices.Filters
{
    public class UserActionFilter : ActionFilterAttribute
    {
        private readonly Re3aya247Entities context;
        public UserActionFilter()
        {
            context = new Re3aya247Entities();
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var user = filterContext.Controller.ControllerContext.HttpContext.User as CustomPrincipal;
            if (user != null)
            {
                if (user.Rolesid == null)
                {
                    user.Rolesid = context.Users.Where(u => u.Id == user.UserId).FirstOrDefault().UserRoles.Select(r => r.RoleId).ToList();
                }
                var usermenu = context.UserMenus.Include(u => u.SystemMenu).Where(u =>
                  (u.IsAllow == true)
                && ((u.UserId == user.UserId) || (user.Rolesid.Contains(u.RoleId)))).ToList();

                var menus = new List<UserMenuViewModel>();
                menus = usermenu.Where(u => !u.SystemMenu.ParentId.HasValue)
                    .Select(u => u.SystemMenu).OrderBy(u => u.Rank).Select(u => new UserMenuViewModel
                    {
                        Id = u.Id,
                        MenuItemName = u.NameAr,
                        Rank = u.Rank,
                        IsAction = u.ActionId.HasValue,
                        ActionLink = u.ActionId.HasValue ? u.SystemAction.ActionLink : "",
                        Childs = GetChildren(usermenu, u.Id)
                    }).OrderBy(u => u.Rank).ToList();

                filterContext.Controller.ViewBag.menu = menus;
            }
        }
        public List<UserMenuViewModel> GetChildren(List<UserMenu> menus, int parentId)
        {
            return menus.Select(u => u.SystemMenu).Where(u => u.ParentId == parentId).OrderBy(u => u.Rank)
                    .Select(c => new UserMenuViewModel
                    {
                        Id = c.Id,
                        MenuItemName = c.NameAr,
                        ParentId = c.ParentId,
                        IsAction = c.ActionId.HasValue,
                        ActionLink = c.ActionId.HasValue ? c.SystemAction.ActionLink : "",
                        Childs = !c.ActionId.HasValue ? GetChildren(menus, c.Id) : null,
                        Rank = c.Rank
                    }).OrderBy(u => u.Rank)
                    .ToList();
        }
    }
}