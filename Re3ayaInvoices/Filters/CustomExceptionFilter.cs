﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Services;
namespace Re3ayaInvoices.Filters
{
    public class CustomExceptionFilter : FilterAttribute, IExceptionFilter
    {
        private readonly CommunicationService communicationService;
        public CustomExceptionFilter()
        {
            communicationService = new CommunicationService();
        }
        public void OnException(ExceptionContext filterContext)
        {
            if (!HttpContext.Current.IsDebuggingEnabled)
            {
                string senderemail = System.Configuration.ConfigurationManager.AppSettings["SenderEmailID"];
                string senderpassword = System.Configuration.ConfigurationManager.AppSettings["SenderEmailPassword"];
                string url = filterContext.HttpContext.Request.Url.ToString();
                string recipientemail = System.Configuration.ConfigurationManager.AppSettings["ExceptionEmailID"];
                var msg = filterContext.Exception.Message != null ? filterContext.Exception.Message : "Unknown Msg";
                msg += url + System.Environment.NewLine +
                        "Error Details:" + System.Environment.NewLine + msg;
                string subject = "We Have An Error!!";

                communicationService.SendEmail(msg, subject, senderemail, senderpassword, recipientemail);
            }
        }
    }
}