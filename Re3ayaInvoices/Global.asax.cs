﻿using Newtonsoft.Json;
using Re3ayaInvoices.CustomAuthentication;
using Re3ayaInvoices.viewModels;
using Re3ayaInvoices.Helpers;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
namespace Re3ayaInvoices
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            var cookiename = Common.Ase_EncryptString("ReInvCookie");
            HttpCookie authCookie = Request.Cookies["ReInvCookie"];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                var serializeModel = JsonConvert.DeserializeObject<CustomSerializeModel>(authTicket.UserData);
                CustomPrincipal principal = new CustomPrincipal(authTicket.Name);
                principal.UserId = serializeModel.UserId;
                principal.FirstName = serializeModel.Name;
                HttpContext.Current.User = principal;
            }
        }
    }
}
