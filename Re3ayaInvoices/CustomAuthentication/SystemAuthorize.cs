﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Re3ayaInvoices.Services;

namespace Re3ayaInvoices.CustomAuthentication
{
    public class SystemAuthorize : AuthorizeAttribute
    {
        private readonly AuthorizeService AuthorizeService;
        private readonly SystemService SystemService;
        private CustomPrincipal CurrentUser;
        public SystemAuthorize()
        {
            AuthorizeService = new AuthorizeService();
            SystemService = new SystemService();
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            CurrentUser = HttpContext.Current.User as CustomPrincipal;
            if (CurrentUser == null)
            {
                return false;
            }
            var user = SystemService.GetUser(CurrentUser.UserId);
            if (!user.IsActive.Value)
            {
                CurrentUser.IsActive = false;

                return false;
            }
            CurrentUser.Rolesid = user.Roles;
            var routingdata = httpContext.Request.RequestContext.RouteData;
            string controller = routingdata.Values["controller"].ToString();
            string action = routingdata.Values["action"].ToString();

            return AuthorizeService.HasAccessTo(controller, action);
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            RedirectToRouteResult routeData = null;

            if (CurrentUser == null)
            {
                routeData = new RedirectToRouteResult
                    (new System.Web.Routing.RouteValueDictionary
                    (new
                    {
                        controller = "Account",
                        action = "Login",
                    }
                    ));
            }
            else if (!CurrentUser.IsActive)
            {
                routeData = new RedirectToRouteResult
                   (new System.Web.Routing.RouteValueDictionary
                   (new
                   {
                       controller = "Account",
                       action = "LogOut",
                   }
                   ));
            }
            else
            {
                routeData = new RedirectToRouteResult
                (new System.Web.Routing.RouteValueDictionary
                 (new
                 {
                     controller = "Error",
                     action = "AccessDenied"
                 }
                 ));
            }

            filterContext.Result = routeData;
        }
    }
}