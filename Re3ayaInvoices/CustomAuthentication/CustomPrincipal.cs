﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace Re3ayaInvoices.CustomAuthentication
{
    public class CustomPrincipal : IPrincipal
    {
        #region Identity Properties
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }
        public bool IsActive { get; set; }
        public List<int?> Rolesid { get; set; }
        #endregion
        public IIdentity Identity
        {
            get; private set;
        }
        public bool IsInRole(string role)
        {
            return true;
            if (string.IsNullOrEmpty(role))
                return true;
            if (Roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public CustomPrincipal(string username)
        {
            Identity = new GenericIdentity(username);
        }
    }
}