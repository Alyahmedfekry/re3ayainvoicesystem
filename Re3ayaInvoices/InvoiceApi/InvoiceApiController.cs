﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Re3ayaInvoices.Services;
using Re3ayaInvoices.viewModels;
namespace Re3ayaInvoices.InvoiceApi
{
    [AllowAnonymous]
    public class InvoiceApiController : ApiController
    {
        private readonly InvoiceService invoiceService;
        private readonly ReportService reportService;
        public InvoiceApiController()
        {
            invoiceService = new InvoiceService();
            reportService = new ReportService();
        }
        [Route("api/InvoiceApi/CreateInvoice")]
        [HttpPost]
        public IHttpActionResult CreateInvoice(CreateInvoiceDTO request)
        {
            invoiceService.CreateInvoice(request.ProviderType, request.ProviderId, request.InvoiceType
                , request.invoicedate);
            return Ok();
        }
        [Route("api/InvoiceApi/ResetInvoice")]
        [HttpPost]
        public IHttpActionResult ResetInvoice(ResetInvoiceData request)
        {
            invoiceService.ResetInvoice(null, request.AppointmentId, request.ProviderType,null);
            return Ok();
        }
        [Route("api/InvoiceApi/GetInvoice")]
        [HttpPost]
        public HttpResponseMessage GetInvoice(InvoiceViewModel invoice)
        {
            string reportname;
            var stream = reportService.GenerateInvoiceFile(invoice.InvoiceId, invoice.ProviderType.Value, out reportname);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = reportname;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

            return response;

        }
        [Route("api/InvoiceApi/GetInvoices")]
        [HttpPost]
        public IHttpActionResult GetInvoices(int? page, InvoiceSearch search)
        {
            var invoices = invoiceService.Invoices(page, search);
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                items = invoices,
                metaData = invoices.GetMetaData()
            });
            return Ok(json);
        }
    }
}
