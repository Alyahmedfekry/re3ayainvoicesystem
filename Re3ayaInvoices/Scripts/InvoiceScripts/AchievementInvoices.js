﻿$(document).ready(function () {
    $('#searchform').attr('action', '/Home/AchievementInvoices');
    $('#From').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });
    $('#To').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    });

    $('#sheet').on('click', function (e) {
        e.preventDefault();
        $('#searchform').attr('action', "/ExcelSheetExport/AchievementInvoices").submit();
        $('#searchform').attr('action', '/Home/AchievementInvoices');
    });
    $('#pdf').on('click', function (e) {
        e.preventDefault();
        $('#searchform').attr('action', "/PdfExport/AchievementInvoices").submit();
        $('#searchform').attr('action', '/Home/AchievementInvoices');
    });
    $('#sendwhatsappmessage').on('click', function (e) {
        e.preventDefault();
        $('#searchform').attr('action', "/Communication/SendWhatsapp_AchievementInvoices").submit();
        $('#searchform').attr('action', '/Home/AchievementInvoices');
    });


});