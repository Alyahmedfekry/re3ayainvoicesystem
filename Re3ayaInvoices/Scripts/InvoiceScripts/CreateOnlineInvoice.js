﻿$(document).ready(function () {
    $('#InvoiceDate').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $(function () {
        $("#form").validate(
            {
                rules:
                {
                    InvoiceDate: "required",
                    //providertype: "required",
                    //invoicetypeid: "required"
                },
                messages: {
                    InvoiceDate: "Please, select a date",
                    //providertype: "Please, select a provider",
                    //invoicetypeid: "Please, select an invoice type",
                },
                submitHandler: function () {
                    $('#create').prop("disabled", true);
                    var invoiceDate = $('#InvoiceDate').val();
                    var subscriptionType = $('#subscriptiontype').val();
                    //var providerType = $('#providertype').val();
                    var providerCode = $('#providercode').val();
                    //var invoiceType = $('#invoicetypeid').val();
                    var cityId = $('#cityid').val();
                    var btn = $('#create');
                    NProgress.start();
                    $.ajax({
                        type: 'get',
                        url: '/Common/GetActivatedProviderIds',
                        data: { providertype:0, providercode: providerCode, cityid: cityId },
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        success: function (data) {
                            debugger;
                            if (data != null) {
                                $.each(data, function (index, element) {
                                    $.ajax(
                                        {
                                            type: 'post',
                                            url: '/Home/CreateOnlineInvoice',
                                            contentType: "application/json; charset=utf-8",
                                            data: JSON.stringify({
                                                invoicedate: invoiceDate,
                                                subscriptiontype: subscriptionType,
                                                //providertype: providerType,
                                                providerid: element,
                                                //invoicetype: invoiceType
                                            }),
                                            async: false,
                                            dataType: 'json',
                                        });
                                });
                                $('#msg').css('display', 'block');
                                $(btn).prop("disabled", false);
                                NProgress.done();
                            }
                        }
                    });
                }
            });
    });
});