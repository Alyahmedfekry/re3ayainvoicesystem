﻿$(document).ready(function () {
    $('[name="From"]').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('[name="To"]').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
});