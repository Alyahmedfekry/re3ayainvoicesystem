﻿$(document).ready(function () {
    $('#FromDate').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#ToDate').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
});