﻿$(document).ready(function () {
    $('[name="from"]').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('[name="to"]').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#CityIds').on('change', function () {
        debugger;
        var cityIds = $(this).val();
        if (cityIds.length == 0) {
            $('#RegionIds').find('option').remove();
        }
        else {
            $.ajax({
                type: 'post',
                url: '/Common/GetMultiRegionList',
                data: JSON.stringify({ cityid: cityIds }),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    $('#RegionIds').select2();
                    $(data).each(function (index, element) {
                        var option = new Option(element.Text, element.Value, false, false);
                        $('#RegionIds').append(option).trigger('change');
                    });
                }
            })
        }
    });
});
