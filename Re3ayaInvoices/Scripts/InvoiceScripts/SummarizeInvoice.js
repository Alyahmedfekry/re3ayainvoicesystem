﻿$(document).ready(function () {
    $('#From').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#To').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
});