﻿$(document).ready(function () {
    var cityIds = $('#CityIds').val();
    if (cityIds.length == 0) {
        $('#RegionIds').find('option').remove();
    }
    else {
        $.ajax({
            type: 'post',
            url: '/Common/GetMultiRegionList',
            data: JSON.stringify({ cityid: cityIds }),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (data) {
                $('#RegionIds').select2();
                $(data).each(function (index, element) {
                    var selected = false;
                    for (var i = 0; i < regionIds.length; i++) {
                        if (element.Value == regionIds[i]) {
                            selected = true;
                        }
                    }
                    var option = new Option(element.Text, element.Value, false, selected);
                    $('#RegionIds').append(option).trigger('change');
                });
            }
        })
    }
    $('#From').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#To').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#CityIds').on('change', function () {
        debugger;
        var cityIds = $(this).val();
        if (cityIds.length == 0) {
            $('#RegionIds').find('option').remove();
        }
        else {
            $.ajax({
                type: 'post',
                url: '/Common/GetMultiRegionList',
                data: JSON.stringify({ cityid: cityIds }),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    $('#RegionIds').select2();
                    $(data).each(function (index, element) {
                            var option = new Option(element.Text, element.Value, false, false);
                            $('#RegionIds').append(option).trigger('change');
                        });
                }
            })
        }
    });
    $('[name="resetinvoice"]').on('click', function (e) {
        e.preventDefault();
        NProgress.start();
        var invoiceId = $(this).parent().attr('id');
        $.ajax({
            type: 'post',
            url: '/Home/ResetInvoice',
            data: JSON.stringify({ id: invoiceId }),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (data) {
                NProgress.done();
                toastr.success(data.msg, 'Done', { timeOut: 5000 });
            },
            error: function (xhr, status, error) {
                NProgress.done();
                toastr.error(error.message, 'error:', { timeOut: 5000 })
            }
        });
    });
    $('[name="cancelinvoice"]').on('click', function (e) {
        e.preventDefault();
        var invoiceId = $(this).parent().attr('id');
        $('#cancelpaidModal .modal-body').find('#resetform').find('#invoiceid').val(invoiceId);
        $('#cancelpaidModal .modal-body').find('#resetform').find('#cancellationtype').val(0);
        $('#cancelpaidModal').modal({ show: true });
    });
    $('[name="cancelpaid"]').on('click', function (e) {
        e.preventDefault();
        var invoiceId = $(this).attr('id');
        $('#cancelpaidModal .modal-body').find('#resetform').find('#invoiceid').val(invoiceId);
        $('#cancelpaidModal .modal-body').find('#resetform').find('#cancellationtype').val(1);
        $('#cancelpaidModal').modal({ show: true });
    });
    $('#resetform').on('submit', function (e) {
        e.preventDefault();
        NProgress.start();
        var invoiceId = $(this).find('#invoiceid').val();
        var _password = $(this).find('#password').val();
        var cancellationType = $(this).find('#cancellationtype').val();
        var actionName = "";
        switch (cancellationType) {
            case '0':
                actionName = 'CancelInvoice';
                break;
            case '1':
                actionName = 'CancelPaid';
                break;
        }
        $.ajax(
            {
                type: 'get',
                url: '/Home/' + actionName + '',
                data: { invoiceid: invoiceId, password: _password },
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    NProgress.done();
                    if (data.status == '0') {
                        toastr.error(data.msg, 'error', { timeOut: 5000 });
                    }
                    else {
                        toastr.success(data.msg, 'Done', { timeOut: 5000 });
                    }
                    ('#resetform').find('#cancellationtype').val('');
                    $('#cancelpaidModal').modal('toggle');
                    location.reload();
                },
                error: function (xhr, status, error) {
                    NProgress.done();
                    ('#resetform').find('#cancellationtype').val('');
                    toastr.error(data.message, 'Ops!', { timeOut: 5000 })
                }
            });
    });
    $('#sheet').on('click', function (e) {
        e.preventDefault();
        $('#searchform').attr('action', "/ExcelSheetExport/InvoicesSheet").submit();
        $('#searchform').attr('action', "/Home/Invoices");
    });
    $('#print').on('click', function (e) {
        e.preventDefault();
        $('#searchform').attr('action', "/Report/DownloadInvoices").submit();
        $('#searchform').attr('action', "/Home/Invoices");
    });
    $('#subscriptionhasreservation').on('click', function (e) {
        e.preventDefault();
        $('#searchform').attr('action', "/Report/DownloadUnpaidSubscriptionInvoices").submit();
        $('#searchform').attr('action', "/Home/Invoices");
    });
    $('[name="appointmentlist"]').on('click', function (e) {
        e.preventDefault();
        var invoiceId = $(this).parent().attr('id');
        $('#appointmentModal .modal-body').load('/Home/InvoiceAppointments?id=' + invoiceId + '', function () {
            $('#appointmentModal').modal({ show: true });
        });
    });
    $('[name="review"]').on('click', function (e) {
        e.preventDefault();
        var invoiceId = $(this).parent().attr('id');
        $('#detailsModal .modal-body').load('/Home/InvoiceDetailsPartial?id=' + invoiceId + '', function () {
            $('#detailsModal').modal({ show: true });
        });
    });
    $('body').on('click', '#resetappointment', function (e) {
        e.preventDefault();
        var invoiceId = $('body').find('#resetappform').find('[name="id"]').val();
        var selectedRows = $('body').find('#appointmentstable > tbody  > tr');
        var confirmedApps = [];
        var canceledApps = [];
        debugger;
        for (var i = 0; i < selectedRows.length; i++) {
            var key = $(selectedRows[i]).children('td:first').find('[name="key"]').val();
            var value = $(selectedRows[i]).children('td:first').find('[name="value"]').is(':checked');
            if (value) {
                confirmedApps.push(key);
                continue;
            }
            canceledApps.push(key);
        }
        $.ajax({
            type: 'post',
            url: '/Home/ResetInvoice',
            data: JSON.stringify({ id: invoiceId, confirmedapps: confirmedApps, canceledapps: canceledApps }), 
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (data) {
                $('#appointmentModal').modal('toggle');
            }
        });
    });
});
$('input[name="IsInsurance"]').on('change', function (e) {
    if (window.confirm("Status Will Be Changed ?")) {
        debugger;
        NProgress.start();
        var _id = $(this).attr('data-value');

        var btn = $(this);
        var status = false;
        if ($(this).prop('checked')) {
            status = true;
        }
        $.ajax(
            {
                type: 'get',
                url: '/Home/UpdateDeductionStatus',
                data: { invoice_id: _id, status: status },
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    debugger;
                    NProgress.done();
                    toastr.success(data.message, 'Done', { timeOut: 5000 });
                },
                error: function (xhr, status, error) {
                    debugger;
                    NProgress.done();
                    toastr.error(error.message, 'Ops!', { timeOut: 5000 })
                }
            });
        return false;
    }
    else {
        debugger;
        var currentStatus = !$(this).prop('checked');
        $(this).prop('checked', currentStatus);
    }
})
function SendWhatsAppMessage(id) {
    $.ajax({
        url: "/Communication/SendWhatsapp",
        data: { invoice_id: id },
        type: "Get",
        success: function (response) {
            if (response.success == true) {
                toastr.success(response.message, "Done")
            }
        }
    })
}
