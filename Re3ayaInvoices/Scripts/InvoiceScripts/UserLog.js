﻿$(document).ready(function () {
    $('#From').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#To').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#sheet').on('click', function (e) {
        $('#searchform').attr('action', "/ExcelSheetExport/UserLogSheet");
        $('#searchform').find(' input[type=submit]').click();
        $('#searchform').attr('action', "/Account/UserLog");
    });
});