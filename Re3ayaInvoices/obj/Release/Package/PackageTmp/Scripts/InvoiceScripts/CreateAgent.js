﻿$(document).ready(function () {
    debugger;
    if (typeof cityIds !== 'undefined') {
        if (cityIds.length > 0) {
            $.ajax(
                {
                    type: 'post',
                    url: '/Common/GetMultiRegionList',
                    data: JSON.stringify({ cityid: cityIds }),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (data) {
                        $('#RegionIds').select2();
                        $(data).each(function (index, element) {
                            var selected = false;
                            $(regionIds).each(function (index, elem) {
                                if (elem == element.Value) {
                                    selected = true;
                                }
                            });
                            var option = new Option(element.Text, element.Value, false, selected);
                            $('#RegionIds').append(option).trigger('change');
                        });
                    }
                });
        }
    }
    $('#CityIds').on('change', function () {
        $('#RegionIds').find('option').remove();
        var cityId = $(this).val();
        if (cityId.length == 0) {
            return false;
        }
        $.ajax(
            {
                type: 'post',
                url: '/Common/GetMultiRegionList',
                data: JSON.stringify({ cityid: cityId }),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data) {
                    $('#RegionIds').select2();
                    $(data).each(function (index, element) {
                        var option = new Option(element.Text, element.Value, false, false);
                        $('#RegionIds').append(option).trigger('change');
                    });
                }
            });
    })
})