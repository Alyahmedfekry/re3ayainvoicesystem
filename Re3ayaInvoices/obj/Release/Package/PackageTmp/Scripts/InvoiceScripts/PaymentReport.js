﻿$(document).ready(function () {
    $('#From').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#To').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#sheet').on('click', function (e) {
        e.preventDefault();
        $('#form').attr('action', "/ExcelSheetExport/PaymentReport").submit();
        $('#form').attr('action', "/Report/PaymentReport");
    });
});