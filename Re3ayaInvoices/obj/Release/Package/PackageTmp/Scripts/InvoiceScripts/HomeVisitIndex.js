﻿    $(document).ready(function () {
        $('#InvoiceDate').datetimepicker({
            timepicker: false,
            format: 'm/d/Y'
        });
    $(function () {
        $("#form").validate(
            {
                rules:
                {
                    InvoiceDate: "required"
                },
                messages: {
                    InvoiceDate: "Please, select a date"
                },
                submitHandler: function () {
                    $('#create').prop("disabled", true);
                    $('#create').html('Create<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>')
                    $('#msg').css('display', 'none');
                    var i = 0;
                    var invoiceDate = $('#InvoiceDate').val();
                    var doctorCode = $('#code').val();
                    var btn = $('#create');
                    var governorate = $('#governorate option:selected').val();
                    NProgress.start();
                    $.ajax(
                        {
                            type: 'get',
                            url: '/Invoices/GetHomevisitDoctors',
                            contentType: "application/json; charset=utf-8",
                            data: { InvoiceDate: invoiceDate, code: doctorCode, governorate: governorate },
                            dataType: 'json',
                            success: function (data) {
                                debugger
                                if (!data.status) {
                                    $('#errormsg').text(data.msg);
                                    NProgress.done();
                                    return;
                                }
                                $('#errormsg').text('');
                                $.each(data.data, function (index, value) {
                                    $.ajax(
                                        {
                                            type: 'post',
                                            url: '/Invoices/CreateHomevisitAppInvoice',
                                            data: JSON.stringify({ id: value, InvoiceDate: invoiceDate }),
                                            contentType: "application/json; charset=utf-8",
                                            dataType: 'json',
                                            async: false,
                                            success: function (data) {
                                            },
                                            error: function (xhr, status, error) {
                                                toastr.error(xhr.statusText);
                                                NProgress.done();
                                                return false;
                                            }
                                        });
                                });
                                if (data.length > 0) {
                                    $('#msg').css('display', 'block');
                                }
                                $(btn).prop("disabled", false);
                                $(btn).html("Create");
                                NProgress.done();
                                window.location.href = '/Invoices/CreatedFailed?type=' + invoiceType+'&invoicedate=' + invoiceDate + '';
                            },
                            error: function (xhr, status, error) {
                                toastr.error(xhr.statusText);
                            }
                        });
                }
            });
    });
});