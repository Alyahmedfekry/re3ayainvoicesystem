﻿$(document).ready(function () {
    $('#fromdate').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $('#todate').datetimepicker({
        timepicker: false,
        format: 'm/d/Y'
    });
    $(function () {
        $("#form").validate(
            {
                rules:
                {
                    fromdate: "required",
                    providertype: "required",
                    todate: "required"
                },
                messages: {
                    fromdate: "Please, select a from date",
                    todate: "Please, select a to date",
                    providertype: "Please, select a provider"
                },
                submitHandler: function () {
                    $('#create').prop("disabled", true);
                    var fromDate = $('#fromdate').val();
                    var toDate = $('#todate').val();
                    var providerType = $('#providertype').val();
                    var providerCode = $('#providercode').val();
                    var cityId = $('#cityid').val();
                    var btn = $('#create');
                    NProgress.start();
                    $.ajax({
                        type: 'get',
                        url: '/Common/GetActivatedProviderIds',
                        data: { providertype: providerType, providercode: providerCode, cityid: cityId },
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        success: function (data) {
                            debugger;
                            if (data != null) {
                                $.each(data, function (index, element) {
                                    $.ajax(
                                        {
                                            type: 'post',
                                            url: '/Home/CreateAppointmentInvoice',
                                            contentType: "application/json; charset=utf-8",
                                            data: JSON.stringify({
                                                fromdate: fromDate,
                                                todate: toDate,
                                                providertype: providerType,
                                                providerid: element,
                                            }),
                                            async: false,
                                            dataType: 'json',
                                        });
                                });
                                $('#msg').css('display', 'block');
                                $(btn).prop("disabled", false);
                                NProgress.done();
                            }
                        }
                    });
                }
            });
    });
});