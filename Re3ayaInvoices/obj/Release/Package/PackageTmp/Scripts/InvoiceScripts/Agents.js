﻿$(document).ready(function () {
    $('[name="activestatus"]').on('change', function () {
        if (window.confirm("Active Status Will Be Changed ?")) {
            var status = $(this).prop("checked");
            NProgress.start();
            var _id = $(this).attr('id');
            $.ajax(
                {
                    type: 'get',
                    url: '/Agent/ChangeActiveStatus',
                    data: { id: _id, isactive: status },
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        NProgress.done();
                        toastr.success(data, 'Done', { timeOut: 5000 });
                    },
                    error: function (xhr, status, error) {
                        NProgress.done();
                    }
                });
        }
        else {
            debugger;
            var currentStatus = !$(this).prop('checked');
            $(this).prop('checked', currentStatus);
        }
    })
})