﻿
select * from ProviderService join ProviderService_Category
on(ProviderService.CategoryId=ProviderService_Category.Id)
where ProviderType=2;

insert into ProviderService(ArabicName,EnglishName,CategoryId)
select 
 Tests.Name_AR
,Tests.Name_EN
,ProviderService_Category.Id
from Tests join TestGroup
on(Tests.Test_Group_ID=TestGroup.Id)
join ProviderService_Category
on((TestGroup.Name = ProviderService_Category.EnglishName)
and(TestGroup.NameAr = ProviderService_Category.ArabicName))
where Tests.IsDelete<>1;

insert into ServicePriceList (ServiceId,Price,ProviderId,ProviderType)
select 
 ProviderService.Id
,Labs.LabsTestsMap.Price
,Labs.LabsTestsMap.LabId
,2
from Labs.LabsTestsMap join Tests
on(Labs.LabsTestsMap.TestId=Tests.Id)
join ProviderService
on((ProviderService.EnglishName=Tests.Name_EN) and (ProviderService.ArabicName = Tests.Name_AR))
join ProviderService_Category
on(ProviderService_Category.Id = ProviderService.CategoryId)
where
(ProviderService_Category.ProviderType=2)
and
(Labs.LabsTestsMap.Price<>0 and Labs.LabsTestsMap.IsDeleted<>1);


select * from Labs.LabsTestsMap join Tests
on(Labs.LabsTestsMap.TestId=Tests.Id)
where Price<>0 and IsDeleted<>1 and Tests.IsDelete<>1;


select 
 ServicePriceList.ProviderId
,ServicePriceList.ServiceId
,ServicePriceList.Price
,count(ServicePriceList.ServiceId)
from ServicePriceList
group by 
 ServicePriceList.ProviderId
,ServicePriceList.ServiceId
,ServicePriceList.Price
having count(ServicePriceList.ServiceId) > 1;

select Labs.LabsTestsMap.* from Labs.LabsTestsMap 
join Tests
on(Tests.Id =Labs.LabsTestsMap.TestId)
where (Tests.IsDelete<>1 and Labs.LabsTestsMap.LabId=3428 and Labs.LabsTestsMap.Price=50);


select * from ServicePriceList where ProviderId=3074;


select * from Tests where Tests.Id=448 or Tests.Id=526;
