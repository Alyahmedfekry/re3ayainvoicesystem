﻿SELECT 
  Inv.InvoiceID as InvoiceID
, Inv.InvoiceStartDate as InvoiceStartDate
, Doc.Doctor_Name_EN as Doctor_Name_EN
, Doc.Doctor_CODE as Doctor_CODE
, Inv.InvoiceCreationDatetime as InvoiceCreationDatetime
, Inv.Invoice_Number as Invoice_Number
, Doc.No_Views as No_Views
, Inv.IsCanceled as IsCanceled
, Inv.Accesscode as Accesscode
, Doc.PhoneNumber1_Master as PhoneNumber1_Master
, dbo.Address_Book.AddressSpeciaalMark as AddressSpeciaalMark
, dbo.Address_Book.Address_Name_AR as Address_Name_AR
, Region.Region_Name_AR as Region_Name_AR
, Governrate.Gov_Name_AR as Gov_Name_AR
, Inv.TotalPaid as TotalPaid
, case when Account_Products.ItemId = 5 then Account_Doctor.Debit else 0 end as ConfirmedAppointments
, case when Account_Products.ItemId = 6 then Account_Doctor.Debit else 0 end as UnconfirmedAppointments

--, Inv.InvoiceEnddate as InvoiceEnddate
--, Inv.TotalAmount as TotalAmount
--,(
--   select Account_Doctor.Debit 
--   from Account_Doctor  inner join Account_Products on Account_Doctor.ProductID=Account_Products.Account_Porduct_ID
--   where 
--   (Account_Doctor.InvoiceID = Inv.InvoiceID) 
--    and
--   (Account_Products.ItemId = 5)
--  ) as ConfirmedAppointments
-- ,(
--   select Account_Doctor.Debit 
--   from  Account_Doctor  inner join Account_Products on Account_Doctor.ProductID=Account_Products.Account_Porduct_ID
--   where 
--   (Account_Doctor.InvoiceID = Inv.InvoiceID) 
--   and
--   (Account_Products.ItemId = 6)
--  ) as UnconfirmedAppointments
   
--, Inv.AddressId as AddressId
--, dbo.Address_Book.Address_Name_EN as Address_Name_EN
--, Doc.DoctorID as DoctorID
--, Doc.Doctor_Name_AR as Doctor_Name_AR
--, Doc.Email as Email
--, Address_Book.RegionID as RegionID
FROM            
Invoice as Inv join Account_Doctor
on (Inv.InvoiceID=Account_Doctor.InvoiceID)
join Account_Products
on (Account_Doctor.ProductID = Account_Products.Account_Porduct_ID)

JOIN Doctor as Doc
ON (Inv.DoctorId = Doc.DoctorID) 
 LEFT JOIN Address_Book 
on (Doc.DoctorID=Address_Book.Doctor_ID and Address_Book.IsMain = 1)
LEFT join Region
on(Region.RegionID = Address_Book.RegionID)
LEFT join Governrate
on(Governrate.GovID = Region.CityID)
where
(day(Inv.InvoiceStartDate) between 1 and 30)
and
(month(Inv.InvoiceStartDate) between 08 and 09)
and
(year(Inv.InvoiceStartDate) between 2021 and 2021)
and (Inv.TypeId = 2)
and (Inv.SerialNumber is  null or Inv.IsPaid is null)
and (Inv.DoctorId is not null)
order by 
 Region.RegionID
,convert(int,Doc.Doctor_CODE) ;

select max (InvoiceStartDate) from Invoice;