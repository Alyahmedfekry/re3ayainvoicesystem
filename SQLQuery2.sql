﻿if( @ProviderType=0 or @ProviderType=1 or @ProviderType=4 )
 SELECT       
  dbo.Doctor.Doctor_Name_AR as Doctor_Name_AR
, dbo.Address_Book.Address_Name_AR as Address_Name_AR
, dbo.Appointment.Appointment_CODE as Appointment_CODE
, dbo.Patient.Patient_FullName as Patient_FullName
, SUBSTRING(dbo.Patient.Patient_Mobile, 0 ,
    case when  CHARINDEX(' ', dbo.Patient.Patient_Mobile ) = 0 then LEN(dbo.Patient.Patient_Mobile) -2
    else CHARINDEX(' ', dbo.Patient.Patient_Mobile) -2 end) as Patient_Mobile
, dbo.Appointment.Appointment_Creation_Datetime as Appointment_Creation_Datetime
, dbo.Appointment.AppointmentType as AppointmentType
, dbo.AppointmentStatus.AppointmentStatusNameEN as AppointmentStatusNameEN
, dbo.Appointment.Fees_For_Doctor as Fees_For_Doctor
, dbo.Doctor.Basic_Video_fees as Basic_Video_fees
, dbo.Doctor.Basic_Chat_fees as Basic_Chat_fees
, dbo.Appointment.Fees_For_Re3aya  as Fees_For_Re3aya

--, dbo.Appointment.Appointment_Date as Appointment_Date
--, dbo.Appointment.Appointment_ID as Appointment_ID

--, dbo.Address_Book.Visit_Fees as Visit_Fees
--, dbo.Address_Book.Re3aya_Discount as Re3aya_Discount
--, dbo.Address_Book.Patient_Discount as Patient_Discount
--, dbo.Address_Book.Address_Name_EN as Address_Name_EN
--, dbo.Address_Book.RegionID as RegionID
--, dbo.Doctor.Doctor_Name_EN as Doctor_Name_EN
--,ISNULL(dbo.Patient.[Patient_NameAR],dbo.Patient.Patient_FullName ) AS Patient_FullName

--,dbo.Patient.PatientID as PatientID
--,dbo.Address_Book.AddressBook_ID as AddressBook_ID

--,dbo.Appointment.[BookingStatus]  as BookingStatus
--, dbo.AppointmentStatus.AppointmentStatusNameAR as AppointmentStatusNameAR

FROM   [dbo].[Invoice_Appointment] inner join dbo.Appointment 
on  [dbo].[Invoice_Appointment].Appointment_Id=dbo.Appointment.Appointment_ID

 INNER JOIN

 [dbo].[AppointmentStatus] 
 on dbo.Appointment.[BookingStatus]=[dbo].[AppointmentStatus].[AppointmentStatusID]
                   Inner Join
				   dbo.Address_Book ON dbo.Appointment.Address_ID = dbo.Address_Book.AddressBook_ID AND dbo.Appointment.Address_ID = dbo.Address_Book.AddressBook_ID 
				   INNER JOIN
                   dbo.Doctor ON dbo.Address_Book.Doctor_ID = dbo.Doctor.DoctorID 
			       INNER JOIN
                   dbo.Patient ON dbo.Appointment.Patient_ID = dbo.Patient.PatientID
						 where   [dbo].[Invoice_Appointment].Invoice_Id=@InvoiceId
else if (@ProviderType=2) --lab
select branch.FullName_AR as Doctor_Name_AR,
 branch.FullName_EN as Doctor_Name_EN, 
 branch.AddressAr as Address_Name_AR,
 branch.Id as AddressBook_ID ,
 Labs.LabsAppointment.AppointmentCODE as Appointment_CODE,
 Labs.LabsAppointment.AppointmentDate as Appointment_Date,
   Labs.LabsAppointment.AppointmentCreationDatetime as Appointment_Creation_Datetime,
 Labs.LabsAppointment.AppointmentActualAmount as Fees_For_Doctor ,
 Labs.LabsAppointment.AppointmentActualAmount as Visit_Fees ,
 
 ((Labs.LabsAppointment.AppointmentActualAmount*10)/100) as Fees_For_Re3aya,
 Labs.LabsAppointment.BookingStatus as BookingStatus,
 --Patient.Patient_FullName as Patient_FullName,
ISNULL(dbo.Patient.[Patient_NameAR],dbo.Patient.Patient_FullName ) AS Patient_FullName
,
SUBSTRING(dbo.Patient.Patient_Mobile, 0 ,
case when  CHARINDEX(' ', dbo.Patient.Patient_Mobile ) = 0 then LEN(dbo.Patient.Patient_Mobile) -2
else CHARINDEX(' ', dbo.Patient.Patient_Mobile) -2 end) as Patient_Mobile,
dbo.AppointmentStatus.AppointmentStatusNameEN as AppointmentStatusNameEN,
 dbo.AppointmentStatus.AppointmentStatusNameAR as AppointmentStatusNameAR,
 '0' as AppointmentType 
 , '0.00' as Basic_Video_fees
, '0.00' as Basic_Chat_fees
from
 [dbo].[Invoice_Appointment] inner join
Labs.LabsAppointment on [dbo].[Invoice_Appointment].Appointment_Id=Labs.LabsAppointment.ID
join Labs.LabsBranches branch
on(branch.Id =Labs.LabsAppointment.BranchId)
join Patient on  Labs.LabsAppointment.[PatientID]=Patient.PatientID

join dbo.AppointmentStatus
on (dbo.AppointmentStatus.AppointmentStatusID=Labs.LabsAppointment.BookingStatus)
where   [dbo].[Invoice_Appointment].Invoice_Id=@InvoiceId


else if (@ProviderType=5)-- rad
select branch.FullName_AR as Doctor_Name_AR,
 branch.FullName_EN as Doctor_Name_EN,
 
 branch.AddressAr as Address_Name_AR,
 branch.Id as AddressBook_ID,
 [Rad].[RadiologyCenterAppointment].AppointmentCODE as Appointment_CODE,
 [Rad].[RadiologyCenterAppointment].AppointmentDate as Appointment_Date,
SUBSTRING(dbo.Patient.Patient_Mobile, 0 ,
case when  CHARINDEX(' ', dbo.Patient.Patient_Mobile ) = 0 then LEN(dbo.Patient.Patient_Mobile) -2
else CHARINDEX(' ', dbo.Patient.Patient_Mobile) -2 end) as Patient_Mobile, 
 [Rad].[RadiologyCenterAppointment].AppointmentCreationDatetime as Appointment_Creation_Datetime,
 [Rad].[RadiologyCenterAppointment].AppointmentActualAmount as Fees_For_Doctor,
 [Rad].[RadiologyCenterAppointment].AppointmentActualAmount as Visit_Fees,
 
 [Rad].[RadiologyCenterAppointment].BookingStatus as BookingStatus, 
 (([Rad].[RadiologyCenterAppointment].AppointmentActualAmount*10)/100) as Fees_For_Re3aya,
ISNULL(dbo.Patient.[Patient_NameAR],dbo.Patient.Patient_FullName ) AS Patient_FullName
, dbo.AppointmentStatus.AppointmentStatusNameEN as AppointmentStatusNameEN,
 dbo.AppointmentStatus.AppointmentStatusNameAR as AppointmentStatusNameAR,
 '0' as AppointmentType
  , '0.00' as Basic_Video_fees
, '0.00' as Basic_Chat_fees
from
        [dbo].[Invoice_Appointment] inner join [Rad].[RadiologyCenterAppointment] on  [dbo].[Invoice_Appointment].Appointment_Id=[Rad].[RadiologyCenterAppointment].ID

join [Rad].[RadiologyCenterBranches] branch
on(branch.Id =[Rad].[RadiologyCenterAppointment].BranchId)
join Patient on  [Rad].[RadiologyCenterAppointment].[PatientID]=Patient.PatientID

join dbo.AppointmentStatus
on (dbo.AppointmentStatus.AppointmentStatusID=[Rad].[RadiologyCenterAppointment].BookingStatus)
 						 where   [dbo].[Invoice_Appointment].Invoice_Id=@InvoiceId


 else if(@ProviderType=9)--home visit
 SELECT        dbo.HomeVisitAppointment.AppointmentDate as Appointment_Date
, dbo.HomeVisitAppointment.Appointment_ID as Appointment_ID
, dbo.HomeVisitAppointment.Appointment_CODE as Appointment_CODE
, dbo.HomeVisitRegions.VisitPrice as Fees_For_Doctor
, dbo.HomeVisitRegions.VisitPrice as Visit_Fees

,case when dbo.HomeVisitAppointment.Re3ayaFees is null
 then 
 (case when dbo.HomeVisitAppointment.Fees is not null then
 ((dbo.HomeVisitAppointment.Fees*10)/100) else
  0  end)
 else HomeVisitAppointment.Re3ayaFees end as Fees_For_Re3aya
 
 , dbo.Region.Region_Name_EN as Address_Name_EN
 , dbo.Region.Region_Name_AR as Address_Name_AR
 , dbo.Region.Region_Name_AR as Region_Name_AR
, dbo.Region.Region_Name_EN as Region_Name_EN
, dbo.HomeVisitRegions.HomeVisitRegions_ID  as AddressBook_ID
, dbo.Doctor.Doctor_Name_AR as Doctor_Name_AR
, dbo.Doctor.Doctor_Name_EN as Doctor_Name_EN
,ISNULL(dbo.Patient.[Patient_NameAR],dbo.Patient.Patient_FullName ) AS Patient_FullName
,SUBSTRING(dbo.Patient.Patient_Mobile, 0 ,
case when  CHARINDEX(' ', dbo.Patient.Patient_Mobile ) = 0 then LEN(dbo.Patient.Patient_Mobile) -2
else CHARINDEX(' ', dbo.Patient.Patient_Mobile) -2 end) as Patient_Mobile, dbo.Patient.PatientID as PatientID
, dbo.Doctor.Basic_Video_fees as Basic_Video_fees
, dbo.Doctor.Basic_Chat_fees as Basic_Chat_fees
 
,dbo.HomeVisitAppointment.AppointmentCreationDate as Appointment_Creation_Datetime
 , dbo.HomeVisitAppointment.AppointmentStatus_Id  as AppointmentStatus_Id,
 [dbo].[AppointmentStatus].[AppointmentStatusNameEN] as AppointmentStatusNameEN
, dbo.AppointmentStatus.AppointmentStatusNameAR as AppointmentStatusNameAR,
 '0' as AppointmentType

 FROM         [dbo].[Invoice_Appointment] inner join dbo.HomeVisitAppointment on [dbo].[Invoice_Appointment].Appointment_Id=dbo.HomeVisitAppointment.Appointment_Id
  INNER JOIN
 [dbo].[AppointmentStatus] on dbo.HomeVisitAppointment.AppointmentStatus_Id=[dbo].[AppointmentStatus].[AppointmentStatusID]
                   Inner Join      dbo.HomeVisitRegions ON dbo.HomeVisitAppointment.Home_Visit_Region_Id = dbo.HomeVisitRegions.HomeVisitRegions_ID
				   Inner join Region on HomeVisitRegions.Region_ID=Region.RegionID
				   INNER JOIN
                         dbo.Doctor ON dbo.HomeVisitRegions.Doctor_ID = dbo.Doctor.DoctorID INNER JOIN
						 dbo.PatientAddress On dbo.HomeVisitAppointment.Patient_Address_Id=dbo.PatientAddress.PatientAddressID INNER Join
                         dbo.Patient ON dbo.PatientAddress.PatientID = dbo.Patient.PatientID
						 						 where   [dbo].[Invoice_Appointment].Invoice_Id=@InvoiceId
	 else if (@ProviderType=3)--intensive care 
												   SELECT      [dbo].[IntensiveCare_Appointment].Appointment_Date as Appointment_Date
, [dbo].[IntensiveCare_Appointment].Id as Appointment_ID
, [dbo].[IntensiveCare_Appointment].Appointment_Code as Appointment_CODE
, '400' as Fees_For_Doctor
, '400' as Visit_Fees
,'40' as Fees_For_Re3aya

 , dbo.Region.Region_Name_AR as Region_Name_AR
, dbo.Region.Region_Name_EN as Region_Name_EN
, [dbo].[Health_Entitiy].[Health_Entitiy_ID]  as AddressBook_ID
, [dbo].[Health_Entitiy].[Health_Entitiy_NameAR] as Doctor_Name_AR
, [dbo].[Health_Entitiy].[Health_Entitiy_NameEN] as Doctor_Name_EN
,ISNULL(dbo.Patient.[Patient_NameAR],dbo.Patient.Patient_FullName ) AS Patient_FullName
,SUBSTRING(dbo.Patient.Patient_Mobile, 0 ,
case when  CHARINDEX(' ', dbo.Patient.Patient_Mobile ) = 0 then LEN(dbo.Patient.Patient_Mobile) -2
else CHARINDEX(' ', dbo.Patient.Patient_Mobile) -2 end) as Patient_Mobile, dbo.Patient.PatientID as PatientID
 
 
,dbo.[IntensiveCare_Appointment].[CreationDate] as Appointment_Creation_Datetime
 , dbo.[IntensiveCare_Appointment].[StatusId]   as AppointmentStatus_Id,
 [dbo].[AppointmentStatus].[AppointmentStatusNameEN] as AppointmentStatusNameEN
, dbo.AppointmentStatus.AppointmentStatusNameAR as AppointmentStatusNameAR
,
 '0' as AppointmentType
  , '0.00' as Basic_Video_fees
, '0.00' as Basic_Chat_fees
 FROM         [dbo].[Invoice_Appointment] inner join [dbo].[IntensiveCare_Appointment] on [dbo].[Invoice_Appointment].Appointment_Id=dbo.[IntensiveCare_Appointment].Id
  INNER JOIN
 [dbo].[AppointmentStatus] on dbo.[IntensiveCare_Appointment].[StatusId]=[dbo].[AppointmentStatus].[AppointmentStatusID]
                   Inner Join      dbo.[Health_Entitiy] ON dbo.[IntensiveCare_Appointment].[HealthEntity_Id]  = dbo.[Health_Entitiy].[Health_Entitiy_ID] 
				   Inner join Region on [Health_Entitiy].[Health_Entitiy_RegionID]=Region.RegionID
				   INNER Join
                         dbo.Patient ON dbo.[IntensiveCare_Appointment].[Patient_Id]  = dbo.Patient.PatientID
						 						 where   [dbo].[Invoice_Appointment].Invoice_Id=@InvoiceId





						  
 