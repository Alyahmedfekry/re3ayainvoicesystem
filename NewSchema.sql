﻿create table MedicalProviderType
(
Id int primary key Identity(0,1) not null,
ArabicName nvarchar(250),
EnglishName nvarchar(250)
);

create table ProviderService_Category(
Id int Primary key Identity(1,1) not null,
ArabicName nvarchar(500),
EnglishName nvarchar(500),
ProviderType int foreign key references MedicalProviderType(Id),
ParentId int foreign key references ProviderService_Category(Id)
);

create table ProviderService(
Id int Primary key Identity(1,1) not null,
ArabicName nvarchar(500),
EnglishName nvarchar(500),
CategoryId int foreign key references ProviderService_Category(Id),
SpecialityId int foreign key references Speciality(SpecialityID),
IsDeleted bit
);

create table ServicePriceList(
Id int Primary key Identity(1,1) not null,
ServiceId int foreign key references ProviderService(Id),
Price decimal,
ProviderId int,
ProviderType int foreign key references MedicalProviderType(Id)
);

create table ProviderAppointment(
Id int Primary key Identity(1,1) not null,
Code nvarchar(500),
CreatedDate datetime,
SessionDate datetime,
SessionStart time,
SessionEnd time,
Doctor_SessionType int,
Doctor_VisitType int foreign key  references Appointment_SessionType(Id),
StatusId int foreign key references AppointmentStatus(AppointmentStatusID),
ProviderFees money,
Discount money,
PatientFees money,
Re3ayaFees money,
ProviderType int foreign key references MedicalProviderType(Id),
ProviderId int,
AddressBookId int foreign key  references Address_Book(AddressBook_ID),
RegionId int foreign key references Region(RegionID),
RegionName nvarchar(250)
);

create table AppointmentServices(
Id int Primary key Identity(1,1) not null,
AppointmentId int foreign key  references ProviderAppointment(Id),
ServiceId int foreign key references ServicePriceList(Id),
ServiceFees money,
Discount money,
NetFees money
);
