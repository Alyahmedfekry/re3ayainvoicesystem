﻿insert into Invoice_Appointment  (Invoice_Id,Appointment_Id)
select InvoiceID,Rad.RadiologyCenterAppointment.ID
from Rad.RadiologyCenterAppointment
join Invoice
on(FORMAT ( Rad.RadiologyCenterAppointment.AppointmentCreationDatetime, 'dd/MM/yyyy ') = FORMAT (InvoiceStartDate, 'dd/MM/yyyy '))
join Health_Entitiy
on ( Health_Entitiy.Health_Entitiy_ID = Invoice.HealthentityId)

where 
    Invoice.DoctorId IS Null 
and Invoice.TypeId = 2
and Health_Entitiy.Health_Entitiy_Type=5;


