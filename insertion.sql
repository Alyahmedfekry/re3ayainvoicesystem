﻿select * from Diagnostic
where Diagnostic.IsDelete<>1;


insert into ProviderService_Category 
(ArabicName,EnglishName,ProviderType,ParentId)
select 
 Name_AR
,Name_EN
,5
,Null
from DiagnosticCategories;



insert into ProviderService
(ArabicName,EnglishName,CategoryId,IsDeleted)
select 
 Diagnostic.Name_AR
,Diagnostic.Name_EN
,ProviderService_Category.Id
,0
from
Diagnostic join DiagnosticCategories
on(Diagnostic.DiagnosticCategoryId = DiagnosticCategories.Id)
join ProviderService_Category
on(ProviderService_Category.EnglishName=DiagnosticCategories.Name_EN)
where Diagnostic.IsDelete<>1;

select * from ProviderService_Category 
where ProviderType=2;

select * from ProviderService;